﻿using UnityEngine;
using UnityEngine.Advertisements;
using System.Collections;

namespace RedBlackTree
{
    public enum EAdsType
    {
        rewardedVideo,
        video,
    }
    public class AdsHandler : IUnityAdsListener
    {
        public static void Show(EAdsType adsType)
        {
            
            Advertisement.Show(adsType.ToString());
        }

        public static void Init()
        {
            if (mSelf == null)
                mSelf = new AdsHandler();
        }




        /* 시스템에 관한 내용 */

#if (UNITY_IOS || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX)
    [SerializeField] string mIPhoneGameId = "3408976";
#else
        static string mAndroidGameId = "3408977";
#endif
        static EAdsType mPlacementId = EAdsType.rewardedVideo;
        static bool mIsTestMode = false;

        static AdsHandler mSelf;


        public AdsHandler()
        {
#if (UNITY_IOS || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX)
        string mGameId = mIPhoneGameId;
#else
            string mGameId = mAndroidGameId;
#endif
            Debug.Log("ads init");
            Advertisement.AddListener(mSelf);
            Advertisement.Initialize(mGameId, mIsTestMode);
        }

        // Implement IUnityAdsListener interface methods:
        public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            // Define conditional logic for each ad completion status:
            if (showResult == ShowResult.Finished)
            {
                // Reward the user for watching the ad to completion.
                Debug.Log("Finished");
            }
            else if (showResult == ShowResult.Skipped)
            {
                // Do not reward the user for skipping the ad.
                Debug.Log("Skipped");
            }
            else if (showResult == ShowResult.Failed)
            {
                Debug.LogWarning("The ad did not finish due to an error.");
            }
        }

        public void OnUnityAdsReady(string placementId)
        {
            var tempString = mPlacementId.ToString();
            Debug.Log("OnUnityAdsReady: " + placementId);
            // If the ready Placement is rewarded, show the ad:
            if (placementId == tempString)
            {
                Advertisement.Show(tempString);
            }
        }

        public void OnUnityAdsDidError(string message)
        {
            // Log the error.
            Debug.Log("OnUnityAdsDidError: " + message);
        }

        public void OnUnityAdsDidStart(string placementId)
        {
            // Optional actions to take when the end-users triggers an ad.
            Debug.Log("OnUnityAdsDidStart: " + placementId);
        }
    }

}