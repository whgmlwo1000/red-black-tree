﻿using UnityEngine;
using System.Collections;

namespace RedBlackTree
{
    public class AdsTester : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {
            AdsHandler.Init();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                print("Ads Test");
                AdsHandler.Show(EAdsType.rewardedVideo);
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            print("Ads Test");
            AdsHandler.Show(EAdsType.video);
        }
    }

}