﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{
    public enum EScriptType
    {
        None = -1,
        /// <summary>
        /// 천상의 열매
        /// </summary>
        Default,

        Count
    }

    public class ToolTipPoll : ObjectManagement.Pool<EScriptType, ToolTip>
    {
        static HashSet<ToolTip> pooledTooltips = new HashSet<ToolTip>();
        /// <summary>
        /// 툴팁을 worldPosition 인 pos에 띄운다
        /// </summary>
        /// <param name="script">출력할내용</param>
        /// <param name="worldPosition">월드위치</param>
        public static ToolTip PopUp(ScriptBase script, Vector2 worldPosition)
        {
            var scriptReader = getScriptReader();
            if (scriptReader == null) return null;

            scriptReader.ChangeNameContext(script.name, script.context);
            scriptReader.mRootTransform = null;
            Vector3 pos = new Vector3(worldPosition.x, worldPosition.y, 20f);
            scriptReader.transform.position = pos;
            scriptReader.PopUp();
            pooledTooltips.Add(scriptReader);
            return scriptReader;
        }

        /// <summary>
        /// 툴팁을 worldPosition에 맞춰띄운다
        /// </summary>
        /// <param name="script">내용: tag는 고유한 해쉬값. 보통 이름을 대입</param>
        /// <param name="rootTrans">따라갈 객체</param>
        /// <param name="offset">위치 오프셋</param>
        public static ToolTip PopUp(ScriptBase script, Transform rootTrans, Vector2 offset)
        {
            var scriptReader = getScriptReader();
            if (scriptReader == null) return null;

            scriptReader.ChangeNameContext(script.name, script.context);
            scriptReader.mRootTransform = rootTrans;
            Vector3 pos = new Vector3(offset.x, offset.y, 20f);
            scriptReader.offset = pos;
            scriptReader.PopUp();
            pooledTooltips.Add(scriptReader);
            return scriptReader;
        }

        /// <summary>
        /// 툴팁을 worldPosition 인 pos에 띄운다
        /// </summary>
        /// <param name="script">출력할내용</param>
        /// <param name="worldPosition">월드위치</param>
        public static ToolTip PopUpInMenu(ScriptBase script, Vector2 worldPosition)
        {
            var scriptReader = getScriptReader();
            if (scriptReader == null) return null;

            foreach (var item in pooledTooltips)
            {
                item.PopDown();
            }
            pooledTooltips.Clear();

            scriptReader.ChangeNameContext(script.name, script.context);
            scriptReader.mRootTransform = null;
            Vector3 pos = new Vector3(worldPosition.x, worldPosition.y, -9f);
            scriptReader.transform.position = pos;
            scriptReader.PopUp();
            return scriptReader;
        }

        // ---------------------------------------------
        // 몰라도 지장없는 곳

        static ToolTip getScriptReader()
        {
            return Get(EScriptType.Default);
        }
    }

    
    public struct ScriptBase
    {
        /// <summary>
        /// 스크립트별 고유한 해쉬값 이를 이용해 풀링함. 보통 이름을 대입
        /// </summary>
        public string name;
        public string context;

        public ScriptBase(string name, string context)
        {
            this.name = name;
            this.context = context;
        }
    }

}