﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using TouchManagement;

namespace RedBlackTree
{
    [RequireComponent(typeof(TouchBox))]
    public class UIToggle : Toggle
    {
        TouchBox touchBox;
        PointerEventData pointerEventData;
        private string onText = "ON";
        private string offText = "OFF";

        Text text;

        static ButtonAudioHandler buttonAudioHandler;

        protected override void Awake()
        {
            base.Awake();
            pointerEventData = new PointerEventData(EventSystem.current);
            touchBox = GetComponent<TouchBox>();
            text = GetComponentInChildren<Text>();
            touchBox.OnTouchStart += OnTouchStart;
            touchBox.OnTouchEnd += OnTouchEnd;
            UpdateText();
        }

        protected override void Start()
        {
            if (!Application.isPlaying) return;
            if (buttonAudioHandler == null)
                buttonAudioHandler = Instantiate(Resources.Load<GameObject>("UI/ButtonAudioHandler")).GetComponent<ButtonAudioHandler>();
        }

        public void UpdateText()
        {
            if (isOn)
            {
                text.text = onText;
            }
            else
            {
                text.text = offText;
            }
        }

        void OnTouchStart(Touch touch, Vector2 worldPosition)
        {
            pointerEventData.position = worldPosition;
            OnPointerDown(pointerEventData);
            buttonAudioHandler.Get(EButtonState.Pushed).AudioSource.Play();
        }

        void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            pointerEventData.position = worldPosition;
            OnPointerUp(pointerEventData);
            if (touchBox.GetTouchIn(worldPosition))
            {
                OnPointerClick(pointerEventData);
                UpdateText();
            }
            buttonAudioHandler.Get(EButtonState.Normal).AudioSource.Play();
        }
    }

}