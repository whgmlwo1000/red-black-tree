﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using TouchManagement;

namespace RedBlackTree
{
    [RequireComponent(typeof(TouchBox))]
    public class UIButton : Button
    {
        TouchBox touchBox;
        PointerEventData pointerEventData;
        new Animator animator;
        bool isMouseUp = false;

        static ButtonAudioHandler buttonAudioHandler;
        protected override void Awake()
        {
            if (!Application.isPlaying) return;
            base.Awake();
            pointerEventData = new PointerEventData(EventSystem.current);
            touchBox = GetComponent<TouchBox>();
            touchBox.OnTouchStart += OnTouchStart;
            touchBox.OnTouchEnd += OnTouchEnd;
            animator = GetComponent<Animator>();
            if(animator)animator.enabled = false;

            
        }

        protected override void Start()
        {
            if (!Application.isPlaying) return;
            if (buttonAudioHandler == null)
                buttonAudioHandler = Instantiate(Resources.Load<GameObject>("UI/ButtonAudioHandler")).GetComponent<ButtonAudioHandler>();
        }

        private void OnApplicationQuit()
        {
            if (buttonAudioHandler)
                DestroyImmediate(buttonAudioHandler.gameObject);
        }

        void OnTouchStart(Touch touch, Vector2 worldPosition)
        {
            if (animator) animator.enabled = true;
            isMouseUp = false;

            pointerEventData.position = worldPosition;
            OnPointerDown(pointerEventData);
            buttonAudioHandler.Get(EButtonState.Pushed).AudioSource.Play();
        }

        void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            isMouseUp = true;
            pointerEventData.position = worldPosition;
            OnPointerUp(pointerEventData);
            if (touchBox.GetTouchIn(worldPosition))
            {
                OnPointerClick(pointerEventData);
            }
            buttonAudioHandler.Get(EButtonState.Normal).AudioSource.Play();
        }

        private void Update()
        {
            if (isMouseUp&&
                animator &&
                animator.GetCurrentAnimatorStateInfo(0).IsName("Normal") &&
                animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.99f)
            {
                animator.enabled = false;
            }
        }
    }

}