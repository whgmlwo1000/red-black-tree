﻿using System.Collections;
using System.Collections.Generic;
using ObjectManagement;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace RedBlackTree
{
    public class ToolTip : ItemTooltip, ObjectManagement.IPoolable<EScriptType>
    {
        [HideInInspector] public Transform mRootTransform = null;
        [HideInInspector] public Vector3 offset;
        [SerializeField]AudioSource audioSource = null;
        VerticalLayoutGroup verticalLayout;

        public bool IsPooled {
            get { return !gameObject.activeSelf; }
        }

        public new GameObject gameObject {
            get { return transform.gameObject; }
        }

        public EScriptType Type {
            get { return EScriptType.Default; }
        }

        bool isWorldCanvas = false;
        public bool IsWorldCanvas {
            get { return isWorldCanvas; }
        }

        public new void PopUp()
        {
            gameObject.SetActive(true);
            mState = ETooltipState.ACTIVATING;
            mRemainPopUpTime = mPopUpTime;
            mSrcHeightSize = mPanel.sizeDelta.y;

            audioSource.Play();


            mAdditionalHeightSize = 0.9f;
            for (int i = 0; i < mTextInfo.Length; i++)
            {
                var textHeight = mTextInfo[i].Text.preferredHeight;
                mAdditionalHeightSize += textHeight;
            }
            if(mMinHeightSize > mAdditionalHeightSize)
            {
                mDstHeightSize = mMinHeightSize;
            }
            else
            {
                mDstHeightSize = mAdditionalHeightSize;
            }


            mDstHeightSize += mDstHeightSize;
        }

        public void ChangeNameContext(string name, string context)
        {
            mTextInfo[0].Text.text = name;
            mTextInfo[1].Text.text = context;
        }
        public void ChangeTextWithTag(string text, string tag="context")
        {
            for (int i = 0; i < mTextInfo.Length; i++)
            {
                if (mTextInfo[i].Key == tag)
                {
                    mTextInfo[i].Text.text = text;
                    break;
                }
            }
        }

        public new void PopDown()
        {
            base.PopDown();
        }

        /// <summary>
        /// 상품 비활성화 메소드
        /// </summary>
        public void Deactivate()
        {
            gameObject.SetActive(false);
        }

        public virtual void Start()
        {
            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        public virtual void OnDestroy()
        {
            SceneManager.activeSceneChanged -= OnActiveSceneChanged;
        }
        protected new void Update()
        {
            switch (mState)
            {
                case ETooltipState.ACTIVATING:
                    if (mRemainPopUpTime > 0.0f)
                    {
                        mRemainPopUpTime -= Time.unscaledDeltaTime;
                        mPanel.sizeDelta = new Vector2(mPanel.sizeDelta.x, Mathf.Lerp(mDstHeightSize, mSrcHeightSize, mRemainPopUpTime / mPopUpTime));
                    }
                    else
                    {
                        mPanel.sizeDelta = new Vector2(mPanel.sizeDelta.x, mDstHeightSize);
                        mState = ETooltipState.ACTIVE;
                        for (int i = 0; i < mTextInfo.Length; i++)
                        {
                            mTextInfo[i].Text.gameObject.SetActive(true);
                        }

                        if (!verticalLayout)
                        {
                            verticalLayout = GetComponentInChildren<VerticalLayoutGroup>();
                        }

                        //LayoutRebuilder.ForceRebuildLayoutImmediate(mTextInfo[0].Text.GetComponent<RectTransform>());
                        //LayoutRebuilder.ForceRebuildLayoutImmediate(mTextInfo[1].Text.GetComponent<RectTransform>());

                        Canvas.ForceUpdateCanvases();
                        verticalLayout.SetLayoutVertical();
                        //verticalLayout.enabled = false;
                        //verticalLayout.enabled = true;
                    }
                    break;
                case ETooltipState.DEACTIVATING:
                    if (mRemainPopUpTime > 0.0f)
                    {
                        mRemainPopUpTime -= Time.unscaledDeltaTime;
                        mPanel.sizeDelta = new Vector2(mPanel.sizeDelta.x, Mathf.Lerp(0.0f, mSrcHeightSize, mRemainPopUpTime / mPopUpTime));
                    }
                    else
                    {
                        mPanel.sizeDelta = new Vector2(mPanel.sizeDelta.x, 0.0f);
                        mState = ETooltipState.DEACTIVE;
                        gameObject.SetActive(false);
                    }
                    break;
            }
            if (mRootTransform)
            {
                transform.position = mRootTransform.position + offset;
            }
        }

        private void OnActiveSceneChanged(Scene src, Scene dst)
        {
            Deactivate();
        }
    }

}