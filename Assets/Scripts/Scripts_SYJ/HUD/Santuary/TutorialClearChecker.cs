﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{
    public class TutorialClearChecker : MonoBehaviour
    {
        private void Start()
        {
            PlayerData.Current.IsClearTutorial = true;
        }
    }

}