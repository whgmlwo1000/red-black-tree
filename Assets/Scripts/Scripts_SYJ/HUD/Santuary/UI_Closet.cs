﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TouchManagement;

namespace RedBlackTree
{
    public class UI_Closet : MonoBehaviour
    {
        [SerializeField] List<Cloth> mCloths = null;
        public PlayerData playerData = null;
        [SerializeField] Transform mGridRoot = null;
        [SerializeField] UI_ClosetBuy closetBuy = null;

        List<ClothSlot> mClothSlots = new List<ClothSlot>();

        static UI_Closet mSelf;
        public static UI_Closet Self {
            get { return mSelf; }
        }


        void Start()
        {
            mSelf = this;

            for (int i = 0; i < mCloths.Count; i++)
            {
                mCloths[i].bHasThis = PlayerData.Current.IsOpenedFairySkin(mCloths[i].id);
            }

            var count = Mathf.Min(mGridRoot.childCount, mCloths.Count);
            for (int i = 0; i < count; i++)
            {
                ClothSlot temp = new ClothSlot(mGridRoot.GetChild(i), mCloths[i]);
                mClothSlots.Add(temp);
            }

            
            closetBuy.Init(mClothSlots);
        }

        void DeselectOther(ClothSlot selectedClothSlot)
        {
            foreach (var item in mClothSlots)
            {
                if(item != selectedClothSlot)
                {
                    item.Select(false);
                }
            }
        }

        public void UpdateState()
        {
            foreach (var item in mClothSlots)
            {
                item.UpdateState();
            }
        }

        [System.Serializable]
        public class Cloth
        {
            public ESkin_Fairy id;
            public Sprite sprite;
            public int price;
            public bool bHasThis;
        }

        public class ClothSlot
        {
            protected Cloth mCloth;
            Image mItemImage;
            protected GameObject mLightObj;
            protected GameObject mSelectMarkObj;

            protected TouchBox touchBox;

            public Cloth Cloth { get { return mCloth; } }

            public ClothSlot(Transform rootTrans, Cloth cloth)
            {
                mItemImage = rootTrans.GetChild(1).GetComponent<Image>();
                mLightObj = rootTrans.GetChild(2).gameObject;
                mSelectMarkObj = rootTrans.GetChild(3).gameObject;
                mCloth = cloth;
                mItemImage.sprite = cloth.sprite;
                mItemImage.color = Color.white;
                touchBox = rootTrans.GetComponent<TouchBox>();
                touchBox.OnTouchEnd += OnTouchEnd;

                mLightObj.SetActive(mCloth.bHasThis);
                mSelectMarkObj.SetActive(PlayerData.Current.FairySkin == cloth.id);
                
            }

            public virtual void Select(bool isSelect)
            {
                mSelectMarkObj.SetActive(isSelect);
                if (isSelect)
                {
                    PlayerData.Current.FairySkin = mCloth.id;
                    SanctuaryFairy.Main.Skin = mCloth.id;
                }
            }

            public void UpdateState()
            {
                mLightObj.SetActive(mCloth.bHasThis);
            }

            public bool HasThisCloth()
            {
                return mCloth.bHasThis;
            }

            public int GetSelectedPrice()
            {
                if(mSelectMarkObj.activeInHierarchy)
                    return mCloth.price;
                return 0;
            }

            public bool IsSelected()
            {
                return mSelectMarkObj.activeSelf;
            }

            protected void OnTouchEnd(Touch touch, Vector2 worldPosition)
            {
                if (!touchBox.GetTouchIn(worldPosition) || !mCloth.bHasThis) return;
                Select(true);
                mSelf.DeselectOther(this);
            }
        }
    }

}