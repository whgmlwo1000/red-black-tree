﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RedBlackTree
{
    public class DPad : MonoBehaviour
    {
        [SerializeField]
        private PlayerCommand playerCommand = null;

        [SerializeField] RawImage[] images = null;
        [SerializeField] Image[] arrowImages = null;

        Canvas canvas;
        float canvasScaleFactor;

        RectTransform stickRectTrans;
        RectTransform backgroundRectTrans;

        float joyRadius;

        [SerializeField]
        Transform leftbottomTrans = null;
        [SerializeField]
        Transform RightTopTrans = null;

        Vector2 startJoyPos;
        Vector2 backgroundInitPos;
        Vector2 joyMovePosDelta;
        Rect joyRect;

        bool isControling;

        static DPad mSelf;
        static bool isAble;

        void Start()
        {
            isAble = false;
            mSelf = this;
            backgroundRectTrans = GetComponent<RectTransform>();
            stickRectTrans = transform.GetChild(0).GetComponent<RectTransform>();
            canvas = GetComponentInParent<Canvas>();
            canvasScaleFactor = (1 / canvas.scaleFactor);

            joyRect = new Rect();
            joyRect.x = leftbottomTrans.position.x;
            joyRect.y = leftbottomTrans.position.y;
            joyRect.xMax = RightTopTrans.position.x;
            joyRect.yMax = RightTopTrans.position.y;

            joyRadius = (RightTopTrans.position.x - leftbottomTrans.position.x)*0.3f;
            backgroundInitPos = backgroundRectTrans.position;

            playerCommand.MoveVertical = playerCommand.MoveHorizontal = 0;
            Invoke("LateStart", 0.5f);
        }

        void LateStart()
        {
            playerCommand.MoveVertical = playerCommand.MoveHorizontal = 0;
            isAble = true;
        }

        bool CollPointRect(Vector2 a, Rect rect)
        {
            return !(a.x < rect.x || a.x > rect.xMax || a.y < rect.y || a.y > rect.yMax);
        }

        private void Update()
        {
            if (!isAble) return;
            if (Input.GetMouseButtonDown(0))
            {
                var mousePos = Input.mousePosition;

                if (CollPointRect(mousePos, joyRect))
                {
                    OnTouchStart(mousePos);
                    isControling = true;
                }
            }
            if (isControling)
            {
                foreach (var item in mSelf.arrowImages)
                {
                    var color = item.color;
                    color.a -= Time.unscaledDeltaTime*2f;
                    color.a = color.a < -2 ? -2 : color.a;
                    item.color = color;
                }

                if (Input.GetMouseButton(0))
                {
                    OnTouchStay(Input.mousePosition);
                }
                if (Input.GetMouseButtonUp(0))
                {
                    OnTouchEnd(Input.mousePosition);
                    isControling = false;
                }
            }
            // 조종중이 아니면 1초있다가 화살표 보이게
            else
            {
                foreach (var item in mSelf.arrowImages)
                {
                    var color = item.color;
                    color.a += Time.unscaledDeltaTime;
                    color.a = color.a > 1 ? 1 : color.a;
                    item.color = color;
                }
            }
        }

        void OnTouchStart(Vector2 worldPos)
        {
            backgroundRectTrans.position = startJoyPos = worldPos;
        }


        void OnTouchStay(Vector2 worldPos)
        {
            joyMovePosDelta = worldPos - startJoyPos;
            Vector2 delta = Vector2.ClampMagnitude(joyMovePosDelta, joyRadius);
            Vector2 movement = delta / joyRadius;

            playerCommand.MoveHorizontal = movement.x;
            playerCommand.MoveVertical = movement.y;

            stickRectTrans.localPosition = delta* canvasScaleFactor;
        }

        void OnTouchEnd(Vector2 worldPos)
        {
            playerCommand.MoveHorizontal = 0.0f;
            playerCommand.MoveVertical = 0.0f;
            stickRectTrans.localPosition = Vector2.zero;
            backgroundRectTrans.position = backgroundInitPos;
        }

        public static void SetAble(bool able)
        {
            isAble = able;
            if (mSelf == null) return;
            foreach (var item in mSelf.images)
            {
                item.enabled = able;
            }
            foreach (var item in mSelf.arrowImages)
            {
                item.enabled = able;
            }
        }

        public static bool GetAble()
        {
            return isAble;
        }
    }

}