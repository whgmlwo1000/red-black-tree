﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;

namespace RedBlackTree
{
    public class UI_ClosetBuy : MonoBehaviour
    {
        [SerializeField] Transform mGridRoot = null;
        [SerializeField] Text mSelectCountText = null;
        [SerializeField] Text mTotalPriceText = null;
        [SerializeField] int mTotalPrice = 0;
        List<BuyClothSlot> mBuyClothSlots;

        static UI_ClosetBuy mSelf;

        public void Init(List<UI_Closet.ClothSlot> clothSlots)
        {
            mSelf = this;
            mBuyClothSlots = new List<BuyClothSlot>();
            for (int i = 0; i < clothSlots.Count; i++)
            {
                mBuyClothSlots.Add(new BuyClothSlot(mGridRoot.GetChild(i), clothSlots[i].Cloth));
            }
        }

        public void SelectAll()
        {
            foreach (var item in mBuyClothSlots)
            {
                item.Select(true);
            }
            UpdateSelectState();
        }

        public void DeselectAll()
        {
            foreach (var item in mBuyClothSlots)
            {
                item.Select(false);
            }
        }

        public void IAPBuyAll()
        {
            if (mTotalPrice <= 0) return;
            CodelessIAPStoreListener.Instance.InitiatePurchase(mTotalPrice.ToString());
        }
        public void OnPurchaseComplete()
        {
            print("buyall: "+ mTotalPrice);
            foreach (var item in mBuyClothSlots)
            {
                if (item.IsSelected())
                    item.Buy();
            }
            UI_Closet.Self.UpdateState();
        }

        public void OnPurchaseFail()
        {
            print("buyall: " + mTotalPrice);
            foreach (var item in mBuyClothSlots)
            {
                if (item.IsSelected())
                    item.Buy();
            }
            UI_Closet.Self.UpdateState();
        }

        void UpdateSelectState()
        {
            int price = 0, count = 0;
            GetTotalCount(ref price, ref count);
            mTotalPrice = price;
            mTotalPriceText.text = price.ToString();
            mSelectCountText.text = count.ToString();

            
        }

        void GetTotalCount(ref int price, ref int count)
        {
            price = count = 0;
            foreach (var item in mBuyClothSlots)
            {
                price += item.GetSelectedPrice();
                if(item.IsSelected())
                    ++count;
            }
        }

        public class BuyClothSlot : UI_Closet.ClothSlot
        {
            Text mPriceText;
            public BuyClothSlot(Transform rootTrans, UI_Closet.Cloth cloth) : base(rootTrans, cloth)
            {
                mPriceText = rootTrans.GetChild(4).GetComponent<Text>();
                if (mPriceText) mPriceText.text = cloth.price.ToString();

                touchBox.OnTouchEnd -= base.OnTouchEnd;
                touchBox.OnTouchEnd += OnTouchEnd;
                mSelectMarkObj.SetActive(false);
            }

            public override void Select(bool isSelect)
            {
                if(!HasThisCloth())
                    mSelectMarkObj.SetActive(isSelect);
                mSelf.UpdateSelectState();
            }

            public void SelectToggle()
            {
                if (!HasThisCloth())
                {
                    mSelectMarkObj.SetActive(!mSelectMarkObj.activeSelf);
                    mSelf.UpdateSelectState();
                }
            }

            public void Buy()
            {
                mCloth.bHasThis = true;
                PlayerData.Current.OpenFairySkin(mCloth.id);
                mLightObj.SetActive(true);
            }

            protected new void OnTouchEnd(Touch touch, Vector2 worldPosition)
            {
                if (!touchBox.GetTouchIn(worldPosition)) return;
                SelectToggle();
            }
        }
    }

}