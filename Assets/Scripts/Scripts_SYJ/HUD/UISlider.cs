﻿using UnityEngine;
using System.Collections;

namespace RedBlackTree
{
    public class UISlider : MonoBehaviour
    {
        int max;

        int value = 8;
        public int Value {
            set {
                Start();
                if (value < 0 || statBlocks.Length < value) return;
                for (int i = 0; i < statBlocks.Length; i++)
                {
                    statBlocks[i].SetActive(i < value);
                }
                this.value = value;
            }
            get {
                return value;
            }
        }

        GameObject[] statBlocks;
        private void Start()
        {
            if (statBlocks != null) return;
            var count = transform.childCount;
            statBlocks = new GameObject[count];

            for (int i = 0; i < count; i++)
            {
                statBlocks[i] = transform.GetChild(i).gameObject;
                statBlocks[i].SetActive(i < value);
            }
        }

        public void Plus()
        {
            ++Value;
        }

        public void Minus()
        {
            --Value;
        }
    }

}