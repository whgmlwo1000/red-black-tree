﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//namespace RedBlackTree
//{
//    public enum EMenuItemTooltip
//    {
//        None = -1,
//        Default,

//        Count
//    }

//    public class MenuToolTipPoll : Object.Pool<EMenuItemTooltip, ToolTip>
//    {
//        /// <summary>
//        /// 툴팁을 worldPosition 인 pos에 띄운다
//        /// </summary>
//        /// <param name="script">출력할내용</param>
//        /// <param name="worldPosition">월드위치</param>
//        public static ToolTip PopUp(ScriptBase script, Vector2 worldPosition)
//        {
//            var scriptReader = getScriptReader();
//            if (scriptReader == null) return null;

//            scriptReader.ChangeNameContext(script.name, script.context);
//            scriptReader.mRootTransform = null;
//            Vector3 pos = new Vector3(worldPosition.x, worldPosition.y, -9f);
//            scriptReader.transform.position = pos;
//            scriptReader.PopUp();
//            return scriptReader;
//        }

//        /// <summary>
//        /// 툴팁을 worldPosition에 맞춰띄운다
//        /// </summary>
//        /// <param name="script">내용: tag는 고유한 해쉬값. 보통 이름을 대입</param>
//        /// <param name="rootTrans">따라갈 객체</param>
//        /// <param name="offset">위치 오프셋</param>
//        public static ToolTip PopUp(ScriptBase script, Transform rootTrans, Vector2 offset)
//        {
//            var scriptReader = getScriptReader();
//            if (scriptReader == null) return null;

//            scriptReader.ChangeNameContext(script.name, script.context);
//            scriptReader.mRootTransform = rootTrans;
//            scriptReader.offset = offset;
//            scriptReader.PopUp();
//            return scriptReader;
//        }

//        // ---------------------------------------------
//        // 몰라도 지장없는 곳

//        static ToolTip getScriptReader()
//        {
//            return Get(EScriptType.Default);
//        }
//    }
//}