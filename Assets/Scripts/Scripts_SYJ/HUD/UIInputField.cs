﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TouchManagement;

namespace RedBlackTree
{
    [RequireComponent(typeof(TouchBox))]
    public class UIInputField : InputField
    {
        PointerEventData pointerEventData;
        TouchBox touchBox;
        protected override void Start()
        {
            //EventSystem.current.SetSelectedGameObject(gameObject);
            pointerEventData = new PointerEventData(EventSystem.current);
            pointerEventData.button = PointerEventData.InputButton.Left;
            pointerEventData.clickCount = 1;
            pointerEventData.Use();

            touchBox = GetComponent<TouchBox>();
            touchBox.OnTouchStart += (Touch touch, Vector2 pos) =>
            {
                pointerEventData.position = pos;
                OnPointerDown(pointerEventData);
            };

            touchBox.OnTouchEnd += (Touch touch, Vector2 pos) =>
            {
                pointerEventData.position = pos;
                OnPointerUp(pointerEventData);
                if (touchBox.GetTouchIn(pos))
                {
                    Select();
                    ActivateInputField();
                    //Select();
                    //OnPointerClick(pointerEventData);
                    
                }
            };
        }
    }

}