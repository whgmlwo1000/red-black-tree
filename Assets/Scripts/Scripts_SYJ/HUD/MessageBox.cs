﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageBox : MonoBehaviour
{
    [SerializeField] AudioSource audioSource = null;
    private void OnEnable()
    {
        audioSource.Play();
    }
}
