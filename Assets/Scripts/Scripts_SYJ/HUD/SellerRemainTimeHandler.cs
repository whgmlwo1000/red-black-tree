﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{
    public class SellerRemainTimeHandler : MonoBehaviour
    {
        /// <summary>
        /// 0.0 ~ 1.0
        /// </summary>
        public float Amount { get { return mAmount; } set { mAmount = value; } }

        /// <summary>
        /// True면 펼쳐짐
        /// </summary>
        /// <param name="value"></param>
        public void Popup(bool value)
        {
            animator.SetBool("IsPopUp", value);
        }



        [SerializeField] UIAbyssMinimap uIAbyssMinimap = null;
        [Range(0,1)]
        float mAmount = 0;

        Animator animator;

        private void Awake()
        {
            animator = GetComponentInChildren<Animator>();
        }

        private void Update()
        {
            uIAbyssMinimap.playerProgressAmount = mAmount;
        }
    }
}