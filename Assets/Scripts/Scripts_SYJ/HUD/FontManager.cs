﻿using UnityEngine;
using System.Collections.Generic;

namespace RedBlackTree
{

    public enum EFontType
    {
        None = -1,
        DOSSaemmul,
        DungGeunMo,
        pixel,
        PixelOperator,
        Count
    }

    public static class FontManager
    {
        private static Font mCurrent;
        public static Font Current
        {
            get
            {
                if (mCurrent == null)
                {
                    mCurrent = mFontDict[StateManager.Font.State];
                }
                return mCurrent;
            }
        }

        static Dictionary<EFontType, Font> mFontDict;
        static FontManager()
        {
            mFontDict = new Dictionary<EFontType, Font>();
            for (EFontType t = EFontType.None + 1; t < EFontType.Count; t++)
            {
                mFontDict.Add(t, Resources.Load<Font>(string.Format("Fonts/{0}", t.ToString())));
                StateManager.Font.AddEnterEvent(t, OnFontChancged);
            }

            if (StateManager.Language.State == ELanguage.Korean)
                StateManager.Font.State = EFontType.DOSSaemmul;
            else if (StateManager.Language.State == ELanguage.English)
                StateManager.Font.State = EFontType.pixel;
        }

        public static void OnFontChancged()
        {
            mCurrent = mFontDict[StateManager.Font.State];
        }
    }

}