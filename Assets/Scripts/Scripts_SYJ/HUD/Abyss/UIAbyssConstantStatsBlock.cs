﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace RedBlackTree
{

    public class UIAbyssConstantStatsBlock : UIAbyssStatsBlock
    {

        public EPlayerStatus_Constant mType;

        public override int GetLevel()
        {
            return playerStatus.GetLevel(mType);
        }
        public override int GetPromotion()
        {
            return playerStatus.GetPromotion(mType);
        }
        public override bool CanPromote()
        {
            return playerStatus.CanPromote(mType);
        }
        public override void Promote()
        {
            base.Promote();
            playerStatus.Promote(mType);
        }
        public override int GetPromotionCost() {
            return playerStatus.GetPromotionCost(mType); ;
        }
    }
}
