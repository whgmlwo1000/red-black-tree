﻿using UnityEngine;
using System.Collections.Generic;

namespace RedBlackTree
{

    public class UIPlayerMenuPassiveItemManager : MonoBehaviour
    {
        Inventory mInventory;

        public static ItemSlot[] mItemSlots;

        [SerializeField] Transform mGridTransform = null;

        void Start()
        {
            mInventory = UIAbyssHandler.Self.PlayerStatus.Inventory;

            var count = mGridTransform.childCount;
            mItemSlots = new ItemSlot[count];
            for (int i = 0; i < count; i++)
            {
                mItemSlots[i] = new ItemSlot();
                mItemSlots[i].Init(mGridTransform.GetChild(i));
            }

            InvokeRepeating("UpdateItem", 1, 0.1f);
        }


        void UpdateItem()
        {
            for (int i = 0; i < mItemSlots.Length; i++)
            {
                if(i < mInventory.CountOfAddedPassiveItems)
                    mItemSlots[i].SetItem(mInventory.GetAddedPassiveItemAt(i));
                else
                    mItemSlots[i].SetItem(null);
            }
        }
    }

}