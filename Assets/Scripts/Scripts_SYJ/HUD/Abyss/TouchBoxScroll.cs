﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TouchManagement;

namespace RedBlackTree
{

    [RequireComponent(typeof(TouchBox))]
    [RequireComponent(typeof(ScrollRect))]
    public class TouchBoxScroll : MonoBehaviour
    {

        TouchBox touchBox;
        ScrollRect scrollRect;
        PointerEventData pointerEventData;

        // Start is called before the first frame update
        void Start()
        {
            touchBox = GetComponent<TouchBox>();
            scrollRect = GetComponent<ScrollRect>();
            pointerEventData = new PointerEventData(EventSystem.current);

            scrollRect.normalizedPosition = new Vector2(0, 0);

            touchBox.OnTouchStart += OnTouchStart;
            touchBox.OnTouchStay += OnTouchStay;
            touchBox.OnTouchEnd += OnTouchEnd;
        }

        void OnTouchStart(Touch touch, Vector2 worldPosition)
        {
            pointerEventData.position = worldPosition;
            scrollRect.OnBeginDrag(pointerEventData);
        }
        void OnTouchStay(Touch touch, Vector2 worldPosition)
        {
            pointerEventData.position = worldPosition;
            scrollRect.OnDrag(pointerEventData);
        }
        void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            pointerEventData.position = worldPosition;
            scrollRect.OnEndDrag(pointerEventData);
        }
    }

}