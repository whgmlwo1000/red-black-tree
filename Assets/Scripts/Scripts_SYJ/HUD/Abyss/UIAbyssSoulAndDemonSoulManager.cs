﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RedBlackTree
{
    public class UIAbyssSoulAndDemonSoulManager : MonoBehaviour
    {
        [SerializeField]
        private Text soulText = null;
        [SerializeField]
        private Text devilSoulText = null;

        PlayerStatus playerStatus;

        public int soulAmount;
        public int devilSoulAmount;

        public int maxSoulAmount = 999;
        public int maxDevilSoulAmount = 999;

        private int prevSoulAmount = 999;
        private int prevDevilSoulAmount = 999;

        void Start()
        {
            playerStatus = UIAbyssHandler.Self.PlayerStatus;
        }

        void Update()
        {
            if (playerStatus != null)
            {
                devilSoulAmount = playerStatus.Inventory.DemonSouls;
                soulAmount = playerStatus.Inventory.Souls;
            }

            if (soulAmount != prevSoulAmount)
            {
                if (maxSoulAmount < soulAmount) soulAmount = maxSoulAmount;
                prevSoulAmount = soulAmount;
                soulText.text = soulAmount.ToString();
            }
            if (devilSoulAmount != prevDevilSoulAmount)
            {
                if (maxDevilSoulAmount < devilSoulAmount) devilSoulAmount = maxDevilSoulAmount;
                prevDevilSoulAmount = devilSoulAmount;
                devilSoulText.text = devilSoulAmount.ToString();
            }
        }
    }
}
