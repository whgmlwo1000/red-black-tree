﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TouchManagement;

namespace RedBlackTree
{
    public enum EPauseButtonState
    {
        None = -1,
        Normal,
        Pushed,
        Paused,
        Count
    }

    

    // 일시정지: 즉시 정지하고 애니메이션 재생
    // 일시정지해제: PlayerMenu의 애니메이션이 종료되면 일시정지 해제함
    [RequireComponent(typeof(TouchBox))]
    public class UIAbyssPauseButton : StateBase<EPauseButtonState>
    {
        [SerializeField] private Animator popoutAnimator = null;
        [SerializeField] protected Sprite[] mPauseSprites = null;
        [SerializeField] AudioClip[] audioClips = null;
        GameObject popoutObj = null;

        Image mButtonImg;
        Action mAfterPopInAction;

        bool mIsEnable;
        static UIAbyssPauseButton mSelf;
        static UIAbyssPauseButton mActivePauseObj;

        AudioSource audioSource;

        /// <summary>
        /// 아이콘을 닫기로 바꾸고 다음닫기시 이벤트를 호출해준다
        /// </summary>
        /// <param name="enable"></param>
        public static void Enable(Action afterPopInAction)
        {
            mSelf.mIsEnable = true;
            mSelf.mAfterPopInAction = afterPopInAction;
            mSelf.State = EPauseButtonState.Paused;
        }

        public static void Disable()
        {
            mSelf.State = EPauseButtonState.Normal;
            mSelf.mIsEnable = false;
        }



        // 플레이어 메뉴 팝업을 위함 ------------------------------

        public override void Start()
        {
            mSelf = this;

            Init();
        }

        public void Init()
        {
            audioSource = GetComponent<AudioSource>();
            var touchBox = GetComponent<TouchBox>();
            touchBox.OnTouchStart += OnButtonDown;
            touchBox.OnTouchEnd += OnButtonUp;
            mButtonImg = GetComponent<Image>();

            pauseCount = 0;
            isSelfPaused = false;


            popoutObj = popoutAnimator.gameObject;
            Invoke("LateStart", 0.01f);


            SetStates(new PauseButtonNormal(this),
                new PauseButtonPushed(this),
                new PauseButtonPaused(this));
        }

        void LateStart()
        {
            popoutObj.SetActive(false);
        }

        protected virtual void OnButtonDown(Touch touch, Vector2 worldPosition)
        {
            // 버튼 활성화된 상태
            State = EPauseButtonState.Pushed;
        }

        protected virtual void OnButtonUp(Touch touch, Vector2 worldPosition)
        {
            // Enable을 통해 이 버튼을 닫기로 사용하면
            // 상태복귀할때 mAction 호출됨
            if (mIsEnable)
            {
                SelfDisable();
            }
            else
            {
                SelfEnable(UnpauseButtonDown); // 이 버튼이 눌렸다는건 플레이어 메뉴를 띄우기 위함
                Pause(true); // 일시정지
                PopInMenu(false); // 메뉴나오기
            }
        }

        void SelfEnable(Action afterPopInAction)
        {
            mIsEnable = true;
            mAfterPopInAction = afterPopInAction;
            State = EPauseButtonState.Paused;
            if (mActivePauseObj)
            {
                mActivePauseObj.SelfDisable();
            }
            mActivePauseObj = this;
            audioSource.PlayOneShot(audioClips[0]);
        }

        void SelfDisable()
        {
            State = EPauseButtonState.Normal;
            mIsEnable = false;
            if(mActivePauseObj == this)
                mActivePauseObj = null;
            audioSource.PlayOneShot(audioClips[1]);
        }

        public override void Update()
        {


            if (Input.GetKeyDown(KeyCode.Escape) && name == "PauseButton")
            {
                OnButtonUp(new Touch(), Vector2.zero);
            }
            base.Update();
            // PlayerMenu의 애니메이션이 종료되면 일시정지 해제함
            if (!mIsEnable && 
                popoutAnimator.isActiveAndEnabled &&
            popoutAnimator.GetCurrentAnimatorStateInfo(0).IsName("disable") &&
            popoutAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.9f)
            {
                Pause(false);
            }
        }

        static int pauseCount = 0;
        bool isSelfPaused = false;
        protected virtual void Pause(bool isPause)
        {
            if (isSelfPaused != isPause)
            {
                pauseCount = isPause ? pauseCount + 1 : pauseCount - 1;
                isSelfPaused = isPause;
            }

            StateManager.Pause.State = pauseCount != 0;
            DPad.SetAble(pauseCount == 0);
            popoutObj.SetActive(isPause);
        }

        void UnpauseButtonDown()
        {
            PopInMenu(true);
        }

        private void PopInMenu(bool popin)
        {
            popoutAnimator.SetBool("disable", popin);
        }

        void OnApplicationPause(bool isPause)
        {
            // 메인일시정지버튼에만 적용되게
            if (isPause && name.Contains("Pause")) 
            {
                var touch = new Touch();
                OnButtonDown(touch, Vector2.zero);
                OnButtonUp(touch, Vector2.zero);
            }
        }

        protected virtual void SetSprite(int idx)
        {
            mButtonImg.sprite = mPauseSprites[idx];
        }



        public class PauseButtonNormal : State<EPauseButtonState>
        {
            UIAbyssPauseButton target;
            public PauseButtonNormal(UIAbyssPauseButton target) : base(EPauseButtonState.Normal) {
                this.target = target;
            }
            public override void Start()
            {
                target.SetSprite(0);
                target.mIsEnable = false;

                target.mAfterPopInAction?.Invoke();
            }
        }
        public class PauseButtonPushed : State<EPauseButtonState>
        {
            UIAbyssPauseButton target;
            public PauseButtonPushed(UIAbyssPauseButton target) : base(EPauseButtonState.Pushed) {
                this.target = target;
            }
            public override void Start()
            {
                target.SetSprite(1);
            }
        }
        public class PauseButtonPaused : State<EPauseButtonState>
        {
            UIAbyssPauseButton target;
            public PauseButtonPaused(UIAbyssPauseButton target) : base(EPauseButtonState.Paused) {
                this.target = target;
            }

            public override void Start()
            {
                target.SetSprite(2);
                target.mIsEnable = true;
            }

            public override void End()
            {
            }
        }
    }

}