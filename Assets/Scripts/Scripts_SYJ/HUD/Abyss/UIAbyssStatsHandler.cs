﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TouchManagement;

namespace RedBlackTree  
{

    public class UIAbyssStatsHandler : MonoBehaviour
    {
        [SerializeField] List<UIAbyssStatsBlock> mStatsBlocks = null;

        PlayerStatus mPlayerStatus;

        [SerializeField] TouchBox mApplyTouchBox = null;
        [SerializeField] TouchBox mResetTouchBox = null;

        [SerializeField] Text costAmountText = null;
        [SerializeField] Text remainAmountText = null;



        static UIAbyssStatsHandler mMain;
        public static UIAbyssStatsHandler Main { get { return mMain; } }

        void Start()
        {
            mMain = this;
            mPlayerStatus = UIAbyssHandler.Self.PlayerStatus;

            mApplyTouchBox.OnTouchStart += ApplyPromotion;
            mResetTouchBox.OnTouchStart += ResetPromotion;

            foreach (var item in mStatsBlocks)
            {
                item.Init(mPlayerStatus);
            }
        }

        public void ApplyPromotion(Touch touch, Vector2 pos)
        {
            mPlayerStatus.ApplyPromotion();
            foreach (var item in mStatsBlocks)
            {
                item.Apply();
            }
        }

        public void ResetPromotion(Touch touch, Vector2 pos)
        {
            mPlayerStatus.ResetPromotion();
            foreach (var item in mStatsBlocks)
            {
                item.Reset();
            }
        }

        private void Update()
        {
            int totalCost = 0;
            foreach (var item in mStatsBlocks)
            {
                totalCost += item.TotalCost;
            }
            costAmountText.text = "-" + mPlayerStatus.PromotionCost.ToString();
            remainAmountText.text = (mPlayerStatus.Inventory.DemonSouls- mPlayerStatus.PromotionCost).ToString();
        }
    }

}