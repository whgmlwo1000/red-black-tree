﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RedBlackTree
{
    public class UIAbyssHealthBar : MonoBehaviour
    {
        public float MaxHealth {
            get {
                return mMaxHealth;
            }
            set {
                mMaxHealth = value;
                unitSize = mMaxHealth / totalUnitCount;

                float ableUnitRatio = health / unitSize;
                prevUnitCount = Mathf.CeilToInt(ableUnitRatio);
                shakeUnitIdx = prevUnitCount - 1;
            }
        }

        // -------- 체력정보들 ----------

        [SerializeField] float mMaxHealth = 1000;
        [SerializeField] public float health = 300;

        [SerializeField] Material blinkMaterial = null;

        public float shakeUnitsLimitPercent = 0.8f;

        private float unitSize; // 체력칸 하나당 가지는 체력량

        private int totalUnitCount; // 총 체력칸 갯수
        private int prevUnitCount; // 이전 체력칸

        private int shakeUnitIdx; // 흔들리는 칸 index

        public Transform unitRootTrans; // 체력칸들 가지고있는 RootTransform

        HealthUnit[] healthUnit;

        // -------- 체력정보들 ----------

        Status_Giant statusGiant;

        void Start()
        {
            if (SceneManager.GetActiveScene().name.Contains(EScene.Scene_Sanctuary.ToString()))
            {
                MaxHealth = 1000;
                health = 1000;
                return;
            }

            statusGiant = UIAbyssHandler.Self.GiantStatus;

            totalUnitCount = unitRootTrans.childCount;
            MaxHealth = statusGiant.MaxLife;

            healthUnit = new HealthUnit[totalUnitCount];
            for (int i = 0; i < totalUnitCount; i++)
            {
                healthUnit[i].trans = unitRootTrans.GetChild(i);
                healthUnit[i].animation = healthUnit[i].trans.GetComponent<Animation>();
            }

            for (int i = 0; i < prevUnitCount; i++)
            {
                healthUnit[i].PlayAnimation(AnimState.Reset);
                healthUnit[i].nextAnimationType = AnimState.NextNone;
            }
            for (int i = prevUnitCount; i < totalUnitCount; i++)
            {
                healthUnit[i].PlayAnimation(AnimState.Disable);
            }
        }

        bool reached = false;
        bool prevInveincible = false;
        float hue;
        float contrast = 1;
        Color addColor;
        void Update()
        {
            if (statusGiant == null) return;
            var invincible = statusGiant.IsInvincible;
            if (prevInveincible != invincible || !reached)
            {
                prevInveincible = invincible;
                if (invincible)
                {
                    hue = Mathf.Lerp(hue, 98, Time.unscaledDeltaTime*5);
                    var sin = Mathf.Sin(Time.time * 15.8f);
                    var _addColor = Color.Lerp(new Color(0.57f, 0.60f, 0), new Color(0.1f, 0.1f, 0, 0), sin);
                    contrast = Mathf.Lerp(contrast, 1.6f, Time.unscaledDeltaTime*5);
                    addColor = Color.Lerp(addColor, _addColor, Time.unscaledDeltaTime*5);
                    reached = false;
                }
                else
                {
                    hue = Mathf.Lerp(hue, 0, Time.unscaledDeltaTime*5);
                    contrast = Mathf.Lerp(contrast, 1, Time.unscaledDeltaTime*5);
                    addColor = Color.Lerp(addColor, new Color(0, 0, 0), Time.unscaledDeltaTime*5);
                    reached = Mathf.Abs(hue - 0) < 0.1f;
                }
                blinkMaterial.SetFloat("_Hue", hue);
                blinkMaterial.SetFloat("_Contrast", contrast);
                blinkMaterial.SetColor("_AddColor", addColor);
            }

            health = statusGiant.Life;

            for (int i = 0; i < healthUnit.Length; i++)
            {
                healthUnit[i].CheckNextAnimation();
            }
            PlayHealthStateAnimation();
        }

        void PlayHealthStateAnimation()
        {
            float ableUnitRatio = health / unitSize;
            // 혹시모를 부동소수점문제 방지
            ableUnitRatio = ableUnitRatio < 0.001f ? 0 : totalUnitCount - 0.001f <= ableUnitRatio ? totalUnitCount : ableUnitRatio;
            int ableUnitCount = Mathf.CeilToInt(ableUnitRatio);

            // 전보다 체력이 한칸 줄었다면
            // 지금 체력칸 갯수(ableUnitCount), 바로 체력칸 갯수(prevUnitCount)
            // 지금 체력칸 갯수(ableUnitCount)을 날려야함
            if (ableUnitCount < prevUnitCount)
            {
                // 줄은 칸 갯수 계산하고 날리는 애니메이션 재생
                for (int i = ableUnitCount; i < prevUnitCount; i++)
                {
                    healthUnit[i].PlayAnimation(AnimState.Remove);
                }
                shakeUnitIdx = ableUnitCount - 1;
                shakeUnitIdx = shakeUnitIdx < 0 ? 0 : shakeUnitIdx;
            }
            // 전보다 체력이 한칸 늘었다면
            // 지금 체력칸 갯수(ableUnitCount), 바로 체력칸 갯수(prevUnitCount)
            else if (ableUnitCount > prevUnitCount)
            {
                for (int i = 0; i <= shakeUnitIdx; i++)
                {
                    healthUnit[i].SetNextAnimation(AnimState.Reset); // 전에 흔들리고있던 칸 멈춤
                }
                for (int i = prevUnitCount; i < ableUnitCount; i++)
                {
                    healthUnit[i].PlayAnimation(AnimState.Heal);
                }
                shakeUnitIdx = ableUnitCount - 1;
            }

            // 한칸이 줄고나서 만약에 줄어들고있는 체력이 있다면
            float ratio = Mathf.Repeat(ableUnitRatio, 1.0f);
            if (0.01f < ratio && ratio < shakeUnitsLimitPercent)
            {
                healthUnit[shakeUnitIdx].SetNextAnimation(AnimState.Damaging);
            }
            else if(shakeUnitsLimitPercent <= ratio)
            {
                healthUnit[shakeUnitIdx].SetNextAnimation(AnimState.Reset);
            }

            prevUnitCount = ableUnitCount;


            // 최대체력에 도달했다면
            if (totalUnitCount == (int)ableUnitRatio)
            {
                healthUnit[ableUnitCount - 1].SetNextAnimation(AnimState.Reset);
            }
        }


        // 체력칸 하나의 상태를 저장함
        [System.Serializable]
        struct HealthUnit
        {
            public Transform trans;
            public Animation animation;
            public AnimState playingAnimationType;
            public AnimState nextAnimationType;

            public void CheckNextAnimation()
            {
                if (nextAnimationType != AnimState.NextNone && !animation.isPlaying)
                {
                    PlayAnimation(nextAnimationType);
                    playingAnimationType = nextAnimationType;
                    nextAnimationType = AnimState.NextNone;
                }
            }

            public void SetNextAnimation(AnimState type)
            {
                if ((type == AnimState.Damaging && (playingAnimationType == AnimState.Reset || playingAnimationType == AnimState.Damaging)) ||
                    (type == AnimState.Reset && (playingAnimationType == AnimState.Damaging)))
                {
                    nextAnimationType = AnimState.NextNone;
                    playingAnimationType = AnimState.NextNone;
                    PlayAnimation(type);
                }else
                    nextAnimationType = type;
            }

            public void PlayAnimation(AnimState type)
            {
                if (AnimState.Disable == type)
                {
                    trans.GetChild(0).gameObject.SetActive(false);
                    animation.enabled = false;
                }
                else
                {
                    trans.GetChild(0).gameObject.SetActive(true);
                    animation.enabled = true;
                }

                switch (type)
                {
                    case AnimState.Disable:
                        playingAnimationType = AnimState.Disable;
                        break;
                    case AnimState.Damaging:
                        if (playingAnimationType == AnimState.Damaging) break;
                        //SetNextAnimation(type);
                        animation.Play("Animation_HealthBar_Damaging", PlayMode.StopAll);
                        playingAnimationType = AnimState.Damaging;
                        break;
                    case AnimState.Reset:
                        //SetNextAnimation(type);
                        animation.Play("Animation_HealthBar_Reset", PlayMode.StopAll);
                        playingAnimationType = AnimState.Reset;
                        break;
                    case AnimState.Remove:
                        if (playingAnimationType == AnimState.Remove) break;
                        animation.Play("Animation_HealthBar_Remove", PlayMode.StopAll);
                        playingAnimationType = AnimState.Remove;
                        nextAnimationType = AnimState.NextNone;
                        break;
                    case AnimState.Heal:
                        if (playingAnimationType == AnimState.Heal) break;
                        animation.Play("Animation_HealthBar_Heal", PlayMode.StopAll);
                        playingAnimationType = AnimState.Heal;
                        nextAnimationType = AnimState.NextNone;
                        break;
                }
            }
            
        }

        public enum AnimState
        {
            Disable, Damaging, Remove, Heal, Reset, NextNone
        }
    }
}