﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace RedBlackTree
{
    
    public class UIAbyssPauseInventoryButton : UIAbyssPauseButton
    {
        //[SerializeField]
        //Image changedButtonImg = null;

        protected override void SetSprite(int idx)
        {
            //changedButtonImg.sprite = mPauseSprites[idx];
        }

        public override void Start()
        {
            Init();
        }
    }

}