﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TouchManagement;

namespace RedBlackTree
{

    public class UIAbyssStatsBlock : MonoBehaviour
    {
        [SerializeField] Sprite defaultStatsBlock = null;
        [SerializeField] Sprite raiseStatsBlock = null;
        [SerializeField] Text mCostText = null;

        Animator mAnimator;

        Image[] statsBlockImages;

        protected PlayerStatus playerStatus;


        // 갯수: 1이면 1개가 켜진다.
        public int raiseStats = 0;
        public int defaultStats = 0;

        public int maxStats = 12;

        TouchBox touchBox;

        public int TotalCost {
            get;
            private set;
        }


        public void Init(PlayerStatus playerStatus)
        {
            mAnimator = GetComponentInChildren<Animator>();

            touchBox = GetComponentInChildren<TouchBox>();
            touchBox.OnTouchStart += UpgradeButtonDown;

            this.playerStatus = playerStatus;

            defaultStats = GetLevel();
            mCostText.text = GetPromotionCost().ToString();
            UpdatePromateButtonState();

            var t = transform;
            int count = t.childCount-1;
            statsBlockImages = new Image[count];
            for (int i = 0; i < count; i++)
            {
                var child = t.GetChild(i+1);
                statsBlockImages[i] = child.GetComponent<Image>();
                SetTexture(i);
            }

        }

        public void UpdateStatus()
        {
            mCostText.text = GetPromotionCost().ToString();

            if (maxStats < raiseStats)
                raiseStats = maxStats;
            if (maxStats < defaultStats)
                defaultStats = maxStats;

            int count = statsBlockImages.Length;
            for (int i = 0; i < count; i++)
            {
                SetTexture(i);
            }
        }

        public void UpgradeButtonDown(Touch touch, Vector2 pos)
        {
            if (CanPromote())
            {
                Promote();
                raiseStats = GetPromotion();
                TotalCost += GetPromotionCost();
                mAnimator.Play("Push");
                //mAnimator.SetTrigger("Down");
            }
                
        }

        public void Apply()
        {
            TotalCost = 0;
            defaultStats = raiseStats + defaultStats;
            raiseStats = 0;
        }

        public void Reset()
        {
            raiseStats = 0;
            TotalCost = 0;
        }

        void UpdatePromateButtonState()
        {
            mAnimator.SetBool("CanPromote", CanPromote());
        }

        void Update()
        {
            UpdatePromateButtonState();
            UpdateStatus();
            BlinkAnimation();
        }

        void BlinkAnimation()
        {
            // TODO 깜박이는애니메이션
            if (raiseStats == 0) return;
            var end = defaultStats + raiseStats;
            for (int i = defaultStats; i < end; i++)
            {
                statsBlockImages[i].color = Color.Lerp(Color.white, Color.grey, Mathf.Repeat(Time.unscaledTime*1.4f, 1));
            }
        }

        void SetTexture(int i)
        {
            if (i < defaultStats)
            {
                statsBlockImages[i].color = Color.white;
                statsBlockImages[i].sprite = defaultStatsBlock;
            }
            else if (i < defaultStats + raiseStats)
            {
                statsBlockImages[i].color = Color.white;
                statsBlockImages[i].sprite = raiseStatsBlock;
            }
            else
            {
                statsBlockImages[i].color = Color.clear;
                statsBlockImages[i].sprite = null;
            }
        }

        public virtual int GetLevel()
        {
            return -1;
        }
        public virtual int GetPromotion()
        {
            return -1;
        }
        public virtual bool CanPromote()
        {
            return false;
        }
        public virtual void Promote()
        {

        }
        public virtual int GetPromotionCost() { return -1; }
    }
}