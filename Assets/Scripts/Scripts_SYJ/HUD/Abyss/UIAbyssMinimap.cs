﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{
    public class UIAbyssMinimap : MonoBehaviour
    {
        [SerializeField]
        private RectTransform lineRectTrans = null;

        [SerializeField]
        private float speed = 3.0f;

        [SerializeField]
        bool isVertical = false;

        [Range(0.0f, 1.0f)]
        public float playerProgressAmount = 1;
        private float lerpPlayerProgressAmount = 0;
        private const float progressCheckEpsilon = 0.01f;

        Status_WaveSystem statusWaveSystem;

        void Start()
        {
            statusWaveSystem = UIAbyssHandler.Self.StatusWaveSystem;
        }

        void Update()
        {
            if(statusWaveSystem)
                playerProgressAmount = (float)statusWaveSystem.WaveIndex / (statusWaveSystem.CountOfWaves-1);

            if (Mathf.Abs(lerpPlayerProgressAmount - playerProgressAmount) > progressCheckEpsilon)
            {
                lerpPlayerProgressAmount = Mathf.LerpUnclamped(lerpPlayerProgressAmount, playerProgressAmount, Time.deltaTime * speed);
            }else
            {
                lerpPlayerProgressAmount = playerProgressAmount;
            }

            if(isVertical)
                lineRectTrans.anchorMax = new Vector2(0.5f, lerpPlayerProgressAmount);
            else
                lineRectTrans.anchorMax = new Vector2(lerpPlayerProgressAmount, 0.5f);
        }
    }

}