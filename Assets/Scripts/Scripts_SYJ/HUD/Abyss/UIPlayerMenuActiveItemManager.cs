﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TouchManagement;

namespace RedBlackTree
{

    public class UIPlayerMenuActiveItemManager : MonoBehaviour
    {
        List<ItemSlot> mItemArray = new List<ItemSlot>();
        ItemSlot[] mActivedItemArray;

        [SerializeField]
        Transform mGridRoot = null;


        private void Start()
        {

            InitItemSlots();
        }

        private void InitItemSlots()
        {
            if (mActivedItemArray == null)
            {
                mActivedItemArray = new ItemSlot[4];
                for (int i = 0; i < mActivedItemArray.Length; i++)
                {
                    mActivedItemArray[i] = new ItemSlot();
                    mActivedItemArray[i].Init(transform.GetChild(i + 1));
                }
            }

            int itemCount = mGridRoot.childCount;
            mItemArray.Capacity = itemCount;
            for (int i = 0; i < itemCount; i++)
            {
                var rootTrans = mGridRoot.GetChild(i);
                var temp = new ItemSlot();
                temp.Init(rootTrans);
                mItemArray.Add(temp);
            }
        }

    }

    public class ItemSlot
    {
        protected Item mRefItem;

        Image iconImage;
        GameObject[] mLightObjs = new GameObject[2];


        public void Init(Transform rootTrans)
        {
            iconImage = rootTrans.GetChild(1).GetComponent<Image>();
            mLightObjs[0] = rootTrans.GetChild(2).gameObject;
            mLightObjs[1] = rootTrans.GetChild(3).gameObject;

            TouchBox touchBox = rootTrans.GetComponent<TouchBox>();
            if (touchBox != null)
            {
                touchBox.OnTouchStart += ShowToolTip;
                touchBox.OnTouchEnd += HideToolTip;
            }


            Reset();
        }

        void Reset()
        {
            mLightObjs[0].SetActive(false);
            mLightObjs[1].SetActive(false);
            iconImage.gameObject.SetActive(false);
            mRefItem = null;
        }

        public Item GetItem()
        {
            return mRefItem;
        }

        public void SetItem(Item refItem)
        {
            if (refItem == null)
            {
                Reset();
                return;
            }

            mRefItem = refItem;
            iconImage.sprite = mRefItem.Icon;
            iconImage.gameObject.SetActive(true);

            var proficiency = mLightObjs.Length - mRefItem.Proficiency;
            proficiency = proficiency < 0 ? 0 : proficiency;
            for (int i = mLightObjs.Length - 1; i >= 0; i--)
            {
                mLightObjs[i].SetActive(i > proficiency - 1);
            }
        }


        ToolTip toolTip;
        void ShowToolTip(Touch touch, Vector2 pos)
        {
            if (mRefItem == null) return;
            ScriptBase scriptBase = new ScriptBase(mRefItem.Name, mRefItem.Option);
            toolTip = ToolTipPoll.PopUpInMenu(scriptBase, iconImage.transform.position);
        }

        void HideToolTip(Touch touch, Vector2 pos)
        {
            if (toolTip == null) return;
            toolTip.PopDown();
        }
    }
}