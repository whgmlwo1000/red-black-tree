﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RedBlackTree
{
    public class UIAbyssActiveItemSlot : MonoBehaviour
    {
        [SerializeField]
        private Transform slotsRoot = null;
        private List<Image> slotImages = new List<Image>();

        private void Start()
        {
            slotsRoot.GetComponentsInChildren(true, slotImages);
        }


        /// <summary>
        /// 0~3까지 idx의 아이템 슬롯의 이미지를 바꿈
        /// </summary>
        /// <param name="idx">0~3까지</param>
        /// <param name="sprite">이미지전달 혹은 Null로 비울수있음</param>
        public void SetItemSprite(int idx, Sprite sprite)
        {
            Debug.Assert(idx < slotImages.Count || idx >= 0, "Out of index 아이템 추가 이상한곳에 함");
            var slotImage = slotImages[idx];
            if (sprite == null)
                slotImage.color = Color.clear;
            else
                slotImage.color = Color.white;

            slotImage.sprite = sprite;
        }
    }
}
