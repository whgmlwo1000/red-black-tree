﻿using UnityEngine;

namespace RedBlackTree
{
    public class UITopPassiveItemManager : MonoBehaviour
    {
        [SerializeField] Transform mGridTransform = null;
        ItemSlot[] mItemSlots;

        void Start()
        {
            Invoke("LateStart", 1);
        }

        void LateStart()
        {
            var count = mGridTransform.childCount;
            mItemSlots = new ItemSlot[count];
            for (int i = 0; i < count; i++)
            {
                mItemSlots[i] = new ItemSlot();
                mItemSlots[i].Init(mGridTransform.GetChild(i));
            }
            InvokeRepeating("UpdateItem", 0.01f, 0.1f);
        }

        void UpdateItem()
        {
            var itemList = UIPlayerMenuPassiveItemManager.mItemSlots;
            var itemCount = 0;
            for (; itemCount < itemList.Length; itemCount++)
            {
                if (itemList[itemCount].GetItem() == null)
                {
                    break;
                }
            }
            var count = mItemSlots.Length;
            for (int i = 0; i < count; i++)
            {
                var invenItemIdx = itemCount - i - 1;
                if (invenItemIdx < 0) mItemSlots[i].SetItem(null);
                else mItemSlots[i].SetItem(itemList[invenItemIdx].GetItem());
            }
        }
    } 
}
