﻿//#define IS_TEST_MODE
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree {
    // 모든 UI 객체의 초기화를 위해 참조됨
    public class UIAbyssHandler : MonoBehaviour
    {

        [SerializeField] Status_Giant giantStatus = null;
        [SerializeField] PlayerStatus playerStatus = null;
        [SerializeField] Status_WaveSystem statusWaveSystem = null;

        public PlayerStatus PlayerStatus { get { return playerStatus; } }
        public Status_Giant GiantStatus { get { return giantStatus; } }
        public Status_WaveSystem StatusWaveSystem { get { return statusWaveSystem; } }
        // ---------------------

        public static UIAbyssHandler Self { get { return mSelf; } }
        static UIAbyssHandler mSelf;

        void Awake()
        {
            mSelf = this;
        }

#if IS_TEST_MODE
       public List<Sprite> sprites;
        void Update()
        {

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                itemSlot.SetItemSprite(Random.Range(0, 4), sprites[Random.Range(0, sprites.Count)]);
                UIAbyssSoulAndDeelSoulManager.devilSoulAmount = Random.Range(0, 999);
                UIAbyssSoulAndDeelSoulManager.soulAmount = Random.Range(0, 1500);
                uiAbyssHealthBar.health = Random.Range(0, 1000);
                uiMinimap.playerProgressAmount = Random.Range(0.0f, 1.0f);
            }
        }
#endif
    }
}