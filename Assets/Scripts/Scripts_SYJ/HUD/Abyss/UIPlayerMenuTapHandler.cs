﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TouchManagement;

namespace RedBlackTree
{

    public class UIPlayerMenuTapHandler : MonoBehaviour
    {

        [SerializeField]
        private Color disableColor = Color.black;
        [SerializeField]
        private Sprite[] sprites = new Sprite[4];
        [SerializeField]
        private GameObject[] tapContents = new GameObject[4];
        private Image[] tapIconImages = new Image[4];

        TouchBox[] touchBox;


        Image Image;

        void Start()
        {
            Transform trans = transform;
            var childCount = trans.childCount;
            touchBox = new TouchBox[childCount];
            for (int i = 0; i < childCount; i++)
            {
                var chlidTrans = trans.GetChild(i);
                touchBox[i] = chlidTrans.GetComponent<TouchBox>();
                tapIconImages[i] = chlidTrans.GetComponent<Image>();
            }
            touchBox[0].OnTouchStart += OnTouchTap01;
            touchBox[1].OnTouchStart += OnTouchTap02;
            //if (childCount > 2)
            //{
            //    touchBox[2].OnTouchStart += OnTouchTap03;
            //    touchBox[3].OnTouchStart += OnTouchTap04;
            //}

            Image = GetComponent<Image>();

            Invoke("LateStart", 0.5f);
            
        }

        void LateStart()
        {
            SetActiveIcon(0);
        }

        void OnTouchTap01(Touch touch, Vector2 worldPosition)
        {
            SetActiveIcon(0);
        }
        void OnTouchTap02(Touch touch, Vector2 worldPosition)
        {
            SetActiveIcon(1);
        }
        void OnTouchTap03(Touch touch, Vector2 worldPosition)
        {
            SetActiveIcon(2);
        }
        void OnTouchTap04(Touch touch, Vector2 worldPosition)
        {
            SetActiveIcon(3);
        }

        void SetActiveIcon(int idx)
        {
            Debug.Assert(0 <= idx && idx < touchBox.Length);
            Image.sprite = sprites[idx];
            for (int i = 0; i < touchBox.Length; i++)
            {
                if (i == idx)
                {
                    tapIconImages[i].color = Color.white;
                    tapContents[i].SetActive(true);
                }
                else
                {
                    tapIconImages[i].color = disableColor;
                    tapContents[i].SetActive(false);
                }
            }
        }
    }

}