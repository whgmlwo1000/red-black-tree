﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchManagement;

namespace RedBlackTree {
    public class UIAbyssDefenseButton : MonoBehaviour
    {
        public bool isDefenseButtonDown;
        void Start()
        {
            var touchBox = GetComponent<TouchBox>();
            touchBox.OnTouchStart += OnDefenseButtonDown;
            touchBox.OnTouchEnd += OnDefenseButtonUp;
            touchBox.OnTouchStay += OnDefenseButtonStay;
        }

        private void OnDefenseButtonUp(Touch touch, Vector2 worldPosition)
        {
            if (StateManager.Pause.State) return;
            isDefenseButtonDown = false;
        }
        private void OnDefenseButtonDown(Touch touch, Vector2 worldPosition)
        {
            if (StateManager.Pause.State) return;
            print("Down");
            isDefenseButtonDown = true;
        }
        private void OnDefenseButtonStay(Touch touch, Vector2 worldPosition)
        {
            if (StateManager.Pause.State) return;
            print("stay");
        }
    }
}