﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;
//using UnityEngine.Audio;
//namespace RedBlackTree
//{

//    public class ButtonEvent_SettingControl : MonoBehaviour
//    {
//        private static float mSFXVolumeCount = 100f;
//        private static float mBGMVolumeCoumt = 100f;
//        private static float mScreenBrightness = 100f;

//        private static int mScreenModeIndex = 0;
//        private static int mVibrationIndex = 0;
//        private static int mMaximumFPSIndex = 0;
//        private static int mVSyncIndex = 0;
//        private static int mLanguageIndex = 0;

//        [SerializeField]
//        private Light mDirectionalLight;
//        [SerializeField]
//        private EConfiguresType mConfigures;
//        [SerializeField]
//        private AudioMixer mAudioMixer;
//        [SerializeField]
//        private Text mText;
//        [SerializeField]
//        private ButtonEvent_SettingControl mScreenMode;
//        private void Start()
//        {
//            SetMenuText();
//        }

//        private void SetMenuText()
//        {
//            switch (mConfigures)
//            {
//                case EConfiguresType.ScreenMode:
//                    if (mScreenModeIndex == 0)
//                    {
//                        if (StateManager.Language.State == ELanguage.English)
//                        {
//                            mText.text = "FULL SCREEN";
//                        }
//                        else if (StateManager.Language.State == ELanguage.Korean)
//                        {
//                            mText.text = "전체 화면";
//                        }
//                    }
//                    else if (mScreenModeIndex == 1)
//                    {
//                        if (StateManager.Language.State == ELanguage.English)
//                        {
//                            mText.text = "WINDOW SCREEN";
//                        }
//                        else if (StateManager.Language.State == ELanguage.Korean)
//                        {
//                            mText.text = "윈도우 화면";
//                        }
//                    }
//                    else if (mScreenModeIndex == 2)
//                    {
//                        if (StateManager.Language.State == ELanguage.English)
//                        {
//                            mText.text = "FULL WINDOW SCREEN";
//                        }
//                        else if (StateManager.Language.State == ELanguage.Korean)
//                        {
//                            mText.text = "전체 윈도우 화면";
//                        }
//                    }
//                    break;
//                case EConfiguresType.ScreenBrightness:
//                    mText.text = mScreenBrightness.ToString();
//                    break;
//                case EConfiguresType.SFX:
//                    mText.text = mSFXVolumeCount.ToString();
//                    break;
//                case EConfiguresType.BGM:
//                    mText.text = mBGMVolumeCoumt.ToString();
//                    break;
//                case EConfiguresType.Vibration:
//                    if (mVibrationIndex == 0)
//                    {
//                        mText.text = "ON";
//                    }
//                    else if (mVibrationIndex == 1)
//                    {
//                        mText.text = "OFF";
//                    }
//                    break;
//                case EConfiguresType.MaximumFPS:
//                    if (mMaximumFPSIndex == 0)
//                    {
//                        mText.text = "30";
//                    }
//                    else if (mMaximumFPSIndex == 1)
//                    {
//                        mText.text = "60";
//                    }
//                    break;
//                case EConfiguresType.VSync:
//                    if (mVSyncIndex == 0)
//                    {
//                        mText.text = "OFF";
//                    }
//                    else if (mVSyncIndex == 1)
//                    {
//                        mText.text = "ON";
//                    }
//                    break;
//                case EConfiguresType.Language:
//                    if (mLanguageIndex == 0)
//                    {
//                        mText.text = "ENGLISH";
//                    }
//                    else if (mLanguageIndex == 1)
//                    {
//                        mText.text = "한국어";
//                    }
//                    break;
//            }
//        }

//        private void SetMenu()
//        {
//            switch (mConfigures)
//            {
//                case EConfiguresType.ScreenMode:
//                    if (Input.GetKeyDown(KeyCode.A))
//                    {
//                        ChangeScreenMode(--mScreenModeIndex);
//                    }
//                    else if (Input.GetKeyDown(KeyCode.D))
//                    {
//                        ChangeScreenMode(++mScreenModeIndex);
//                    }
//                    break;
//                case EConfiguresType.ScreenBrightness:
//                    if (Input.GetKeyDown(KeyCode.A))
//                    {
//                        ChangeBrightness(-1);

//                    }
//                    else if (Input.GetKeyDown(KeyCode.D))
//                    {
//                        ChangeBrightness(+1);
//                    }
//                    break;
//                case EConfiguresType.SFX:
//                    if (Input.GetKeyDown(KeyCode.A))
//                    {
//                        ChangeSFX(-1);
//                    }
//                    else if (Input.GetKeyDown(KeyCode.D))
//                    {
//                        ChangeSFX(+1);
//                    }
//                    break;
//                case EConfiguresType.BGM:
//                    if (Input.GetKeyDown(KeyCode.A))
//                    {
//                        ChangeBGM(-1);
//                    }
//                    else if (Input.GetKeyDown(KeyCode.D))
//                    {
//                        ChangeBGM(+1);
//                    }
//                    break;
//                case EConfiguresType.MaximumFPS:
//                    if (Input.GetKeyDown(KeyCode.A))
//                    {
//                        ChangeFPS(--mMaximumFPSIndex);
//                    }
//                    else if (Input.GetKeyDown(KeyCode.D))
//                    {
//                        ChangeFPS(++mMaximumFPSIndex);
//                    }
//                    break;
//                case EConfiguresType.VSync:
//                    if (Input.GetKeyDown(KeyCode.A))
//                    {
//                        ChangeVSync(--mVSyncIndex);
//                    }
//                    else if (Input.GetKeyDown(KeyCode.D))
//                    {
//                        ChangeVSync(++mVSyncIndex);
//                    }
//                    break;
//                case EConfiguresType.Language:
//                    if (Input.GetKeyDown(KeyCode.A))
//                    {
//                        ChangeLanguage(--mLanguageIndex);
//                    }
//                    else if (Input.GetKeyDown(KeyCode.D))
//                    {
//                        ChangeLanguage(++mLanguageIndex);
//                    }
//                    break;
//            }
//        }

//        public void ChangeScreenMode(int nextIndex)
//        {
//            if (nextIndex > 2)
//            {
//                mScreenModeIndex = 0;
//            }
//            else if (nextIndex < 0)
//            {
//                mScreenModeIndex = 2;
//            }

//            if (mScreenModeIndex == 0)
//            {
//                // Full Screen
//                Screen.SetResolution(1920, 1080, true);

//                if (StateManager.Language.State == ELanguage.English)
//                {
//                    mText.text = "FULL SCREEN";
//                }
//                else if (StateManager.Language.State == ELanguage.Korean)
//                {
//                    mText.text = "전체 화면";
//                }
//            }
//            else if (mScreenModeIndex == 1)
//            {
//                // Window Screen
//                Screen.SetResolution(1280, 720, false);

//                if (StateManager.Language.State == ELanguage.English)
//                {
//                    mText.text = "WINDOW SCREEN";
//                }
//                else if (StateManager.Language.State == ELanguage.Korean)
//                {
//                    mText.text = "윈도우 화면";
//                }
//            }
//            else if (mScreenModeIndex == 2)
//            {
//                // Full Window Screen
//                Screen.SetResolution(1920, 1080, false);

//                if (StateManager.Language.State == ELanguage.English)
//                {
//                    mText.text = "FULL WINDOW SCREEN";
//                }
//                else if (StateManager.Language.State == ELanguage.Korean)
//                {
//                    mText.text = "전체 윈도우 화면";
//                }
//            }
//        }

//        public void ChangeBrightness(int value)
//        {
//            if (value > 0)
//            {
//                //밝기 up
//                mScreenBrightness += 10f;
//            }
//            else if (value < 0)
//            {
//                //밝기 down
//                mScreenBrightness -= 10f;
//            }

//            mScreenBrightness = Mathf.Clamp(mScreenBrightness, 0f, 100f);

//            if (mDirectionalLight != null)
//            {
//                mDirectionalLight.intensity = mScreenBrightness * 0.01f * 2f;
//            }

//            mText.text = mScreenBrightness.ToString();
//        }

//        public void ChangeSFX(int value)
//        {
//            if (mAudioMixer != null)
//            {
//                if (value > 0)
//                {
//                    //볼륨 up
//                    mSFXVolumeCount += 10f;
//                }
//                else if (value < 0)
//                {
//                    //볼륨 down
//                    mSFXVolumeCount -= 10f;
//                }

//                mSFXVolumeCount = Mathf.Clamp(mSFXVolumeCount, 0f, 100f);
//                mAudioMixer.SetFloat("SFX", (((float)mSFXVolumeCount / 100f) - 1) * 20f);
//                mText.text = mSFXVolumeCount.ToString();

//                if (mSFXVolumeCount == 0)
//                {
//                    mAudioMixer.SetFloat("SFX", -40f);
//                }
//            }
//        }

//        public void ChangeBGM(int value)
//        {
//            if (mAudioMixer != null)
//            {
//                if (value > 0)
//                {
//                    //볼륨 up
//                    mBGMVolumeCoumt += 10f;
//                }
//                else if (value < 0)
//                {
//                    //볼륨 down
//                    mBGMVolumeCoumt -= 10f;
//                }

//                mBGMVolumeCoumt = Mathf.Clamp(mBGMVolumeCoumt, 0f, 100f);
//                mAudioMixer.SetFloat("Music", (((float)mBGMVolumeCoumt / 100f) - 1) * 20f);
//                mText.text = mBGMVolumeCoumt.ToString();

//                if (mBGMVolumeCoumt == 0)
//                {
//                    mAudioMixer.SetFloat("Music", -40f);
//                }
//            }
//        }

//        public void ChangeFPS(int nextIndex)
//        {
//            if (nextIndex > 1)
//            {
//                mMaximumFPSIndex = 0;
//            }
//            else if (nextIndex < 0)
//            {
//                mMaximumFPSIndex = 1;
//            }

//            if (mMaximumFPSIndex == 0)
//            {
//                // FPS = 30
//                Application.targetFrameRate = 30;
//                mText.text = "30";
//            }
//            else if (mMaximumFPSIndex == 1)
//            {
//                // FPS = 60
//                Application.targetFrameRate = 60;
//                mText.text = "60";
//            }
//        }

//        public void ChangeVSync(int nextIndex)
//        {
//            if (nextIndex > 1)
//            {
//                mVSyncIndex = 0;
//            }
//            else if (nextIndex < 0)
//            {
//                mVSyncIndex = 1;
//            }

//            if (mVSyncIndex == 0)
//            {
//                // VSync = Off
//                QualitySettings.vSyncCount = 0;
//                mText.text = "OFF";
//            }
//            else if (mVSyncIndex == 1)
//            {
//                // VSync = On
//                QualitySettings.vSyncCount = 1;
//                mText.text = "ON";
//            }
//        }

//        public void ChangeLanguage(int nextIndex)
//        {
//            if (nextIndex > 1)
//            {
//                mLanguageIndex = 0;
//            }
//            else if (nextIndex < 0)
//            {
//                mLanguageIndex = 1;
//            }

//            if (mLanguageIndex == 0)
//            {
//                // English
//                StateManager.Font.State = EFontType.pixel;
//                mText.text = "ENGLISH";
//                mScreenMode.ChangeScreenModeFont();
//            }
//            else if (mLanguageIndex == 1)
//            {
//                // Korean
//                StateManager.Font.State = EFontType.DOSSaemmul;
//                mText.text = "한국어";
//                mScreenMode.ChangeScreenModeFont();
//            }
//        }

//        public void ChangeScreenModeFont()
//        {
//            if (mScreenModeIndex == 0)
//            {
//                if (StateManager.Language.State == ELanguage.English)
//                {
//                    mText.text = "FULL SCREEN";
//                }
//                else if (StateManager.Language.State == ELanguage.Korean)
//                {
//                    mText.text = "전체 화면";
//                }
//            }
//            else if (mScreenModeIndex == 1)
//            {
//                if (StateManager.Language.State == ELanguage.English)
//                {
//                    mText.text = "WINDOW SCREEN";
//                }
//                else if (StateManager.Language.State == ELanguage.Korean)
//                {
//                    mText.text = "윈도우 화면";
//                }
//            }
//            else if (mScreenModeIndex == 2)
//            {
//                if (StateManager.Language.State == ELanguage.English)
//                {
//                    mText.text = "FULL WINDOW SCREEN";
//                }
//                else if (StateManager.Language.State == ELanguage.Korean)
//                {
//                    mText.text = "전체 윈도우 화면";
//                }
//            }
//        }

//        public void ChangeMenuValue(bool isPositive)
//        {
//            switch (mConfigures)
//            {
//                case EConfiguresType.ScreenMode:
//                    if (isPositive)
//                    {
//                        ChangeScreenMode(++mScreenModeIndex);
//                    }
//                    else
//                    {
//                        ChangeScreenMode(--mScreenModeIndex);
//                    }
//                    break;
//                case EConfiguresType.ScreenBrightness:
//                    if (isPositive)
//                    {
//                        ChangeBrightness(+1);
//                    }
//                    else
//                    {
//                        ChangeBrightness(-1);
//                    }
//                    break;
//                case EConfiguresType.SFX:
//                    if (isPositive)
//                    {
//                        ChangeSFX(+1);
//                    }
//                    else
//                    {
//                        ChangeSFX(-1);
//                    }
//                    break;
//                case EConfiguresType.BGM:
//                    if (isPositive)
//                    {
//                        ChangeBGM(+1);
//                    }
//                    else
//                    {
//                        ChangeBGM(-1);
//                    }
//                    break;
//                case EConfiguresType.MaximumFPS:
//                    if (isPositive)
//                    {
//                        ChangeFPS(++mMaximumFPSIndex);
//                    }
//                    else
//                    {
//                        ChangeFPS(--mMaximumFPSIndex);
//                    }
//                    break;
//                case EConfiguresType.VSync:
//                    if (isPositive)
//                    {
//                        ChangeVSync(++mVSyncIndex);
//                    }
//                    else
//                    {
//                        ChangeVSync(--mVSyncIndex);
//                    }
//                    break;
//                case EConfiguresType.Language:
//                    if (isPositive)
//                    {
//                        ChangeLanguage(++mLanguageIndex);
//                    }
//                    else
//                    {
//                        ChangeLanguage(--mLanguageIndex);
//                    }
//                    break;
//            }
//        }
//    }

//    public enum EConfiguresType
//    {
//        None = -1,
//        ScreenMode,
//        ScreenBrightness,
//        SFX,
//        BGM,
//        Vibration,
//        MaximumFPS,
//        VSync,
//        DashType,
//        Language,
//        Count
//    }


//}