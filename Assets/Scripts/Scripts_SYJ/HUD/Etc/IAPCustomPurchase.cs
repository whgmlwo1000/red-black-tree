﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPCustomPurchase : MonoBehaviour
{
    public void Buy(string id)
    {
        CodelessIAPStoreListener.Instance.InitiatePurchase(id);
    }
}
