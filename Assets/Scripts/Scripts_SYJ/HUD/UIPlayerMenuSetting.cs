﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

namespace RedBlackTree
{
    public class UIPlayerMenuSetting : MonoBehaviour
    {
        [SerializeField] AudioMixer audioMixer = null;
        [SerializeField] UISlider entire = null;
        [SerializeField] UISlider bgm = null;
        [SerializeField] UISlider sfx = null;
        [SerializeField] UIToggle vibration = null;

        [SerializeField] Text mEntrieVolume = null;
        [SerializeField] Text mBGMVolume = null;
        [SerializeField] Text mSFXVolume = null;
        [SerializeField] Text mVibration = null;
        [SerializeField] Text mResetAll = null;

        Volumes mVolumes;

        public void ReturnToSanctuary()
        {
            SceneAsyncLoader.LoadScene(EScene.Scene_Sanctuary);
        }

        public void GotoTitle()
        {
            SceneAsyncLoader.LoadScene(EScene.Scene_Title);
        }

        public void ChangeEntireVolume()
        {
            mVolumes.mEntire = entire.Value;
            mVolumes.Save();
            float value = ConvertVolume(entire.Value);
            audioMixer.SetFloat("Master", value);
        }

        public void ChangeBGMVolume()
        {
            mVolumes.mBGM = bgm.Value;
            mVolumes.Save();
            float value = ConvertVolume(bgm.Value);
            audioMixer.SetFloat("Music", value);
        }

        public void ChangeSFXVolume()
        {
            mVolumes.mSFX = sfx.Value;
            mVolumes.Save();
            float value = ConvertVolume(sfx.Value);
            audioMixer.SetFloat("SFX", value);
        }

        bool ignoreFirst = true;
        public void ChangeVibration(bool isOn)
        {
            if (ignoreFirst) { ignoreFirst = false;  return; }
            StateManager.Vibration.State = isOn;
            PlayerData.Current.Vibration = isOn;
        }

        public void ResetAll()
        {
            UIAbyssHandler.Self.PlayerStatus.Reset();
            PlayerData.Current.Reset();
            SceneAsyncLoader.LoadScene(EScene.Scene_Title);
        }

        public static float ConvertVolume(float value)
        {
            value /= 9.0f;
            float min = -50, max = 0;
            float total = max - min;
            return value * total + min;
        }


        private void Start()
        {
            mEntrieVolume.text = DataManager.HasTag("Setting_EntireVolume") ? DataManager.GetScript("Setting_EntireVolume") : "";
            mBGMVolume.text = DataManager.HasTag("Setting_BGMVolume") ? DataManager.GetScript("Setting_BGMVolume") : "";
            mSFXVolume.text = DataManager.HasTag("Setting_SFXVolume") ? DataManager.GetScript("Setting_SFXVolume") : "";
            mVibration.text = DataManager.HasTag("Setting_Vibration") ? DataManager.GetScript("Setting_Vibration") : "";
            mResetAll.text = DataManager.HasTag("Setting_ResetAll") ? DataManager.GetScript("Setting_ResetAll") : "";

            mVolumes = PlayerData.Current.Volumes;
            entire.Value = mVolumes.mEntire;
            bgm.Value = mVolumes.mBGM;
            sfx.Value = mVolumes.mSFX;

            StateManager.Vibration.State = PlayerData.Current.Vibration;


            ChangeEntireVolume();
            ChangeBGMVolume();
            ChangeSFXVolume();
            vibration.isOn = StateManager.Vibration.State;
            vibration.UpdateText();
        }

        [System.Serializable]
        public class Volumes
        {
            public int mEntire;
            public int mBGM;
            public int mSFX;

            public Volumes()
            {
                mEntire = 9;
                mBGM = 9;
                mSFX = 9;
            }
            public void Save()
            {
                PlayerData.Current.Volumes = this;
            }
        }
    }

}