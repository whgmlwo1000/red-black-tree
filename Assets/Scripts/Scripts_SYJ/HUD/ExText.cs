﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace RedBlackTree
{
    [ExecuteInEditMode]
    public class ExText : Text
    {
        protected override void Start()
        {
            base.Start();
            font = FontManager.Current;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }
    } 
}