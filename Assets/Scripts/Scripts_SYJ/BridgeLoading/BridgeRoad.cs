﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RedBlackTree
{
    public class BridgeRoad : MonoBehaviour
    {
        [SerializeField] RectTransform mRoadRectTrans = null;
        [SerializeField] RectTransform mDownRectTrans = null;

        Transform mTargetTrans;
        Transform mMarkerTrans;

        [SerializeField] Image mStageColorImage = null;


        // 미달, 도달
        float[] mStageLevelRoadAmounts = { 0/*, 0.06f, 0.5f*/, 1 };

        float mStageLevel;
        float mNextStageLevel;
        float mBrigeAmount;
        float mNextBrigeAmount; // mBrigeAmount를 보간하기위한 타겟값

        float mRoadGrowSpeed = 5; // 가로
        float mBridgeGrowSpeed = 3; // 세로

        public void SetColor(Color color)
        {
            mStageColorImage.color = color;
        }

        public void InitStageLevel(int level)
        {
            level = Mathf.Clamp(level, 0, mStageLevelRoadAmounts.Length-1);
            mNextStageLevel = mStageLevel = mStageLevelRoadAmounts[level];
            mRoadRectTrans.anchorMax = new Vector2(mStageLevel, 1);

            if(level > 0)
            {
                mNextBrigeAmount = mBrigeAmount = 1;
            }
            mDownRectTrans.anchorMax = new Vector2(1, mBrigeAmount);
        }

        public virtual void SetNextStageLevel(int level, RectTransform markerTrans)
        {
            level = Mathf.Clamp(level, 0, mStageLevelRoadAmounts.Length-1);
            mNextStageLevel = mStageLevelRoadAmounts[level];

            if (level == 1)
            {
                mBrigeAmount = 0;
                mNextBrigeAmount = 1;
            }

            mMarkerTrans = markerTrans;
        }
        void Update()
        {

            if (Mathf.Abs(mBrigeAmount- mNextBrigeAmount) <= .03f)
            {
                mStageLevel = Mathf.Lerp(mStageLevel, mNextStageLevel, Time.deltaTime * mRoadGrowSpeed);
                mTargetTrans = mRoadRectTrans.GetChild(0);
                if (Mathf.Abs(mStageLevel - mNextStageLevel) <= .01f)
                {
                    mStageLevel = mNextStageLevel;
                }
            }
            else
            {
                mTargetTrans = mDownRectTrans.GetChild(0);
            }
            mBrigeAmount = Mathf.Lerp(mBrigeAmount, mNextBrigeAmount, Time.deltaTime * mBridgeGrowSpeed);

            mRoadRectTrans.anchorMax = new Vector2(mStageLevel, 1);
            mDownRectTrans.anchorMax = new Vector2(1, mBrigeAmount);

            if(mMarkerTrans && mTargetTrans)
                mMarkerTrans.position = mTargetTrans.position;
        }
    }

}