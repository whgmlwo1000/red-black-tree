﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



namespace RedBlackTree
{
    public enum EScene
    {
        None = -1,
        Scene_Intro,
        Scene_Loading,
        Scene_Bridge,
        Scene_GameOver,
        Scene_GameClear,
        Scene_Opening,
        Scene_Title,
        Scene_Sanctuary,
        Scene_Abyss_Tutorial,

        __Abyss__, 
        // 이 아래로만 브릿지씬으로 로딩됨
        Scene_Abyss_Midgard,
        Count
    }
    public class SceneAsyncLoader : MonoBehaviour
    {
        public enum LoadSceneType
        {
            None,
            AutoLoad,
            LoadAndWaitForInput,
            Count
        }

        public static bool IsCompleted { get; private set; } = false;

        private static EScene mNextScene = EScene.None;
        public static EScene NextScene {
            get {
                if (mNextScene == EScene.None)
                {
                    var scene = GetESceneByName(SceneManager.GetActiveScene().name);
                    StateManager.Scene.State = mNextScene = scene;
                }
                return mNextScene;
            }
            set { mNextScene = value; }
        }

        static Dictionary<EScene, LoadSceneType> mLoadSceneDict = new Dictionary<EScene, LoadSceneType>();

        static float minLoadTime = 1.5f;
        static float bridgeMinLoadTime = 3.0f;
        static float repeatTime = .1f;

        static AsyncOperation mAsyncOperation;

        bool mIsKeyDown;

        [SerializeField] Animator mFadeAnimator = null;
        [SerializeField] GameObject enableObjWhenLoadEnd = null;


        /// <summary>
        /// 로딩 씬 -> nextScene
        /// </summary>
        /// <param name="nextScene">로드할 씬 이름</param>
        public static void LoadScene(EScene nextScene)
        {
            StateManager.Pause.State = false;
            Time.timeScale = 1;
            NextScene = nextScene;
            IsCompleted = false;
            StateManager.Scene.State = NextScene;

            // 성역보다 더 아래에 있는 씬들이라면 심연으로 치고 브릿지씬으로감
            if (mLoadSceneDict[NextScene] == LoadSceneType.LoadAndWaitForInput)
            {
                SceneManager.LoadScene(EScene.Scene_Bridge.ToString());
            }
            else
            {
                if (IsAbyssScene(StateManager.Scene.PrevState))
                {
                    if(StateManager.GameOver.State)
                        SceneManager.LoadScene(EScene.Scene_GameOver.ToString());
                    else if(StateManager.GameClear.State)
                        SceneManager.LoadScene(EScene.Scene_GameClear.ToString());
                    else
                        SceneManager.LoadScene(EScene.Scene_Loading.ToString());
                }
                else
                {
                    SceneManager.LoadScene(EScene.Scene_Loading.ToString());
                }
            }
        }

        /// <summary>
        /// 로딩씬에서만 호출 애니메이션이 종료되면 씬이 넘어감
        /// </summary>
        public static void ShowNextScene()
        {
            mAsyncOperation.allowSceneActivation = true;
        }


        public static void OnSceneChanged()
        {
            if (NextScene == StateManager.Scene.State) return;

            // 심연씬이면 브릿지씬으로 로딩
            LoadScene(StateManager.Scene.State);
        }




        static SceneAsyncLoader()
        {
            InitSceneList();
        }

        /// <summary>
        /// 로딩씬에서만 호출
        /// </summary>
        private void Start()
        {
            StateManager.Pause.State = false;
            Time.timeScale = 1;
            mAsyncOperation = SceneManager.LoadSceneAsync(NextScene.ToString());
            mAsyncOperation.allowSceneActivation = false;

            mFadeAnimator.gameObject.SetActive(true);

            mIsKeyDown = false;

            // 아무씬->심연
            if (IsAbyssScene(NextScene))
            {
                InvokeRepeating("CheckAnyKeyInput", bridgeMinLoadTime, repeatTime);
            }
            else
            {
                // 심연에->성역  죽거나 클리어
                if (IsAbyssScene(StateManager.Scene.PrevState) && (StateManager.GameOver.State || StateManager.GameClear.State))
                {
                    InvokeRepeating("CheckAnyKeyInput", bridgeMinLoadTime, repeatTime);
                }
                // 기본 로딩 이동
                else
                {
                    InvokeRepeating("PassCompleteTime", minLoadTime, repeatTime);
                }
            }
        }

        void PassCompleteTime()
        {
            if (mAsyncOperation.progress >= 0.9f)
                FadeOut();
        }

        void CheckAnyKeyInput()
        {
            if (mAsyncOperation.progress >= 0.9f)
            {
                if (enableObjWhenLoadEnd)
                    enableObjWhenLoadEnd.SetActive(true);
                if (mIsKeyDown)
                {
                    enableObjWhenLoadEnd.SetActive(false);
                    FadeOut();
                }
            }
        }


        public void FadeOut()
        {
            mFadeAnimator.SetBool("FadeIn", false);

            // 로딩씬에서 탈출할때 초기화
            StateManager.GameOver.State = false;
            StateManager.GameClear.State = false;
        }


        private void Update()
        {
            if ((Input.anyKeyDown || Input.touchCount > 0) && enableObjWhenLoadEnd.activeSelf)
                mIsKeyDown = true;
        }


        static void InitSceneList()
        {
            for (EScene i = EScene.None + 1; i <= EScene.Scene_Sanctuary; i++)
            {
                mLoadSceneDict.Add(i, LoadSceneType.AutoLoad);
                StateManager.Scene.AddEnterEvent(i, OnSceneChanged);
            }
            for (EScene i = EScene.Scene_Sanctuary + 1; i < EScene.Count; i++)
            {
                mLoadSceneDict.Add(i, LoadSceneType.LoadAndWaitForInput);
                StateManager.Scene.AddEnterEvent(i, OnSceneChanged);
            }
        }

        static EScene GetESceneByName(string sceneName)
        {
            for (EScene i = EScene.None + 1; i < EScene.Count; i++)
            {
                
                if (string.Equals(sceneName, (i.ToString())))
                {
                    return i;
                }
            }
            return EScene.None;
        }

        static bool IsAbyssScene(EScene scene)
        {
            return scene > EScene.__Abyss__;
        }

        public class SceneStateHandler : StateHandler<EScene>
        {
            public new EScene State {
                get {
                    if (base.State == EScene.None)
                    {
                        var scene = GetESceneByName(SceneManager.GetActiveScene().name);
                        base.State = scene;
                    }
                    return base.State;
                }
                set {
                    base.State = value;
                }
            }
        }
    }
}