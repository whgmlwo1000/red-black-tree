﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{
    public static class AbyssSceneManager
    {
        static Queue<EScene> mClearedAbyss;
        static Queue<EScene> mUnclearedAbyss;

        public static void GoToNextStage()
        {
            SceneAsyncLoader.LoadScene(mUnclearedAbyss.Peek());
        }

        public static void InitAndShuffleAbyssScene(int sceneCount = 3)
        {
            mClearedAbyss.Clear();
            mUnclearedAbyss.Clear();

            mUnclearedAbyss.Enqueue(EScene.Scene_Abyss_Midgard);
            for (int i = 0; i < sceneCount-1; i++)
            {
                mUnclearedAbyss.Enqueue(EScene.__Abyss__ + Random.Range(1, EScene.Count - EScene.__Abyss__));
            }

            // 임시 출시 미드가르드가 처음에 나오는 버전이라 수정함
            //for (int i = 0; i < sceneCount; i++)
            //{
            //    mUnclearedAbyss.Enqueue(EScene.__Abyss__ + Random.Range(1, EScene.Count - EScene.__Abyss__));
            //}            
        }

        public static EStage ConvertESceneToEStage(EScene scene)
        {
            switch (scene)
            {
                case EScene.Scene_Abyss_Midgard:
                    return EStage.Midgard;
            }
            return EStage.Midgard;
        }


        public static int GetClearCount()
        {
            return mClearedAbyss.Count;
        }

        public static Queue<EScene>.Enumerator GetClearedAbyss()
        {
            return mClearedAbyss.GetEnumerator();
        }

        public static Queue<EScene>.Enumerator GetUnclearedAbyss()
        {
            return mUnclearedAbyss.GetEnumerator();
        }

        static void OnChangedScene()
        {
            Debug.Assert(mUnclearedAbyss.Dequeue() == StateManager.Scene.PrevState);
            mClearedAbyss.Enqueue(StateManager.Scene.PrevState);
        }

        static AbyssSceneManager()
        {
            for (EScene i = EScene.__Abyss__+1; i < EScene.Count; i++)
            {
                StateManager.Scene.AddExitEvent(i, OnChangedScene);
            }
            mClearedAbyss = new Queue<EScene>();
            mUnclearedAbyss = new Queue<EScene>();
        }
    } 
}
