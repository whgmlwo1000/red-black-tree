﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{

    public class BridgeSceneManager : MonoBehaviour
    {
        const int STAGE_COUNT = 1;

        [Range(1, 4)]
        [SerializeField] int mStageLevel;

        [SerializeField] Vector2 roadOffset = new Vector2(52.8f, -15.9f);


        [SerializeField] List<EStage> mStages = new List<EStage>();

        [SerializeField] List<BridgeRoad> mRoads = new List<BridgeRoad>();
        [SerializeField] BridgeRoad mLastStage = null;

        [SerializeField] RectTransform mMarkerTrans = null;

        [SerializeField] Color[] mStageColor = new Color[(int)EStage.Count];

        private void Start()
        {
            var sceneEnumerator = AbyssSceneManager.GetClearedAbyss();
            while (sceneEnumerator.MoveNext())
            {
                mStages.Add(AbyssSceneManager.ConvertESceneToEStage(sceneEnumerator.Current));
            }
            sceneEnumerator = AbyssSceneManager.GetUnclearedAbyss();
            while (sceneEnumerator.MoveNext())
            {
                mStages.Add(AbyssSceneManager.ConvertESceneToEStage(sceneEnumerator.Current));
            }

            mStageLevel = AbyssSceneManager.GetClearCount()+1;


            var startPos = mRoads[0].GetComponent<RectTransform>().anchoredPosition;
            for (int i = 0; i < mRoads.Count; i++)
            {
                mRoads[i].SetColor(mStageColor[(int)mStages[i]]);
                mRoads[i].GetComponent<RectTransform>().anchoredPosition = startPos + new Vector2(roadOffset.x* i, roadOffset.y* i);
            }
            mLastStage.GetComponent<RectTransform>().anchoredPosition = startPos + new Vector2(roadOffset.x * mRoads.Count, roadOffset.y * mRoads.Count);

            var stageLevel = mStageLevel - 1; // stageLevel이 1이면 이제 1로 간다는 뜻
            for (int i = 0; i < mRoads.Count; i++)
            {
                mRoads[i].InitStageLevel(stageLevel);
                stageLevel -= STAGE_COUNT;
            }

            var lastIndex = (mStageLevel-1) / STAGE_COUNT;
            if (lastIndex < mRoads.Count)
            {
                mRoads[lastIndex].SetNextStageLevel(mStageLevel - lastIndex * STAGE_COUNT, mMarkerTrans);
            }
            else
            {
                mLastStage.SetNextStageLevel(1, mMarkerTrans);
            }
        }

        
    }

}