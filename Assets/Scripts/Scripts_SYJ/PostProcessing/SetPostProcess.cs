﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPostProcess
{
    private static float mSaturation = 0.7f;
    private static float mBleedAmount = -0.2f;
    private static float mDistortAmount = 0.03f;

    private static float mCurSaturation = 1;
    private static float mCurBleedAmount = -1;
    private static float mCurDistortAmount = 0;

    public static void LowHealthPointPostProcess(Material material)
    {
        if (material != null)
        {
            //현재 생명력이 최대 생명력의 25%이하가 되면 효과가 발동되도록 구현함
            mCurSaturation -= Time.deltaTime * 0.5f;
            mCurBleedAmount += Time.deltaTime * 0.5f;
            mCurDistortAmount += Time.deltaTime * 0.05f;

            mCurSaturation = Mathf.Clamp(mCurSaturation, mSaturation, 1);
            mCurBleedAmount = Mathf.Clamp(mCurBleedAmount, -1, mBleedAmount);
            mCurDistortAmount = Mathf.Clamp(mCurDistortAmount, 0.0f, mDistortAmount);

            // (0~1) : 0에 가까울수록 흑백으로 변함
            material.SetFloat("_Saturation", mCurSaturation);
            // (-1~0) : -1에 가까울수록 피효과가 사라짐 0일때 최대
            material.SetFloat("_bleedAmount", mCurBleedAmount);
            // (-0.06~0.06) : 0에 가까울수록 왜곡이 사라짐 그 이상도 가능하지만 너무 심하게 왜곡됨
            material.SetFloat("_distortAmount", mCurDistortAmount);
        }
    }

    public static void NormalHealthPointPostProcess(Material material)
    {
        mCurSaturation += Time.deltaTime * 0.5f;
        mCurBleedAmount -= Time.deltaTime * 0.5f;
        mCurDistortAmount -= Time.deltaTime * 0.05f;

        mCurSaturation = Mathf.Clamp(mCurSaturation, mSaturation, 1);
        mCurBleedAmount = Mathf.Clamp(mCurBleedAmount, -1, 0);
        mCurDistortAmount = Mathf.Clamp(mCurDistortAmount, 0.0f, mDistortAmount);

        // (0~1) : 0에 가까울수록 흑백으로 변함
        material.SetFloat("_Saturation", mCurSaturation);
        // (-1~0) : -1에 가까울수록 피효과가 사라짐 0일때 최대
        material.SetFloat("_bleedAmount", mCurBleedAmount);
        // (-0.06~0.06) : 0에 가까울수록 왜곡이 사라짐 그 이상도 가능하지만 너무 심하게 왜곡됨
        material.SetFloat("_distortAmount", mCurDistortAmount);
    }

    public static void GameOverHealthPointPostProcess(Material material)
    {
        // (0~1) : 0에 가까울수록 흑백으로 변함
        material.SetFloat("_Saturation", 0);
        // (-1~0) : -1에 가까울수록 피효과가 사라짐 0일때 최대
        material.SetFloat("_bleedAmount", -1);
        // (-0.06~0.06) : 0에 가까울수록 왜곡이 사라짐 그 이상도 가능하지만 너무 심하게 왜곡됨
        material.SetFloat("_distortAmount", 0);
    }

}
