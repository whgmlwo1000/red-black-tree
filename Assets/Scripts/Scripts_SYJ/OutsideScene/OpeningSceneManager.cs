﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

namespace RedBlackTree
{
    public class OpeningSceneManager : MonoBehaviour
    {
        Page[] mPages = null;
        int mCurrentPageNum = -1;

        [SerializeField] Animator mFadeAnimtor = null;
        [SerializeField] AudioSource mButtonAudioSource = null;
        public void NextPage()
        {
            ++mCurrentPageNum;
            if (mCurrentPageNum >= mPages.Length - 1) // 마지막페이지가 보였으면
            {
                Invoke("StartFade", 14.5f);
                Invoke("MoveToNextScene", 15f);
            }
            if (mCurrentPageNum >= mPages.Length)
            {
                StartFade();
                Invoke("MoveToNextScene", 0.5f);
            }

            for (int i = 0; i < mPages.Length; i++)
            {
                if (i == mCurrentPageNum)
                {
                    mPages[i].mTargetColor = new Color(1, 1, 1, 1);
                }
                else
                {
                    mPages[i].mTargetColor = Color.clear;
                }
            }



        }

        void StartFade()
        {
            mFadeAnimtor.SetBool("FadeIn", false);

        }

        void DelayReveal()
        {

        }

        void MoveToNextScene()
        {
            SceneManager.LoadScene(EScene.Scene_Abyss_Tutorial.ToString());
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                mButtonAudioSource.Play();
                NextPage();
            }

            for (int i = 0; i < mPages.Length; i++)
            {
                mPages[i].Update();
            }
        }

        private void Start()
        {
            var count = transform.childCount;
            mPages = new Page[count];
            for (int i = 0; i < count; i++)
            {
                mPages[i] = new Page();
                mPages[i].mText = transform.GetChild(i).GetComponent<Text>(); ;
            }

            NextPage();
        }


        [System.Serializable]
        public class Page
        {
            public Text mText;
            public Color mTargetColor;

            public Page() { }

            public void Update()
            {
                mText.color = Color.Lerp(mText.color, mTargetColor, Time.unscaledDeltaTime * 2);
            }
        }
    }

}