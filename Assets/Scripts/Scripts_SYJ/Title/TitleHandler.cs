﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{
    public class TitleHandler : MonoBehaviour
    {
        [SerializeField] GameObject mSettingObj = null;
        [SerializeField] GameObject mCreditObj = null;
        [SerializeField] GameObject mFadeObj = null;
        [SerializeField] GameObject mExitCheckWindow = null;

        public void OnGameStart()
        {
            mFadeObj.SetActive(true);
        }

        public void OnSetting()
        {
            if (!mCreditObj.activeSelf)
                mSettingObj.SetActive(true);
        }

        public void OnCredit()
        {
            if (!mSettingObj.activeSelf)
                mCreditObj.SetActive(true);
        }

        public void OnExit()
        {
            Application.Quit();
        }


        public void LoadSanctuary()
        {
            print(PlayerData.Current.IsClearTutorial);
            if (PlayerData.Current.IsClearTutorial)
                SceneAsyncLoader.LoadScene(EScene.Scene_Sanctuary);
            else
                SceneAsyncLoader.LoadScene(EScene.Scene_Opening);
        }


        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                mExitCheckWindow.SetActive(true);
            }
        }
    }

}