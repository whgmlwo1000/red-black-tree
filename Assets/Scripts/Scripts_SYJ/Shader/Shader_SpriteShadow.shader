﻿Shader "Custom/SpriteShadow"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_OutlineColor("OutlineColor", Color) = (1, 1, 1, 1)
		[MaterialToggle]_isActive("Active Outline", int) = 0
	}
		SubShader
		{
			Tags{ "Queue" = "Transparent" }
			Cull Off
			Blend One OneMinusSrcAlpha

			Pass
		{
			CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

			struct v2f {
			float4 pos : SV_POSITION;
			float2 uv : TEXCOORD0;
		};

		sampler2D _MainTex;
		float4 _MainTex_TexelSize;
		fixed4 _OutlineColor;
		int _isActive;

		v2f vert(appdata_base v) {
			v2f o;
			o.pos = UnityObjectToClipPos(v.vertex);
			o.uv = v.texcoord;
			return o;
		}

		fixed4 frag(v2f i) : SV_Target
		{
			fixed4 col = tex2D(_MainTex, i.uv);
		col.rgb *= col.a;

		fixed4 outlineColor = _OutlineColor;


		fixed upAlpha = tex2D(_MainTex, i.uv + fixed2(0, _MainTex_TexelSize.y)).a;
		fixed leftAlpha = tex2D(_MainTex, i.uv - fixed2(_MainTex_TexelSize.x, 0)).a;

		fixed checkWeight = saturate(upAlpha + leftAlpha);
		checkWeight *= _isActive;

				return (1 - col.a) * checkWeight * outlineColor + col;
			}
			ENDCG
		}
	}
}
