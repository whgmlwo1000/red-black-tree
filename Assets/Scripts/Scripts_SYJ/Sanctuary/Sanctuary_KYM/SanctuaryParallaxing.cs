﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SanctuaryParallaxing : MonoBehaviour
{
    [SerializeField]
    private float mParallaxCoef = 0;

    private Camera mCamera;
    private float mStartX;

    private void Awake()
    {
        mStartX = this.transform.position.x;
        mCamera = Camera.main;
    }

    private void Update()
    {
        this.transform.position = new Vector3((mCamera.gameObject.transform.position.x * mParallaxCoef) + mStartX, this.transform.position.y, this.transform.position.z);
    }
}
