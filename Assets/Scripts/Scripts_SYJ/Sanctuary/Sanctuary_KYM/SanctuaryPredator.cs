﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{
    public enum ESanctuaryPredatorState
    {
        None = -1,
        Nothing,
        Move,
        MovetoPortal,
        EnterPortal,
        Count
    }
    public class SanctuaryPredator : StateBase<ESanctuaryPredatorState>
    {
        public float speed = 2.0f;
        public float dir;

        [HideInInspector]
        public Animator mAnimator = null;

        public float mEntryMinDistance = 0.5f;
        [SerializeField]
        private Transform[] mPortalEntryPoint = null;

        public Vector2[] PortalEntryPos {
            get { return mPortalEntryPos; }
        }
        private Vector2[] mPortalEntryPos = new Vector2[2];

        [HideInInspector]
        public Transform trans;

        public override void Start()
        {
            base.Start();
            mAnimator = this.GetComponent<Animator>();
            for (int i = 0; i < mPortalEntryPoint.Length; i++)
            {
                mPortalEntryPos[i] = mPortalEntryPoint[i].position;
            }

            SanctuaryToAbyssPortalHandler.AddEnterEvent(name, OnMoveToPortal);

            trans = transform;

            SetStates(new SanctuaryPredatorNothing(this),
                new SanctuaryPredatorMove(this),
                new SanctuaryPredatorMovetoPortal(this),
                new SanctuaryPredatorEnterPortal(this));
        }

        public override void Update()
        {
            base.Update();
            trans.Translate(dir * speed * Time.deltaTime, 0, 0);
        }

        public void MoveTo(float dir)
        {
            this.dir = dir > 0 ? 1 : (dir < 0 ? -1 : 0);
            //this.dir = Mathf.Clamp(dir, -1.0f, 1.0f);
        }

        void OnMoveToPortal()
        {
            State = ESanctuaryPredatorState.MovetoPortal;
        }

        public void OnEnterPortal()
        {
            SanctuaryToAbyssPortalHandler.Ready(name);
        }

        public void ClosePortal()
        {
            SanctuaryToAbyssPortalHandler.Get.ClosePortal();
        }
    }

    public class SanctuaryPredatorNothing : State<ESanctuaryPredatorState>
    {
        SanctuaryPredator owner;
        public SanctuaryPredatorNothing(SanctuaryPredator controller) : base(ESanctuaryPredatorState.Nothing)
        {
            owner = controller;
        }

        public override void Start()
        {
            owner.mAnimator.SetBool("IsMove", false);
        }
    }

    public class SanctuaryPredatorMove : State<ESanctuaryPredatorState>
    {
        protected SanctuaryPredator owner;
        public SanctuaryPredatorMove(SanctuaryPredator controller, ESanctuaryPredatorState state = ESanctuaryPredatorState.Move) : base(state)
        {
            owner = controller;
        }

        public override void Start()
        {
            owner.mAnimator.SetBool("IsMove", true);
        }
        public override void Update()
        {

        }
    }

    public class SanctuaryPredatorMovetoPortal : SanctuaryPredatorMove
    {
        Vector2 targetPos;
        public SanctuaryPredatorMovetoPortal(SanctuaryPredator controller) : base(controller, ESanctuaryPredatorState.MovetoPortal)
        {
            targetPos = GetCloserEntry(controller.PortalEntryPos);
        }

        public override void Start()
        {
            base.Start();
        }
        public override void Update()
        {
            owner.MoveTo(targetPos.x - owner.trans.position.x);

            if (Mathf.Abs(owner.trans.position.x - targetPos.x) <= owner.mEntryMinDistance)
            {
                owner.State = ESanctuaryPredatorState.EnterPortal;
            }
        }

        Vector2 GetCloserEntry(Vector2[] mPortalEntryPos)
        {
            float minDistance = 100000000;
            int minDistanceIdx = 0;
            for (int i = 0; i < mPortalEntryPos.Length; i++)
            {
                var distance = Vector2.Distance(mPortalEntryPos[i], owner.trans.position);
                if (minDistance > distance)
                {
                    minDistance = distance;
                    minDistanceIdx = i;
                }
            }
            return mPortalEntryPos[minDistanceIdx];
        }
    }

    public class SanctuaryPredatorEnterPortal : State<ESanctuaryPredatorState>
    {
        SanctuaryPredator owner;
        public SanctuaryPredatorEnterPortal(SanctuaryPredator controller) : base(ESanctuaryPredatorState.EnterPortal)
        {
            owner = controller;
        }

        public override void Start()
        {
            owner.mAnimator.SetBool("EnterPortal", true);
        }

        //public void OnEnterPortal()
        //{
            
        //}
    } 
}