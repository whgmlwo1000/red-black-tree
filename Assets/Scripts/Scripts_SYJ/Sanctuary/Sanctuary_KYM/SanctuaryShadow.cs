﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SanctuaryShadow : MonoBehaviour
{
    [SerializeField]
    private Transform mAlphaPosition = null;
    [SerializeField]
    private Transform mFairyPosition = null;
    [SerializeField]
    private float mWorldY = 0;

    private RawImage screenFrameImage;

    private void Awake()
    {
        screenFrameImage = GetComponent<RawImage>();
    }

    private void Update()
    {
        if(mFairyPosition.position.y >= mAlphaPosition.position.y)
        {
            screenFrameImage.color = new Color(1, 1, 1, 1 - (mFairyPosition.position.y - mAlphaPosition.position.y) / mWorldY);
        }   
        else
        {
            screenFrameImage.color = new Color(1, 1, 1, 0.8f);
        }
    }
}
