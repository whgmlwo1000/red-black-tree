﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class SanctuaryToAbyssPortalHandler : MonoBehaviour
    {
        [SerializeField]
        private CameraController mCamera = null;
        [SerializeField]
        private EScene mEnterScene = EScene.None;

        [SerializeField]
        private AudioSource mAudio = null;
        [SerializeField]
        private AudioClip mDoorOpenAudio = null;
        [SerializeField]
        private AudioClip mDoorCloseAudio = null;
        [SerializeField]
        private AudioClip mDoorOpeningAudio = null;

        public delegate void OnEnter();
        /// <summary>
        /// 등록된 이벤트들이 포탈들어간다는 플레이어의 인풋이후 호출된다.
        /// 애니메이션 재생용
        /// </summary>
        public static event OnEnter OnEnterEvent;

        public static Dictionary<string, bool> readyCheckDictionary = new Dictionary<string, bool>();

        WaitForSeconds halfSecondWait = new WaitForSeconds(0.3f);
        WaitUntil waitCameraFade;

        static SanctuaryToAbyssPortalHandler self;
        public static SanctuaryToAbyssPortalHandler Get {
            get { return self; }
        }

        /// <summary>
        /// 함수 등록시 포탈들어가고 씬 전환되기전 호출
        /// </summary>
        /// <param name="name"></param>
        /// <param name="onEnter"></param>
        public static void AddEnterEvent(string name, OnEnter onEnter)
        {
            if (readyCheckDictionary.ContainsKey(name)) return;
            readyCheckDictionary.Add(name, false);
            OnEnterEvent += onEnter;
        }

        /// <summary>
        /// 포탈 들어가는걸 기다리기위해 준비상태 받음 모두 레디시 넘어감
        /// </summary>
        /// <param name="name"></param>
        public static void Ready(string name)
        {
            readyCheckDictionary[name] = true;
        }

        private void Awake()
        {
            self = this;
            waitCameraFade = new WaitUntil(() => !mCamera.IsBlackOutTriggered);
            readyCheckDictionary.Clear();
            OnEnterEvent = null;
        }

        public void EnterPotal()
        {
            OnEnterEvent();
            StartCoroutine(Fade());
        }

        public void ClosePortal()
        {
            GetComponent<Animator>().SetBool("IsEnter", true);
        }

        private IEnumerator Fade()
        {
            var looping = true;
            while (looping)
            {
                looping = false;
                foreach (var a in readyCheckDictionary)
                {
                    if (!a.Value)
                    {
                        looping = true;
                        break;
                    }
                }
                yield return halfSecondWait;
            }
            mCamera.Zoom(3, 2);
            yield return halfSecondWait;

            // 모두 준비가 끝나면 페이드되고 다음씬으로 넘어감
            AbyssSceneManager.InitAndShuffleAbyssScene();
            mCamera.BlackOut(0.9f, 1f);
            yield return waitCameraFade;
            SceneAsyncLoader.LoadScene(mEnterScene);
        }

        public void DoorOpen()
        {
            mAudio.clip = mDoorOpenAudio;
            mAudio.Play();
            mAudio.loop = false;
        }

        public void DoorClose()
        {
            mAudio.clip = mDoorCloseAudio;
            mAudio.Play();
            mAudio.loop = false;
        }

        public void DoorOpening()
        {
            mAudio.clip = mDoorOpeningAudio;
            mAudio.Play();
            mAudio.loop = true;
        }
    }

}