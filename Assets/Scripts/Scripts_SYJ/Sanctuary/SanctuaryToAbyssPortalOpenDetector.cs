﻿//#define USE_DISTANCE_DETECT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchManagement;

namespace RedBlackTree
{
    [RequireComponent(typeof(TouchBox))]
    public class SanctuaryToAbyssPortalOpenDetector : MonoBehaviour
    {
#if USE_DISTANCE_DETECT
        private float detectMinDistance = 4;
        private float enterDistance = 4;

        Transform trans;
#endif


        TouchBox touchBox;

        bool isAnimEnable = false;
        Animator mAnimator;

        SanctuaryToAbyssPortalHandler portalHandler;

        private void Awake()
        {
            mAnimator = GetComponentInParent<Animator>();
            touchBox = GetComponent<TouchBox>();
            touchBox.OnTouchStart += OnTouchStart;
            portalHandler = GetComponentInParent<SanctuaryToAbyssPortalHandler>();
#if USE_DISTANCE_DETECT
            trans = transform;
#endif

        }

        private void Update()
        {
            
#if USE_DISTANCE_DETECT
            if (Vector2.Distance(FairyTrans.position, trans.position) < detectMinDistance)
            {
                if (!isAnimEnable)
                    mAnimator.SetBool("IsStart", true);

                isAnimEnable = true;
            }
            else
            {
                if (isAnimEnable)
                    mAnimator.SetBool("IsStart", false);

                isAnimEnable = false;
            }
#endif
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (!isAnimEnable) return;
            portalHandler.EnterPotal();
            SanctuaryFairy.Main.gameObject.SetActive(false);
            DPad.SetAble(false);
        }

        void OnTouchStart(Touch touch, Vector2 worldPos)
        {
#if !USE_DISTANCE_DETECT
            isAnimEnable = !isAnimEnable;
            mAnimator.SetBool("IsStart", isAnimEnable);
#endif
        }
    }

}