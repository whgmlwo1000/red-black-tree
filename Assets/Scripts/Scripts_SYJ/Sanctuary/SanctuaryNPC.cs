﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchManagement;


namespace RedBlackTree
{
    [RequireComponent(typeof(AudioSource))]
    public class SanctuaryNPC : StateBase<SanctuaryNPC.ENpcState>
    {
        //[SerializeField]
        //string mName = "NPC_Telperion";
        [SerializeField]
        int mTagCount = 2;
        [SerializeField] AudioClip[] audioClips = null;

        string[] mContext;

        //[SerializeField]
        float mScriptOffsetY = 3;

        //[SerializeField]
        float mScirptPopdownRange = 22.0f;
        float mSciprtSqrtRange;

        TouchBox touchBox;

        Transform trans;
        Transform fairyTrans;

        protected ToolTip scriptReader;

        int mPrevScriptIdx = -1;

        AudioSource audioSource;

        public override void Start()
        {
            for (ECitizen i = ECitizen.None + 1; i < ECitizen.Count; i++)
            {
                if (name.Contains(i.ToString()))
                    if (!PlayerData.Current.IsRescued(i))
                    {
                        gameObject.SetActive(false);
                        break;
                    }
            }

            audioSource = GetComponent<AudioSource>();

            base.Start();
            touchBox = GetComponent<TouchBox>();
            touchBox.OnTouchEnd += OnTouchEnd;

            trans = transform;
            fairyTrans = SanctuaryFairy.Main.transform;
            mScirptPopdownRange *= (Screen.width / (float)Screen.height) * (9f / 16f);
            mSciprtSqrtRange = mScirptPopdownRange * mScirptPopdownRange;

            // 태그에 맞는 대사들 로드함
            string nameTag = name + "_Name";
            mContext = new string[mTagCount];
            for (int i = 0; i < mTagCount; i++)
            {
                string contextTag = name + "_"+ (i+1).ToString("D2");
                mContext[i] = DataManager.HasTag(contextTag) ? DataManager.GetScript(contextTag) : "";
            }
            name = DataManager.HasTag(nameTag) ? DataManager.GetScript(nameTag) : "";
        }

        protected virtual void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            if(scriptReader && scriptReader.isActiveAndEnabled)
            {
                scriptReader.PopDown();
                scriptReader = null;
            }
            else
            {
                if (touchBox.GetTouchIn(worldPosition))
                {
                    ScriptBase script = new ScriptBase(name, mContext[GetScriptIdx()]);
                    scriptReader = ToolTipPoll.PopUp(script, transform.position + new Vector3(0, mScriptOffsetY, 0));
                    PlayRandomSound();
                }
            }
        }

        public override void Update()
        {
            base.Update();
            if (mSciprtSqrtRange < (trans.position - fairyTrans.position).sqrMagnitude)
            {

                if (scriptReader != null)
                {
                    scriptReader.PopDown();
                }
            }
        }

        int GetScriptIdx()
        {
            var idx = Random.Range(0, mContext.Length);
            if(mPrevScriptIdx== idx)
            {
                idx = (idx + 1) % mContext.Length;
            }
            mPrevScriptIdx = idx;
            return idx;
        }

        void PlayRandomSound()
        {
            audioSource.PlayOneShot(audioClips[Random.Range(0,audioClips.Length)]);
        }

        public enum ENpcState
        {
            None = -1,
            Communicating,
            Selling,
            Count
        }
    }
}
