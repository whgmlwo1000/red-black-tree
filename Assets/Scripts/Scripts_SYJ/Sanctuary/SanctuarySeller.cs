﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{
    public class SanctuarySeller : MonoBehaviour
    {
        ProductStand productStand;
        void Start()
        {
            productStand = GetComponentInChildren<ProductStand>();
            Invoke("DelayStart", 0.1f);
        }

        void DelayStart()
        {
            productStand.DisplayProducts();

        }
    } 
}
