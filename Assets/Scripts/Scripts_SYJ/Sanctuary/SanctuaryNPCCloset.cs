﻿using UnityEngine;
using System.Collections;
using CameraManagement;

namespace RedBlackTree
{
    public class SanctuaryNPCCloset : SanctuaryNPC
    {
        [SerializeField] CameraTarget mMain = null;
        [SerializeField] CameraTarget mMyTarget = null;
        [SerializeField] Vector3 mFairyOffset = new Vector3(8.83f, -1.8f,0);
        [SerializeField] Animator mClosetAnimator = null;


        protected override void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            base.OnTouchEnd(touch, worldPosition);

            if (scriptReader)
            {
                CameraController.Main.Target = mMyTarget;
                mClosetAnimator.gameObject.SetActive(true);
                mClosetAnimator.SetBool("disable", false);
                DPad.SetAble(false);
            }
            else
            {
                DisableCloset();
            }
        }

        void DisableCloset()
        {
            CameraController.Main.Target = mMain;
            mClosetAnimator.SetBool("disable", true);
            DPad.SetAble(true);

            if (scriptReader)
            {
                scriptReader.PopDown();
                scriptReader = null;
            }
        }




        public override void Update()
        {
            base.Update();
            if (!scriptReader) return;

            // 정령이 카메라에 보이는곳으로 자동으로 움직임
            var fairy = SanctuaryFairy.Main;
            var offset = mMyTarget.transform.position + mFairyOffset - fairy.transform.position;
            if (offset.sqrMagnitude > 1)
            {
                offset = offset.normalized;
                fairy.Move(offset.x, offset.y);
            }
            else
            {
                fairy.Move(0, 0);
            }
        }
    }

}