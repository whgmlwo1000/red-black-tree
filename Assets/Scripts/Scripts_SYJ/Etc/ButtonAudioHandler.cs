﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{
    public enum EButtonState
    {
        Normal,
        Pushed
    }
    public class ButtonAudioHandler : AudioHandler<EButtonState>
    {
        public static ButtonAudioHandler self;
    } 
}
