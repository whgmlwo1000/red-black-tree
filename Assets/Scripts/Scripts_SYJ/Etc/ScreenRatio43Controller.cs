﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using CameraManagement;

namespace RedBlackTree
{
    public class ScreenRatio43Controller : MonoBehaviour
    {
        [SerializeField] CameraController mCameraController = null;
        [SerializeField] Transform UI_AbssyTransform = null;
        [SerializeField] RectTransform mTopUIRectTrans = null;
        [SerializeField] [Range(0f, 1f)] float mLatterBoxMin = 0;
        float mLatterBoxMax = 1f;

        void Start()
        {
            ChangeRatio();
        }

        void ChangeRatio()
        {
            var ratio = Screen.height / (float)Screen.width;
            var amount = (ratio - 0.562f) * 5.333f; // 0~1
            var verticalSize = amount * 4f + 12;
            mCameraController.VerticalSize = Mathf.Clamp(verticalSize,12,16);
            var size = mLatterBoxMin- mLatterBoxMax;
            var latterBoxSize = amount * size + mLatterBoxMax;
            mCameraController.LetterBox(Mathf.Clamp(latterBoxSize, mLatterBoxMin, mLatterBoxMax));

            var anchorMax = mTopUIRectTrans.anchorMax;
            var anchorMin = mTopUIRectTrans.anchorMin;
            anchorMax.y = anchorMin.y = amount * size * 0.5f + mLatterBoxMax;
            mTopUIRectTrans.anchorMax = anchorMax;
            mTopUIRectTrans.anchorMin = anchorMin;

            var canvasScalers = UI_AbssyTransform.GetComponentsInChildren<CanvasScaler>();
            foreach (var item in canvasScalers)
            {
                item.matchWidthOrHeight = Mathf.Clamp01(1-amount);
            }
        }
    }

}