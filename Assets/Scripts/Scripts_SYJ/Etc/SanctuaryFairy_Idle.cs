﻿using UnityEngine;

namespace RedBlackTree
{
    public class SanctuaryFairy_Idle : State<EStateType_Fairy>
    {
        SanctuaryFairy mSanctuaryFairy;
        public SanctuaryFairy_Idle(SanctuaryFairy fairy) : base(EStateType_Fairy.Wait)
        {
            mSanctuaryFairy = fairy;
        }

        public override void Start()
        {
            mSanctuaryFairy.Animator.SetInteger("state", (int)Type);
            if (mSanctuaryFairy.HorizontalDirection == EHorizontalDirection.Right)
                mSanctuaryFairy.Angle = 0.0f;
            else if (mSanctuaryFairy.HorizontalDirection == EHorizontalDirection.Left)
                mSanctuaryFairy.Angle = 180.0f;
        }

        public override void FixedUpdate()
        {
            Vector2 diff = mSanctuaryFairy.Velocity;

            if (Mathf.Abs(diff.x) > ValueConstants.MovementThreshold + mSanctuaryFairy.StopVelocity
                || Mathf.Abs(diff.y) > ValueConstants.MovementThreshold + mSanctuaryFairy.StopVelocity)
            {
                mSanctuaryFairy.State = EStateType_Fairy.Move;
            }
        }
    }
}
