﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{
    public class SanctuaryFairy_Move : State<EStateType_Fairy>
    {
        SanctuaryFairy mSanctuaryFairy;
        public SanctuaryFairy_Move(SanctuaryFairy fairy) : base(EStateType_Fairy.Move)
        {
            mSanctuaryFairy = fairy;
        }

        public override void Start()
        {
            mSanctuaryFairy.Animator.SetInteger("state", (int)Type);
            mSanctuaryFairy.SetDirection(mSanctuaryFairy.Velocity, false);
        }

        public override void FixedUpdate()
        {
            Vector2 diff = mSanctuaryFairy.Velocity;

            if (Mathf.Abs(diff.x) <= ValueConstants.MovementThreshold + mSanctuaryFairy.StopVelocity
                && Mathf.Abs(diff.y) <= ValueConstants.MovementThreshold + mSanctuaryFairy.StopVelocity)
            {
                mSanctuaryFairy.State = EStateType_Fairy.Wait;
                return;
            }

            mSanctuaryFairy.SetDirection(diff, false);
        }
    } 
}
