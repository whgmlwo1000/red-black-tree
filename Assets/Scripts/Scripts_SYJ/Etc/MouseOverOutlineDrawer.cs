﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchManagement;

namespace RedBlackTree
{

    [RequireComponent(typeof(TouchBox))]
    public class MouseOverOutlineDrawer : MonoBehaviour
    {
        [SerializeField] Material mOutlineMaterial = null;
        SpriteRenderer mSpriteRenderer;

        //MaterialPropertyBlock mpb = new MaterialPropertyBlock();

        void Start()
        {
            mSpriteRenderer = GetComponent<SpriteRenderer>();
            TouchBox touchBox = GetComponent<TouchBox>();
            mSpriteRenderer.material = mOutlineMaterial;

            touchBox.OnTouchStart += OnTouchEnter;
            touchBox.OnTouchEnd += OnTouchEnd;

            SetOutline(0);
            //mpb.SetInt("_isActive", 1);
        }

        void SetOutline(int toggle)
        {
            mSpriteRenderer.material.SetInt("_isActive", toggle);
            //mSpriteRenderer.SetPropertyBlock(toggle==1?mpb:null);
        }

        //마우스가 해당 객체의 콜라이더 안에 들어가 있을때(1번 호출)
        private void OnTouchEnter(Touch touch, Vector2 worldPosition)
        {
            SetOutline(1);
        }

        //마우스가 해당 객체의 콜라이더 밖으로 나갔을때(1번 호출)
        private void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            SetOutline(0);

        }
    }

}