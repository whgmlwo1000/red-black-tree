﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveController : MonoBehaviour
{
    public void SetDisable()
    {
        gameObject.SetActive(false);
    }
}
