﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

namespace RedBlackTree
{
    public class IntroVideoPlayer : MonoBehaviour
    {
        [SerializeField]
        private EScene mNextScene = EScene.None;

        private VideoPlayer vp;

        private void Awake()
        {
            vp = GetComponent<VideoPlayer>();
            vp.Play();
        }

        private void Update()
        {
            if (Input.anyKeyDown)
            {
                SceneManager.LoadScene(mNextScene.ToString());
            }
        }
    }

}