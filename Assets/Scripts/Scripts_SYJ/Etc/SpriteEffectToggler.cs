﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{

    [ExecuteInEditMode]
    public class SpriteEffectToggler : MonoBehaviour
    {
        public int Toggle {
            get { return mToggle; }
            set {
                mToggle = value;
                mSpriteRenderer.material.SetInt("_isActive", mToggle);
            }
        }
        [SerializeField]
        [Range(0, 1)]
        int mToggle = 0; //외곽선 활성/비활성화(0 : 비활성, 1 : 활성)

        SpriteRenderer mSpriteRenderer = null;

        void Start()
        {
            mSpriteRenderer = GetComponent<SpriteRenderer>();
        }
    }

}