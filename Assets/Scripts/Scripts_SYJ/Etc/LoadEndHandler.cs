﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{
    public class LoadEndHandler : MonoBehaviour
    {
        public void OnLoadEnd()
        {
            SceneAsyncLoader.ShowNextScene();
        }
    }

}