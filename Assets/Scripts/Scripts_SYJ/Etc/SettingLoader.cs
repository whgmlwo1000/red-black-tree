﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace RedBlackTree
{
    public class SettingLoader : MonoBehaviour
    {
        [SerializeField] AudioMixer mAudioMixer;
        private void Start()
        {
            var volumes = PlayerData.Current.Volumes;
            mAudioMixer.SetFloat("Master", UIPlayerMenuSetting.ConvertVolume(volumes.mEntire));
            mAudioMixer.SetFloat("Music", UIPlayerMenuSetting.ConvertVolume(volumes.mBGM));
            mAudioMixer.SetFloat("SFX", UIPlayerMenuSetting.ConvertVolume(volumes.mSFX));

            StateManager.Vibration.State = PlayerData.Current.Vibration;

        }
    } 
}
