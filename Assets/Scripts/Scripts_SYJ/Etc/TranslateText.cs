﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RedBlackTree
{
    public class TranslateText : MonoBehaviour
    {
        Text mText;
        [SerializeField] public string mTag = "";
        void Start()
        {
            mText = GetComponent<Text>();
            mText.text = DataManager.HasTag(mTag) ? DataManager.GetScript(mTag) : "";
        }

    } 
}
