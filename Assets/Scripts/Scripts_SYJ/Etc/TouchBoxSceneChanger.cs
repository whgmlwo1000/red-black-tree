﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchManagement;

namespace RedBlackTree
{

    public class TouchBoxSceneChanger : SimpleSceneChanger
    {
        TouchBox touchBox;
        private void Start()
        {
            touchBox = GetComponent<TouchBox>();
            touchBox.OnTouchEnd += OnTouchEnd;
        }

        void OnTouchEnd(Touch touch, Vector2 worldPos)
        {
            if(touchBox.GetTouchIn(worldPos))
                NextScene();
        }
    }

}