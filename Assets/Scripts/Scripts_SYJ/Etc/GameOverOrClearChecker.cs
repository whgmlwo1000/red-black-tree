﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{
    public class GameOverOrClearChecker : MonoBehaviour
    {
        private void Start()
        {
            var clear = StateManager.GameClear.State;
            var over = StateManager.GameOver.State;
            var trans = GetComponent<TranslateText>();
            Debug.Assert(!clear || !over);
            if (clear)
                trans.mTag = "GameClear";
            else if(over)
                trans.mTag = "GameOver";
            trans.enabled = true;
        }
    }

}