﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace RedBlackTree
{

    public class SimpleSceneChanger : MonoBehaviour
    {

        [SerializeField]
        private EScene mNextScene = EScene.None;

        public void NextScene()
        {
            SceneManager.LoadScene(mNextScene.ToString());
        }
    }

}