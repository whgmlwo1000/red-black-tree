﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{

    public class SanctuaryFairy : APhysicalActor<EStateType_Fairy>
    {

        [SerializeField]
        private PlayerCommand playerCommand = null;

        public float moveSpeed = 20;
        public float maxSpeed = 20;
        public float resistance = 1;
        public float StopVelocity { get { return 0.2f; } }
        

        Animator mAnimator;
        public Animator Animator {
            get { return mAnimator; }
        }

        public override EActorType ActorType {
            get { return EActorType.Fairy; }
        }

        private ESkin_Fairy mSkin;
        public ESkin_Fairy Skin {
            set {
                if (mSkin != value)
                {
                    mSkin = value;
                    MainAnimator.SetInteger("skin", (int)mSkin);
                }
            }
        }



        static SanctuaryFairy main;
        public static SanctuaryFairy Main {
            get { return main; }
        }



        public override void Awake()
        {
            main = this;
            mAnimator = GetComponent<Animator>();
            base.Awake();

            SetStates(new SanctuaryFairy_Idle(this),
                new SanctuaryFairy_Move(this));
        }

        public new void Start()
        {
            Skin = PlayerData.Current.FairySkin;
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();
            if (!StateManager.Pause.State && DPad.GetAble())
            {
                float inputX = Input.GetAxis("Horizontal");
                float inputY = Input.GetAxis("Vertical");
                //if (Mathf.Abs(inputX) < 1-0.01f) inputX = 0;
                //if (Mathf.Abs(inputY) < 1-0.01f) inputY = 0;
                inputX = inputX != 0 ? inputX : playerCommand.MoveHorizontal;
                inputY = inputY != 0 ? inputY : playerCommand.MoveVertical;
                if(inputY==0 && inputX==0)
                    AddResistance(resistance);
                else
                {
                    Move(inputX, inputY);

                    //if(inputY > 0)
                    //SetVerticalSpeed(-inputY * moveSpeed, EVerticalDirection.Down);
                    //else
                    //SetVerticalSpeed(inputY * moveSpeed, EVerticalDirection.Up);

                    //if (inputX > 0)
                    //    SetHorizontalSpeed(inputX * moveSpeed, EHorizontalDirection.Right);
                    //else
                    //    SetHorizontalSpeed(-inputX * moveSpeed, EHorizontalDirection.Left);
                }
                //AddVelocity(moveSpeed, new Vector2(inputX * moveSpeed, inputY * moveSpeed));
            }
        }

        public void Move(float x, float y)
        {
            Velocity = new Vector2(x * moveSpeed, y * moveSpeed);
        }

        public override void Update()
        {
            base.Update();
            
        }
    }
}