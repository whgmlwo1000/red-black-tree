﻿using UnityEngine;
using System.Collections.Generic;
using ObjectManagement;

namespace TriggerManagement
{
    public class Trigger2D : MonoBehaviour, IHandleable<string>
    {
        public delegate void OnTrigger(Collider2D collider);

        [SerializeField]
        private int mTriggerId = 0;
        public int TriggerId { get { return mTriggerId; } }

        public string Type { get { return mType; } }
        [SerializeField]
        private string mType = default;

        public Collider2D Trigger { get; private set; }
        public event OnTrigger OnTriggerEnter;
        public event OnTrigger OnTriggerExit;

        private Collider2D[] mColliders;
        private List<Collider2D> mColliderList;

        [SerializeField]
        [Tooltip("World Space에 고정될 것인지 설정하는 플래그 변수")]
        private bool mIsFixed = false;

        private bool mIsUpdated = false;

        public bool IsTriggered
        {
            get { return mColliderList.Count > 0; }
        }

        public Collider2D[] GetColliders()
        {
            if (mIsUpdated)
            {
                mIsUpdated = false;

                int index = 0;
                mColliderList.Find(delegate (Collider2D collision)
                {
                    mColliders[index++] = collision;
                    if(index < mColliders.Length)
                    {
                        return false;
                    }
                    return true;
                });
                if(index < mColliders.Length)
                {
                    mColliders[index] = null;
                }
            }
            return mColliders;
        }

        public Collider2D[] GetColliders(out int count)
        {
            count = mColliderList.Count;
            if (mIsUpdated)
            {
                mIsUpdated = false;

                int index = 0;
                mColliderList.Find(delegate (Collider2D collision)
                {
                    mColliders[index++] = collision;
                    if (index < mColliders.Length)
                    {
                        return false;
                    }
                    return true;
                });
                if (index < mColliders.Length)
                {
                    mColliders[index] = null;
                }
            }
            return mColliders;
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            mIsUpdated = true;
            mColliderList.Add(collision);
            OnTriggerEnter?.Invoke(collision);
        }
        public void OnTriggerExit2D(Collider2D collision)
        {
            mIsUpdated = true;
            mColliderList.Remove(collision);
            OnTriggerExit?.Invoke(collision);
        }

        public virtual void Awake()
        {
            Trigger = GetComponent<Collider2D>();
            mColliderList = new List<Collider2D>();
            mColliders = new Collider2D[50];
        }
        public virtual void Start()
        {
            if (mIsFixed)
            {
                transform.SetParent(null);
            }
        }

        public virtual void OnDisable()
        {
            mColliderList.ForEach(delegate (Collider2D collision)
            {
                OnTriggerExit?.Invoke(collision);
            });
            mColliderList.Clear();
            mIsUpdated = true;
        }
    }

}