﻿using UnityEngine;

namespace TriggerManagement
{
    public class SubTrigger2D : MonoBehaviour
    {
        private Trigger2D mParentTrigger;

        public void Awake()
        {
            mParentTrigger = GetComponentInParent<Trigger2D>();
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            mParentTrigger.OnTriggerEnter2D(collision);
        }

        public void OnTriggerExit2D(Collider2D collision)
        {
            mParentTrigger.OnTriggerExit2D(collision);
        }
    }
}