﻿using UnityEngine;
using ObjectManagement;

namespace TriggerManagement
{
    public class TriggerHandler2D : Handler<string, Trigger2D>
    {
        [SerializeField]
        private int mTriggerId = 0;
        public int TriggerId { get { return mTriggerId; } }

        public override bool CanHandle(Trigger2D childObject)
        {
            return TriggerId == childObject.TriggerId;
        }

        public Collider2D[] GetColliders(string triggerType)
        {
            return Get(triggerType).GetColliders();
        }
        public Collider2D[] GetColliders(string triggerType, out int count)
        {
            return Get(triggerType).GetColliders(out count);
        }

        public bool IsTriggered(string triggerType)
        {
            return Get(triggerType).IsTriggered;
        }

        public Collider2D GetTrigger(string triggerFlag)
        {
            return Get(triggerFlag).Trigger;
        }
        public void AddEnterEvent(string triggerFlag, Trigger2D.OnTrigger onTrigger)
        {
            Get(triggerFlag).OnTriggerEnter += onTrigger;
        }
        public void RemoveEnterEvent(string triggerFlag, Trigger2D.OnTrigger onTrigger)
        {
            Get(triggerFlag).OnTriggerEnter -= onTrigger;
        }
        public void AddExitEvent(string triggerFlag, Trigger2D.OnTrigger onTrigger)
        {
            Get(triggerFlag).OnTriggerExit += onTrigger;
        }
        public void RemoveExitEvent(string triggerFlag, Trigger2D.OnTrigger onTrigger)
        {
            Get(triggerFlag).OnTriggerExit -= onTrigger;
        }
    }

}