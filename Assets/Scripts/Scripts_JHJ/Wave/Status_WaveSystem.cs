﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New Status_WaveSystem", menuName = "Wave/Status", order = 1000)]
    public class Status_WaveSystem : ScriptableObject
    {
        [SerializeField]
        private BasicStatus_WaveSystem mBasicStatus = null;
        public BasicStatus_WaveSystem BasicStatus { get { return mBasicStatus; } }

        public bool MerchantTriggered { get; set; }
        public bool BossTriggered { get; set; }

        public int CountOfCitizens { get { return mBasicStatus.Citizens.Length; } }

        public int CountOfWaves { get { return mBasicStatus.Waves.Length; } }

        private int mWaveIndex;
        public int WaveIndex
        {
            get { return mWaveIndex; }
            set { mWaveIndex = Mathf.Clamp(value, 0, mBasicStatus.Waves.Length - 1); }
        }

        public float RemainTime { get; set; }

        public WaveInfo CurWave
        {
            get { return mBasicStatus.Waves[mWaveIndex]; }
        }

        public WaveInfo GetWave(int index)
        {
            return mBasicStatus.Waves[index];
        }

        public ECitizen GetCitizen(int index)
        {
            return mBasicStatus.Citizens[index];
        }

        public float GetCitizenPossibility(int index)
        {
            return mBasicStatus.CitizenPossibilities[index];
        }
    }
}