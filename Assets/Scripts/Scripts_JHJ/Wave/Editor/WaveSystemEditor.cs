﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(WaveSystem))]
    public class WaveSystemEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SerializedProperty statusProp = serializedObject.FindProperty("mStatus");

            statusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Status")
                , statusProp.objectReferenceValue, typeof(Status_WaveSystem), false);

            serializedObject.ApplyModifiedProperties();
        }
    }
}