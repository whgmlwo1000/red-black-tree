﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_WaveSystem))]
    public class BasicStatus_WaveSystemEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SerializedProperty citizenProps = serializedObject.FindProperty("Citizens");
            SerializedProperty citizenPossibilityProps = serializedObject.FindProperty("CitizenPossibilities");
            SerializedProperty waveProps = serializedObject.FindProperty("Waves");
            float totalTime = 0.0f;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Citizen Count"), GUILayout.MaxWidth(120));
            int citizenCount = EditorGUILayout.IntField(citizenProps.arraySize, GUILayout.MaxWidth(100));
            citizenCount = Mathf.Max(citizenCount, 0);
            if(citizenProps.arraySize != citizenCount || citizenPossibilityProps.arraySize != citizenCount)
            {
                citizenProps.arraySize = citizenCount;
                citizenPossibilityProps.arraySize = citizenCount;
                serializedObject.ApplyModifiedProperties();
            }
            EditorGUILayout.EndHorizontal();

            for(int indexOfCitizen = 0; indexOfCitizen < citizenCount; indexOfCitizen++)
            {
                SerializedProperty typeProp = citizenProps.GetArrayElementAtIndex(indexOfCitizen);
                SerializedProperty possibilityProp = citizenPossibilityProps.GetArrayElementAtIndex(indexOfCitizen);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent("Citizen Type"), GUILayout.MaxWidth(120));
                ECitizen type = (ECitizen)typeProp.intValue;
                type = (ECitizen)EditorGUILayout.EnumPopup(type, GUILayout.MaxWidth(100));
                typeProp.intValue = (int)type;

                EditorGUILayout.LabelField(new GUIContent("Possibility"), GUILayout.MaxWidth(100));
                possibilityProp.floatValue = Mathf.Clamp01(EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(50)));
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.Space();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Wave Count", "웨이브의 수"), GUILayout.MaxWidth(120));
            int waveCount = EditorGUILayout.IntField(waveProps.arraySize, GUILayout.MaxWidth(100));
            waveCount = Mathf.Max(waveCount, 1);
            if(waveProps.arraySize != waveCount)
            {
                waveProps.arraySize = waveCount;
                serializedObject.ApplyModifiedProperties();
            }
            EditorGUILayout.EndHorizontal();

            for(int waveIndex = 0; waveIndex < waveCount; waveIndex++)
            {
                EditorGUILayout.Space();
                SerializedProperty waveInfoProp = waveProps.GetArrayElementAtIndex(waveIndex);

                SerializedProperty waveTypeProp = waveInfoProp.FindPropertyRelative("WaveType");
                SerializedProperty timeProp = waveInfoProp.FindPropertyRelative("Time");

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Wave Type", waveIndex + 1), "웨이브 종류"), GUILayout.MaxWidth(120));
                EWave waveType = (EWave)waveTypeProp.intValue;
                waveType = (EWave)EditorGUILayout.EnumPopup(waveType, GUILayout.MaxWidth(100));
                waveTypeProp.intValue = (int)waveType;

                if(waveIndex > 0)
                {
                    if (GUILayout.Button(new GUIContent("Up"), GUILayout.MaxWidth(50)))
                    {
                        BasicStatus_WaveSystem waveSystem = target as BasicStatus_WaveSystem;

                        WaveInfo temp = waveSystem.Waves[waveIndex];
                        waveSystem.Waves[waveIndex] = waveSystem.Waves[waveIndex - 1];
                        waveSystem.Waves[waveIndex - 1] = temp;
                    }
                }
                else
                {
                    GUILayout.Label("", GUILayout.MaxWidth(50));
                }

                if (waveIndex < waveCount - 1)
                {
                    if (GUILayout.Button(new GUIContent("Down"), GUILayout.MaxWidth(50)))
                    {
                        BasicStatus_WaveSystem waveSystem = target as BasicStatus_WaveSystem;

                        WaveInfo temp = waveSystem.Waves[waveIndex];
                        waveSystem.Waves[waveIndex] = waveSystem.Waves[waveIndex + 1];
                        waveSystem.Waves[waveIndex + 1] = temp;
                    }
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent("Time", "웨이브 시간"), GUILayout.MaxWidth(120));
                timeProp.floatValue = EditorGUILayout.FloatField(timeProp.floatValue, GUILayout.MaxWidth(100));
                timeProp.floatValue = Mathf.Max(timeProp.floatValue, 0.01f);
                totalTime += timeProp.floatValue;
                EditorGUILayout.EndHorizontal();

                // Monster Wave
                if(waveType == EWave.Monster)
                {
                    SerializedProperty monsterWaveProps = waveInfoProp.FindPropertyRelative("MonsterWaves");
                    SerializedProperty boxWaveProps = waveInfoProp.FindPropertyRelative("BoxWaves");
                    SerializedProperty fruitTreeWaveProps = waveInfoProp.FindPropertyRelative("FruitTreeWaves");
                    SerializedProperty blessedFairyWaveProps = waveInfoProp.FindPropertyRelative("BlessedFairyWaves");
                    SerializedProperty corruptedFairyWaveProps = waveInfoProp.FindPropertyRelative("CorruptedFairyWaves");

                    EditorGUILayout.BeginHorizontal();

                    // Monster Types
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Monster Count", "생성할 수 있는 몬스터의 수"), GUILayout.MaxWidth(120));
                    int monsterCount = EditorGUILayout.IntField(monsterWaveProps.arraySize, GUILayout.MaxWidth(100));
                    monsterCount = Mathf.Max(monsterCount, 0);
                    if(monsterWaveProps.arraySize != monsterCount)
                    {
                        monsterWaveProps.arraySize = monsterCount;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    for(int monsterWaveIndex = 0; monsterWaveIndex < monsterCount; monsterWaveIndex++)
                    {
                        SerializedProperty monsterWaveProp = monsterWaveProps.GetArrayElementAtIndex(monsterWaveIndex);
                        SerializedProperty monsterTypeProp = monsterWaveProp.FindPropertyRelative("Type");
                        SerializedProperty possibilityProp = monsterWaveProp.FindPropertyRelative("Possibility");

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Monster Type", monsterWaveIndex + 1)), GUILayout.MaxWidth(120));
                        EMonster monsterType = (EMonster)monsterTypeProp.intValue;
                        monsterType = (EMonster)EditorGUILayout.EnumPopup(monsterType, GUILayout.MaxWidth(100));
                        monsterTypeProp.intValue = (int)monsterType;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Possibility", monsterWaveIndex + 1)), GUILayout.MaxWidth(120));
                        possibilityProp.floatValue = EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(100));
                        possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    // Box Types
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Box Count", "생성할 수 있는 박스의 수"), GUILayout.MaxWidth(120));
                    int boxCount = EditorGUILayout.IntField(boxWaveProps.arraySize, GUILayout.MaxWidth(100));
                    boxCount = Mathf.Max(boxCount, 0);
                    if (boxWaveProps.arraySize != boxCount)
                    {
                        boxWaveProps.arraySize = boxCount;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    for (int boxWaveIndex = 0; boxWaveIndex < boxCount; boxWaveIndex++)
                    {
                        SerializedProperty boxWaveProp = boxWaveProps.GetArrayElementAtIndex(boxWaveIndex);
                        SerializedProperty boxTypeProp = boxWaveProp.FindPropertyRelative("Type");
                        SerializedProperty possibilityProp = boxWaveProp.FindPropertyRelative("Possibility");

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Box Type", boxWaveIndex + 1)), GUILayout.MaxWidth(120));
                        EBox boxType = (EBox)boxTypeProp.intValue;
                        boxType = (EBox)EditorGUILayout.EnumPopup(boxType, GUILayout.MaxWidth(100));
                        boxTypeProp.intValue = (int)boxType;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Possibility", boxWaveIndex + 1)), GUILayout.MaxWidth(120));
                        possibilityProp.floatValue = EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(100));
                        possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    // FruitTree Types
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Fruit Tree Count", "생성할 수 있는 열매 나무의 수"), GUILayout.MaxWidth(120));
                    int fruitTreeCount = EditorGUILayout.IntField(fruitTreeWaveProps.arraySize, GUILayout.MaxWidth(100));
                    fruitTreeCount = Mathf.Max(fruitTreeCount, 0);
                    if (fruitTreeWaveProps.arraySize != fruitTreeCount)
                    {
                        fruitTreeWaveProps.arraySize = fruitTreeCount;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    for (int fruitTreeWaveIndex = 0; fruitTreeWaveIndex < fruitTreeCount; fruitTreeWaveIndex++)
                    {
                        SerializedProperty fruitTreeWaveProp = fruitTreeWaveProps.GetArrayElementAtIndex(fruitTreeWaveIndex);
                        SerializedProperty fruitTypeProp = fruitTreeWaveProp.FindPropertyRelative("Type");
                        SerializedProperty possibilityProp = fruitTreeWaveProp.FindPropertyRelative("Possibility");

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Fruit Tree Type", fruitTreeWaveIndex + 1)), GUILayout.MaxWidth(120));
                        EFruitTree fruitTreeType = (EFruitTree)fruitTypeProp.intValue;
                        fruitTreeType = (EFruitTree)EditorGUILayout.EnumPopup(fruitTreeType, GUILayout.MaxWidth(100));
                        fruitTypeProp.intValue = (int)fruitTreeType;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Possibility", fruitTreeWaveIndex + 1)), GUILayout.MaxWidth(120));
                        possibilityProp.floatValue = EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(100));
                        possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    // BlessedFairy Types
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Blessed Fairy Count", "생성할 수 있는 축복받은 요정의 수"), GUILayout.MaxWidth(120));
                    int blessedFairyCount = Mathf.Max(EditorGUILayout.IntField(blessedFairyWaveProps.arraySize, GUILayout.MaxWidth(100)), 0);
                    if (blessedFairyWaveProps.arraySize != blessedFairyCount)
                    {
                        blessedFairyWaveProps.arraySize = blessedFairyCount;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    for (int blessedFairyWaveIndex = 0; blessedFairyWaveIndex < blessedFairyCount; blessedFairyWaveIndex++)
                    {
                        SerializedProperty blessedFairyWaveProp = blessedFairyWaveProps.GetArrayElementAtIndex(blessedFairyWaveIndex);
                        SerializedProperty blessedFairyTypeProp = blessedFairyWaveProp.FindPropertyRelative("Type");
                        SerializedProperty possibilityProp = blessedFairyWaveProp.FindPropertyRelative("Possibility");

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Blessed Fairy Type", blessedFairyWaveIndex + 1)), GUILayout.MaxWidth(120));
                        EBlessedFairy blessedFairyType = (EBlessedFairy)blessedFairyTypeProp.intValue;
                        blessedFairyType = (EBlessedFairy)EditorGUILayout.EnumPopup(blessedFairyType, GUILayout.MaxWidth(100));
                        blessedFairyTypeProp.intValue = (int)blessedFairyType;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Possibility", blessedFairyWaveIndex + 1)), GUILayout.MaxWidth(120));
                        possibilityProp.floatValue = EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(100));
                        possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    // Corrupted Fairy Types
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Corrupted Fairy Count", "생성할 수 있는 방황하는 요정의 수"), GUILayout.MaxWidth(120));
                    int corruptedFairyCount = Mathf.Max(EditorGUILayout.IntField(corruptedFairyWaveProps.arraySize, GUILayout.MaxWidth(100)), 0);
                    if (corruptedFairyWaveProps.arraySize != corruptedFairyCount)
                    {
                        corruptedFairyWaveProps.arraySize = corruptedFairyCount;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    for (int corruptedFairyWaveIndex = 0; corruptedFairyWaveIndex < corruptedFairyCount; corruptedFairyWaveIndex++)
                    {
                        SerializedProperty corruptedWaveProp = corruptedFairyWaveProps.GetArrayElementAtIndex(corruptedFairyWaveIndex);
                        SerializedProperty corruptedFairyTypeProp = corruptedWaveProp.FindPropertyRelative("Type");
                        SerializedProperty possibilityProp = corruptedWaveProp.FindPropertyRelative("Possibility");

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Corrupted Fairy Type", corruptedFairyWaveIndex + 1)), GUILayout.MaxWidth(120));
                        ECorruptedFairy corruptedFairyType = (ECorruptedFairy)corruptedFairyTypeProp.intValue;
                        corruptedFairyType = (ECorruptedFairy)EditorGUILayout.EnumPopup(corruptedFairyType, GUILayout.MaxWidth(100));
                        corruptedFairyTypeProp.intValue = (int)corruptedFairyType;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Possibility", corruptedFairyWaveIndex + 1)), GUILayout.MaxWidth(120));
                        possibilityProp.floatValue = EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(100));
                        possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    EditorGUILayout.EndHorizontal();
                }
                else if(waveType == EWave.Merchant)
                {
                    SerializedProperty merchantWaveProp = waveInfoProp.FindPropertyRelative("MerchantWave");
                    SerializedProperty merchantTypeProp = merchantWaveProp.FindPropertyRelative("Type");

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Merchant Type"), GUILayout.MaxWidth(120));
                    EMerchant merchantType = (EMerchant)merchantTypeProp.intValue;
                    merchantType = (EMerchant)EditorGUILayout.EnumPopup(merchantType, GUILayout.MaxWidth(100));
                    merchantTypeProp.intValue = (int)merchantType;
                    EditorGUILayout.EndHorizontal();
                }
                else if(waveType == EWave.Rest)
                {
                    SerializedProperty boxWaveProps = waveInfoProp.FindPropertyRelative("BoxWaves");
                    SerializedProperty fruitTreeWaveProps = waveInfoProp.FindPropertyRelative("FruitTreeWaves");
                    SerializedProperty blessedFairyWaveProps = waveInfoProp.FindPropertyRelative("BlessedFairyWaves");
                    SerializedProperty corruptedFairyWaveProps = waveInfoProp.FindPropertyRelative("CorruptedFairyWaves");

                    // Box Types
                    EditorGUILayout.BeginHorizontal();
                    // Box Types
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Box Count", "생성할 수 있는 박스의 수"), GUILayout.MaxWidth(120));
                    int boxCount = EditorGUILayout.IntField(boxWaveProps.arraySize, GUILayout.MaxWidth(100));
                    boxCount = Mathf.Max(boxCount, 0);
                    if (boxWaveProps.arraySize != boxCount)
                    {
                        boxWaveProps.arraySize = boxCount;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    for (int boxWaveIndex = 0; boxWaveIndex < boxCount; boxWaveIndex++)
                    {
                        SerializedProperty boxWaveProp = boxWaveProps.GetArrayElementAtIndex(boxWaveIndex);
                        SerializedProperty boxTypeProp = boxWaveProp.FindPropertyRelative("Type");
                        SerializedProperty possibilityProp = boxWaveProp.FindPropertyRelative("Possibility");

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Box Type", boxWaveIndex + 1)), GUILayout.MaxWidth(120));
                        EBox boxType = (EBox)boxTypeProp.intValue;
                        boxType = (EBox)EditorGUILayout.EnumPopup(boxType, GUILayout.MaxWidth(100));
                        boxTypeProp.intValue = (int)boxType;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Possibility", boxWaveIndex + 1)), GUILayout.MaxWidth(120));
                        possibilityProp.floatValue = EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(100));
                        possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    // FruitTree Types
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Fruit Tree Count", "생성할 수 있는 열매 나무의 수"), GUILayout.MaxWidth(120));
                    int fruitTreeCount = EditorGUILayout.IntField(fruitTreeWaveProps.arraySize, GUILayout.MaxWidth(100));
                    fruitTreeCount = Mathf.Max(fruitTreeCount, 0);
                    if (fruitTreeWaveProps.arraySize != fruitTreeCount)
                    {
                        fruitTreeWaveProps.arraySize = fruitTreeCount;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    for (int fruitTreeWaveIndex = 0; fruitTreeWaveIndex < fruitTreeCount; fruitTreeWaveIndex++)
                    {
                        SerializedProperty fruitTreeWaveProp = fruitTreeWaveProps.GetArrayElementAtIndex(fruitTreeWaveIndex);
                        SerializedProperty fruitTypeProp = fruitTreeWaveProp.FindPropertyRelative("Type");
                        SerializedProperty possibilityProp = fruitTreeWaveProp.FindPropertyRelative("Possibility");

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Fruit Tree Type", fruitTreeWaveIndex + 1)), GUILayout.MaxWidth(120));
                        EFruitTree fruitTreeType = (EFruitTree)fruitTypeProp.intValue;
                        fruitTreeType = (EFruitTree)EditorGUILayout.EnumPopup(fruitTreeType, GUILayout.MaxWidth(100));
                        fruitTypeProp.intValue = (int)fruitTreeType;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Possibility", fruitTreeWaveIndex + 1)), GUILayout.MaxWidth(120));
                        possibilityProp.floatValue = EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(100));
                        possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    // BlessedFairy Types
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Blessed Fairy Count", "생성할 수 있는 축벅받은 요정의 수"), GUILayout.MaxWidth(120));
                    int blessedFairyCount = Mathf.Max(EditorGUILayout.IntField(blessedFairyWaveProps.arraySize, GUILayout.MaxWidth(100)), 0);
                    if (blessedFairyWaveProps.arraySize != blessedFairyCount)
                    {
                        blessedFairyWaveProps.arraySize = blessedFairyCount;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    for (int blessedFairyWaveIndex = 0; blessedFairyWaveIndex < blessedFairyCount; blessedFairyWaveIndex++)
                    {
                        SerializedProperty blessedFairyWaveProp = blessedFairyWaveProps.GetArrayElementAtIndex(blessedFairyWaveIndex);
                        SerializedProperty blessedFairyTypeProp = blessedFairyWaveProp.FindPropertyRelative("Type");
                        SerializedProperty possibilityProp = blessedFairyWaveProp.FindPropertyRelative("Possibility");

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Blessed Fairy Type", blessedFairyWaveIndex + 1)), GUILayout.MaxWidth(120));
                        EBlessedFairy blessedFairyType = (EBlessedFairy)blessedFairyTypeProp.intValue;
                        blessedFairyType = (EBlessedFairy)EditorGUILayout.EnumPopup(blessedFairyType, GUILayout.MaxWidth(100));
                        blessedFairyTypeProp.intValue = (int)blessedFairyType;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Possibility", blessedFairyWaveIndex + 1)), GUILayout.MaxWidth(120));
                        possibilityProp.floatValue = EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(100));
                        possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    // Corrupted Fairy Types
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Corrupted Fairy Count", "생성할 수 있는 방황하는 요정의 수"), GUILayout.MaxWidth(120));
                    int corruptedFairyCount = Mathf.Max(EditorGUILayout.IntField(corruptedFairyWaveProps.arraySize, GUILayout.MaxWidth(100)), 0);
                    if (corruptedFairyWaveProps.arraySize != corruptedFairyCount)
                    {
                        corruptedFairyWaveProps.arraySize = corruptedFairyCount;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    for (int corruptedFairyWaveIndex = 0; corruptedFairyWaveIndex < corruptedFairyCount; corruptedFairyWaveIndex++)
                    {
                        SerializedProperty corruptedWaveProp = corruptedFairyWaveProps.GetArrayElementAtIndex(corruptedFairyWaveIndex);
                        SerializedProperty corruptedFairyTypeProp = corruptedWaveProp.FindPropertyRelative("Type");
                        SerializedProperty possibilityProp = corruptedWaveProp.FindPropertyRelative("Possibility");

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Corrupted Fairy Type", corruptedFairyWaveIndex + 1)), GUILayout.MaxWidth(120));
                        ECorruptedFairy corruptedFairyType = (ECorruptedFairy)corruptedFairyTypeProp.intValue;
                        corruptedFairyType = (ECorruptedFairy)EditorGUILayout.EnumPopup(corruptedFairyType, GUILayout.MaxWidth(100));
                        corruptedFairyTypeProp.intValue = (int)corruptedFairyType;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Possibility", corruptedFairyWaveIndex + 1)), GUILayout.MaxWidth(120));
                        possibilityProp.floatValue = EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(100));
                        possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    EditorGUILayout.EndHorizontal();
                }
                else if(waveType == EWave.Boss)
                {
                    SerializedProperty monsterWaveProps = waveInfoProp.FindPropertyRelative("MonsterWaves");
                    SerializedProperty boxWaveProps = waveInfoProp.FindPropertyRelative("BoxWaves");
                    SerializedProperty fruitTreeWaveProps = waveInfoProp.FindPropertyRelative("FruitTreeWaves");
                    SerializedProperty blessedFairyWaveProps = waveInfoProp.FindPropertyRelative("BlessedFairyWaves");
                    SerializedProperty corruptedFairyWaveProps = waveInfoProp.FindPropertyRelative("CorruptedFairyWaves");

                    EditorGUILayout.BeginHorizontal();

                    // Monster Types
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Monster Count", "생성할 수 있는 몬스터의 수"), GUILayout.MaxWidth(120));
                    int monsterCount = EditorGUILayout.IntField(monsterWaveProps.arraySize, GUILayout.MaxWidth(100));
                    monsterCount = Mathf.Max(monsterCount, 0);
                    if (monsterWaveProps.arraySize != monsterCount)
                    {
                        monsterWaveProps.arraySize = monsterCount;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    for (int monsterWaveIndex = 0; monsterWaveIndex < monsterCount; monsterWaveIndex++)
                    {
                        SerializedProperty monsterWaveProp = monsterWaveProps.GetArrayElementAtIndex(monsterWaveIndex);
                        SerializedProperty monsterTypeProp = monsterWaveProp.FindPropertyRelative("Type");
                        SerializedProperty possibilityProp = monsterWaveProp.FindPropertyRelative("Possibility");

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Monster Type", monsterWaveIndex + 1)), GUILayout.MaxWidth(120));
                        EMonster monsterType = (EMonster)monsterTypeProp.intValue;
                        monsterType = (EMonster)EditorGUILayout.EnumPopup(monsterType, GUILayout.MaxWidth(100));
                        monsterTypeProp.intValue = (int)monsterType;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Possibility", monsterWaveIndex + 1)), GUILayout.MaxWidth(120));
                        possibilityProp.floatValue = EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(100));
                        possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    // Box Types
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Box Count", "생성할 수 있는 박스의 수"), GUILayout.MaxWidth(120));
                    int boxCount = EditorGUILayout.IntField(boxWaveProps.arraySize, GUILayout.MaxWidth(100));
                    boxCount = Mathf.Max(boxCount, 0);
                    if (boxWaveProps.arraySize != boxCount)
                    {
                        boxWaveProps.arraySize = boxCount;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    for (int boxWaveIndex = 0; boxWaveIndex < boxCount; boxWaveIndex++)
                    {
                        SerializedProperty boxWaveProp = boxWaveProps.GetArrayElementAtIndex(boxWaveIndex);
                        SerializedProperty boxTypeProp = boxWaveProp.FindPropertyRelative("Type");
                        SerializedProperty possibilityProp = boxWaveProp.FindPropertyRelative("Possibility");

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Box Type", boxWaveIndex + 1)), GUILayout.MaxWidth(120));
                        EBox boxType = (EBox)boxTypeProp.intValue;
                        boxType = (EBox)EditorGUILayout.EnumPopup(boxType, GUILayout.MaxWidth(100));
                        boxTypeProp.intValue = (int)boxType;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Possibility", boxWaveIndex + 1)), GUILayout.MaxWidth(120));
                        possibilityProp.floatValue = EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(100));
                        possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    // FruitTree Types
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Fruit Tree Count", "생성할 수 있는 열매 나무의 수"), GUILayout.MaxWidth(120));
                    int fruitTreeCount = EditorGUILayout.IntField(fruitTreeWaveProps.arraySize, GUILayout.MaxWidth(100));
                    fruitTreeCount = Mathf.Max(fruitTreeCount, 0);
                    if (fruitTreeWaveProps.arraySize != fruitTreeCount)
                    {
                        fruitTreeWaveProps.arraySize = fruitTreeCount;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    for (int fruitTreeWaveIndex = 0; fruitTreeWaveIndex < fruitTreeCount; fruitTreeWaveIndex++)
                    {
                        SerializedProperty fruitTreeWaveProp = fruitTreeWaveProps.GetArrayElementAtIndex(fruitTreeWaveIndex);
                        SerializedProperty fruitTypeProp = fruitTreeWaveProp.FindPropertyRelative("Type");
                        SerializedProperty possibilityProp = fruitTreeWaveProp.FindPropertyRelative("Possibility");

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Fruit Tree Type", fruitTreeWaveIndex + 1)), GUILayout.MaxWidth(120));
                        EFruitTree fruitTreeType = (EFruitTree)fruitTypeProp.intValue;
                        fruitTreeType = (EFruitTree)EditorGUILayout.EnumPopup(fruitTreeType, GUILayout.MaxWidth(100));
                        fruitTypeProp.intValue = (int)fruitTreeType;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Possibility", fruitTreeWaveIndex + 1)), GUILayout.MaxWidth(120));
                        possibilityProp.floatValue = EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(100));
                        possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    // BlessedFairy Types
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Blessed Fairy Count", "생성할 수 있는 축복받은 요정의 수"), GUILayout.MaxWidth(120));
                    int blessedFairyCount = Mathf.Max(EditorGUILayout.IntField(blessedFairyWaveProps.arraySize, GUILayout.MaxWidth(100)), 0);
                    if (blessedFairyWaveProps.arraySize != blessedFairyCount)
                    {
                        blessedFairyWaveProps.arraySize = blessedFairyCount;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    for (int blessedFairyWaveIndex = 0; blessedFairyWaveIndex < blessedFairyCount; blessedFairyWaveIndex++)
                    {
                        SerializedProperty blessedFairyWaveProp = blessedFairyWaveProps.GetArrayElementAtIndex(blessedFairyWaveIndex);
                        SerializedProperty blessedFairyTypeProp = blessedFairyWaveProp.FindPropertyRelative("Type");
                        SerializedProperty possibilityProp = blessedFairyWaveProp.FindPropertyRelative("Possibility");

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Blessed Fairy Type", blessedFairyWaveIndex + 1)), GUILayout.MaxWidth(120));
                        EBlessedFairy blessedFairyType = (EBlessedFairy)blessedFairyTypeProp.intValue;
                        blessedFairyType = (EBlessedFairy)EditorGUILayout.EnumPopup(blessedFairyType, GUILayout.MaxWidth(100));
                        blessedFairyTypeProp.intValue = (int)blessedFairyType;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Possibility", blessedFairyWaveIndex + 1)), GUILayout.MaxWidth(120));
                        possibilityProp.floatValue = EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(100));
                        possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    // Corrupted Fairy Types
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Corrupted Fairy Count", "생성할 수 있는 방황하는 요정의 수"), GUILayout.MaxWidth(120));
                    int corruptedFairyCount = Mathf.Max(EditorGUILayout.IntField(corruptedFairyWaveProps.arraySize, GUILayout.MaxWidth(100)), 0);
                    if (corruptedFairyWaveProps.arraySize != corruptedFairyCount)
                    {
                        corruptedFairyWaveProps.arraySize = corruptedFairyCount;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.Space();
                    for (int corruptedFairyWaveIndex = 0; corruptedFairyWaveIndex < corruptedFairyCount; corruptedFairyWaveIndex++)
                    {
                        SerializedProperty corruptedWaveProp = corruptedFairyWaveProps.GetArrayElementAtIndex(corruptedFairyWaveIndex);
                        SerializedProperty corruptedFairyTypeProp = corruptedWaveProp.FindPropertyRelative("Type");
                        SerializedProperty possibilityProp = corruptedWaveProp.FindPropertyRelative("Possibility");

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Corrupted Fairy Type", corruptedFairyWaveIndex + 1)), GUILayout.MaxWidth(120));
                        ECorruptedFairy corruptedFairyType = (ECorruptedFairy)corruptedFairyTypeProp.intValue;
                        corruptedFairyType = (ECorruptedFairy)EditorGUILayout.EnumPopup(corruptedFairyType, GUILayout.MaxWidth(100));
                        corruptedFairyTypeProp.intValue = (int)corruptedFairyType;
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. Possibility", corruptedFairyWaveIndex + 1)), GUILayout.MaxWidth(120));
                        possibilityProp.floatValue = EditorGUILayout.FloatField(possibilityProp.floatValue, GUILayout.MaxWidth(100));
                        possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();

                    EditorGUILayout.EndHorizontal();
                }
            }

            EditorGUILayout.Space();
            EditorGUILayout.LabelField(new GUIContent(string.Format("Total Time : {0} s", totalTime)), EditorStyles.boldLabel);

            serializedObject.ApplyModifiedProperties();
        }
    }
}