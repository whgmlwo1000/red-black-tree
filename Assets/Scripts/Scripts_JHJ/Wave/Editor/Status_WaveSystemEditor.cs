﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Status_WaveSystem))]
    public class Status_WaveSystemEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");

            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Basic Status"), basicStatusProp.objectReferenceValue, typeof(BasicStatus_WaveSystem), false);

            serializedObject.ApplyModifiedProperties();
            
            Status_WaveSystem status = target as Status_WaveSystem;
            if (status.BasicStatus != null)
            {
                EditorGUILayout.Space();

                EditorGUILayout.LabelField(new GUIContent(string.Format("Wave Count : {0}", status.CountOfWaves)));

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent("Wave Index"), GUILayout.MaxWidth(150));
                status.WaveIndex = EditorGUILayout.IntField(status.WaveIndex);
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent("Merchant Triggered", "상인 등장 조건 성립 여부 플래그"), GUILayout.MaxWidth(150));
                status.MerchantTriggered = EditorGUILayout.Toggle(status.MerchantTriggered);
                EditorGUILayout.EndHorizontal();
            }
        }
    }
}