﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_Wave", menuName = "Wave/BasicStatus", order = 1000)]
    public class BasicStatus_WaveSystem : ScriptableObject
    {
        public ECitizen[] Citizens = new ECitizen[0];
        public float[] CitizenPossibilities = new float[0];

        public WaveInfo[] Waves = new WaveInfo[0];
    }

    [System.Serializable]
    public class WaveInfo
    {
        public EWave WaveType = EWave.None;

        public float Time = 1.0f;

        public MerchantWave MerchantWave = new MerchantWave();

        public MonsterWave[] MonsterWaves = new MonsterWave[0];

        public BoxWave[] BoxWaves = new BoxWave[0];
        public FruitTreeWave[] FruitTreeWaves = new FruitTreeWave[0];

        public BlessedFairyWave[] BlessedFairyWaves = new BlessedFairyWave[0];

        public CorruptedFairyWave[] CorruptedFairyWaves = new CorruptedFairyWave[0];
    }

    public class Wave<E> 
    {
        public E Type = default;
        public float Possibility = 1.0f;
    }
    [System.Serializable]
    public class MonsterWave : Wave<EMonster>
    { }
    [System.Serializable]
    public class BoxWave : Wave<EBox>
    { }
    [System.Serializable]
    public class FruitTreeWave : Wave<EFruitTree>
    { }
    [System.Serializable]
    public class MerchantWave : Wave<EMerchant>
    { }

    [System.Serializable]
    public class BlessedFairyWave : Wave<EBlessedFairy>
    { }

    [System.Serializable]
    public class CorruptedFairyWave : Wave<ECorruptedFairy>
    { }
}