﻿using UnityEngine;
using System.Collections.Generic;

namespace RedBlackTree
{
    public enum EWave
    {
        None = -1,

        Monster,
        Merchant,
        Rest,
        Boss, 

        Count
    }

    public class WaveSystem : StateBase<EWave>
    {
        [SerializeField]
        private Status_WaveSystem mStatus = null;
        public Status_WaveSystem Status { get { return mStatus; } }

        private Dictionary<int, SortedList<float, ECitizen>> mCitizenWaveDict;
        public int CountOfCitizens
        {
            get
            {
                if(mCitizenWaveDict.ContainsKey(Status.WaveIndex))
                {
                    return mCitizenWaveDict[Status.WaveIndex].Count;
                }
                return 0;
            }
        }

        public override void Awake()
        {
            base.Awake();

            Status.MerchantTriggered = false;

            SetStates(new WaveSystem_Rest(this)
                , new WaveSystem_Monster(this)
                , new WaveSystem_Merchant(this)
                , new WaveSystem_Boss(this));
        }

        public override void Start()
        {
            base.Start();
            mStatus.WaveIndex = 0;
            State = mStatus.CurWave.WaveType;

            mCitizenWaveDict = new Dictionary<int, SortedList<float, ECitizen>>();

            for(int indexOfCitizen = 0; indexOfCitizen < Status.CountOfCitizens; indexOfCitizen++)
            {
                float selection = Random.Range(0.0f, 1.0f);
                if(selection < Status.GetCitizenPossibility(indexOfCitizen))
                {
                    int tryCount;
                    int waveIndex = (int)Random.Range(0.0f, Status.CountOfWaves);
                    for (tryCount = 0; tryCount < Status.CountOfWaves; tryCount++)
                    {
                        if(Status.GetWave(waveIndex).WaveType != EWave.Merchant
                            && Status.GetWave(waveIndex).WaveType != EWave.Boss)
                        {
                            break;
                        }
                    }

                    // 시도 횟수가 웨이브 수 이하라면, 
                    if (tryCount < Status.CountOfWaves)
                    {
                        if(!mCitizenWaveDict.ContainsKey(waveIndex))
                        {
                            mCitizenWaveDict.Add(waveIndex, new SortedList<float, ECitizen>());
                        }

                        mCitizenWaveDict[waveIndex].Add(Random.Range(0.0f, Status.GetWave(waveIndex).Time), Status.GetCitizen(indexOfCitizen));
                    }
                }
            }
        }

        public void SetNextWave()
        {
            mStatus.WaveIndex++;
            State = mStatus.CurWave.WaveType;
        }

        public float GetCitizenTime(int index)
        {
            return mCitizenWaveDict[Status.WaveIndex].Keys[index];
        }

        public ECitizen GetCitizenType(int index)
        {
            return mCitizenWaveDict[Status.WaveIndex].Values[index];
        }

        public void RemoveCitizen(int index)
        {
            mCitizenWaveDict[Status.WaveIndex].RemoveAt(index);
        }
    }
}