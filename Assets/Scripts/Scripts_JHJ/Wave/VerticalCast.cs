﻿using UnityEngine;
using System.Collections;

namespace RedBlackTree
{
    public class VerticalCast : MonoBehaviour
    {
        private static VerticalCast mMain;

        public static Vector2 Position { get { return mMain.transform.position; } }

        public static Vector2 Cast(EVerticalDirection direction)
        {
            Vector2 position = mMain.transform.position;
            
            if(direction == EVerticalDirection.Up)
            {
                return Physics2D.Raycast(position, Vector2.up, 30.0f, (int)ELayerFlags.Terrain).point;
            }
            else if(direction == EVerticalDirection.Down)
            {
                return Physics2D.Raycast(position, Vector2.down, 30.0f, (int)ELayerFlags.Terrain).point;
            }

            return position;
        }

        public void Awake()
        {
            if (mMain == null)
            {
                mMain = this;
            }
        }

        public void OnDestroy()
        {
            if (mMain == this)
            {
                mMain = null;
            }
        }
    }
}