﻿using UnityEngine;
using DynamicLevel;

namespace RedBlackTree
{
    public class WaveSystem_Merchant : State<EWave>
    {
        private WaveSystem mWaveSystem;
        private bool mIsMerchant;
        private bool mIsSetState;
        private Merchant mMerchant;

        public WaveSystem_Merchant(WaveSystem waveSystem) : base(EWave.Merchant)
        {
            mWaveSystem = waveSystem;
        }

        public override void Start()
        {
            DynamicLevelGenerator.State = 1;
            mWaveSystem.Status.RemainTime = mWaveSystem.Status.CurWave.Time;
            mWaveSystem.Status.MerchantTriggered = false;
            mIsMerchant = false;
            mIsSetState = true;
        }

        public override void Update()
        {
            if (!mIsMerchant)
            {
                if (mWaveSystem.Status.MerchantTriggered)
                {
                    mIsMerchant = true;
                    mMerchant = MerchantPool.Get(mWaveSystem.Status.CurWave.MerchantWave.Type);
                    mMerchant.Spawn();
                    mMerchant.RemainTimeHandler.Popup(true);
                    mMerchant.RemainTimeHandler.Amount = 1.0f;
                }
            }
            else if (mWaveSystem.Status.RemainTime > 0.0f
                && mMerchant.State != EStateType_Merchant.Exit)
            {
                if (mMerchant.State == EStateType_Merchant.Sell)
                {
                    mMerchant.RemainTimeHandler.Amount = mWaveSystem.Status.RemainTime / mWaveSystem.Status.CurWave.Time;
                    mWaveSystem.Status.RemainTime -= Time.deltaTime;
                }
            }
            else if (mIsSetState)
            {
                mMerchant.RemainTimeHandler.Popup(false);
                mIsSetState = false;
                DynamicLevelGenerator.State = 0;
            }
            else if (!mWaveSystem.Status.MerchantTriggered)
            {
                mMerchant.Leave();
                mWaveSystem.SetNextWave();
            }
        }
    }
}