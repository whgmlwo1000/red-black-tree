﻿    using UnityEngine;
using System.Collections.Generic;
using DynamicLevel;

namespace RedBlackTree
{
    public class WaveSystem_Monster : State<EWave>
    {
        private WaveSystem mWaveSystem;

        private SortedList<float, Wave<EMonster>> mMonsterWaveList;
        private SortedList<float, Wave<EBox>> mBoxWaveList;
        private SortedList<float, Wave<EFruitTree>> mFruitTreeList;
        private SortedList<float, Wave<EBlessedFairy>> mBlessedFairyList;
        private SortedList<float, Wave<ECorruptedFairy>> mCorruptedFairyList;

        public WaveSystem_Monster(WaveSystem waveSystem) : base(EWave.Monster)
        {
            mWaveSystem = waveSystem;

            mMonsterWaveList = new SortedList<float, Wave<EMonster>>();
            mBoxWaveList = new SortedList<float, Wave<EBox>>();
            mFruitTreeList = new SortedList<float, Wave<EFruitTree>>();
            mBlessedFairyList = new SortedList<float, Wave<EBlessedFairy>>();
            mCorruptedFairyList = new SortedList<float, Wave<ECorruptedFairy>>();
        }

        public override void Start()
        {
            DynamicLevelGenerator.State = 0;

            mWaveSystem.Status.RemainTime = mWaveSystem.Status.CurWave.Time;

            mMonsterWaveList.Clear();
            mBoxWaveList.Clear();
            mFruitTreeList.Clear();
            mBlessedFairyList.Clear();
            mCorruptedFairyList.Clear();

            Wave<EMonster>[] monsterWaves = mWaveSystem.Status.CurWave.MonsterWaves;
            for (int indexOfWave = 0; indexOfWave < monsterWaves.Length; indexOfWave++)
            {
                mMonsterWaveList.Add(Random.Range(0.0f, mWaveSystem.Status.RemainTime), monsterWaves[indexOfWave]);
            }

            Wave<EBox>[] boxWaves = mWaveSystem.Status.CurWave.BoxWaves;
            for (int indexOfWave = 0; indexOfWave < boxWaves.Length; indexOfWave++)
            {
                mBoxWaveList.Add(Random.Range(0.0f, mWaveSystem.Status.RemainTime), boxWaves[indexOfWave]);
            }

            Wave<EFruitTree>[] fruitTreeWaves = mWaveSystem.Status.CurWave.FruitTreeWaves;
            for (int indexOfWave = 0; indexOfWave < fruitTreeWaves.Length; indexOfWave++)
            {
                mFruitTreeList.Add(Random.Range(0.0f, mWaveSystem.Status.RemainTime), fruitTreeWaves[indexOfWave]);
            }

            Wave<EBlessedFairy>[] blessedFairyWaves = mWaveSystem.Status.CurWave.BlessedFairyWaves;
            for (int indexOfWave = 0; indexOfWave < blessedFairyWaves.Length; indexOfWave++)
            {
                mBlessedFairyList.Add(Random.Range(0.0f, mWaveSystem.Status.RemainTime), blessedFairyWaves[indexOfWave]);
            }

            Wave<ECorruptedFairy>[] corruptedFairyWaves = mWaveSystem.Status.CurWave.CorruptedFairyWaves;
            for (int indexOfWave = 0; indexOfWave < corruptedFairyWaves.Length; indexOfWave++)
            {
                mCorruptedFairyList.Add(Random.Range(0.0f, mWaveSystem.Status.RemainTime), corruptedFairyWaves[indexOfWave]);
            }
        }

        public override void Update()
        {
            // Time is Remained
            if (mWaveSystem.Status.RemainTime > 0.0f)
            {
                mWaveSystem.Status.RemainTime -= Time.deltaTime;
                float curTime = mWaveSystem.Status.CurWave.Time - mWaveSystem.Status.RemainTime;

                // Monster Wave Selection
                while (mMonsterWaveList.Count > 0 && mMonsterWaveList.Keys[0] < curTime)
                {
                    Wave<EMonster> monsterWave = mMonsterWaveList.Values[0];
                    mMonsterWaveList.RemoveAt(0);

                    float selection = Random.Range(0.0f, 1.0f);
                    if (selection < monsterWave.Possibility)
                    {
                        Monster monster = MonsterPool.Get(monsterWave.Type);
                        if (monster != null)
                        {
                            monster.Spawn();
                        }
                    }
                }

                // Box Wave Selection
                while (mBoxWaveList.Count > 0 && mBoxWaveList.Keys[0] < curTime)
                {
                    Wave<EBox> boxWave = mBoxWaveList.Values[0];
                    mBoxWaveList.RemoveAt(0);

                    float selection = Random.Range(0.0f, 1.0f);
                    if (selection < boxWave.Possibility + LevelManager.BoxPossibilityIncrement)
                    {
                        Box box = BoxPool.Get(boxWave.Type);
                        if (box != null)
                        {
                            int verticalSelection = (int)Random.Range(0.0f, 2.0f);
                            // Up Direction
                            if (verticalSelection == 0)
                            {
                                box.Spawn(VerticalCast.Cast(EVerticalDirection.Up), true);
                            }
                            // Down Direciton
                            else
                            {
                                box.Spawn(VerticalCast.Cast(EVerticalDirection.Down), false);
                            }
                        }
                    }
                }

                // Fruit Tree Wave Selection
                while (mFruitTreeList.Count > 0 && mFruitTreeList.Keys[0] < curTime)
                {
                    Wave<EFruitTree> fruitTreeWave = mFruitTreeList.Values[0];
                    mFruitTreeList.RemoveAt(0);

                    float selection = Random.Range(0.0f, 1.0f);
                    if (selection < fruitTreeWave.Possibility + LevelManager.FruitTreePossibilityIncrement)
                    {
                        FruitTree fruitTree = FruitTreePool.Get(fruitTreeWave.Type);
                        if (fruitTree != null)
                        {
                            int verticalSelection = (int)Random.Range(0.0f, 2.0f);
                            int horizontalSelection = (int)Random.Range(0.0f, 2.0f);
                            EHorizontalDirection horizontalDirection = (horizontalSelection == 0 ? EHorizontalDirection.Left : EHorizontalDirection.Right);
                            // Up Direction
                            if (verticalSelection == 0)
                            {
                                fruitTree.GenerateOn(VerticalCast.Cast(EVerticalDirection.Up), horizontalDirection, true);
                            }
                            // Down Direction
                            else
                            {
                                fruitTree.GenerateOn(VerticalCast.Cast(EVerticalDirection.Down), horizontalDirection, false);
                            }
                        }
                    }
                }

                // Blessed Fairy Wave Selection
                while (mBlessedFairyList.Count > 0 && mBlessedFairyList.Keys[0] < curTime)
                {
                    Wave<EBlessedFairy> blessedFairyWave = mBlessedFairyList.Values[0];
                    mBlessedFairyList.RemoveAt(0);

                    float selection = Random.Range(0.0f, 1.0f);
                    if (selection < blessedFairyWave.Possibility)
                    {
                        BlessedFairy blessedFairy = BlessedFairyPool.Get(blessedFairyWave.Type);
                        if (blessedFairy != null)
                        {
                            blessedFairy.Spawn();
                        }
                    }
                }

                // Corrupted Fairy Wave Selection
                while (mCorruptedFairyList.Count > 0 && mCorruptedFairyList.Keys[0] < curTime)
                {
                    Wave<ECorruptedFairy> corruptedFairyWave = mCorruptedFairyList.Values[0];
                    mCorruptedFairyList.RemoveAt(0);

                    float selection = Random.Range(0.0f, 1.0f);
                    if (selection < corruptedFairyWave.Possibility)
                    {
                        CorruptedFairy corruptedFairy = CorruptedFairyPool.Get(corruptedFairyWave.Type);
                        if (corruptedFairy != null)
                        {
                            corruptedFairy.Spawn();
                        }
                    }
                }

                while (mWaveSystem.CountOfCitizens > 0 && PlayerData.Current.IsRescued(mWaveSystem.GetCitizenType(0)))
                {
                    mWaveSystem.RemoveCitizen(0);
                }

                int citizenCount = mWaveSystem.CountOfCitizens;
                if (citizenCount > 0)
                {
                    float citizenTime = mWaveSystem.GetCitizenTime(0);
                    if (citizenTime < curTime)
                    {
                        ECitizen citizenType = mWaveSystem.GetCitizenType(0);
                        mWaveSystem.RemoveCitizen(0);
                        ArrestedCitizen citizen = ArrestedCitizenPool.Get(citizenType);
                        if (citizen != null)
                        {
                            int verticalSelection = (int)Random.Range(0.0f, 2.0f);
                            // Up Direction
                            if (verticalSelection == 0)
                            {
                                citizen.Spawn(VerticalCast.Cast(EVerticalDirection.Up), EVerticalDirection.Up);
                            }
                            // Down Direction
                            else
                            {
                                citizen.Spawn(VerticalCast.Cast(EVerticalDirection.Down), EVerticalDirection.Down);
                            }
                        }
                    }
                }
            }
            // Time is Ended.
            else if (Monster.MonsterList.Count == 0)
            {
                mWaveSystem.SetNextWave();
            }
        }
    }
}