﻿using UnityEngine;

namespace RedBlackTree
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class BossTrigger : MonoBehaviour
    {
        [SerializeField]
        private bool mTriggerValue = false;

        [SerializeField]
        private Status_WaveSystem mStatus = null;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Giant giant = collision.GetComponentInParent<Giant>();
            if (giant != null)
            {
                mStatus.BossTriggered = mTriggerValue;
            }
        }
    }
}