﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Csv;

namespace RedBlackTree
{
    public static class DataManager
    {
        private static Dictionary<string, Dictionary<string, string>> mScriptDict;

        static DataManager()
        {
            //Application.streamingAssetsPath
            mScriptDict = CsvReader.Read(Resources.Load<TextAsset>("Script").text, false);
        }

        /// <summary>
        /// Script.csv에서 읽어온 Script 중 tag에 위치한 script를 반환하는 메소드
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public static string GetScript(string tag)
        {
            return mScriptDict[tag][StateManager.Language.State.ToString()];
        }

        public static bool HasTag(string tag)
        {
            return tag != null && mScriptDict.ContainsKey(tag);
        }

        /// <summary>
        /// Script.csv에서 읽어온 Script 중 tag에 위치한 script에 parameters를 포매팅해서 반환하는 메소드
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static string GetScript(string tag, params object[] parameters)
        {
            return string.Format(mScriptDict[tag][StateManager.Language.State.ToString()], parameters);
        }
    }
}