﻿using UnityEngine;
using System.Collections.Generic;
using RedBlackTree;

namespace DynamicLevel
{
    public class DynamicLevelGenerator
        : MonoBehaviour
    {
        private static DynamicLevelGenerator mMain;

        private int mState;
        public static int State
        {
            get { return mMain.mState; }
            set
            {
                if (mMain.mFrameDict.ContainsKey(value))
                {
                    mMain.mState = value;
                    mMain.mStateFrameDict = mMain.mFrameDict[value];

                    for (int indexOfLayer = 0; indexOfLayer < mMain.mStateLayerDicts.Length; indexOfLayer++)
                    {
                        if (mMain.mLayerDicts[indexOfLayer].ContainsKey(value))
                        {
                            mMain.mStateLayerDicts[indexOfLayer] = mMain.mLayerDicts[indexOfLayer][value];
                        }
                        else
                        {
                            mMain.mStateLayerDicts[indexOfLayer] = null;
                        }
                    }
                }
            }
        }

        private Dictionary<int, Dictionary<ESocketDirection, Dictionary<int, List<SinglePool<DynamicLevelFrame>>>>> mFrameDict;
        private Dictionary<ESocketDirection, Dictionary<int, List<SinglePool<DynamicLevelFrame>>>> mStateFrameDict;
        private Dictionary<int, Dictionary<int, List<SinglePool<DynamicLevelLayer>>>>[] mLayerDicts;
        private Dictionary<int, List<SinglePool<DynamicLevelLayer>>>[] mStateLayerDicts;

        [SerializeField]
        private Transform mLevelHolder = null;

        [SerializeField]
        private DynamicLevelFrame mStartFrame = null;

        [SerializeField]
        private DynamicLevelFrame[] mFrames = null;
        public int CountOfFrames { get { return mFrames.Length; } }

        [SerializeField]
        private int[] mFrameCounts = null;

        [System.Serializable]
        private class LayerInfo
        {
            public DynamicLevelLayer[] Layers = null;
            public int[] LayerCounts = null;
        }

        [SerializeField]
        private LayerInfo[] mLayerInfo = null;
        public int LayerDepth { get { return mLayerInfo.Length; } }

        [SerializeField]
        private int mActiveDepth = 1;
        [SerializeField]
        private int mInactiveDepth = 2;

        [SerializeField]
        private Transform mPlayerTransform = null;
        // 프레임 탐색을 위한 프레임 큐
        private Queue<DynamicLevelFrame> mGeneratedFrameQueue;
        private List<DynamicLevelFrame> mCheckedFrameList;
        // 생성될 프레임으로 선택된 프레임 리스트
        private List<DynamicLevelFrame> mSelectedFrameList;
        // 생성될 레이어로 선택될 레이어 리스트
        private List<DynamicLevelLayer> mSelectedLayerList;

        private DynamicLevelFrame mPrevFrame;


        public void Awake()
        {
            mMain = this;
            // 프레임을 상태번호-소켓방향-소켓번호 순으로 분류한 프레임 풀 리스트 사전
            mFrameDict = new Dictionary<int, Dictionary<ESocketDirection, Dictionary<int, List<SinglePool<DynamicLevelFrame>>>>>();
            // 레이어를 상태번호-프레임번호 순으로 분류한 레이어 풀 리스트 사전
            mLayerDicts = new Dictionary<int, Dictionary<int, List<SinglePool<DynamicLevelLayer>>>>[mLayerInfo.Length];
            mStateLayerDicts = new Dictionary<int, List<SinglePool<DynamicLevelLayer>>>[mLayerDicts.Length];
            // 프레임 탐색을 위한 큐
            mGeneratedFrameQueue = new Queue<DynamicLevelFrame>(CountOfFrames);
            // 검사된 프레임들을 저장하는 리스트
            mCheckedFrameList = new List<DynamicLevelFrame>(CountOfFrames);
            mCheckedFrameList.Add(mStartFrame);
            // 프레임 생성시 선택된 프레임 리스트
            mSelectedFrameList = new List<DynamicLevelFrame>(CountOfFrames);

            int maxCountOfLayers = 0;
            for(int indexOfLayerInfo = 0; indexOfLayerInfo < mLayerInfo.Length; indexOfLayerInfo++)
            {
                if(maxCountOfLayers < mLayerInfo[indexOfLayerInfo].Layers.Length)
                {
                    maxCountOfLayers = mLayerInfo[indexOfLayerInfo].Layers.Length;
                }
            }

            // 레이어 생성시 선택된 레이어 리스트
            mSelectedLayerList = new List<DynamicLevelLayer>(maxCountOfLayers);

            if (mLevelHolder == null)
            {
                mLevelHolder = new GameObject("Level Holder").transform;
            }

            // 모든 프레임에 대해
            for (int indexOfFrames = 0; indexOfFrames < mFrames.Length; indexOfFrames++)
            {
                DynamicLevelFrame frame = mFrames[indexOfFrames];
                // 해당 프레임의 풀 생성
                SinglePool<DynamicLevelFrame> framePool = new SinglePool<DynamicLevelFrame>(frame, mFrameCounts[indexOfFrames], mLevelHolder);

                // 프레임의 상태 번호에 대해
                for (int indexOfStateType = 0; indexOfStateType < frame.CountOfStateTypes; indexOfStateType++)
                {
                    if (!mFrameDict.ContainsKey(frame.GetStateType(indexOfStateType)))
                    {
                        mFrameDict.Add(frame.GetStateType(indexOfStateType), new Dictionary<ESocketDirection, Dictionary<int, List<SinglePool<DynamicLevelFrame>>>>());
                    }

                    Dictionary<ESocketDirection, Dictionary<int, List<SinglePool<DynamicLevelFrame>>>> _frameDict = mFrameDict[frame.GetStateType(indexOfStateType)];
                    // 프레임의 소켓 번호에 대해
                    for (int indexOfSocket = 0; indexOfSocket < frame.CountOfSockets; indexOfSocket++)
                    {
                        DynamicLevelSocket socket = frame.GetSocket(indexOfSocket);
                        // 해당 소켓 방향을 키로 가지고 있지 않다면,
                        if (!_frameDict.ContainsKey(socket.Direction))
                        {
                            _frameDict.Add(socket.Direction, new Dictionary<int, List<SinglePool<DynamicLevelFrame>>>());
                        }
                        Dictionary<int, List<SinglePool<DynamicLevelFrame>>> __frameDict = _frameDict[socket.Direction];
                        // 해당 소켓 번호를 키로 가지고 있지 않다면, 
                        if (!__frameDict.ContainsKey(socket.Type))
                        {
                            __frameDict.Add(socket.Type, new List<SinglePool<DynamicLevelFrame>>());
                        }
                        // 해당 프레임 풀을 리스트에 추가한 적이 없다면,
                        if (!__frameDict[socket.Type].Contains(framePool))
                        {
                            __frameDict[socket.Type].Add(framePool);
                        }
                    }
                }
            }

            // 레이어를 상태 ID, 프레임 ID 별로 분류하여 저장
            for (int indexOfLayers_0 = 0; indexOfLayers_0 < mLayerInfo.Length; indexOfLayers_0++)
            {
                DynamicLevelLayer[] layers = mLayerInfo[indexOfLayers_0].Layers;
                Dictionary<int, Dictionary<int, List<SinglePool<DynamicLevelLayer>>>> layerDict = new Dictionary<int, Dictionary<int, List<SinglePool<DynamicLevelLayer>>>>();
                mLayerDicts[indexOfLayers_0] = layerDict;
                for (int indexOfLayers_1 = 0; indexOfLayers_1 < mLayerInfo[indexOfLayers_0].Layers.Length; indexOfLayers_1++)
                {
                    DynamicLevelLayer layer = layers[indexOfLayers_1];
                    SinglePool<DynamicLevelLayer> layerPool = new SinglePool<DynamicLevelLayer>(layer
                        , mLayerInfo[indexOfLayers_0].LayerCounts[indexOfLayers_1], mLevelHolder);

                    for (int indexOfStateType = 0; indexOfStateType < layer.CountOfStateTypes; indexOfStateType++)
                    {
                        if (!layerDict.ContainsKey(layer.GetStateType(indexOfStateType)))
                        {
                            layerDict.Add(layer.GetStateType(indexOfStateType), new Dictionary<int, List<SinglePool<DynamicLevelLayer>>>());
                        }

                        Dictionary<int, List<SinglePool<DynamicLevelLayer>>> frameNumberLayerDict = layerDict[layer.GetStateType(indexOfStateType)];
                        for (int indexOfFrameType = 0; indexOfFrameType < layer.CountOfFrameTypes; indexOfFrameType++)
                        {
                            if (!frameNumberLayerDict.ContainsKey(layer.GetFrameType(indexOfFrameType)))
                            {
                                frameNumberLayerDict.Add(layer.GetFrameType(indexOfFrameType), new List<SinglePool<DynamicLevelLayer>>());
                            }

                            frameNumberLayerDict[layer.GetFrameType(indexOfFrameType)].Add(layerPool);
                        }
                    }
                }
            }

            State = 0;
        }

        public void FixedUpdate()
        {
            if (!StateManager.Pause.State)
            {
                CheckLevel();
            }
        }

        public void OnDestroy()
        {
            mMain = null;
        }

        private void CheckLevel()
        {
            // 플레이어 위치
            Vector2 playerPosition = mPlayerTransform.position;
            // 생성된 모든 프레임 중에서 플레이어가 위치하는 프레임을 선택
            DynamicLevelFrame curFrame = mCheckedFrameList.Find(delegate (DynamicLevelFrame frame)
            {
                Vector2 leftBottomPosition = frame.LeftBottomAnchor.position;
                Vector2 rightTopPosition = frame.RightTopAnchor.position;

                if (leftBottomPosition.x <= playerPosition.x - Mathf.Epsilon
                    && playerPosition.x + Mathf.Epsilon <= rightTopPosition.x
                    && leftBottomPosition.y <= playerPosition.y - Mathf.Epsilon
                    && playerPosition.y + Mathf.Epsilon <= rightTopPosition.y)
                {
                    return true;
                }
                return false;
            });

            // 플레이어가 위치하는 프레임이 바뀐 경우
            if (curFrame != null && mPrevFrame != curFrame)
            {
                mPrevFrame = curFrame;
                // 탐색예정인 프레임을 저장하는 큐
                mGeneratedFrameQueue.Clear();
                mCheckedFrameList.Clear();
                mGeneratedFrameQueue.Enqueue(curFrame);
                mCheckedFrameList.Add(curFrame);
                // 현재 탐색중인 프레임의 깊이
                int depth = 0;
                // 현재 깊이에 존재하는 프레임의 개수
                int countOfFramesInCurDepth = 1;
                // 다음 깊이에 존재하는 프레임의 개수
                int countOfFramesInNextDepth = 0;
                // 현재 깊이가 활성화 깊이에 도달할때까지, 생성된 프레임 큐가 빌때까지
                while (depth < mActiveDepth && mGeneratedFrameQueue.Count > 0)
                {
                    // 탐색할 프레임 하나를 꺼냄
                    curFrame = mGeneratedFrameQueue.Dequeue();

                    // 현재 프레임의 소켓 중 연결되어 있는 일련의 소켓에 대해, 
                    curFrame.ForEachConnectedSocket(delegate (int indexOfSocket)
                    {
                        DynamicLevelFrame connectedFrame = curFrame.GetSocket(indexOfSocket).ConnectedFrame;
                        if (!mCheckedFrameList.Contains(connectedFrame))
                        {
                            mGeneratedFrameQueue.Enqueue(connectedFrame);
                            mCheckedFrameList.Add(connectedFrame);
                            countOfFramesInNextDepth++;
                        }
                    });

                    // 탐색하는 프레임의 모든 소켓에 대해, 
                    while (curFrame.CountOfAvailableSocketIndex > 0)
                    {
                        // 탐색중인 프레임의 소켓 중 무작위로 하나를 고름
                        int indexOfSocket = curFrame.GetRandomIndexOfAvailableSockets();
                        DynamicLevelSocket curSocket = curFrame.GetSocket(indexOfSocket);
                        Vector2 curSocketPosition = curSocket.Transform.position;

                        Dictionary<int, List<SinglePool<DynamicLevelFrame>>> subStateFrameDict = null;
                        ESocketDirection selectedDirection = ESocketDirection.None;
                        // 소켓 방향에 따라 적절한 소켓 사전 선택
                        if (curSocket.Direction == ESocketDirection.Right)
                        {
                            if (mStateFrameDict.ContainsKey(ESocketDirection.Left))
                            {
                                subStateFrameDict = mStateFrameDict[ESocketDirection.Left];
                                selectedDirection = ESocketDirection.Left;
                            }
                        }
                        else if (curSocket.Direction == ESocketDirection.Left)
                        {
                            if (mStateFrameDict.ContainsKey(ESocketDirection.Right))
                            {
                                subStateFrameDict = mStateFrameDict[ESocketDirection.Right];
                                selectedDirection = ESocketDirection.Right;
                            }
                        }
                        else if (curSocket.Direction == ESocketDirection.Down)
                        {
                            if (mStateFrameDict.ContainsKey(ESocketDirection.Up))
                            {
                                subStateFrameDict = mStateFrameDict[ESocketDirection.Up];
                                selectedDirection = ESocketDirection.Up;
                            }
                        }
                        else if (curSocket.Direction == ESocketDirection.Up)
                        {
                            if (mStateFrameDict.ContainsKey(ESocketDirection.Down))
                            {
                                subStateFrameDict = mStateFrameDict[ESocketDirection.Down];
                                selectedDirection = ESocketDirection.Down;
                            }
                        }

                        mSelectedFrameList.Clear();

                        // 선택된 소켓을 가진 사전이 존재하고, 소켓의 번호에 맞는 프레임 풀이 존재한다면, 
                        if (subStateFrameDict != null && subStateFrameDict.ContainsKey(curSocket.Type))
                        {
                            List<SinglePool<DynamicLevelFrame>> framePoolList = subStateFrameDict[curSocket.Type];

                            // 해당 소켓을 가진 모든 프레임 풀에 대해, 
                            framePoolList.ForEach(delegate (SinglePool<DynamicLevelFrame> framePool)
                            {
                                // 풀에서 선택된 프레임을 하나 꺼낼 수 있다면, 
                                DynamicLevelFrame selectedFrame = framePool.Get();
                                if (selectedFrame != null)
                                {
                                    // 선택된 프레임의 좌측 하단, 우측 상단 앵커의 Local Position을 저장
                                    Vector2 selectedFramePosition = selectedFrame.transform.position;
                                    Vector2 leftBottomPositionOfSelectedFrame = selectedFrame.LeftBottomAnchor.position;
                                    Vector2 rightTopPositionOfSelectedFrame = selectedFrame.RightTopAnchor.position;

                                    Vector2 leftBottomLocalPositionOfSelectedFrame = leftBottomPositionOfSelectedFrame - selectedFramePosition;
                                    Vector2 rightTopLocalPositionOfSelectedFrame = rightTopPositionOfSelectedFrame - selectedFramePosition;

                                    // 선택된 프레임에 등록된 소켓 중 selectedDirection과 curSocket.Type을 가지는 소켓을 대상으로 조건을 만족하는 소켓들이 선택되었다면, 
                                    if (selectedFrame.SelectSocket(selectedDirection, curSocket.Type, delegate (int indexOfSelectedSocket)
                                 {
                                     Vector2 selectedSocketPosition = selectedFrame.GetSocket(indexOfSelectedSocket).Transform.position;
                                     Vector2 selectedSocketLocalPosition = selectedSocketPosition - selectedFramePosition;
                                     Vector2 connectedFramePosition = curSocketPosition - selectedSocketLocalPosition;
                                     Vector2 leftBottomPositionOfConnectedFrame = connectedFramePosition + leftBottomLocalPositionOfSelectedFrame;
                                     Vector2 rightTopPositionOfConnectedFrame = connectedFramePosition + rightTopLocalPositionOfSelectedFrame;

                                     DynamicLevelFrame overlappedFrame = mCheckedFrameList.Find(delegate (DynamicLevelFrame generatedFrame)
                                 {
                                     Vector2 leftBottomPositionOfGeneratedFrame = generatedFrame.LeftBottomAnchor.position;
                                     Vector2 rightTopPositionOfGeneratedFrame = generatedFrame.RightTopAnchor.position;

                                     float horizontalOverlap = (leftBottomPositionOfConnectedFrame.x - rightTopPositionOfGeneratedFrame.x) * (rightTopPositionOfConnectedFrame.x - leftBottomPositionOfGeneratedFrame.x);
                                     float verticalOverlap = (leftBottomPositionOfConnectedFrame.y - rightTopPositionOfGeneratedFrame.y) * (rightTopPositionOfConnectedFrame.y - leftBottomPositionOfGeneratedFrame.y);

                                     // 생성된 프레임과 연결할 프레임 사이에 중첩이 발생한다면, 
                                     if (horizontalOverlap < -Mathf.Epsilon && verticalOverlap < -Mathf.Epsilon)
                                     {
                                         return true;
                                     }
                                     return false;
                                 });
                                     // 중첩된 프레임이 존재하지 않는다면, 
                                     if (overlappedFrame == null)
                                     {
                                         return true;
                                     }
                                     // 중첩된 프레임이 존재한다면, 
                                     return false;
                                 }))
                                    {
                                        // 위 조건에 만족하는 프레임을 리스트에 저장
                                        mSelectedFrameList.Add(selectedFrame);
                                    }
                                }
                            });
                        }

                        // 연결할 수 있는 프레임이 존재하는 경우
                        if (mSelectedFrameList.Count > 0)
                        {
                            // 가중치를 기반으로 연결할 수 있는 프레임을 하나 선택
                            float weightSum = 0.0f;
                            mSelectedFrameList.ForEach(delegate (DynamicLevelFrame selectedFrame)
                            {
                                weightSum += selectedFrame.Weight;
                            });

                            float selection = Random.Range(0.0f, weightSum);
                            DynamicLevelFrame nextFrame = mSelectedFrameList.Find(delegate (DynamicLevelFrame selectedFrame)
                            {
                                if (selection <= selectedFrame.Weight)
                                {
                                    return true;
                                }
                                selection -= selectedFrame.Weight;
                                return false;
                            });

                            // 선택된 프레임을 현재 프레임의 지정된 소켓에 연결
                            curFrame.ConnectAt(indexOfSocket, nextFrame, nextFrame.GetRandomIndexOfSelectedSockets());
                            mGeneratedFrameQueue.Enqueue(nextFrame);
                            mCheckedFrameList.Add(nextFrame);
                            countOfFramesInNextDepth++;

                            // 연결된 프레임에 레이어 추가
                            for (int indexOfLayer = 0; indexOfLayer < mStateLayerDicts.Length; indexOfLayer++)
                            {
                                // 추가될 수 있는 레이어가 존재하는 경우
                                if (mStateLayerDicts[indexOfLayer] != null && mStateLayerDicts[indexOfLayer].ContainsKey(nextFrame.FrameType))
                                {
                                    List<SinglePool<DynamicLevelLayer>> layerPoolList = mStateLayerDicts[indexOfLayer][nextFrame.FrameType];
                                    mSelectedLayerList.Clear();

                                    weightSum = 0.0f;
                                    layerPoolList.ForEach(delegate (SinglePool<DynamicLevelLayer> layerPool)
                                    {
                                        DynamicLevelLayer selectedLayer = layerPool.Get();
                                        if (selectedLayer != null)
                                        {
                                            mSelectedLayerList.Add(selectedLayer);
                                            weightSum += selectedLayer.Weight;
                                        }
                                    });

                                    if (mSelectedLayerList.Count > 0)
                                    {
                                        selection = Random.Range(0.0f, weightSum);

                                        DynamicLevelLayer nextLayer = mSelectedLayerList.Find(delegate (DynamicLevelLayer selectedLayer)
                                        {
                                            if (selection <= selectedLayer.Weight)
                                            {
                                                return true;
                                            }
                                            selection -= selectedLayer.Weight;
                                            return false;
                                        });

                                        nextFrame.AddLayer(nextLayer);
                                    }
                                }
                            }
                        }
                        // 연결할 수 있는 프레임이 존재하지 않는 경우
                        else
                        {
                            curFrame.BlockAt(indexOfSocket);
                        }
                    }
                    // 현재 깊이의 남은 프레임 수를 체크
                    countOfFramesInCurDepth--;
                    if (countOfFramesInCurDepth <= 0)
                    {
                        // 현재 깊이에 남은 프레임이 존재하지 않는다면, 깊이를 증가시킴
                        countOfFramesInCurDepth = countOfFramesInNextDepth;
                        countOfFramesInNextDepth = 0;
                        depth++;
                    }
                }
                // 비활성화 깊이 - 1 까지 BFS 탐색을 실시
                while (depth < mInactiveDepth - 1 && mGeneratedFrameQueue.Count > 0)
                {
                    curFrame = mGeneratedFrameQueue.Dequeue();
                    curFrame.ForEachConnectedSocket(delegate (int indexOfSocket)
                    {
                        DynamicLevelFrame connectedFrame = curFrame.GetSocket(indexOfSocket).ConnectedFrame;
                        if (!mCheckedFrameList.Contains(connectedFrame))
                        {
                            mGeneratedFrameQueue.Enqueue(connectedFrame);
                            mCheckedFrameList.Add(connectedFrame);
                            countOfFramesInNextDepth++;
                        }
                    });
                    countOfFramesInCurDepth--;
                    if (countOfFramesInCurDepth <= 0)
                    {
                        countOfFramesInCurDepth = countOfFramesInNextDepth;
                        countOfFramesInNextDepth = 0;
                        depth++;
                    }
                }

                // 이후의 모든 Frame은 제거
                while (mGeneratedFrameQueue.Count > 0)
                {
                    curFrame = mGeneratedFrameQueue.Dequeue();
                    for (int indexOfSocket = 0; indexOfSocket < curFrame.CountOfSockets; indexOfSocket++)
                    {
                        if (!mCheckedFrameList.Contains(curFrame.GetSocket(indexOfSocket).ConnectedFrame))
                        {
                            curFrame.DisconnectAt_Recursive(indexOfSocket);
                        }
                    }
                }
            }
        }
    }
    public interface ISinglePoolable
    {
        bool CanPool { get; }
    }

    public class SinglePool<T>
        where T : MonoBehaviour, ISinglePoolable
    {
        private int mIndex;
        private T[] mObjects;

        public SinglePool(T prefab, int count, Transform parent)
        {
            mObjects = new T[count];

            for (int indexOfObjects = 0; indexOfObjects < count; indexOfObjects++)
            {
                mObjects[indexOfObjects] = GameObject.Instantiate<T>(prefab, parent);
                mObjects[indexOfObjects].gameObject.SetActive(false);
            }
        }

        public T Get()
        {
            for (int count = 0; count < mObjects.Length; count++)
            {
                int indexOfObjects = mIndex;
                mIndex = (mIndex + 1 < mObjects.Length ? mIndex + 1 : 0);
                if (mObjects[indexOfObjects].CanPool)
                {
                    return mObjects[indexOfObjects];
                }
            }
            return null;
        }
    }
}