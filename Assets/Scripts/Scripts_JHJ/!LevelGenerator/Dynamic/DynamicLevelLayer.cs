﻿using UnityEngine;
using System.Collections.Generic;

namespace DynamicLevel
{
    public class DynamicLevelLayer
        : MonoBehaviour, ISinglePoolable
    {
        /// <summary>
        /// 레이어 블로커
        /// </summary>
        [System.Serializable]
        public class LayerBlocker
        {
            [SerializeField]
            private GameObject[] mBlockingObjects = null;
            public bool IsBlocked { get; private set; }

            public void SetBlocker(bool isEnabled)
            {
                if(IsBlocked != isEnabled)
                {
                    IsBlocked = isEnabled;

                    for(int indexOfBlockings = 0; indexOfBlockings < mBlockingObjects.Length; indexOfBlockings++)
                    {
                        if(mBlockingObjects[indexOfBlockings].activeSelf)
                        {
                            mBlockingObjects[indexOfBlockings].SetActive(false);
                        }
                        else
                        {
                            mBlockingObjects[indexOfBlockings].SetActive(true);
                        }
                    }
                }
            }
        }

        public bool CanPool { get { return !gameObject.activeSelf; } }

        [SerializeField]
        private int[] mFrameTypes = null;

        public int CountOfFrameTypes { get { return mFrameTypes.Length; } }

        [SerializeField]
        private int[] mStateTypes = null;

        public int CountOfStateTypes { get { return mStateTypes.Length; } }

        [SerializeField]
        private float mWeight = 0.0f;
        /// <summary>
        /// 가중치
        /// </summary>
        public float Weight { get { return mWeight; } }

        // 좌측 상단 앵커
        [SerializeField]
        private Transform mLeftTopAnchor = null;
        public Transform LeftTopAnchor { get { return mLeftTopAnchor; } }

        // 레이어 블로커
        [SerializeField]
        private LayerBlocker[] mBlockers = null;

        /// <summary>
        /// 초기화시 호출되는 이벤트
        /// </summary>
        public event System.Action OnRemove;
        public event System.Action OnPlace; 

        /// <summary>
        /// 상태 타입을 가졌는가 반환하는 메소드
        /// </summary>
        public bool HasStateType(int stateType)
        {
            for (int indexOfStateNumber = 0; indexOfStateNumber < mStateTypes.Length; indexOfStateNumber++)
            {
                if (mStateTypes[indexOfStateNumber] == stateType)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 배치될 수 있는 상태 Id를 반환하는 메소드
        /// </summary>
        public int GetStateType(int indexOfStateType)
        {
            return mStateTypes[indexOfStateType];
        }

        /// <summary>
        /// 프레임 타입을 가졌는가 반환하는 메소드
        /// </summary>
        public bool HasFrameType(int frameType)
        {
            for (int indexOfFrameNumbers = 0; indexOfFrameNumbers < mFrameTypes.Length; indexOfFrameNumbers++)
            {
                if (mFrameTypes[indexOfFrameNumbers] == frameType)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 배치될 수 있는 프레임 Id를 반환하는 메소드
        /// </summary>
        public int GetFrameType(int indexOfFrameType)
        {
            return mFrameTypes[indexOfFrameType];
        }

        /// <summary>
        /// 전달된 frame에 레이어를 배치하는 메소드
        /// </summary>
        public void PlaceOn(DynamicLevelFrame frame)
        {
            // 좌측 상단 좌표를 기준으로 위치 설정
            Vector2 leftTopPosition = new Vector2(frame.LeftBottomAnchor.position.x, frame.RightTopAnchor.position.y);
            Vector2 localLeftTopAnchorPosition = mLeftTopAnchor.localPosition;
            transform.position = leftTopPosition - localLeftTopAnchorPosition;
            gameObject.SetActive(true);
            OnPlace?.Invoke();

            // 프레임의 소켓 상태에 따른 블로커 설정
            for (int indexOfSockets = 0; indexOfSockets < mBlockers.Length; indexOfSockets++)
            {
                mBlockers[indexOfSockets].SetBlocker(frame.GetSocket(indexOfSockets).IsBlocked);
            }

        }

        public void Remove()
        {
            OnRemove?.Invoke();
            // 비활성화
            gameObject.SetActive(false);
        }

        /// <summary>
        /// indexOfBlockers 번째 블로커를 설정/해제하는 메소드
        /// </summary>
        public void SetBlocker(int indexOfBlockers, bool isEnabled)
        {
            mBlockers[indexOfBlockers].SetBlocker(isEnabled);
        }
    }
}

