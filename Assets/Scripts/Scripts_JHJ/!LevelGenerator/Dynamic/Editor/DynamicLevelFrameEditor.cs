﻿using UnityEngine;
using UnityEditor;

namespace DynamicLevel
{
    [CustomEditor(typeof(DynamicLevelFrame))]
    public class DynamicLevelFrameEditor : Editor
    {
        private bool mShowSocket = false;
        private bool mShowStateType = false;

        private void OnSceneGUI()
        {
            DynamicLevelFrame frame = target as DynamicLevelFrame;

            if (frame.LeftBottomAnchor != null && frame.RightTopAnchor != null)
            {
                Vector3 leftBottomAnchorPosition = frame.LeftBottomAnchor.position;
                Vector3 rightTopAnchorPosition = frame.RightTopAnchor.position;
                float scale = 1.0f;
                Handles.TransformHandle(ref leftBottomAnchorPosition, Quaternion.identity, ref scale);
                Handles.TransformHandle(ref rightTopAnchorPosition, Quaternion.identity, ref scale);

                rightTopAnchorPosition = Vector3.Max(rightTopAnchorPosition, leftBottomAnchorPosition);
                frame.LeftBottomAnchor.position = leftBottomAnchorPosition;
                frame.RightTopAnchor.position = rightTopAnchorPosition;

                Handles.Label(leftBottomAnchorPosition, new GUIContent("Left Bottom Anchor"));
                Handles.Label(rightTopAnchorPosition, new GUIContent("Right Top Anchor"));

                Handles.DrawSolidRectangleWithOutline(new Rect(leftBottomAnchorPosition, rightTopAnchorPosition - leftBottomAnchorPosition), new Color(1.0f, 0.7f, 0.0f, 0.1f), new Color(1.0f, 1.0f, 0.0f, 0.5f));

                for (int indexOfSocket = 0; indexOfSocket < frame.CountOfSockets; indexOfSocket++)
                {
                    DynamicLevelSocket socket = frame.GetSocket(indexOfSocket);
                    if (socket.Transform != null)
                    {
                        Vector3 socketPosition = socket.Transform.position;

                        Handles.TransformHandle(ref socketPosition, Quaternion.identity, ref scale);
                        Handles.Label(socket.Transform.position, new GUIContent(string.Format("Type_{0}", socket.Type)));
                        socket.Transform.position = socketPosition;


                        if (socket.Direction == ESocketDirection.Right)
                        {
                            Handles.color = new Color(0.0f, 1.0f, 0.0f, 0.6f);
                            Handles.SphereHandleCap(0, socketPosition, Quaternion.identity, 1.0f, EventType.Repaint);
                        }
                        else if (socket.Direction == ESocketDirection.Left)
                        {
                            Handles.color = new Color(1.0f, 0.0f, 0.0f, 0.6f);
                            Handles.SphereHandleCap(0, socketPosition, Quaternion.identity, 1.0f, EventType.Repaint);
                        }
                        else if (socket.Direction == ESocketDirection.Up)
                        {
                            Handles.color = new Color(0.0f, 0.0f, 1.0f, 0.6f);
                            Handles.SphereHandleCap(0, socketPosition, Quaternion.identity, 1.0f, EventType.Repaint);
                        }
                        else if (socket.Direction == ESocketDirection.Down)
                        {
                            Handles.color = new Color(1.0f, 0.0f, 1.0f, 0.6f);
                            Handles.SphereHandleCap(0, socketPosition, Quaternion.identity, 1.0f, EventType.Repaint);
                        }
                    }
                }
            }
        }

        public override void OnInspectorGUI()
        {
            SerializedProperty frameTypeProp = serializedObject.FindProperty("mFrameType");
            SerializedProperty stateTypeProps = serializedObject.FindProperty("mStateTypes");
            SerializedProperty weightProp = serializedObject.FindProperty("mWeight");
            SerializedProperty socketProps = serializedObject.FindProperty("mSockets");
            SerializedProperty leftBottomAnchorProp = serializedObject.FindProperty("mLeftBottomAnchor");
            SerializedProperty rightTopAnchorProp = serializedObject.FindProperty("mRightTopAnchor");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Frame Type"), GUILayout.MaxWidth(80));
            frameTypeProp.intValue = EditorGUILayout.IntField(frameTypeProp.intValue, GUILayout.MaxWidth(50));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Weight"), GUILayout.MaxWidth(80));
            weightProp.floatValue = EditorGUILayout.FloatField(weightProp.floatValue, GUILayout.MaxWidth(50));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(new GUIContent("State Type Count"), GUILayout.MaxWidth(100));
            int countOfStateTypes = EditorGUILayout.IntField(stateTypeProps.arraySize);
            countOfStateTypes = Mathf.Max(countOfStateTypes, 0);
            if (stateTypeProps.arraySize != countOfStateTypes)
            {
                stateTypeProps.arraySize = countOfStateTypes;
                serializedObject.ApplyModifiedProperties();
            }

            EditorGUILayout.LabelField(new GUIContent("Show State Type"), GUILayout.MaxWidth(100));
            mShowStateType = EditorGUILayout.Toggle(mShowStateType, GUILayout.MaxWidth(50));

            EditorGUILayout.EndHorizontal();

            if (mShowStateType)
            {
                for (int indexOfStateType = 0; indexOfStateType < countOfStateTypes; indexOfStateType++)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent(string.Format("[{0}]. State Type", indexOfStateType)), GUILayout.MaxWidth(100));
                    SerializedProperty stateTypeProp = stateTypeProps.GetArrayElementAtIndex(indexOfStateType);
                    stateTypeProp.intValue = EditorGUILayout.IntField(stateTypeProp.intValue, GUILayout.MaxWidth(50));
                    EditorGUILayout.EndHorizontal();
                }
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Left Bottom Anchor"), GUILayout.MaxWidth(110));
            leftBottomAnchorProp.objectReferenceValue = EditorGUILayout.ObjectField(leftBottomAnchorProp.objectReferenceValue, typeof(Transform), true);
            if (leftBottomAnchorProp.objectReferenceValue == null)
            {
                DynamicLevelFrame frame = target as DynamicLevelFrame;
                Transform leftBottomAnchor = new GameObject("Left Bottom Anchor").transform;
                leftBottomAnchor.SetParent(frame.transform);
                leftBottomAnchor.localPosition = new Vector3(-5.0f, -5.0f, 0.0f);
                leftBottomAnchor.localRotation = Quaternion.identity;
                leftBottomAnchor.localScale = Vector3.one;
                leftBottomAnchorProp.objectReferenceValue = leftBottomAnchor;

                serializedObject.ApplyModifiedProperties();
            }

            EditorGUILayout.LabelField(new GUIContent("Right Top Anchor"), GUILayout.MaxWidth(100));
            rightTopAnchorProp.objectReferenceValue = EditorGUILayout.ObjectField(rightTopAnchorProp.objectReferenceValue, typeof(Transform), true);
            if (rightTopAnchorProp.objectReferenceValue == null)
            {
                DynamicLevelFrame frame = target as DynamicLevelFrame;
                Transform rightTopAnchor = new GameObject("Right Top Anchor").transform;
                rightTopAnchor.SetParent(frame.transform);
                rightTopAnchor.localPosition = new Vector3(5.0f, 5.0f, 0.0f);
                rightTopAnchor.localRotation = Quaternion.identity;
                rightTopAnchor.localScale = Vector3.one;
                rightTopAnchorProp.objectReferenceValue = rightTopAnchor;

                serializedObject.ApplyModifiedProperties();
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Socket Count"), GUILayout.MaxWidth(80));
            int countOfSockets = EditorGUILayout.IntField(socketProps.arraySize, GUILayout.MaxWidth(50));
            if (socketProps.arraySize != countOfSockets)
            {
                socketProps.arraySize = countOfSockets;
                serializedObject.ApplyModifiedProperties();
            }

            EditorGUILayout.LabelField(new GUIContent("Show Sockets"), GUILayout.MaxWidth(80));
            mShowSocket = EditorGUILayout.Toggle(mShowSocket);
            EditorGUILayout.EndHorizontal();
            if (mShowSocket)
            {
                for (int indexOfSocket = 0; indexOfSocket < countOfSockets; indexOfSocket++)
                {
                    SerializedProperty socketProp = socketProps.GetArrayElementAtIndex(indexOfSocket);
                    SerializedProperty socketTypeProp = socketProp.FindPropertyRelative("mType");
                    SerializedProperty socketDirectionProp = socketProp.FindPropertyRelative("mDirection");
                    SerializedProperty socketTransformProp = socketProp.FindPropertyRelative("mTransform");
                    SerializedProperty socketBlockerProps = socketProp.FindPropertyRelative("mBlockers");

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent(string.Format("[{0}]. Type", indexOfSocket)), GUILayout.MaxWidth(80));
                    socketTypeProp.intValue = EditorGUILayout.IntField(socketTypeProp.intValue, GUILayout.MaxWidth(50));
                    EditorGUILayout.LabelField(new GUIContent("Direction"), GUILayout.MaxWidth(50));
                    ESocketDirection direction = (ESocketDirection)socketDirectionProp.intValue;
                    direction = (ESocketDirection)EditorGUILayout.EnumPopup(direction, GUILayout.MaxWidth(70));
                    socketDirectionProp.intValue = (int)direction;
                    EditorGUILayout.LabelField(new GUIContent("Transform"), GUILayout.MaxWidth(70));
                    socketTransformProp.objectReferenceValue = EditorGUILayout.ObjectField(socketTransformProp.objectReferenceValue, typeof(Transform), true);
                    if (socketTransformProp.objectReferenceValue == null)
                    {
                        DynamicLevelFrame frame = target as DynamicLevelFrame;
                        Transform socketTransform = null;
                        Transform[] transforms = frame.GetComponentsInChildren<Transform>(true);
                        for (int indexOfTransform = 0; indexOfTransform < transforms.Length; indexOfTransform++)
                        {
                            if (transforms[indexOfTransform].name.Equals(string.Format("Socket Transform_{0}", indexOfSocket)))
                            {
                                socketTransform = transforms[indexOfTransform];
                                break;
                            }
                        }
                        if (socketTransform == null)
                        {
                            socketTransform = new GameObject(string.Format("Socket Transform_{0}", indexOfSocket)).transform;
                            socketTransform.SetParent(frame.transform);
                            socketTransform.localPosition = Vector3.zero;
                            socketTransform.localRotation = Quaternion.identity;
                            socketTransform.localScale = Vector3.one;
                        }
                        socketTransformProp.objectReferenceValue = socketTransform;
                        serializedObject.ApplyModifiedProperties();
                    }

                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent("Blocker Count"), GUILayout.MaxWidth(100));
                    int countOfBlockers = EditorGUILayout.IntField(socketBlockerProps.arraySize, GUILayout.MaxWidth(50));
                    if (socketBlockerProps.arraySize != countOfBlockers)
                    {
                        socketBlockerProps.arraySize = countOfBlockers;
                        serializedObject.ApplyModifiedProperties();
                    }
                    EditorGUILayout.EndHorizontal();
                    for (int indexOfBlocker = 0; indexOfBlocker < countOfBlockers; indexOfBlocker++)
                    {
                        SerializedProperty blockerProp = socketBlockerProps.GetArrayElementAtIndex(indexOfBlocker);

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("[{0}]-[{1}]. Blocker", indexOfSocket, indexOfBlocker)), GUILayout.MaxWidth(100));
                        blockerProp.objectReferenceValue = EditorGUILayout.ObjectField(blockerProp.objectReferenceValue, typeof(GameObject), true);
                        EditorGUILayout.EndHorizontal();
                    }

                    EditorGUILayout.Space();
                }
            }


            serializedObject.ApplyModifiedProperties();
        }
    }
}