﻿using UnityEngine;
using UnityEditor;

namespace DynamicLevel
{
    [CustomEditor(typeof(DynamicLevelGenerator))]
    public class DynamicLevelGeneratorEditor : Editor
    {
        private bool mShowFrames = false;
        private bool[] mShowLayers;

        public override void OnInspectorGUI()
        {
            SerializedProperty levelHolderProp = serializedObject.FindProperty("mLevelHolder");
            SerializedProperty startFrameProp = serializedObject.FindProperty("mStartFrame");
            SerializedProperty frameProps = serializedObject.FindProperty("mFrames");
            SerializedProperty frameCountProps = serializedObject.FindProperty("mFrameCounts");
            SerializedProperty layerInfoProps = serializedObject.FindProperty("mLayerInfo");
            SerializedProperty activeDepthProp = serializedObject.FindProperty("mActiveDepth");
            SerializedProperty inactiveDepthProp = serializedObject.FindProperty("mInactiveDepth");
            SerializedProperty playerTransformProp = serializedObject.FindProperty("mPlayerTransform");

            levelHolderProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Level Holder", "생성될 레벨의 부모 오브젝트")
                , levelHolderProp.objectReferenceValue, typeof(Transform), true);
            playerTransformProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Player Transform", "레벨 생성의 기준이 될 Player Transform")
                , playerTransformProp.objectReferenceValue, typeof(Transform), true);

            EditorGUILayout.Space();

            startFrameProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Start Level Frame", "레벨 생성의 시작점이 되는 Level Frame")
                , startFrameProp.objectReferenceValue, typeof(DynamicLevelFrame), true);
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Active Depth", "플레이어를 기준으로 생성되는 레벨의 깊이"), GUILayout.MaxWidth(80));
            activeDepthProp.intValue = EditorGUILayout.IntField(activeDepthProp.intValue);
            activeDepthProp.intValue = Mathf.Max(activeDepthProp.intValue, 1);
            EditorGUILayout.LabelField(new GUIContent("Inactive Depth", "플레이어를 기준으로 비활성화 되는 레벨의 깊이"), GUILayout.MaxWidth(100));
            inactiveDepthProp.intValue = EditorGUILayout.IntField(inactiveDepthProp.intValue);
            inactiveDepthProp.intValue = Mathf.Max(inactiveDepthProp.intValue, activeDepthProp.intValue + 1);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(new GUIContent("Frame Count", "생성될 수 있는 Level Frame Count"), GUILayout.MaxWidth(80));
            int countOfFrames = EditorGUILayout.IntField(frameProps.arraySize, GUILayout.MaxWidth(50));
            countOfFrames = Mathf.Max(countOfFrames, 0);
            if (frameProps.arraySize != countOfFrames || frameCountProps.arraySize != countOfFrames)
            {
                frameProps.arraySize = countOfFrames;
                frameCountProps.arraySize = countOfFrames;
                serializedObject.ApplyModifiedProperties();
            }

            EditorGUILayout.LabelField(new GUIContent("Show Frames"), GUILayout.MaxWidth(80));
            mShowFrames = EditorGUILayout.Toggle(mShowFrames);
            EditorGUILayout.EndHorizontal();

            if (mShowFrames)
            {
                for (int indexOfFrame = 0; indexOfFrame < countOfFrames; indexOfFrame++)
                {
                    SerializedProperty frameProp = frameProps.GetArrayElementAtIndex(indexOfFrame);
                    SerializedProperty frameCountProp = frameCountProps.GetArrayElementAtIndex(indexOfFrame);

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent(string.Format("Frame [{0}]", indexOfFrame)), GUILayout.Width(80));
                    frameProp.objectReferenceValue = EditorGUILayout.ObjectField(frameProp.objectReferenceValue, typeof(DynamicLevelFrame), true);
                    EditorGUILayout.LabelField(new GUIContent("Count"), GUILayout.Width(40));
                    frameCountProp.intValue = EditorGUILayout.IntField(frameCountProp.intValue, GUILayout.MaxWidth(50));
                    frameCountProp.intValue = Mathf.Max(frameCountProp.intValue, 1);
                    EditorGUILayout.EndHorizontal();
                }
            }

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Layer Depth", "생성될 레이어의 총 계층 수"), GUILayout.MaxWidth(80));
            int layerDepth = EditorGUILayout.IntField(layerInfoProps.arraySize, GUILayout.MaxWidth(50));
            layerDepth = Mathf.Max(layerDepth, 0);
            if (layerInfoProps.arraySize != layerDepth)
            {
                layerInfoProps.arraySize = layerDepth;
                serializedObject.ApplyModifiedProperties();
            }
            EditorGUILayout.EndHorizontal();

            if(mShowLayers == null)
            {
                mShowLayers = new bool[layerDepth];
            }
            else if(mShowLayers.Length != layerDepth)
            {
                bool[] newShowLayers = new bool[layerDepth];
                for (int indexOfDepth = 0; indexOfDepth < layerDepth && indexOfDepth < mShowLayers.Length; indexOfDepth++)
                {
                    newShowLayers[indexOfDepth] = mShowLayers[indexOfDepth];
                }
                mShowLayers = newShowLayers;
            }

            for (int indexOfDepth = 0; indexOfDepth < layerDepth; indexOfDepth++)
            {
                SerializedProperty layerInfoProp = layerInfoProps.GetArrayElementAtIndex(indexOfDepth);
                SerializedProperty layerProps = layerInfoProp.FindPropertyRelative("Layers");
                SerializedProperty layerCountProps = layerInfoProp.FindPropertyRelative("LayerCounts");

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent(string.Format("[{0}]. Layer Count", indexOfDepth)), GUILayout.MaxWidth(100));
                int countOfLayers = EditorGUILayout.IntField(layerProps.arraySize, GUILayout.MaxWidth(50));
                countOfLayers = Mathf.Max(countOfLayers, 0);
                if (layerProps.arraySize != countOfLayers || layerCountProps.arraySize != countOfLayers)
                {
                    layerProps.arraySize = countOfLayers;
                    layerCountProps.arraySize = countOfLayers;
                    serializedObject.ApplyModifiedProperties();
                }
                EditorGUILayout.LabelField(new GUIContent("Show Layers"), GUILayout.MaxWidth(80));
                mShowLayers[indexOfDepth] = EditorGUILayout.Toggle(mShowLayers[indexOfDepth]);
                EditorGUILayout.EndHorizontal();

                if(mShowLayers[indexOfDepth])
                {
                    for (int indexOfLayer = 0; indexOfLayer < countOfLayers; indexOfLayer++)
                    {
                        SerializedProperty layerProp = layerProps.GetArrayElementAtIndex(indexOfLayer);
                        SerializedProperty layerCountProp = layerCountProps.GetArrayElementAtIndex(indexOfLayer);

                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField(new GUIContent(string.Format("Layer [{0}]", indexOfLayer)), GUILayout.Width(80));
                        layerProp.objectReferenceValue = EditorGUILayout.ObjectField(layerProp.objectReferenceValue, typeof(DynamicLevelLayer), true);
                        EditorGUILayout.LabelField(new GUIContent("Count"), GUILayout.Width(40));
                        layerCountProp.intValue = EditorGUILayout.IntField(layerCountProp.intValue, GUILayout.MaxWidth(50));
                        layerCountProp.intValue = Mathf.Max(layerCountProp.intValue, 1);
                        EditorGUILayout.EndHorizontal();
                    }
                }

                EditorGUILayout.Space();
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}