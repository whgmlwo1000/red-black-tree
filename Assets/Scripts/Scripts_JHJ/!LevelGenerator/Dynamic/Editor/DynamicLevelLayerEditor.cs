﻿using UnityEngine;
using UnityEditor;

namespace DynamicLevel
{
    [CustomEditor(typeof(DynamicLevelLayer))]
    public class DynamicLevelLayerEditor : Editor
    {
        private void OnSceneGUI()
        {
            DynamicLevelLayer layer = target as DynamicLevelLayer;

            if (layer.LeftTopAnchor != null)
            {
                Vector3 leftTopAnchorPosition = layer.LeftTopAnchor.position;
                float scale = 1.0f;
                Handles.TransformHandle(ref leftTopAnchorPosition, Quaternion.identity, ref scale);
                layer.LeftTopAnchor.position = leftTopAnchorPosition;

                Handles.Label(leftTopAnchorPosition, new GUIContent("Left Top Anchor"));
            }
        }

        public override void OnInspectorGUI()
        {
            SerializedProperty frameTypeProps = serializedObject.FindProperty("mFrameTypes");
            SerializedProperty stateTypeProps = serializedObject.FindProperty("mStateTypes");
            SerializedProperty weightProp = serializedObject.FindProperty("mWeight");
            SerializedProperty leftTopAnchorProp = serializedObject.FindProperty("mLeftTopAnchor");
            SerializedProperty socketBlockerProps = serializedObject.FindProperty("mBlockers");

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(new GUIContent("Weight"), GUILayout.MaxWidth(50));
            weightProp.floatValue = EditorGUILayout.FloatField(weightProp.floatValue);
            weightProp.floatValue = Mathf.Max(weightProp.floatValue, 0.0f);

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(new GUIContent("Frame Type Count"), GUILayout.MaxWidth(110));
            int countOfFrameTypes = EditorGUILayout.IntField(frameTypeProps.arraySize);
            countOfFrameTypes = Mathf.Max(countOfFrameTypes, 0);
            if(frameTypeProps.arraySize != countOfFrameTypes)
            {
                frameTypeProps.arraySize = countOfFrameTypes;
                serializedObject.ApplyModifiedProperties();
            }

            EditorGUILayout.EndHorizontal();

            for(int indexOfFrameType = 0; indexOfFrameType < countOfFrameTypes; indexOfFrameType++)
            {
                EditorGUILayout.BeginHorizontal();

                SerializedProperty frameTypeProp = frameTypeProps.GetArrayElementAtIndex(indexOfFrameType);
                EditorGUILayout.LabelField(new GUIContent(string.Format("[{0}]. Frame Type", indexOfFrameType)), GUILayout.MaxWidth(110));
                frameTypeProp.intValue = EditorGUILayout.IntField(frameTypeProp.intValue);

                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.EndVertical();
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(new GUIContent("State Type Count"), GUILayout.MaxWidth(110));
            int countOfStateTypes = EditorGUILayout.IntField(stateTypeProps.arraySize);
            countOfStateTypes = Mathf.Max(countOfStateTypes, 0);
            if (stateTypeProps.arraySize != countOfStateTypes)
            {
                stateTypeProps.arraySize = countOfStateTypes;
                serializedObject.ApplyModifiedProperties();
            }

            EditorGUILayout.EndHorizontal();

            for (int indexOfStateType = 0; indexOfStateType < countOfStateTypes; indexOfStateType++)
            {
                EditorGUILayout.BeginHorizontal();

                SerializedProperty stateTypeProp = stateTypeProps.GetArrayElementAtIndex(indexOfStateType);
                EditorGUILayout.LabelField(new GUIContent(string.Format("[{0}]. State Type", indexOfStateType)), GUILayout.MaxWidth(110));
                stateTypeProp.intValue = EditorGUILayout.IntField(stateTypeProp.intValue);

                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(new GUIContent("Left Top Anchor"), GUILayout.MaxWidth(100));
            leftTopAnchorProp.objectReferenceValue = EditorGUILayout.ObjectField(leftTopAnchorProp.objectReferenceValue, typeof(Transform), true);
            if(leftTopAnchorProp.objectReferenceValue == null)
            {
                DynamicLevelLayer layer = target as DynamicLevelLayer;
                Transform leftTopAnchor = new GameObject("Left Top Anchor").transform;
                leftTopAnchor.SetParent(layer.transform);
                leftTopAnchor.localPosition = Vector3.zero;
                leftTopAnchor.localRotation = Quaternion.identity;
                leftTopAnchor.localScale = Vector3.one;
                leftTopAnchorProp.objectReferenceValue = leftTopAnchor;
                serializedObject.ApplyModifiedProperties();
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Socket Count"), GUILayout.MaxWidth(80));
            int countOfSockets = EditorGUILayout.IntField(socketBlockerProps.arraySize, GUILayout.MaxWidth(50));
            countOfSockets = Mathf.Max(countOfSockets, 0);
            if(socketBlockerProps.arraySize != countOfSockets)
            {
                socketBlockerProps.arraySize = countOfSockets;
                serializedObject.ApplyModifiedProperties();
            }
            EditorGUILayout.EndHorizontal();

            for(int indexOfSocket = 0; indexOfSocket < countOfSockets; indexOfSocket++)
            {
                SerializedProperty blockerProps = socketBlockerProps.GetArrayElementAtIndex(indexOfSocket).FindPropertyRelative("mBlockingObjects");

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent(string.Format("[{0}]. Blocker Count", indexOfSocket)), GUILayout.MaxWidth(120));
                int countOfBlockers = EditorGUILayout.IntField(blockerProps.arraySize, GUILayout.MaxWidth(50));
                countOfBlockers = Mathf.Max(countOfBlockers, 0);
                if(blockerProps.arraySize != countOfBlockers)
                {
                    blockerProps.arraySize = countOfBlockers;
                    serializedObject.ApplyModifiedProperties();
                }
                EditorGUILayout.EndHorizontal();

                for(int indexOfBlocker = 0; indexOfBlocker < countOfBlockers; indexOfBlocker++)
                {
                    SerializedProperty blockerProp = blockerProps.GetArrayElementAtIndex(indexOfBlocker);

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent(string.Format("[{0}]-[{1}]. Blocker", indexOfSocket, indexOfBlocker)), GUILayout.MaxWidth(120));
                    blockerProp.objectReferenceValue = EditorGUILayout.ObjectField(blockerProp.objectReferenceValue, typeof(GameObject), true);
                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.Space();
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}