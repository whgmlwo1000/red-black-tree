﻿using UnityEngine;
using System;

namespace StaticLevel
{

    [Serializable]
    public class LevelSocket
    {
        /// <summary>
        /// 해당 소켓이 등록된 Frame
        /// </summary>
        public LevelFrame Frame { get; set; }
        /// <summary>
        /// 해당 소켓의 태그 번호
        /// </summary>
        public int Tag { get; set; }
        /// <summary>
        /// 해당 소켓의 위치
        /// </summary>
        public Transform Transform { get { return mTransform; } }

        [SerializeField]
        private Transform mTransform = null;

        /// <summary>
        /// 해당 소켓의 방향
        /// </summary>
        public EDirection Direction { get { return mDirection; } }

        [SerializeField]
        private EDirection mDirection = EDirection.None;

        /// <summary>
        /// 해당 소켓의 타입
        /// </summary>
        public ESocketType Type { get { return mType; } }

        [SerializeField]
        private ESocketType mType = ESocketType.None;

        [SerializeField]
        private GameObject[] mBlockers = null;

        /// <summary>
        /// 해당 소켓을 대체 오브젝트로 막는 메소드
        /// </summary>
        public void Block()
        {
            for (int i = 0; i < mBlockers.Length; i++)
            {
                if (mBlockers[i].activeInHierarchy)
                {
                    mBlockers[i].SetActive(false);
                }
                else
                {
                    mBlockers[i].SetActive(true);
                }
            }
        }
    }
}
