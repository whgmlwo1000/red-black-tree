﻿using System;
using System.Collections.Generic;

namespace RedBlackTree
{
    public class StateBase<E> : Entity
    {
        /// <summary>
        /// 현재 State를 설정하거나 반환하는 프로퍼티
        /// </summary>
        public E State
        {
            get
            {
                return mState.Type;
            }
            set
            {
                State<E> prevState = mState;
                mState = mStateDict[value];
                if (!mComparer.Equals(prevState.Type, value))
                {
                    PrevState = prevState.Type;
                    prevState.End();
                }
                mState.Start();
            }
        }

        public E PrevState { get; private set; }

        private State<E> mState;

        private Dictionary<E, State<E>> mStateDict;

        private EqualityComparer<E> mComparer;

        /// <summary>
        /// 일련의 상태 객체들을 설정하는 메소드
        /// </summary>
        /// <param name="states"></param>
        public void SetStates(params State<E>[] states)
        {
            mState = states[0];
            for (int i = 0; i < states.Length; i++)
            {
                mStateDict.Add(states[i].Type, states[i]);
            }
        }

        public void SetState(State<E> state)
        {
            if(!mStateDict.ContainsKey(state.Type))
            {
                mStateDict.Add(state.Type, state);
            }
            else
            {
                mStateDict[state.Type] = state;
            }
        }

        public virtual void Awake()
        {
            mStateDict = new Dictionary<E, State<E>>();
            mComparer = EqualityComparer<E>.Default;
        }

        public virtual void Start()
        {
            mState?.Start();
        }

        public virtual void Update()
        {
            mState?.Update();
        }

        public virtual void FixedUpdate()
        {
            mState?.FixedUpdate();
        }
    }
}