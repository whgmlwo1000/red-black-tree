﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace RedBlackTree
{
    public abstract class State<E> 
    {
        /// <summary>
        /// 현재 State의 플래그 프로퍼티
        /// </summary>
        public E Type { get; private set; }

        /// <summary>
        /// 현재 State의 플래그를 설정하는 생성자
        /// </summary>
        /// <param name="type"></param>
        public State(E type)
        {
            Type = type;
        }

        /// <summary>
        /// 행동 조건 검사 후 준비 메소드
        /// </summary>
        public virtual void Start() { }
        /// <summary>
        /// 행동 실행 메소드
        /// </summary>
        public virtual void Update() { }
        /// <summary>
        /// 행동 실행 메소드
        /// </summary>
        public virtual void FixedUpdate() { }
        /// <summary>
        /// 행동 마무리 메소드
        /// </summary>
        public virtual void End() { }

        /// <summary>
        /// 현재 상태의 플래그 변수를 string 형태로 반환하는 메소드
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Type.ToString();
        }
    }
}