﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolygonColliderAssistant : MonoBehaviour
{
    [SerializeField]
    private Transform[] mPointTransforms = null;
    private Vector2[] mPoints;
    private PolygonCollider2D mCollider;

    private void OnDrawGizmosSelected()
    {
        mCollider = GetComponent<PolygonCollider2D>();
        if (mCollider != null && mPointTransforms != null)
        {
            mCollider.points = new Vector2[mPointTransforms.Length];
            mPoints = new Vector2[mPointTransforms.Length];
            for(int indexOfPoint = 0; indexOfPoint < mPointTransforms.Length; indexOfPoint++)
            {
                if(mPointTransforms[indexOfPoint] != null)
                {
                    mPoints[indexOfPoint] = mPointTransforms[indexOfPoint].localPosition;
                }
            }

            mCollider.SetPath(0, mPoints);
        }
    }

}
