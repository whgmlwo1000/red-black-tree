﻿using System;
using UnityEngine;

namespace ObjectManagement
{
    public interface IHandleable<EType>
    {
        EType Type { get; }
    }

    public interface IPoolable<EType> : IHandleable<EType>
    {
        bool IsWorldCanvas { get; }
        bool IsPooled { get; }
        GameObject gameObject { get; }
    }
}