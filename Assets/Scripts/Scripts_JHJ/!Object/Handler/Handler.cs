﻿using UnityEngine;
using System.Collections.Generic;


namespace ObjectManagement
{
    /// <summary>
    /// 자식 오브젝트를 태그를 지정해서 관리하고 반환해주는 클래스
    /// </summary>
    public class Handler<E, T> : MonoBehaviour where T : IHandleable<E>
    {
        private Dictionary<E, T> mObjectDict;

        public T[] Objects { get; private set; }

        public int Count
        {
            get { return mObjectDict.Count; }
        }

        public T Get(E objectType)
        {
            if (mObjectDict.ContainsKey(objectType))
            {
                return mObjectDict[objectType];
            }
            return default;
        }

        public Dictionary<E, T>.Enumerator GetEnumerator()
        {
            return mObjectDict.GetEnumerator();
        }

        public virtual bool CanHandle(T childObject)
        {
            return true;
        }

        public virtual void Awake()
        {
            mObjectDict = new Dictionary<E, T>();
            Objects = GetComponentsInChildren<T>(true);

            for(int i=0; i< Objects.Length; i++)
            {
                if (CanHandle(Objects[i]))
                {
                    mObjectDict.Add(Objects[i].Type, Objects[i]);
                }
            }
        }
    }
}