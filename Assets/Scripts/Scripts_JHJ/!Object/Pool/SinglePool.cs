﻿using UnityEngine;

namespace ObjectManagement
{
    public class SinglePool<T> : MonoBehaviour where T : MonoBehaviour
    {
        [SerializeField]
        private GameObject mPrefab = null;
        [SerializeField]
        private int mCount = 0;

        private T[] mObjectPool;
        private int mIndexOfPool;

        public void Awake()
        {
            mObjectPool = new T[mCount];

            for(int indexOfCount = 0; indexOfCount < mCount; indexOfCount++)
            {
                GameObject instObject = Instantiate(mPrefab, Vector3.zero, Quaternion.identity, transform);
                mObjectPool[indexOfCount] = instObject.GetComponent<T>();
            }
        }

        public T Get()
        {
            T nextObject = default;

            for (int i = 0; i < mObjectPool.Length; i++)
            {
                int curIndex = mIndexOfPool++;
                mIndexOfPool = (mIndexOfPool < mObjectPool.Length ? mIndexOfPool : 0);
                if (!mObjectPool[curIndex].gameObject.activeSelf)
                {
                    nextObject = mObjectPool[curIndex];
                    break;
                }
            }
            return nextObject;
        }



    }
}