﻿using UnityEngine;

namespace ObjectManagement
{
    public class WorldCanvasPool : MonoBehaviour
    {
        public static WorldCanvasPool Main { get; private set; } = null;

        private Canvas mCanvas;

        [SerializeField]
        private Camera mWorldCamera = null;

        public void Awake()
        {
            if (Main == null)
            {
                DontDestroyOnLoad(gameObject);
                Main = this;
                mCanvas = GetComponent<Canvas>();
                mCanvas.worldCamera = mWorldCamera;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}