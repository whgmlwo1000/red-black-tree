﻿using UnityEngine;
using UnityEditor;

namespace ObjectManagement
{
    public class PoolEditor<E> : Editor
    {
        private const string PrefabObject = "{0}. Prefab";
        private const string PrefabObjectIn = "{0}. {1}";

        public sealed override void OnInspectorGUI()
        {
            SetProperties();
            serializedObject.ApplyModifiedProperties();
        }

        public virtual void SetProperties()
        {
            SerializedProperty prefabProps = serializedObject.FindProperty("mPrefabs");
            SerializedProperty countOfObjectsProps = serializedObject.FindProperty("mCountsOfObjects");

            int count = EditorGUILayout.IntField(new GUIContent("Count Of Pools", "생성할 풀의 수"), prefabProps.arraySize);
            if(prefabProps.arraySize != count || countOfObjectsProps.arraySize != count)
            {
                prefabProps.arraySize = count;
                countOfObjectsProps.arraySize = count;
                serializedObject.ApplyModifiedProperties();
            }

            EditorGUILayout.Space();

            for(int indexOfProp = 0; indexOfProp < prefabProps.arraySize; indexOfProp++)
            {
                SerializedProperty prefabProp = prefabProps.GetArrayElementAtIndex(indexOfProp);
                SerializedProperty countOfObjectsProp = countOfObjectsProps.GetArrayElementAtIndex(indexOfProp);

                EditorGUILayout.BeginHorizontal();

                if (prefabProp.objectReferenceValue != null)
                {
                    GameObject gameObject = prefabProp.objectReferenceValue as GameObject;
                    IPoolable<E> poolable = gameObject.GetComponent<IPoolable<E>>();
                    prefabProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent(string.Format(PrefabObjectIn, indexOfProp + 1, poolable.Type))
                        , prefabProp.objectReferenceValue, typeof(GameObject), true);
                }
                else
                {
                    prefabProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent(string.Format(PrefabObject, indexOfProp + 1))
                        , prefabProp.objectReferenceValue, typeof(GameObject), true);
                }

                countOfObjectsProp.intValue = EditorGUILayout.IntField(countOfObjectsProp.intValue, GUILayout.MaxWidth(50));
                countOfObjectsProp.intValue = Mathf.Max(countOfObjectsProp.intValue, 0);

                EditorGUILayout.EndHorizontal();
            }
        }
    }
}