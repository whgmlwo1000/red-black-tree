﻿using UnityEngine;
using System.Collections.Generic;
using System;
using RedBlackTree;

namespace ArchSpirit
{
    public static class StateManager
    {
        public static StateHandler<string> Language { get; private set; }
        public static StateHandler<bool> Pause { get; private set; }
        public static StateHandler<bool> GameOver { get; private set; }
        public static StateHandler<EFontType> Font { get; private set; }

        public static SceneAsyncLoader.SceneStateHandler Scene { get; private set; }
        public static StateHandler<bool> Vibration { get; private set; }

        static StateManager()
        {
            Language = new StateHandler<string>();
            Pause = new StateHandler<bool>();
            GameOver = new StateHandler<bool>();
            Font = new StateHandler<EFontType>();
            Scene = new SceneAsyncLoader.SceneStateHandler();
            Scene.State = EScene.None;

            Vibration = new StateHandler<bool>();
            Vibration.State = true;

            Language.State = "Korean";

            Pause.State = false;

            Pause.AddEnterEvent(true, OnPauseEnter);
            Pause.AddExitEvent(true, OnPauseExit);
        }

        private static void OnPauseEnter()
        {
            Time.timeScale = 0.0f;
        }
        private static void OnPauseExit()
        {
            Time.timeScale = 1.0f;
        }
    }

    public class StateHandler<E>
    {
        /// <summary>
        /// 상태 프로퍼티
        /// </summary>
        public E State
        {
            get { return mCurState; }
            set
            {
                // 기존의 상태값과 다르다면
                if (!mComparer.Equals(value, mCurState))
                {
                    // 기존의 상태값 저장
                    PrevState = mCurState;
                    // 현재 상태값 변경
                    mCurState = value;

                    // 이전 상태값이 이벤트 객체로 등록되어 있다면
                    if (PrevState != null && mEventDict.ContainsKey(PrevState))
                    {
                        mEventDict[PrevState].Exit();
                    }
                    // 변경된 상태값이 이벤트 객체로 등록되어 있다면
                    if (mEventDict.ContainsKey(mCurState))
                    {
                        mEventDict[mCurState].Enter();
                    }
                }
            }
        }
        /// <summary>
        /// 이전 상태 프로퍼티
        /// </summary>
        public E PrevState { get; private set; }

        private E mCurState;
        private Dictionary<E, Event> mEventDict;

        private EqualityComparer<E> mComparer;

        public StateHandler()
        {
            mEventDict = new Dictionary<E, Event>();
            mComparer = EqualityComparer<E>.Default;
        }

        /// <summary>
        /// State 진입 이벤트 추가
        /// </summary>
        /// <param name="e"></param>
        /// <param name="action"></param>
        public void AddEnterEvent(E e, Action action)
        {
            if (!mEventDict.ContainsKey(e))
            {
                mEventDict.Add(e, new Event());
            }

            mEventDict[e].OnEnter += action;
        }
        /// <summary>
        /// State 탈출 이벤트 추가
        /// </summary>
        /// <param name="e"></param>
        /// <param name="action"></param>
        public void AddExitEvent(E e, Action action)
        {
            if (!mEventDict.ContainsKey(e))
            {
                mEventDict.Add(e, new Event());
            }

            mEventDict[e].OnExit += action;
        }
        /// <summary>
        /// State 진입 이벤트 제거
        /// </summary>
        /// <param name="e"></param>
        /// <param name="action"></param>
        public void RemoveEnterEvent(E e, Action action)
        {
            mEventDict[e].OnEnter -= action;
        }
        /// <summary>
        /// State 탈출 이벤트 제거
        /// </summary>
        /// <param name="e"></param>
        /// <param name="action"></param>
        public void RemoveExitEvent(E e, Action action)
        {
            mEventDict[e].OnExit -= action;
        }

        /// <summary>
        /// 이벤트 메소드 관리 클래스
        /// </summary>
        private class Event
        {
            public event Action OnEnter;
            public event Action OnExit;

            public void Enter()
            {
                OnEnter?.Invoke();
            }
            public void Exit()
            {
                OnExit?.Invoke();
            }
        }
    }
}