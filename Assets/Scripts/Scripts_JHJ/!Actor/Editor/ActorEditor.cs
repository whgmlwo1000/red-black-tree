﻿using UnityEngine;
using UnityEditor;

namespace ArchSpirit
{
    [CustomEditor(typeof(Actor))]
    public class ActorEditor : Editor
    {
        public sealed override void OnInspectorGUI()
        {
            SetProperties();
            serializedObject.ApplyModifiedProperties();
        }

        public virtual Transform SetProperties()
        {
            Actor actor = target as Actor;

            SerializedProperty mainTransformProp = serializedObject.FindProperty("mMainTransform");
            SerializedProperty mainRendererProp = serializedObject.FindProperty("mMainRendererHandler");
            SerializedProperty horizontalDirectionProp = serializedObject.FindProperty("mHorizontalDirection");
            SerializedProperty verticalFlipProp = serializedObject.FindProperty("mVerticalFlip");
            SerializedProperty angleProp = serializedObject.FindProperty("mAngle");

            Transform mainTransform = mainTransformProp.objectReferenceValue as Transform;
            mainTransformProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Main Transform", "Rotation, Scale의 적용 대상이 되는 오브젝트"), mainTransformProp.objectReferenceValue, typeof(Transform), true);

            if (mainTransform != null)
            {
                MainRendererHandler mainRendererHandler = mainTransform.GetComponentInChildren<MainRendererHandler>(true);
                if(mainRendererHandler != null)
                {
                    mainRendererProp.objectReferenceValue = mainRendererHandler;
                }
                else
                {
                    GameObject mainRendererObject = new GameObject("Main Renderers", typeof(MainRendererHandler));
                    mainRendererObject.transform.eulerAngles = Vector3.zero;
                    mainRendererObject.transform.localScale = Vector3.one;
                    mainRendererObject.transform.SetParent(mainTransform);
                    mainRendererObject.transform.localPosition = Vector3.zero;
                    mainRendererProp.objectReferenceValue = mainRendererObject.GetComponent<MainRendererHandler>();
                }

                actor.HorizontalDirection = (EHorizontalDirection)EditorGUILayout.EnumPopup(new GUIContent("Horizontal Direction", "수평 방향"), actor.HorizontalDirection);
                horizontalDirectionProp.intValue = (int)actor.HorizontalDirection;
                actor.VerticalFlip = EditorGUILayout.Toggle(new GUIContent("Vertical Flip", "수직 반전"), actor.VerticalFlip);
                verticalFlipProp.boolValue = actor.VerticalFlip;
                actor.Angle = EditorGUILayout.FloatField(new GUIContent("Angle", "각도"), actor.Angle);
                angleProp.floatValue = actor.Angle;
            }

            return mainTransform;
        }
    }
}