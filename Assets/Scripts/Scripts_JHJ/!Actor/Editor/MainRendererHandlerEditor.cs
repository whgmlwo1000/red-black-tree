﻿using UnityEngine;
using UnityEditor;

namespace ArchSpirit
{
    [CustomEditor(typeof(MainRendererHandler))]
    public class MainRendererHandlerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            MainRendererHandler handler = target as MainRendererHandler;

            if (GUILayout.Button("Make Main Renderer !"))
            {
                int countOfHandler = handler.GetComponentsInChildren<MainRenderer>(true).Length;

                Transform mainRendererTransform = new GameObject(string.Format("New Main Renderer {0}", countOfHandler), typeof(MainRenderer)).transform;
                mainRendererTransform.SetParent(handler.transform);
                mainRendererTransform.localPosition = Vector3.zero;
                mainRendererTransform.localRotation = Quaternion.identity;
                mainRendererTransform.localScale = Vector3.one;
            }
        }
    }
}