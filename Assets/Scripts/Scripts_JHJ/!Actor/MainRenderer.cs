﻿using UnityEngine;
using ObjectManagement;

namespace ArchSpirit
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class MainRenderer : MonoBehaviour, IHandleable<string>
    {
        [SerializeField]
        private string mType = null;
        public string Type { get { return mType; } }

        public SpriteRenderer Renderer { get; private set; }

        private void Awake()
        {
            Renderer = GetComponent<SpriteRenderer>();
        }
    }
}