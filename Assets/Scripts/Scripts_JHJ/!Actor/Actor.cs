﻿using UnityEngine;
using System.Collections.Generic;

namespace ArchSpirit
{
    [RequireComponent(typeof(Animator))]
    public class Actor : StateBase<string>
    {
        public Animator MainAnimator { get; private set; }
        private Dictionary<string, int> mAnimationDict;

        private string mAnimationState = null;
        public string AnimationState
        {
            get { return mAnimationState; }
            set { mAnimationState = value; MainAnimator.SetInteger("state", mAnimationDict[mAnimationState]); }
        }

        [SerializeField]
        private Transform mMainTransform = null;
        public Transform MainTransform { get { return mMainTransform; } }

        [SerializeField]
        private MainRendererHandler mMainRendererHandler = null;
        public MainRendererHandler MainRendererHandler { get { return mMainRendererHandler; } }

        [SerializeField]
        private EHorizontalDirection mHorizontalDirection = EHorizontalDirection.Right;
        /// <summary>
        /// 수평 방향
        /// </summary>
        public EHorizontalDirection HorizontalDirection
        {
            get { return mHorizontalDirection; }
            set
            {
                if (mHorizontalDirection != value)
                {
                    if (!mIsAngleSet)
                    {
                        if (mAngle >= 0.0f)
                        {
                            mAngle = 180.0f - mAngle;
                        }
                        else
                        {
                            mAngle = -180.0f - mAngle;
                        }
                    }
                    else
                    {
                        mIsAngleSet = false;
                    }

                    mHorizontalDirection = value;
                    Vector3 localScale = mMainTransform.localScale;
                    mMainTransform.localScale = new Vector3(-localScale.x, localScale.y, localScale.z);
                }
                else if (mIsAngleSet)
                {
                    mIsAngleSet = false;
                }
            }
        }

        [SerializeField]
        private bool mVerticalFlip = false;
        /// <summary>
        /// 수직 반전
        /// </summary>
        public bool VerticalFlip
        {
            get { return mVerticalFlip; }
            set
            {
                if (mVerticalFlip != value)
                {
                    mVerticalFlip = value;
                    Vector3 localScale = mMainTransform.localScale;
                    mMainTransform.localScale = new Vector3(localScale.x, -localScale.y, localScale.z);
                }
            }
        }
        /// <summary>
        /// 각도와 일치하는 방향
        /// </summary>
        public Vector2 Direction { get { return new Vector2(Mathf.Cos(mAngle * Mathf.Deg2Rad), Mathf.Sin(mAngle * Mathf.Deg2Rad)); } }

        private bool mIsAngleSet;
        [SerializeField]
        private float mAngle = 0.0f;
        /// <summary>
        /// -180~180 도 사이의 각도를 반환하거나 이보다 큰 각도로 대입시 변환시키는 프로퍼티
        /// </summary>
        public float Angle
        {
            get { return mAngle; }
            set
            {
                mIsAngleSet = true;
                while (value > 180.0f)
                {
                    value -= 360.0f;
                }
                while (value <= -180.0f)
                {
                    value += 360.0f;
                }

                mAngle = value;

                // 오른쪽을 보고있는 경우
                if (mAngle >= -90.0f && mAngle <= 90.0f)
                {
                    HorizontalDirection = EHorizontalDirection.Right;
                    mMainTransform.eulerAngles = new Vector3(0.0f, 0.0f, mAngle);
                }
                // 왼쪽을 보고있는 경우
                else
                {
                    HorizontalDirection = EHorizontalDirection.Left;
                    mMainTransform.eulerAngles = new Vector3(0.0f, 0.0f, mAngle - 180.0f);
                }
            }
        }

        public override void Awake()
        {
            base.Awake();

            Vector2 lossyScale = mMainTransform.lossyScale;
            if (lossyScale.x > 0.0f)
            {
                mHorizontalDirection = EHorizontalDirection.Right;
            }
            else
            {
                mHorizontalDirection = EHorizontalDirection.Left;
            }

            if (lossyScale.y > 0.0f)
            {
                mVerticalFlip = false;
            }
            else
            {
                mVerticalFlip = true;
            }

            MainAnimator = GetComponent<Animator>();
            mAnimationDict = new Dictionary<string, int>();
        }

        public override void Update()
        {
            if (!StateManager.Pause.State)
            {
                base.Update();
            }
        }

        public override void FixedUpdate()
        {
            if (!StateManager.Pause.State)
            {
                base.FixedUpdate();
            }
        }

        /// <summary>
        /// 현재 객체 기준으로 해당 방향으로 각도를 설정하는 메소드
        /// </summary>
        public void SetDirection(Vector2 direction, bool isNormalized)
        {
            if (!isNormalized)
            {
                direction.Normalize();
            }

            // 윗방향으로 움직인 경우
            if (direction.y >= -Mathf.Epsilon)
            {
                Angle = Mathf.Acos(direction.x) * Mathf.Rad2Deg;
            }
            // 아랫 방향으로 움직인 경우
            else
            {
                Angle = -Mathf.Acos(direction.x) * Mathf.Rad2Deg;
            }
        }

        public void AddAnimationState(string state, int animationState)
        {
            mAnimationDict.Add(state, animationState);
        }

        public void SetAnimationState(string state, int animationState)
        {
            mAnimationDict[state] = animationState;
        }
    }
}