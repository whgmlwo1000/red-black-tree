﻿using UnityEngine;

namespace ArchSpirit
{
    public enum EPhysicalMode
    {
        OnGround, 
        InAir
    }

    [RequireComponent(typeof(Rigidbody2D))]
    public class PhysicalActor : Actor
    {
        public static PhysicsMaterial2D ZeroFrictionMaterial { get; private set; }

        [SerializeField]
        private Transform mCenterTransform = null;
        public Transform CenterTransform { get { return mCenterTransform; } }

        [SerializeField]
        private MainColliderHandler mMainColliderHandler = null;
        public MainColliderHandler MainColliderHandler { get { return mMainColliderHandler; } }

        public Rigidbody2D MainRigidbody { get; private set; }

        private PhysicsMaterial2D mMainPhysicsMaterial;
        public PhysicsMaterial2D MainPhysicsMateial
        {
            get { return mMainPhysicsMaterial; }
            set
            {
                mMainPhysicsMaterial = value;
                if(!IsMoving)
                {
                    MainRigidbody.sharedMaterial = mMainPhysicsMaterial;
                }
            }
        }

        public bool IsMoving { get; private set; }

        [SerializeField]
        private float mMaxSpeed = 10.0f;
        public float MaxSpeed
        {
            get { return mMaxSpeed; }
            set { mMaxSpeed = Mathf.Abs(value); }
        }

        [SerializeField]
        private float mTimeCloseToMaxSpeed = 0.5f;
        public float TimeCloseToMaxSpeed
        {
            get { return mTimeCloseToMaxSpeed; }
            set  { mTimeCloseToMaxSpeed = Mathf.Max(value, Time.fixedDeltaTime); }
        }

        [SerializeField]
        private EPhysicalMode mPhysicalMode = EPhysicalMode.OnGround;
        public EPhysicalMode PhysicalMode
        {
            get { return mPhysicalMode; }
            set
            {
                if(mPhysicalMode != value)
                {
                    mPhysicalMode = value;
                    if(PhysicalMode == EPhysicalMode.InAir)
                    {
                        MainRigidbody.gravityScale = 0.0f;
                    }
                    else if(PhysicalMode == EPhysicalMode.OnGround)
                    {
                        MainRigidbody.gravityScale = 1.0f;
                    }
                }
            }
        }

        /// <summary>
        /// 이동을 위한 추진력
        /// </summary>
        public float DrivingForce
        {
            get { return MaxSpeed * Time.fixedDeltaTime * MainRigidbody.mass / TimeCloseToMaxSpeed; }
        }

        private Vector2 mPrevVelocity = Vector2.zero;
        /// <summary>
        /// 추진력을 제어하기 위한 가상 저항력
        /// </summary>
        public Vector2 VirtualResistance
        {
            get { return -mPrevVelocity * DrivingForce / MaxSpeed; }
        }

        public override void Awake()
        {
            base.Awake();
            MainRigidbody = GetComponent<Rigidbody2D>();
            if(ZeroFrictionMaterial == null)
            {
                ZeroFrictionMaterial = Resources.Load<PhysicsMaterial2D>("Common/PhysicsMaterial2D_ZeroFriction");
            }

            if (PhysicalMode == EPhysicalMode.InAir)
            {
                MainRigidbody.gravityScale = 0.0f;
            }
            else if (PhysicalMode == EPhysicalMode.OnGround)
            {
                MainRigidbody.gravityScale = 1.0f;
            }
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();

            if (!StateManager.Pause.State)
            {
                if(mPhysicalMode == EPhysicalMode.InAir)
                {
                    MainRigidbody.AddForce(VirtualResistance, ForceMode2D.Impulse);
                }
                else if(mPhysicalMode == EPhysicalMode.OnGround && IsMoving)
                {
                    MainRigidbody.AddForce(new Vector2(VirtualResistance.x, 0.0f), ForceMode2D.Impulse);
                }
                mPrevVelocity = MainRigidbody.velocity;

                // 속도 설정이 호출된 경우
                if (IsMoving)
                {
                    // 이번 Fixed Update의 마찰력을 0으로 만듬
                    IsMoving = false;
                    if (MainRigidbody.sharedMaterial != ZeroFrictionMaterial)
                    {
                        MainRigidbody.sharedMaterial = ZeroFrictionMaterial;
                    }
                }
                // 속도 설정 후의 FixedUpdate 다음 FixedUpdate인 경우
                else if (MainRigidbody.sharedMaterial == ZeroFrictionMaterial)
                {
                    // PhysicsMaterial을 원상태로 되돌림
                    MainRigidbody.sharedMaterial = MainPhysicsMateial;
                }
            }
        }

        public void Move(Vector2 direction)
        {
            if (!Mathf.Approximately(direction.x, 0.0f) || !Mathf.Approximately(direction.y, 0.0f))
            {
                IsMoving = true;
                MainRigidbody.AddForce(direction * DrivingForce, ForceMode2D.Impulse);
            }
        }

        public void MoveRelative(Vector2 relativeDireciton)
        {
            if (!Mathf.Approximately(relativeDireciton.x, 0.0f) || !Mathf.Approximately(relativeDireciton.y, 0.0f))
            {
                IsMoving = true;
                MainRigidbody.AddRelativeForce(relativeDireciton * DrivingForce, ForceMode2D.Impulse);
            }
        }
    }
}