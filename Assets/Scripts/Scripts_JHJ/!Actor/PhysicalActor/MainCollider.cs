﻿using UnityEngine;
using ObjectManagement;

namespace ArchSpirit
{
    public class MainCollider : MonoBehaviour, IHandleable<string>
    {
        [SerializeField]
        private string mType = null;
        public string Type { get { return mType; } }

        public Collider2D Collider { get; private set; }

        public void Awake()
        {
            Collider = GetComponent<Collider2D>();
        }
    }
}