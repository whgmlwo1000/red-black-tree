﻿using UnityEngine;
using UnityEditor;

namespace ArchSpirit
{
    [CustomEditor(typeof(MainColliderHandler))]
    public class MainColliderHandlerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            MainColliderHandler handler = target as MainColliderHandler;
            if (GUILayout.Button("Make Main Collider !"))
            {
                int countOfMainColliders = handler.GetComponentsInChildren<MainCollider>(true).Length;
                Transform mainColliderTransform = new GameObject(string.Format("New Main Collider {0}", countOfMainColliders), typeof(MainCollider)).transform;
                mainColliderTransform.SetParent(handler.transform);
                mainColliderTransform.localPosition = Vector3.zero;
                mainColliderTransform.localRotation = Quaternion.identity;
                mainColliderTransform.localScale = Vector3.one;
            }
        }
    }
}