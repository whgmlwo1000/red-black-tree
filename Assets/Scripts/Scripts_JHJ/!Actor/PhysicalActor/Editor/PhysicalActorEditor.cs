﻿using UnityEngine;
using UnityEditor;

namespace ArchSpirit
{
    [CustomEditor(typeof(PhysicalActor))]
    public class PhysicalActorEditor : ActorEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            EditorGUILayout.Space();

            SerializedProperty centerTransformProp = serializedObject.FindProperty("mCenterTransform");
            SerializedProperty mainColliderHandlerProp = serializedObject.FindProperty("mMainColliderHandler");
            SerializedProperty maxSpeedProp = serializedObject.FindProperty("mMaxSpeed");
            SerializedProperty timeCloseToMaxSpeedProp = serializedObject.FindProperty("mTimeCloseToMaxSpeed");
            SerializedProperty physicalModeProp = serializedObject.FindProperty("mPhysicalMode");

            if (mainTransform != null)
            {
                MainColliderHandler mainColliderHandler = mainTransform.GetComponentInChildren<MainColliderHandler>(true);
                if (mainColliderHandler != null)
                {
                    mainColliderHandlerProp.objectReferenceValue = mainColliderHandler;
                }
                else
                {
                    GameObject mainColliderHandlerObject = new GameObject("Main Colliders", typeof(MainColliderHandler));
                    mainColliderHandlerObject.transform.eulerAngles = Vector3.zero;
                    mainColliderHandlerObject.transform.localScale = Vector3.one;
                    mainColliderHandlerObject.transform.SetParent(mainTransform);
                    mainColliderHandlerObject.transform.localPosition = Vector3.zero;
                    mainColliderHandlerProp.objectReferenceValue = mainColliderHandlerObject.GetComponent<MainColliderHandler>();
                }
            }
            
            centerTransformProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Center Transform"), centerTransformProp.objectReferenceValue, typeof(Transform), true);

            EditorGUILayout.Space();


            EPhysicalMode physicalMode = (EPhysicalMode)physicalModeProp.intValue;
            physicalMode = (EPhysicalMode)EditorGUILayout.EnumPopup("Physical Mode", physicalMode);
            physicalModeProp.intValue = (int)physicalMode;

            maxSpeedProp.floatValue = Mathf.Abs(EditorGUILayout.FloatField("Max Speed", maxSpeedProp.floatValue));
            timeCloseToMaxSpeedProp.floatValue = Mathf.Max(EditorGUILayout.FloatField("Time Close to Max Speed", timeCloseToMaxSpeedProp.floatValue), Time.fixedDeltaTime);
            

            return mainTransform;
        }
    }
}