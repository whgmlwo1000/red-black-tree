﻿using UnityEngine;
using ObjectManagement;

namespace ArchSpirit
{
    public class MainColliderHandler : Handler<string, MainCollider>
    {
        public Collider2D GetCollider(string type)
        {
            return Get(type).Collider;
        }
    }
}