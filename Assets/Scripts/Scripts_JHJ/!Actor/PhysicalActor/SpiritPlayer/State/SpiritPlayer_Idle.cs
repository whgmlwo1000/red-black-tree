﻿using UnityEngine;

namespace ArchSpirit
{
    public class SpiritPlayer_Idle : State<string>
    {
        private SpiritPlayer mPlayer;

        public SpiritPlayer_Idle(SpiritPlayer player) : base("Idle")
        {
            mPlayer = player;
        }

        public override void Start()
        {
            if (mPlayer.HorizontalDirection == EHorizontalDirection.Right)
            {
                mPlayer.Angle = 0.0f;
            }
            else if (mPlayer.HorizontalDirection == EHorizontalDirection.Left)
            {
                mPlayer.Angle = 180.0f;
            }
            mPlayer.AnimationState = Type;
        }

        public override void FixedUpdate()
        {
            mPlayer.Move(mPlayer.PlayerCommand.MoveDirection);

            Vector2 deltaPosition = mPlayer.DeltaPosition;
            if (Mathf.Abs(deltaPosition.x) > Constants.MOVEMENT_THRESHOLD ||
                Mathf.Abs(deltaPosition.y) > Constants.MOVEMENT_THRESHOLD)
            {
                mPlayer.State = "Move";
            }
        }
    }
}