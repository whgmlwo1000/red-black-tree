﻿using UnityEngine;

namespace ArchSpirit
{
    public class SpiritPlayer_Move : State<string>
    {
        private SpiritPlayer mPlayer;

        public SpiritPlayer_Move(SpiritPlayer player) : base("Move")
        {
            mPlayer = player;
        }

        public override void Start()
        {
            mPlayer.AnimationState = Type;

            mPlayer.SetDirection(mPlayer.DeltaPosition, false);
        }

        public override void FixedUpdate()
        {
            mPlayer.Move(mPlayer.PlayerCommand.MoveDirection);

            Vector2 deltaPosition = mPlayer.DeltaPosition;
            if (Mathf.Abs(deltaPosition.x) > Constants.MOVEMENT_THRESHOLD ||
                Mathf.Abs(deltaPosition.y) > Constants.MOVEMENT_THRESHOLD)
            {
                mPlayer.SetDirection(deltaPosition, false);
            }
            else
            {
                mPlayer.State = "Idle";
            }
            
        }
    }
}