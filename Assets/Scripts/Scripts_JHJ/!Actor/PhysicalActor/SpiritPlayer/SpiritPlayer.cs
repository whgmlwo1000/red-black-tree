﻿using UnityEngine;

namespace ArchSpirit
{
    public class SpiritPlayer : PhysicalActor
    {
        [SerializeField]
        private PlayerCommand mPlayerCommand = null;
        public PlayerCommand PlayerCommand { get { return mPlayerCommand; } }

        public Vector2 DeltaPosition { get; private set; }

        private Vector2 mPrevPosition;

        public override void Awake()
        {
            base.Awake();

            AddAnimationState("Idle", 0);
            AddAnimationState("Move", 1);

            AddStates(new SpiritPlayer_Idle(this)
                , new SpiritPlayer_Move(this));
            State = "Idle";
        }

        public void Start()
        {
            mPrevPosition = transform.position;
        }

        public override void FixedUpdate()
        {
            if (!StateManager.Pause.State)
            {
                Vector2 curPosition = transform.position;
                if (DungeonProceeder.Main != null)
                {
                    curPosition += DungeonProceeder.Main.DeltaPosition;
                    transform.position = curPosition;
                }
                
                DeltaPosition = curPosition - mPrevPosition;
                mPrevPosition = curPosition;
            }

            base.FixedUpdate();
        }
    }
}