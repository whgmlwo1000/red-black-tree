﻿using UnityEngine;
using UnityEditor;

namespace ArchSpirit
{
    [CustomEditor(typeof(SpiritPlayer))]
    public class SpiritPlayerEditor : PhysicalActorEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            EditorGUILayout.Space();

            SerializedProperty playerCommandProp = serializedObject.FindProperty("mPlayerCommand");

            playerCommandProp.objectReferenceValue = EditorGUILayout.ObjectField("Player Command", playerCommandProp.objectReferenceValue, typeof(PlayerCommand), false);

            return mainTransform;
        }
    }
}