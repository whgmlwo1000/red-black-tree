﻿using UnityEngine;
using UnityEditor;
using ObjectManagement;

namespace ArchSpirit
{
    [CustomEditor(typeof(EffectPool))]
    public class EffectPoolEditor : PoolEditor<EEffectType>
    {
    }
}