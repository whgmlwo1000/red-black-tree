﻿using ObjectManagement;
using UnityEngine;

namespace ArchSpirit
{
    public class Effect : Actor, IPoolable<EEffectType>
    {

        public virtual bool IsPooled { get { return !gameObject.activeSelf; } }

        [SerializeField]
        private bool mIsWorldCanvas = false;
        public bool IsWorldCanvas { get { return mIsWorldCanvas; } }

        [SerializeField]
        private EEffectType mType = EEffectType.None;
        public EEffectType Type { get { return mType; } }

        [SerializeField]
        private int mEffectCount = 1;
        public int EffectCount { get { return mEffectCount; } }


    }
}