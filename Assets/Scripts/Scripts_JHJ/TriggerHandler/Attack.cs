﻿using UnityEngine;
using System;
using ObjectManagement;

public class Attack<EAttackFlags> : MonoBehaviour, IHandleable<EAttackFlags>
    where EAttackFlags : Enum
{
    [SerializeField]
    private EAttackFlags mType = default;
    public EAttackFlags Type { get { return mType; } }
}
