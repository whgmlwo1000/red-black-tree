﻿using UnityEngine;
using System.Collections.Generic;
using ObjectManagement;

public class TriggerHandler2D<ETriggerType>
    : Handler<ETriggerType, Trigger2D<ETriggerType>> where ETriggerType : System.Enum
{
    public Collider2D[] GetColliders(ETriggerType triggerFlag)
    {
        return Get(triggerFlag).GetColliders();
    }
    public Collider2D[] GetColliders(ETriggerType triggerType, out int count)
    {
        return Get(triggerType).GetColliders(out count);
    }

    public bool IsTriggered(ETriggerType triggerType)
    {
        return Get(triggerType).IsTriggered;
    }

    public Collider2D GetTrigger(ETriggerType triggerFlag)
    {
        return Get(triggerFlag).Trigger;
    }
    public void AddEnterEvent(ETriggerType triggerFlag, Trigger2D<ETriggerType>.OnTrigger onTrigger)
    {
        Get(triggerFlag).OnTriggerEnter += onTrigger;
    }
    public void RemoveEnterEvent(ETriggerType triggerFlag, Trigger2D<ETriggerType>.OnTrigger onTrigger)
    {
        Get(triggerFlag).OnTriggerEnter -= onTrigger;
    }
    public void AddExitEvent(ETriggerType triggerFlag, Trigger2D<ETriggerType>.OnTrigger onTrigger)
    {
        Get(triggerFlag).OnTriggerExit += onTrigger;
    }
    public void RemoveExitEvent(ETriggerType triggerFlag, Trigger2D<ETriggerType>.OnTrigger onTrigger)
    {
        Get(triggerFlag).OnTriggerExit -= onTrigger;
    }
}
