﻿using System;
using UnityEngine;
using System.Collections.Generic;
using ObjectManagement;

public class Trigger2D<ETriggerType> : MonoBehaviour, IHandleable<ETriggerType> where ETriggerType : Enum
{
    public delegate void OnTrigger(Collider2D collider);

    public ETriggerType Type { get { return mType; } }
    [SerializeField]
    private ETriggerType mType = default;

    public Collider2D Trigger { get; private set; }
    public event OnTrigger OnTriggerEnter;
    public event OnTrigger OnTriggerExit;

    private Collider2D[] mColliders;
    private HashSet<Collider2D> mColliderSet;

    [SerializeField]
    [Tooltip("World Space에 고정될 것인지 설정하는 플래그 변수")]
    private bool mIsFixed = false;

    private bool mIsUpdated = false;

    public bool IsTriggered
    {
        get { return mColliderSet.Count > 0; }
    }

    public Collider2D[] GetColliders()
    {
        if (mIsUpdated)
        {
            mIsUpdated = false;
            HashSet<Collider2D>.Enumerator enumerator = mColliderSet.GetEnumerator();
            int index = 0;
            while (index < mColliders.Length && enumerator.MoveNext())
            {
                mColliders[index++] = enumerator.Current;
            }
            if (index < mColliders.Length)
            {
                mColliders[index] = null;
            }
        }
        return mColliders;
    }

    public Collider2D[] GetColliders(out int count)
    {
        count = mColliderSet.Count;
        if (mIsUpdated)
        {
            mIsUpdated = false;
            HashSet<Collider2D>.Enumerator enumerator = mColliderSet.GetEnumerator();
            int index = 0;
            while (index < mColliders.Length && enumerator.MoveNext())
            {
                mColliders[index++] = enumerator.Current;
            }
            if (index < mColliders.Length)
            {
                mColliders[index] = null;
            }
        }
        return mColliders;
    }

    protected virtual void Awake()
    {
        Trigger = GetComponent<Collider2D>();
        mColliderSet = new HashSet<Collider2D>();
        mColliders = new Collider2D[50];
    }
    protected virtual void Start()
    {
        if (mIsFixed)
        {
            transform.SetParent(null);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        int layer = collision.gameObject.layer;

        mIsUpdated = true;
        mColliderSet.Add(collision);
        OnTriggerEnter?.Invoke(collision);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        int layer = collision.gameObject.layer;

        mIsUpdated = true;
        mColliderSet.Remove(collision);
        OnTriggerExit?.Invoke(collision);
    }
    protected virtual void OnDisable()
    {
        mColliderSet.Clear();
    }
}
