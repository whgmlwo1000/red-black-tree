﻿using UnityEngine;

namespace RedBlackTree
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class BgmPlayerTrigger : MonoBehaviour
    {
        [SerializeField]
        private int mBgmIndex = 0;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            BgmPlayer.Play(mBgmIndex, 1.0f);
        }
    }
}