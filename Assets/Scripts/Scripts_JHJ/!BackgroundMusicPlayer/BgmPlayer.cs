﻿using UnityEngine;

namespace RedBlackTree
{
    public enum EStateType_BgmPlayer
    {
        Start = 0, 
        Play = 1
    }

    [RequireComponent(typeof(AudioHandler_BgmPlayer))]
    public class BgmPlayer : StateBase<EStateType_BgmPlayer>
    {
        private static BgmPlayer mMain;

        public static void Play(int index, float fadeTime)
        {
            mMain.PrevBgmIndex = mMain.BgmIndex;
            mMain.BgmIndex = index;
            mMain.FadeTime = fadeTime;
            mMain.State = EStateType_BgmPlayer.Start;
        }

        public static void Play(int index)
        {
            mMain.PrevBgmIndex = mMain.BgmIndex;
            mMain.BgmIndex = index;
            mMain.State = EStateType_BgmPlayer.Play;
        }

        public static void Pause()
        {
            mMain.AudioHandler.Get(mMain.BgmIndex).AudioSource.Pause();
        }

        public static void UnPause()
        {
            mMain.AudioHandler.Get(mMain.BgmIndex).AudioSource.UnPause();
        }
        
        public AudioHandler_BgmPlayer AudioHandler { get; private set; }

        public int BgmIndex { get; private set; }
        public int PrevBgmIndex { get; private set; }
        public float FadeTime { get; private set; }

        public override void Awake()
        {
            base.Awake();
            mMain = this;
            AudioHandler = GetComponent<AudioHandler_BgmPlayer>();

            SetStates(new BgmPlayer_Play(this)
                , new BgmPlayer_Start(this));
        }

        public override void Start()
        {
            base.Start();
            AudioHandler.Get(0).AudioSource.Play();
        }
    }
}