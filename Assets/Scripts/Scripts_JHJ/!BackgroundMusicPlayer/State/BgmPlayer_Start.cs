﻿using UnityEngine;

namespace RedBlackTree
{
    public class BgmPlayer_Start : State<EStateType_BgmPlayer>
    {

        private BgmPlayer mPlayer;
        private float mRemainTime_Fade;
        private AudioSource mPrevAudioSource;
        private float mPrevVolume;
        private AudioSource mCurAudioSource;
        private float mCurVolume;

        private bool mIsStarted;

        public BgmPlayer_Start(BgmPlayer player) : base(EStateType_BgmPlayer.Start)
        {
            mPlayer = player;
        }

        public override void Start()
        {
            mRemainTime_Fade = mPlayer.FadeTime;

            if (!mIsStarted)
            {
                mIsStarted = true;

                mPrevAudioSource = mPlayer.AudioHandler.Get(mPlayer.PrevBgmIndex).AudioSource;
                mPrevVolume = mPrevAudioSource.volume;
            }
            else
            {
                mPrevAudioSource.Stop();
                mPrevAudioSource = mCurAudioSource;
                mPrevVolume = mCurVolume;
            }
            
            mCurAudioSource = mPlayer.AudioHandler.Get(mPlayer.BgmIndex).AudioSource;
            mCurVolume = mCurAudioSource.volume;
            mCurAudioSource.Play();
            mCurAudioSource.volume = 0.0f;
        }

        public override void End()
        {
            mPlayer.AudioHandler.Get(mPlayer.PrevBgmIndex).AudioSource.Stop();
            mPrevAudioSource.volume = mPrevVolume;
            mCurAudioSource.volume = mCurVolume;
            mIsStarted = false;
        }

        public override void Update()
        {
            if(mRemainTime_Fade > 0.0f)
            {
                mRemainTime_Fade -= Time.unscaledDeltaTime;
                mPrevAudioSource.volume = Mathf.Lerp(0.0f, mPrevVolume, mRemainTime_Fade / mPlayer.FadeTime);
                mCurAudioSource.volume = Mathf.Lerp(mCurVolume, 0.0f, mRemainTime_Fade / mPlayer.FadeTime);
            }
            else
            {
                mPlayer.State = EStateType_BgmPlayer.Play;
            }
        }
    }
}