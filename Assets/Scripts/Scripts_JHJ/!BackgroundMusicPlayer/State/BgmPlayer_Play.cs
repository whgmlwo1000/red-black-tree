﻿using UnityEngine;

namespace RedBlackTree
{
    public class BgmPlayer_Play : State<EStateType_BgmPlayer>
    {
        private BgmPlayer mPlayer;

        public BgmPlayer_Play(BgmPlayer player) : base(EStateType_BgmPlayer.Play)
        {
            mPlayer = player;
        }

        public override void Start()
        {
            if (mPlayer.PrevState != EStateType_BgmPlayer.Start)
            {
                mPlayer.AudioHandler.Get(mPlayer.PrevBgmIndex).AudioSource.Stop();
                mPlayer.AudioHandler.Get(mPlayer.BgmIndex).AudioSource.Play();
            }
        }
    }
}