﻿using UnityEngine;


namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New PlayerCommand", menuName = "_Player/Command", order = 1000)]
    public class PlayerCommand : ScriptableObject
    {
        public bool Fairy_Attack;
        public Vector2 Fairy_AttackPosition;

        public bool Giant_Defense;

        #region D-Pad
        public float MoveHorizontal;
        public float MoveVertical;
        #endregion

        public bool IsThrown;
    }
}