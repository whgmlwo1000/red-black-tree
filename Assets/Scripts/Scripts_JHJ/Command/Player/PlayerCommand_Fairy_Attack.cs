﻿using UnityEngine;
using System.Collections;
using TouchManagement;

namespace RedBlackTree
{
    public class PlayerCommand_Fairy_Attack : MonoBehaviour
    {
        private ITouchReceiver mTouchReceiver;
        [SerializeField]
        private PlayerCommand mCommand = null;

        public void Awake()
        {
            mTouchReceiver = GetComponent<ITouchReceiver>();

            mTouchReceiver.OnWarmUp += OnWarmUp;
            mTouchReceiver.OnTouchEnd += OnTouchEnd;
        }

        public void OnWarmUp()
        {
            mCommand.Fairy_Attack = false;
        }

        public void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            if (!mCommand.IsThrown)
            {
                mCommand.Fairy_Attack = true;
                mCommand.Fairy_AttackPosition = worldPosition;
            }
            else
            {
                mCommand.IsThrown = false;
            }
        }
    }
}