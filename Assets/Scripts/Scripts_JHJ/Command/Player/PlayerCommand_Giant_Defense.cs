﻿using UnityEngine;
using System.Collections;
using TouchManagement;

namespace RedBlackTree
{
    public class PlayerCommand_Giant_Defense : MonoBehaviour
    {
        private ITouchReceiver mTouchReceiver;
        [SerializeField]
        private PlayerCommand mCommand = null;

        public void Awake()
        {
            mTouchReceiver = GetComponent<ITouchReceiver>();

            mTouchReceiver.OnWarmUp += OnWarmUp;
            mTouchReceiver.OnTouchEnd += OnTouchEnd;
        }

        public void OnWarmUp()
        {
            mCommand.Giant_Defense = false;
        }

        public void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            if (mTouchReceiver.GetTouchIn(worldPosition))
            {
                mCommand.Giant_Defense = true;
            }
        }

    }
}