﻿using UnityEngine;
using System.Collections.Generic;

namespace StateAgent
{
    public interface IAgent
    {
        float DeltaTime { get; }
        float RemainTime { get; }

        void Update();
    }

    public class Agent<EStateType> : IAgent
        where EStateType : System.Enum
    {
        private Dictionary<EStateType, State<EStateType>> mStateDict;
        private State<EStateType> mState;

        public EStateType State
        {
            get { return mState.Type; }
            set
            {
                State<EStateType> prevState = mState;
                mState = mStateDict[value];
                if (!mComparer.Equals(prevState.Type, value))
                {
                    prevState.End();
                }
                mState.Start();
            }
        }

        private EqualityComparer<EStateType> mComparer;

        private float mPrevRemainTime;
        public float DeltaTime { get; private set; }
        public float RemainTime { get; private set; }
        public const float TIME_TERM = 0.1f;

        public Agent()
        {
            mStateDict = new Dictionary<EStateType, State<EStateType>>();
            mComparer = EqualityComparer<EStateType>.Default;
            RemainTime = Random.Range(0.0f, TIME_TERM);
            mPrevRemainTime = RemainTime;
        }

        public void SetStates(params State<EStateType>[] states)
        {
            mState = states[0];
            for (int i = 0; i < states.Length; i++)
            {
                mStateDict.Add(states[i].Type, states[i]);
            }
            mState.Start();
        }

        public void Update()
        {
            if (RemainTime > 0.0f)
            {
                RemainTime -= Time.deltaTime;
            }
            else
            {
                DeltaTime = mPrevRemainTime - RemainTime;
                RemainTime += TIME_TERM;
                mPrevRemainTime = RemainTime;
                mState?.Update();
            }
        }
    }
}