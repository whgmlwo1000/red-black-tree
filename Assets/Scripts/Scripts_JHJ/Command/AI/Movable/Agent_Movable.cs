﻿using UnityEngine;

namespace RedBlackTree
{
    public interface IAgent_Movable : StateAgent.IAgent
    {
        float MoveHorizontal { get; set; }
        float MoveVertical { get; set; }
    }

    public class Agent_Movable<EStateType> : StateAgent.Agent<EStateType>, IAgent_Movable
        where EStateType : System.Enum
    {
        public float MoveHorizontal { get; set; }
        public float MoveVertical { get; set; }
    }
}