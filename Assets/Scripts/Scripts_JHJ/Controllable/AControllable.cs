﻿using System;

public interface IControllable
{
    AInput Input { get; }

    void SetInputs(params AInput[] input);
    void SetIndexOfInputs(int index);
}

public abstract class AControllable<EState> : RedBlackTree.StateBase<EState>, IControllable
    where EState : Enum
{
    public AInput Input { get { return mInputs[mIndexOfInput]; } }

    private AInput[] mInputs;
    private int mIndexOfInput;

    /// <summary>
    /// 입력 객체를 할당하는 메소드
    /// </summary>
    /// <param name="input"></param>
    public void SetInputs(params AInput[] input)
    {
        mInputs = input;
    }
    /// <summary>
    /// 설정된 입력 객체 중 실행할 입력 객체의 인덱스 값을 설정하는 메소드
    /// </summary>
    /// <param name="index"></param>
    public void SetIndexOfInputs(int index)
    {
        if (index < mInputs.Length)
        {
            mIndexOfInput = index;
        }
    }

    public override void Update()
    {
        base.Update();
        Input.Check();
    }
}