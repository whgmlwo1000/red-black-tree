﻿public abstract class AInput
{
    /// <summary>
    /// 수평 이동
    /// </summary>
    public float MoveHorizontal { get; set; } = 0.0f;
    public bool CanMoveHorizontal { get; set; } = true;
    /// <summary>
    /// 수직 이동
    /// </summary>
    public float MoveVertical { get; set; } = 0.0f;
    public bool CanMoveVertical { get; set; } = true;

    /// <summary>
    /// 주 기술
    /// </summary>
    public bool[] Skill { get; private set; }
    public bool[] SkillDown { get; private set; }
    public bool[] SkillUp { get; private set; }
    public bool[] CanSkill { get; private set; }

    public bool AnySkill
    {
        get
        {
            for (int i = 0; i < Skill.Length; i++)
            {
                if (Skill[i])
                {
                    return true;
                }
            }
            return false;
        }
    }

    public bool AnySkillDown
    {
        get
        {
            for (int i = 0; i < SkillDown.Length; i++)
            {
                if (SkillDown[i])
                {
                    return true;
                }
            }
            return false;
        }
    }

    public bool AnySkillUp
    {
        get
        {
            for (int i = 0; i < SkillUp.Length; i++)
            {
                if (SkillUp[i])
                {
                    return true;
                }
            }
            return false;
        }
    }

    /// <summary>
    /// 상호작용
    /// </summary>
    public bool Interact { get; set; } = false;
    public bool InteractDown { get; set; } = false;
    public bool InteractUp { get; set; } = false;
    public bool CanInteract { get; set; } = true;

    /// <summary>
    /// 대시
    /// </summary>
    public bool Dash { get; set; } = false;
    public bool DashDown { get; set; } = false;
    public bool DashUp { get; set; } = false;
    public bool CanDash { get; set; } = true;

    /// <summary>
    /// 메뉴 수평 이동
    /// </summary>
    public float MoveHorizontalMenu { get; set; } = 0.0f;
    public bool CanMoveHorizontalMenu { get; set; } = false;
    /// <summary>
    /// 메뉴 수직 이동
    /// </summary>
    public float MoveVerticalMenu { get; set; } = 0.0f;
    public bool CanMoveVerticalMenu { get; set; } = false;
    /// <summary>
    /// 메뉴 확인
    /// </summary>
    public bool Confirm { get; set; } = false;
    public bool ConfirmDown { get; set; } = false;
    public bool ConfirmUp { get; set; } = false;
    public bool CanConfirm { get; set; } = true;
    /// <summary>
    /// 메뉴 취소
    /// </summary>
    public bool Cancel { get; set; } = false;
    public bool CancelDown { get; set; } = false;
    public bool CancelUp { get; set; } = false;
    public bool CanCancel { get; set; } = true;
    /// <summary>
    /// 메뉴 호출
    /// </summary>
    public bool Menu { get; set; } = false;
    public bool MenuDown { get; set; } = false;
    public bool MenuUp { get; set; } = false;
    public bool CanMenu { get; set; } = false;
    /// <summary>
    /// Esc
    /// </summary>
    public bool Escape { get; set; } = false;
    public bool EscapeDown { get; set; } = false;
    public bool EscapeUp { get; set; } = false;
    public bool CanEscape { get; set; } = true;


    public AInput(int countOfSkill)
    {
        Skill = new bool[countOfSkill];
        SkillDown = new bool[countOfSkill];
        SkillUp = new bool[countOfSkill];
        CanSkill = new bool[countOfSkill];

        for (int i = 0; i < CanSkill.Length; i++)
        {
            CanSkill[i] = true;
        }
    }

    public abstract void Check();

    /// <summary>
    /// 수평 이동 입력값 설정
    /// </summary>
    /// <param name="value"></param>
    public void SetMoveHorizontal(float value)
    {
        if (CanMoveHorizontal)
        {
            MoveHorizontal = value;
        }
        else
        {
            MoveHorizontal = 0.0f;
        }
    }
    /// <summary>
    /// 수직 이동 입력값 설정
    /// </summary>
    /// <param name="value"></param>
    public void SetMoveVertical(float value)
    {
        if (CanMoveVertical)
        {
            MoveVertical = value;
        }
        else
        {
            MoveVertical = 0.0f;
        }
    }

    /// <summary>
    /// 주 기술 입력값 설정
    /// </summary>
    /// <param name="value"></param>
    /// <param name="index"></param>
    public void SetSkill(bool value, int index)
    {
        if (CanSkill[index])
        {
            if (value)
            {
                if (!Skill[index] && !SkillDown[index])
                {
                    SkillDown[index] = true;
                    SkillUp[index] = false;
                }
                else
                {
                    SkillDown[index] = false;
                }

                Skill[index] = true;
            }
            else
            {
                if (Skill[index] && !SkillUp[index])
                {
                    SkillUp[index] = true;
                    SkillDown[index] = false;
                }
                else
                {
                    SkillUp[index] = false;
                }

                Skill[index] = false;
            }
        }
        else
        {
            Skill[index] = false;
            SkillDown[index] = false;
            SkillUp[index] = false;
        }
    }
    /// <summary>
    /// 상호작용 입력값 설정
    /// </summary>
    /// <param name="value"></param>
    public void SetInteract(bool value)
    {
        if (CanInteract)
        {
            if (value)
            {
                if (!Interact && !InteractDown)
                {
                    InteractDown = true;
                    InteractUp = false;
                }
                else
                {
                    InteractDown = false;
                }

                Interact = true;
            }
            else
            {
                if (Interact && !InteractUp)
                {
                    InteractUp = true;
                    InteractDown = false;
                }
                else
                {
                    InteractUp = false;
                }

                Interact = false;
            }
        }
        else
        {
            Interact = false;
            InteractDown = false;
            InteractUp = false;
        }
    }
    /// <summary>
    /// 대시 입력값 설정
    /// </summary>
    /// <param name="value"></param>
    public void SetDash(bool value)
    {
        if (CanDash)
        {
            if (value)
            {
                if (!Dash && !DashDown)
                {
                    DashDown = true;
                    DashUp = false;
                }
                else
                {
                    DashDown = false;
                }

                Dash = true;
            }
            else
            {
                if (Dash && !DashUp)
                {
                    DashUp = true;
                    DashDown = false;
                }
                else
                {
                    DashUp = false;
                }

                Dash = false;
            }
        }
        else
        {
            Dash = false;
            DashDown = false;
            DashUp = false;
        }
    }

    /// <summary>
    /// 메뉴 수평 이동 입력값 설정
    /// </summary>
    /// <param name="value"></param>
    public void SetMoveHorizontalMenu(float value)
    {
        if (CanMoveHorizontalMenu)
        {
            MoveHorizontalMenu = value;
        }
        else
        {
            MoveHorizontalMenu = 0.0f;
        }
    }
    /// <summary>
    /// 메뉴 수직 이동 입력값 설정
    /// </summary>
    /// <param name="value"></param>
    public void SetMoveVerticalMenu(float value)
    {
        if (CanMoveVerticalMenu)
        {
            MoveVerticalMenu = value;
        }
        else
        {
            MoveVerticalMenu = 0.0f;
        }
    }
    /// <summary>
    /// 메뉴 호출 입력값 설정
    /// </summary>
    /// <param name="value"></param>
    public void SetMenu(bool value)
    {
        if (CanMenu)
        {
            if (value)
            {
                if (!Menu && !MenuDown)
                {
                    MenuDown = true;
                    MenuUp = false;
                }
                else
                {
                    MenuDown = false;
                }

                Menu = true;
            }
            else
            {
                if (Menu && !MenuUp)
                {
                    MenuUp = true;
                    MenuDown = false;
                }
                else
                {
                    MenuUp = false;
                }

                Menu = false;
            }
        }
        else
        {
            Menu = false;
            MenuDown = false;
            MenuUp = false;
        }
    }
    /// <summary>
    /// 메뉴 확인 입력값 설정
    /// </summary>
    /// <param name="value"></param>
    public void SetConfirm(bool value)
    {
        if (CanConfirm)
        {
            if (value)
            {
                if (!Confirm && !ConfirmDown)
                {
                    ConfirmDown = true;
                    ConfirmUp = false;
                }
                else
                {
                    ConfirmDown = false;
                }

                Confirm = true;
            }
            else
            {
                if (Confirm && !ConfirmUp)
                {
                    ConfirmUp = true;
                    ConfirmDown = false;
                }
                else
                {
                    ConfirmUp = false;
                }

                Confirm = false;
            }
        }
        else
        {
            Confirm = false;
            ConfirmDown = false;
            ConfirmUp = false;
        }
    }
    /// <summary>
    /// 메뉴 취소 입력값 설정
    /// </summary>
    /// <param name="value"></param>
    public void SetCancel(bool value)
    {
        if (CanCancel)
        {
            if (value)
            {
                if (!Cancel && !CancelDown)
                {
                    CancelDown = true;
                    CancelUp = false;
                }
                else
                {
                    CancelDown = false;
                }

                Cancel = true;
            }
            else
            {
                if (Cancel && !CancelUp)
                {
                    CancelUp = true;
                    CancelDown = false;
                }
                else
                {
                    CancelUp = false;
                }

                Cancel = false;
            }
        }
        else
        {
            Cancel = false;
            CancelDown = false;
            CancelUp = false;
        }
    }
    /// <summary>
    /// Esc 입력값 설정
    /// </summary>
    /// <param name="value"></param>
    public void SetEscape(bool value)
    {
        if (CanEscape)
        {
            if (value)
            {
                if (!Escape && !EscapeDown)
                {
                    EscapeDown = true;
                    EscapeUp = false;
                }
                else
                {
                    EscapeDown = false;
                }

                Escape = true;
            }
            else
            {
                if (Escape && !EscapeUp)
                {
                    EscapeUp = true;
                    EscapeDown = false;
                }
                else
                {
                    EscapeUp = false;
                }

                Escape = false;
            }
        }
        else
        {
            Escape = false;
            EscapeDown = false;
            EscapeUp = false;
        }
    }

}
