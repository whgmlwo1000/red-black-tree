﻿using UnityEngine;

namespace RedBlackTree
{
    public interface IDamaged
    {
        Damage Damage { get; }
        Heal Heal { get; }

        bool InvokeDamage();
        bool InvokeHeal();
    }
}