﻿using UnityEngine;
using System.Collections;

namespace RedBlackTree
{
    public enum EDamageType
    {
        None = -1, 

        Life, 
        Shield, 

        Count
    }

    public class Damage
    {
        public bool IsImportant { get; private set; }

        public int Value { get; private set; }

        public EDamageType Type { get; private set; }

        public bool IsCritical { get; private set; }

        public IActor SrcActor { get; private set; }
        public Vector2 SrcPosition { get; private set; }
        public float SrcForce { get; private set; }

        public void Set(bool isImportant
            , EDamageType type
            , int value
            , float srcForce
            , bool isCritical
            , IActor srcActor
            , Vector2 srcPosition)
        {
            IsImportant = isImportant;
            Type = type;
            Value = Mathf.Max(value, 0);
            IsCritical = isCritical;
            SrcForce = srcForce;
            SrcActor = srcActor;
            SrcPosition = srcPosition;
        }
    }
}