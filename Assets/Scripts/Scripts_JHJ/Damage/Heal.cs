﻿using UnityEngine;
using System.Collections;

namespace RedBlackTree
{
    public class Heal
    {
        public bool IsImportant { get; private set; }
        public EDamageType Type { get; private set; }
        public int Value { get; private set; }
        public float RateValue { get; private set; }

        public void Set(bool isImportant
            , EDamageType type
            , int value
            , float rateValue)
        {
            IsImportant = isImportant;
            Type = type;
            Value = Mathf.Max(value, 0);
            RateValue = Mathf.Clamp01(rateValue);
        }
    }
}