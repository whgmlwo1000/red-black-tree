﻿using UnityEngine;

namespace RedBlackTree
{
    public class MonsterSpace : MonoBehaviour
    {
        private static MonsterSpace mMain;

        public static Vector3 GetRandomPosition()
        {
            Vector2 leftBottomPosition = mMain.mLeftBottomAnchor.position;
            Vector2 rightTopPosition = mMain.mRightTopAnchor.position;

            return new Vector3(Random.Range(leftBottomPosition.x, rightTopPosition.x), Random.Range(leftBottomPosition.y, rightTopPosition.y), Random.Range(-1.0f, 1.0f));
        }

        [SerializeField]
        private Transform mLeftBottomAnchor = null;
        public Transform LeftBottomAnchor { get { return mLeftBottomAnchor; } }
        [SerializeField]
        private Transform mRightTopAnchor = null;
        public Transform RightTopAnchor { get { return mRightTopAnchor; } }

        public void Awake()
        {
            mMain = this;
        }

        public void OnDestroy()
        {
            mMain = null;
        }
    }
}