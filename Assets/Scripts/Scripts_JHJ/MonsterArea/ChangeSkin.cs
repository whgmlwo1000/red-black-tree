﻿using UnityEngine;
using TouchManagement;

namespace RedBlackTree
{
    /// <summary>
    /// 임시 클래스
    /// </summary>
    public class ChangeSkin : MonoBehaviour
    {
        private ITouchReceiver mReceiver;

        public void Awake()
        {
            mReceiver = GetComponent<ITouchReceiver>();
            mReceiver.OnTouchEnd += OnTouchEnd;
        }

        public void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            if (mReceiver.GetTouchIn(worldPosition))
            {
                PlayerData.Current.FairySkin++;
                if(PlayerData.Current.FairySkin == ESkin_Fairy.Count)
                {
                    PlayerData.Current.FairySkin = ESkin_Fairy.None + 1;
                }
            }
        }
    }
}