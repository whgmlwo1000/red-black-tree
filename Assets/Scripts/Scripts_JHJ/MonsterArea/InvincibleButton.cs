﻿using UnityEngine;
using UnityEngine.UI;
using TouchManagement;

namespace RedBlackTree
{
    /// <summary>
    /// 임시 클래스 
    /// </summary>
    public class InvincibleButton : MonoBehaviour
    {
        private ITouchReceiver mReceiver;
        private Text mText;

        public void Awake()
        {
            mText = GetComponent<Text>();
            mReceiver = GetComponent<ITouchReceiver>();
            mReceiver.OnTouchEnd += OnTouchEnd;
        }

        public void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            if (mReceiver.GetTouchIn(worldPosition))
            {
                Giant.Main.InvincibleForTest = !Giant.Main.InvincibleForTest;
                if(Giant.Main.InvincibleForTest)
                {
                    mText.text = "Invincible On";
                }
                else
                {
                    mText.text = "Invincible Off";
                }
            }
        }
    }
}