﻿using UnityEngine;
using TouchManagement;
using CameraManagement;

namespace RedBlackTree
{
    [RequireComponent(typeof(TouchBox))]
    public class SkipTutorialButton : MonoBehaviour
    {
        private ITouchReceiver mTouchReceiver;
        private bool mIsTouched = false;

        public void Awake()
        {
            mTouchReceiver = GetComponent<ITouchReceiver>();
            mTouchReceiver.OnTouchEnd += OnTouchEnd;
        }

        public void Update()
        {
            if(mIsTouched && !CameraController.Main.IsBlackOutTriggered)
            {
                StateManager.Scene.State = EScene.Scene_Sanctuary;
            }
        }

        private void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            if (!mIsTouched)
            {
                mIsTouched = true;
                CameraController.Main.BlackOut(1.0f, 1.0f);
            }
        }
    }
}