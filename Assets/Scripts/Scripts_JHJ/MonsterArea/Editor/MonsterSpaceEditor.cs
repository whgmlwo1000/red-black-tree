﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(MonsterSpace))]
    public class MonsterSpaceEditor : Editor
    {
        private void OnSceneGUI()
        {
            MonsterSpace space = target as MonsterSpace;

            if(space.LeftBottomAnchor != null && space.RightTopAnchor != null)
            {
                Vector3 leftBottomPosition = space.LeftBottomAnchor.position;
                Vector3 rightTopPosition = space.RightTopAnchor.position;
                float scale = 1.0f;

                Handles.Label(leftBottomPosition, new GUIContent("Left Bottom Anchor"));
                Handles.Label(rightTopPosition, new GUIContent("Right Top Anchor"));
                Handles.DrawSolidRectangleWithOutline(new Rect(leftBottomPosition, rightTopPosition - leftBottomPosition), new Color(0.2f, 0.2f, 0.2f, 0.2f), new Color(1.0f, 1.0f, 1.0f, 0.6f));
                Handles.TransformHandle(ref leftBottomPosition, Quaternion.identity, ref scale);
                Handles.TransformHandle(ref rightTopPosition, Quaternion.identity, ref scale);

                rightTopPosition = Vector3.Max(leftBottomPosition, rightTopPosition);

                space.LeftBottomAnchor.position = leftBottomPosition;
                space.RightTopAnchor.position = rightTopPosition;
            }
        }

        public override void OnInspectorGUI()
        {
            SerializedProperty leftBottomAnchorProp = serializedObject.FindProperty("mLeftBottomAnchor");
            SerializedProperty rightTopAnchorProp = serializedObject.FindProperty("mRightTopAnchor");

            leftBottomAnchorProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Left Bottom Anchor"), leftBottomAnchorProp.objectReferenceValue, typeof(Transform), true);
            Transform leftBottomAnchor = leftBottomAnchorProp.objectReferenceValue as Transform;
            if (leftBottomAnchor == null)
            {
                MonsterSpace space = target as MonsterSpace;
                if (space.GetComponentInParent<Canvas>() != null)
                {
                    leftBottomAnchor = new GameObject("Left Bottom Anchor", typeof(RectTransform)).transform;
                }
                else
                {
                    leftBottomAnchor = new GameObject("Left Bottom Anchor").transform;
                }

                leftBottomAnchor.SetParent(space.transform);
                leftBottomAnchor.localPosition = new Vector3(-5.0f, -5.0f, 0.0f);
                leftBottomAnchor.localRotation = Quaternion.identity;
                leftBottomAnchor.localScale = Vector3.one;
                leftBottomAnchorProp.objectReferenceValue = leftBottomAnchor;
            }
            rightTopAnchorProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Right Top Anchor"), rightTopAnchorProp.objectReferenceValue, typeof(Transform), true);
            Transform rightTopAnchor = rightTopAnchorProp.objectReferenceValue as Transform;
            if(rightTopAnchor == null)
            {
                MonsterSpace space = target as MonsterSpace;
                if(space.GetComponentInParent<Canvas>() != null)
                {
                    rightTopAnchor = new GameObject("Right Top Anchor", typeof(RectTransform)).transform;
                }
                else
                {
                    rightTopAnchor = new GameObject("Right Top Anchor").transform;
                }

                rightTopAnchor.SetParent(space.transform);
                rightTopAnchor.localPosition = new Vector3(5.0f, 5.0f, 0.0f);
                rightTopAnchor.localRotation = Quaternion.identity;
                rightTopAnchor.localScale = Vector3.one;
                rightTopAnchorProp.objectReferenceValue = rightTopAnchor;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}