﻿
namespace RedBlackTree
{
    public enum ELanguage
    {
        None = -1,

        Korean,
        English,

        Count
    }

    public enum EHorizontalDirection
    {
        Left = -1, Right = 1
    }

    public enum EVerticalDirection
    {
        Down = -1, Up = 1
    }

    public enum EOuterGame
    {
        None = -1,


        Count
    }

    public enum EInnerGame
    {
        None = -1,


        Count
    }

    public enum ECitizen
    {
        None = -1,

        Gulbate, 
        Hresbulg, 
        Lataposk, 

        Count
    }

    /// <summary>
    /// 스테이지
    /// </summary>
    //[System.Flags]
    public enum EStage
    {
        None = -1,

        Asgard = 0, // 아스가르드
        Midgard = 1, // 미드가르드
        Alfheim = 2, // 알프헤임
        Niflheim = 3, // 니플헤임
        Hell = 4, // 헬

        Count
    }

    /// <summary>
    /// 행동 위치
    /// </summary>
    public enum EActionPosition
    {
        None = -1,
        Bottom = 0, // 하단
        Middle = 1, // 중단
        Top = 2, // 상단
        Count
    }

    /// <summary>
    /// 전투 유형
    /// </summary>
    public enum ECombatType
    {
        None = -1,
        Dealer = 0, // 공격 유형
        Tank = 1, // 돌격 유형
        Support = 2, // 지원 유형
        Count
    }

    /// <summary>
    /// 액터 크기 타입
    /// </summary>
    public enum ESize
    {
        None = -1,

        Small, // 소형
        Medium, // 중형
        Large, // 대형
        ExtraLarge, // 특대형

        Count
    }

    public enum ERace
    {
        None = -1,

        Beast = 0, // 야수
        Demon = 1, // 악마
        Dragon = 2, // 용
        Dwarf = 3, // 난쟁이
        Elemental = 4, // 정령
        Elf = 5, // 엘프
        Fairy = 6, // 요정
        God = 7, // 신
        Giant = 8, // 거인
        Human = 9, // 인간
        Machine = 10, // 기계
        Undead = 11, // 언데드

        Count
    }
}