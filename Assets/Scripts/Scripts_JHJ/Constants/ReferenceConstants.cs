﻿using UnityEngine;
using System.Collections;

namespace RedBlackTree
{
    public static class ReferenceConstants
    {
        /// <summary>
        /// 마찰력이 0인 Material
        /// </summary>
        public static PhysicsMaterial2D ZeroFriction { get; private set; }

        static ReferenceConstants()
        {
            ZeroFriction = Resources.Load<PhysicsMaterial2D>("Common/PhysicsMaterial2D_ZeroFriction");
        }
    }
}