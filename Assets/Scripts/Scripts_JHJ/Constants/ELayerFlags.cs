﻿
namespace RedBlackTree
{
    public enum ELayerFlags
    {
        Default = 1 << 0,
        TransparentFX = 1 << 1,
        IgnoreRaycast = 1 << 2,
        Water = 1 << 4,
        UI = 1 << 5,
        Terrain = 1 << 8,
        TerrainTrigger = 1 << 9,
        Player = 1 << 10,
        PlayerAttack = 1 << 11,
        Monster = 1 << 12,
        MonsterAttack = 1 << 13,
        Projectile = 1 << 14,
        ProjectileTrigger = 1 << 15,
        Box = 1 << 16, 
        DefaultTrigger = 1 << 17, 

        SantuaryObjects = 1 << 20,

        Background = 1 << 30,
        Overlay = 1 << 31,
    }
}