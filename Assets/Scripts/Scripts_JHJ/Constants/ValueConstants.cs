﻿using UnityEngine;

namespace RedBlackTree
{
    public static class ValueConstants
    {
        /// <summary>
        /// 데미지로 인한 무적 시간
        /// </summary>
        public const float InvincibleTimeByHit = 0.1f;

        /// <summary>
        /// 움직임으로 판단하는 한계값
        /// </summary>
        public const float MovementThreshold = 0.01F;

        public const float OneFrameInAnimation = 0.1f;

        /// <summary>
        /// 습득하는 객체의 최대 속도
        /// </summary>
        public const float MaxSpeedOfAcquisition = 50.0f;

        /// <summary>
        /// 피격에 의한 넉백 상태 판단 한계값
        /// </summary>
        public const float HitMovementThreshold = 0.5f;

        /// <summary>
        /// 전투 유형의 수
        /// </summary>
        public const int CountOfCombatTypes = 3;
        /// <summary>
        /// 행동 위치의 수
        /// </summary>
        public const int CountOfActionPositions = 3;
        /// <summary>
        /// 최대 몬스터 공간 개수
        /// </summary>
        public const int MaxMonsterSpaces = 10;
        /// <summary>
        /// 투사체 최대 지속시간
        /// </summary>
        public const float ProjectileTime = 5.0f;
        /// <summary>
        /// 피격에 의한 색변화 시간
        /// </summary>
        public const float SpriteHitEffectTime = 0.1f;
        /// <summary>
        /// 몬스터가 죽고 사라지기까지 걸리는 시간
        /// </summary>
        public const float MonsterDieFadeTime = 2.0f;

        /// <summary>
        /// 액티브 아이템 장착 공간 개수
        /// </summary>
        public const int ActiveItemMountSpace = 4;

        /// <summary>
        /// Deactivator 가 객체를 비활성화 시키는 거리
        /// </summary>
        public const float DeactivatorDistance = 100.0f;

        /// <summary>
        /// 치명타로 인한 데미지 계수
        /// </summary>
        public const float CriticalMagnification = 1.5f;

        /// <summary>
        /// Product를 구매하기 위해 움직여야 하는 터치의 거리
        /// </summary>
        public const float TouchDistanceOfBuying = 100.0f;

        /// <summary>
        /// 구매하지 못하는 아이템을 표기하기 위한 시간
        /// </summary>
        public const float ShowExpensiveTime = 0.3f;

        /// <summary>
        /// 구매하지 못하는 아이템을 표기하는 세기
        /// </summary>
        public const float PowerOfExpensiveShow = 0.1f;

        /// <summary>
        /// Product 가 Inventory Bag으로 날아갈때 곡선을 잠시 보여주는 시간
        /// </summary>
        public const float ProductBoughtShowTime = 0.5f;

        /// <summary>
        /// 몬스터 스폰 시간
        /// </summary>
        public const float MonsterSpawnTime = 1.1f;
        /// <summary>
        /// 몬스터가 피격당하고, 하얗게 되어 파티클이 터지기 까지 걸리는 시간
        /// </summary>
        public const float MonsterDieParticleTime = 0.2f;
        /// <summary>
        /// 몬스터가 사라진후 대기하는 시간
        /// </summary>
        public const float MonsterDieWaitTime = 3.0f;

        /// <summary>
        /// 열매 대기 시간
        /// </summary>
        public const float FruitWaitTime = 1.0f;

        /// <summary>
        /// 열매가 던져졌다고 판단하는 거리
        /// </summary>
        public const float FruitThrowDistance = 3.0f;

        /// <summary>
        /// 도트데미지 시간 텀
        /// </summary>
        public const float DotDamageTerm = 0.2f;

        /// <summary>
        /// 튕겨져 나간 후 다음 충돌 체크까지 대기 시간
        /// </summary>
        public const float BumpTerm = 0.06f;
    }
}