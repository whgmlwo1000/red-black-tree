﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Gesture
{
    [CustomEditor(typeof(GestureRecognizer))]
    public class GestureRecognizerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            GestureRecognizer recognizer = target as GestureRecognizer;
            //SerializedProperty isHCRFProp = serializedObject.FindProperty("mIsHCRF");
            SerializedProperty accuracyStepProp = serializedObject.FindProperty("mAccuracyStep");

            //EditorGUILayout.BeginHorizontal();
            //EditorGUILayout.LabelField("Use HCRF", GUILayout.MaxWidth(100));
            //isHCRFProp.boolValue = EditorGUILayout.Toggle(isHCRFProp.boolValue);
            //EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Accuracy Step", GUILayout.MaxWidth(120));
            accuracyStepProp.intValue = Mathf.Max(EditorGUILayout.IntField(accuracyStepProp.intValue), 5);
            EditorGUILayout.EndHorizontal();

            serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("Show Gesture Samples!"))
            {
                GestureDataWindow.Open();
            }

            EditorGUILayout.Space();

            if(Application.isPlaying && GUILayout.Button("Re-Learn!"))
            {
                recognizer.LearnData();
            }
        }
    }
}