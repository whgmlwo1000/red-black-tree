﻿using UnityEngine;
using UnityEditor;

namespace Gesture
{
    [CustomEditor(typeof(GesturePanel))]
    public class GesturePanelEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SerializedProperty commandProp = serializedObject.FindProperty("mCommand");
            SerializedProperty modeProp = serializedObject.FindProperty("mMode");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Command", GUILayout.MaxWidth(100));
            commandProp.objectReferenceValue = EditorGUILayout.ObjectField(commandProp.objectReferenceValue, typeof(GestureCommand), false);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Mode", GUILayout.MaxWidth(100));
            EGestureMode mode = (EGestureMode)modeProp.intValue;
            mode = (EGestureMode)EditorGUILayout.EnumPopup(mode);
            modeProp.intValue = (int)mode;
            EditorGUILayout.EndHorizontal();

            if(mode == EGestureMode.Train)
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Train Info", EditorStyles.boldLabel);

                SerializedProperty trainOutputProp = serializedObject.FindProperty("mTrainOutput");

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Output", GUILayout.MaxWidth(100));
                EGesture output = (EGesture)trainOutputProp.intValue;
                output = (EGesture)EditorGUILayout.EnumPopup(output);
                trainOutputProp.intValue = (int)output;
                EditorGUILayout.EndHorizontal();
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}