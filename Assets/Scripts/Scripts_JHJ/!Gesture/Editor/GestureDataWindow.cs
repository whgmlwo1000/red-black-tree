﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Gesture
{
    public class GestureDataWindow : EditorWindow
    {
        private Vector2 mScrollPosition;

        public static void Open()
        {
            GetWindow(typeof(GestureDataWindow));
        }

        private void OnGUI()
        {
            Dictionary<EGesture, List<GestureSample>> sampleDict = new Dictionary<EGesture, List<GestureSample>>();
            for (EGesture e = EGesture.None + 1; e < EGesture.Count; e++)
            {
                sampleDict.Add(e, new List<GestureSample>());
            }

            List<GestureSample> sampleList = GestureRecognizer.Data.SampleList;
            sampleList.ForEach(delegate (GestureSample sample)
            {
                sampleDict[sample.Output].Add(sample);
            });

            GestureSample removeSample = null;

            GUILayout.BeginHorizontal();
            for (EGesture e = EGesture.None + 1; e < EGesture.Count; e++)
            {
                GUILayout.Label(e.ToString(), EditorStyles.boldLabel, GUILayout.Width(102));
            }
            GUILayout.EndHorizontal();

            mScrollPosition = GUILayout.BeginScrollView(mScrollPosition);
            GUILayout.BeginHorizontal();
            for (EGesture e = EGesture.None + 1; e < EGesture.Count; e++)
            {
                GUILayout.BeginVertical(GUILayout.Width(100));
                List<GestureSample> categorizedSampleList = sampleDict[e];

                if (categorizedSampleList.Count != 0)
                {
                    categorizedSampleList.ForEach(delegate (GestureSample sample)
                    {
                        //GUILayout.Label(sample.Texture, GUILayout.Width(100), GUILayout.Height(100));
                        GUILayout.Box(sample.Texture, GUILayout.Width(100), GUILayout.Height(100));
                        if (GUILayout.Button("Remove", GUILayout.Width(100), GUILayout.Height(20)))
                        {
                            removeSample = sample;
                        }
                    });
                }
                else
                {
                    GUILayout.FlexibleSpace();
                }
                GUILayout.EndVertical();
            }
            GUILayout.EndHorizontal();
            GUILayout.EndScrollView();

            if (removeSample != null)
            {
                sampleList.Remove(removeSample);
                GestureRecognizer.SaveData();
            }
        }

        //private void OnGUI()
        //{
        //    if(mSampleList != null)
        //    {

        //    }

        //    int sampleIndex = 0;
        //    int removeIndex = -1;

        //    recognizer.Data.SampleList.ForEach(delegate (GestureSample sample)
        //    {
        //        EditorGUILayout.LabelField(string.Format("Output : {0}", sample.Output.ToString()), GUILayout.Width(200));
        //        EditorGUILayout.BeginHorizontal();
        //        GUILayout.Box(sample.Texture, GUILayout.Width(100), GUILayout.Height(100));
        //        if (GUILayout.Button("Remove", GUILayout.Width(100), GUILayout.Height(100)))
        //        {
        //            removeIndex = sampleIndex;
        //        }
        //        EditorGUILayout.EndHorizontal();
        //        sampleIndex++;
        //    });

        //    if (removeIndex > -1)
        //    {
        //        recognizer.Data.SampleList.RemoveAt(removeIndex);
        //        recognizer.SaveData();
        //    }
        //}
    }
}