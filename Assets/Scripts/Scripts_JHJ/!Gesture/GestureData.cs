﻿using UnityEngine;
using System.Collections.Generic;

namespace Gesture
{
    public enum EGesture
    {
        None = -1, 

        LeftToRight, 
        RightToLeft, 
        UpToDown, 
        DownToUp, 

        DownLeft, 
        DownRight, 
        UpLeft, 
        UpRight, 

        Count
    }

    [System.Serializable]
    public class GestureSample
    {
        [SerializeField]
        private EGesture mOutput;
        public EGesture Output { get { return mOutput; } }
        [SerializeField]
        private Vector2[] mSequence;
        public Vector2[] Sequence { get { return mSequence; } }

        [SerializeField]
        private Vector2 mMinPoint;

        [SerializeField]
        private Vector2 mMaxPoint;

        public const int TEXTURE_SIZE = 100;

        private Texture2D mTexture;
        public Texture2D Texture
        {
            get
            {
                if(mTexture == null)
                {
                    Vector2 size = mMaxPoint - mMinPoint;
                    Vector2 extendedSize = (size.x > size.y ? new Vector2(size.x, size.x) : new Vector2(size.y, size.y));
                    Vector2 padding = (extendedSize - size) * 0.5f;

                    mTexture = new Texture2D(TEXTURE_SIZE, TEXTURE_SIZE);
                    mTexture.wrapMode = TextureWrapMode.Clamp;
                    mTexture.filterMode = FilterMode.Point;
                    for(int sequenceIndex = 0; sequenceIndex < mSequence.Length; sequenceIndex++)
                    {
                        int x = (int)(TEXTURE_SIZE * (mSequence[sequenceIndex].x - mMinPoint.x + padding.x) / extendedSize.x);
                        int y = (int)(TEXTURE_SIZE * (mSequence[sequenceIndex].y - mMinPoint.y + padding.y) / extendedSize.y);

                        mTexture.SetPixel(x, y, Color.Lerp(Color.red, Color.blue, (float)sequenceIndex / mSequence.Length));
                    }
                    mTexture.Apply();
                }
                return mTexture;
            }
        }

        public GestureSample(EGesture output, Vector2[] sequence, Vector2 minPoint, Vector2 maxPoint)
        {
            mOutput = output;
            mSequence = sequence;

            mMinPoint = minPoint;
            mMaxPoint = maxPoint;
        }
    }

    [System.Serializable]
    public class GestureData
    {
        [SerializeField]
        private List<GestureSample> mSampleList = new List<GestureSample>();
        public List<GestureSample> SampleList { get { return mSampleList; } }
    }
}