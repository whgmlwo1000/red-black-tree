﻿using Accord.Math;
using Accord.Statistics;
using Accord.Statistics.Distributions.Fitting;
using Accord.Statistics.Distributions.Multivariate;
//using Accord.Statistics.Models.Fields;
//using Accord.Statistics.Models.Fields.Functions;
//using Accord.Statistics.Models.Fields.Learning;
using Accord.Statistics.Models.Markov;
using Accord.Statistics.Models.Markov.Learning;
using Accord.Statistics.Models.Markov.Topology;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;

namespace Gesture
{
    public class GestureRecognizer : MonoBehaviour
    {
        public static GestureRecognizer Main { get; private set; }

        private HiddenMarkovClassifier<MultivariateNormalDistribution, double[]> mMarkovModel;
        //private HiddenConditionalRandomField<double[]> mRandomCondition;

        private static GestureData mData;
        public static GestureData Data
        {
            get
            {
                if(mData == null)
                {
                    BetterStreamingAssets.Initialize();
                    if (BetterStreamingAssets.FileExists("Gesture.db"))
                    {
                        string json = BetterStreamingAssets.ReadAllText("Gesture.db");
                        mData = JsonUtility.FromJson<GestureData>(json);
                    }
                    else
                    {
                        mData = new GestureData();
                    }
                }
                return mData;
            }
        }

        [SerializeField]
        private int mAccuracyStep = 50;

        //[SerializeField]
        //private bool mIsHCRF = false;

        private double[][] mInput;

        private Thread mThread;

        public void Awake()
        {
            if (Main == null)
            {
                Main = this;
                DontDestroyOnLoad(gameObject);

                mThread = new Thread(new ThreadStart(Run));
                mThread.Start();
                InitializeInput();
                LearnData();
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void OnDestroy()
        {
            Main = null;
            mThread.Abort();
        }

        public static bool IsRunning { get; private set; } = false;
        public static EGesture Output { get; private set; } = EGesture.None;

        public static void SaveData()
        {
            string json = JsonUtility.ToJson(Data);
            StreamWriter writer = new StreamWriter(Application.streamingAssetsPath + "/Gesture.db");
            writer.Write(json);
            writer.Flush();
            writer.Close();
        }

        public void Decide(List<Vector2> sequenceList)
        {
            if (mMarkovModel == null || IsRunning)
            {
                return;
            }

            Preprocess(sequenceList);
            IsRunning = true;
        }

        public void LearnData()
        {
            int states = 5;
            int iterations = 0;
            double tolerance = 0.01;
            bool rejection = false;

            double[][][] inputs = new double[Data.SampleList.Count][][];
            int[] outputs = new int[Data.SampleList.Count];

            int sampleIndex = 0;
            Data.SampleList.ForEach(delegate (GestureSample sample)
            {
                Preprocess(sample.Sequence);
                mInput.ZScores().Add(10);
                inputs[sampleIndex] = mInput;
                InitializeInput();
                outputs[sampleIndex++] = (int)sample.Output;
            });

            string[] names = new string[(int)EGesture.Count];
            for (EGesture e = EGesture.None + 1; e < EGesture.Count; e++)
            {
                names[(int)e] = e.ToString();
            }

            mMarkovModel = new HiddenMarkovClassifier<MultivariateNormalDistribution, double[]>(names.Length
                , new Forward(states), new MultivariateNormalDistribution(2), names);

            // Create the learning algorithm for the ensemble classifier
            var teacher = new HiddenMarkovClassifierLearning<MultivariateNormalDistribution, double[]>(mMarkovModel)
            {
                // Train each model using the selected convergence criteria
                Learner = i => new BaumWelchLearning<MultivariateNormalDistribution, double[]>(mMarkovModel.Models[i])
                {
                    Tolerance = tolerance,
                    MaxIterations = iterations,

                    FittingOptions = new NormalOptions()
                    {
                        Regularization = 1e-5
                    }
                }
            };

            teacher.Empirical = true;
            teacher.Rejection = rejection;


            // Run the learning algorithm
            teacher.Learn(inputs, outputs);

            //if (mIsHCRF)
            //{
            //    iterations = 100;
            //    tolerance = 0.01;

            //    mRandomCondition = new HiddenConditionalRandomField<double[]>(new MarkovMultivariateFunction(mMarkovModel));

            //    // Create the learning algorithm for the ensemble classifier
            //    var conditionalTeacher = new HiddenResilientGradientLearning<double[]>(mRandomCondition)
            //    {
            //        MaxIterations = iterations,
            //        Tolerance = tolerance
            //    };

            //    // Run the learning algorithm
            //    conditionalTeacher.Learn(inputs, outputs);
#if UNITY_EDITOR
                // Classify all training instances
                //Data.SampleList.ForEach(delegate (GestureSample sample)
                //{
                //    Preprocess(sample.Sequence);
                //    mInput.ZScores().Add(10);
                //    sample.RecognizedAs = (EGesture)mRandomCondition.Decide(mInput);
                //});
#endif
            //}
#if UNITY_EDITOR
            //else
            //{
                // Classify all training instances
                //Data.SampleList.ForEach(delegate (GestureSample sample)
                //{
                //    Preprocess(sample.Sequence);
                //    mInput.ZScores().Add(10);
                //    sample.RecognizedAs = (EGesture)mMarkovModel.Decide(mInput);
                //});
            //}
#endif
        }

        private void InitializeInput()
        {
            mInput = new double[mAccuracyStep][];
            for (int i = 0; i < mInput.Length; i++)
            {
                mInput[i] = new double[2];
            }
        }

        private void Run()
        {
            while (true)
            {
                if (IsRunning)
                {
                    mInput.ZScores().Add(10);
                    Output = (EGesture)mMarkovModel.Decide(mInput);
                    //Output = (EGesture)(!mIsHCRF ? mMarkovModel.Decide(mInput) : mRandomCondition.Decide(mInput));
                    IsRunning = false;
                }
                Thread.Sleep(500);
            }
        }

        private void Preprocess(Vector2[] sequence)
        {
            // Sequence의 Point 개수가 Input 용량과 같은 경우
            if (mInput.Length == sequence.Length)
            {
                for (int i = 0; i < mInput.Length; i++)
                {
                    mInput[i][0] = sequence[i].x;
                    mInput[i][1] = sequence[i].y;
                }
            }
            // Sequence의 Point 개수가 Input 용량과 다른 경우
            else
            {
                // Input 에 저장될 Point 는 Sequence 의 Point 사이를 보간한 값
                float increase = (float)(sequence.Length - 1) / mInput.Length;
                for (int inputIndex = 0; inputIndex < mInput.Length; inputIndex++)
                {
                    float increaseResult = increase * inputIndex;
                    int floorIndex = Mathf.FloorToInt(increaseResult);
                    int ceilIndex = Mathf.CeilToInt(increaseResult);
                    Vector2 floor = sequence[floorIndex];
                    Vector2 ceil = sequence[ceilIndex];
                    Vector2 point = (floor + (ceil - floor) * (increaseResult - floorIndex));

                    mInput[inputIndex][0] = point.x;
                    mInput[inputIndex][1] = point.y;
                }
            }
        }

        private void Preprocess(List<Vector2> sequenceList)
        {
            if(mInput.Length == sequenceList.Count)
            {
                for(int i = 0; i < mInput.Length; i++)
                {
                    Vector2 point = sequenceList[i];
                    mInput[i][0] = point.x;
                    mInput[i][1] = point.y;
                }
            }
            else
            {
                float increase = (float)(sequenceList.Count - 1) / mInput.Length;
                for(int inputIndex = 0; inputIndex < mInput.Length; inputIndex++)
                {
                    float increaseResult = increase * inputIndex;
                    int floorIndex = Mathf.FloorToInt(increaseResult);
                    int ceilIndex = Mathf.CeilToInt(increaseResult);
                    Vector2 floor = sequenceList[floorIndex];
                    Vector2 ceil = sequenceList[ceilIndex];
                    Vector2 point = (floor + (ceil - floor) * (increaseResult - floorIndex));

                    mInput[inputIndex][0] = point.x;
                    mInput[inputIndex][1] = point.y;
                }
            }
        }
    }
}