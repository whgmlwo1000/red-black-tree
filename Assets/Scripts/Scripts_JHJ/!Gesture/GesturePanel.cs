﻿using UnityEngine;
using System.Collections.Generic;
using TouchManagement;

namespace Gesture
{
    public enum EGestureMode
    {
        None = -1,

        Train,
        Test,
        Decide,
    }

    [RequireComponent(typeof(TouchBox))]
    public class GesturePanel : MonoBehaviour
    {
        private List<Vector2> mSequence;
        private Vector2 mMinPoint;
        private Vector2 mMaxPoint;
        private Vector2 mPrevPoint;
        private TouchBox mTouchBox;

        [SerializeField]
        private GestureCommand mCommand = null;

        [SerializeField]
        private EGestureMode mMode = EGestureMode.None;

        [SerializeField]
        private EGesture mTrainOutput = EGesture.None;

        private GestureRecognizer mRecognizer = null;
        private bool mIsWaiting = false;

        public void Awake()
        {
            mTouchBox = GetComponent<TouchBox>();

            mSequence = new List<Vector2>();

            mTouchBox.OnWarmUp += OnWarmUp;
            mTouchBox.OnTouchStart += OnTouchStart;
            mTouchBox.OnTouchStay += OnTouchStay;
            mTouchBox.OnTouchEnd += OnTouchEnd;
        }

        public void Start()
        {
            mRecognizer = GestureRecognizer.Main;
        }

        public void Update()
        {
            if (mIsWaiting && !GestureRecognizer.IsRunning)
            {
                mIsWaiting = false;
                mCommand.Gesture = GestureRecognizer.Output;
            }
        }

        private void OnWarmUp()
        {
            mCommand.Touch = false;
        }

        private void OnTouchStart(Touch touch, Vector2 worldPosition)
        {
            mSequence.Clear();
            mPrevPoint = touch.position;
            mSequence.Add(mPrevPoint);
            mMinPoint = mPrevPoint;
            mMaxPoint = mPrevPoint;
        }

        private void OnTouchStay(Touch touch, Vector2 worldPosition)
        {
            Vector2 curPoint = touch.position;
            if (Vector2.Distance(curPoint, mPrevPoint) > 10.0f)
            {
                mSequence.Add(curPoint);
                mPrevPoint = curPoint;
                mMinPoint = Vector2.Min(curPoint, mMinPoint);
                mMaxPoint = Vector2.Max(curPoint, mMaxPoint);
            }
        }

        private void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            OnTouchStay(touch, worldPosition);

            if (mMode == EGestureMode.Train)
            {
                if (mSequence.Count > 5)
                {
                    NormalizeSequence();
                    GestureRecognizer.Data.SampleList.Add(new GestureSample(mTrainOutput, mSequence.ToArray(), mMinPoint, mMaxPoint));
                    GestureRecognizer.SaveData();
                }
            }
            else if (mMode == EGestureMode.Decide)
            {
                if (!mIsWaiting && mCommand.Gesture == EGesture.None && mSequence.Count > 5)
                {
                    mIsWaiting = true;
                    NormalizeSequence();
                    mRecognizer.Decide(mSequence);
                }
                else
                {
                    mCommand.Touch = true;
                    mCommand.TouchWorldPosition = worldPosition;
                }
            }
            else if (mMode == EGestureMode.Test)
            {
                if (mSequence.Count > 5)
                {
                    if (!mIsWaiting)
                    {
                        mIsWaiting = true;
                        NormalizeSequence();
                        mRecognizer.Decide(mSequence);
                    }
                }
            }
        }

        private void NormalizeSequence()
        {
            Vector2 centerPoint = new Vector2((mMinPoint.x + mMaxPoint.x) * 0.5f, (mMinPoint.y + mMaxPoint.y) * 0.5f);
            mMinPoint -= centerPoint;
            mMaxPoint -= centerPoint;
            for(int i = 0; i < mSequence.Count; i++)
            {
                mSequence[i] -= centerPoint;
            }
        }
    }
}