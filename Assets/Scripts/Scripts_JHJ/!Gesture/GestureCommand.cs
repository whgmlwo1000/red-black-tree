﻿using UnityEngine;
using UnityEditor;

namespace Gesture
{
    [CreateAssetMenu(fileName = "New GestureCommand", menuName = "Gesture/Command", order = 1000)]
    public class GestureCommand : ScriptableObject
    {
        public bool Touch;
        public Vector2 TouchWorldPosition;

        public EGesture Gesture;
    }
}