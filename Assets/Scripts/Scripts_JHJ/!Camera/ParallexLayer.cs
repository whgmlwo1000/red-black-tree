﻿using UnityEngine;
using System.Collections.Generic;

namespace CameraManagement
{
    public enum EParallexType
    {
        Automatic,
        Customize,
        Endless,
    }

    public class ParallexLayer : MonoBehaviour
    {
        [SerializeField]
        private EParallexType mParallexType = EParallexType.Automatic;
        public EParallexType ParallexType { get { return mParallexType; } }

        [SerializeField]
        private Vector2 mParallexRatio = Vector2.zero;
        public Vector2 ParallexRatio { get { return mParallexRatio; } }

        [SerializeField]
        private Transform mMinAnchor = null;
        public Transform MinAnchor { get { return mMinAnchor; } }

        [SerializeField]
        private Transform mMaxAnchor = null;
        public Transform MaxAnchor { get { return mMaxAnchor; } }

        public Vector2 CenterLocalAnchor
        {
            get { return (mMinAnchor.localPosition + mMaxAnchor.localPosition) * 0.5f; }
        }

        public Vector2 Size
        {
            get { return mMaxAnchor.position - mMinAnchor.position; }
        }

        [SerializeField]
        private bool mIsEndlessInHorizontal = false;
        public bool IsEndlessInHorizontal { get { return mIsEndlessInHorizontal; } }

        [SerializeField]
        private bool mIsEndlessInVertical = false;
        public bool IsEndlessInVertical { get { return mIsEndlessInVertical; } }

        [SerializeField]
        private Transform mLayer = null;

        private Transform mLeftBottomLayer;
        private Transform mRightBottomLayer;
        private Transform mRightTopLayer;
        private Transform mLeftTopLayer;

        public void Awake()
        {
            if (ParallexType == EParallexType.Endless)
            {
                Vector2 layerSize = Size;
                mLeftBottomLayer = mLayer;
                Vector2 leftBottomLayerPosition = mLeftBottomLayer.position;
                mRightBottomLayer = Instantiate(mLayer.gameObject, transform).transform;
                mRightBottomLayer.position = new Vector3(leftBottomLayerPosition.x + layerSize.x, leftBottomLayerPosition.y);
                mRightTopLayer = Instantiate(mLayer.gameObject, transform).transform;
                mRightTopLayer.position = new Vector3(leftBottomLayerPosition.x + layerSize.x, leftBottomLayerPosition.y + layerSize.y);
                mLeftTopLayer = Instantiate(mLayer.gameObject, transform).transform;
                mLeftTopLayer.position = new Vector3(leftBottomLayerPosition.x, leftBottomLayerPosition.y + layerSize.y);

                if(!mIsEndlessInHorizontal)
                {
                    mRightBottomLayer.gameObject.SetActive(false);
                    mRightTopLayer.gameObject.SetActive(false);
                }

                if(!mIsEndlessInVertical)
                {
                    mLeftTopLayer.gameObject.SetActive(false);
                    mRightTopLayer.gameObject.SetActive(false);
                }
            }
        }



        public void UpdateEndlessLayer(Vector2 cameraPosition)
        {
            Vector2 centerLocalPosition = CenterLocalAnchor;
            Vector2 leftBottomLayerPosition = mLeftBottomLayer.position;
            Vector2 leftBottomLayerCenterPosition = leftBottomLayerPosition + centerLocalPosition;
            Vector2 rightTopLayerPosition = mRightTopLayer.position;
            Vector2 rightTopLayerCenterPosition = rightTopLayerPosition + centerLocalPosition;

            // 카메라가 아직 정상적인 범위내에 존재하는 경우
            if ((!mIsEndlessInHorizontal || (cameraPosition.x - leftBottomLayerCenterPosition.x) * (cameraPosition.x - rightTopLayerCenterPosition.x) <= Mathf.Epsilon)
                && (!mIsEndlessInVertical || (cameraPosition.y - leftBottomLayerCenterPosition.y) * (cameraPosition.y - rightTopLayerCenterPosition.y) <= Mathf.Epsilon))
            {
                return;
            }
            // 카메라가 정상 범위를 벗어난 경우
            else
            {
                Vector2 layerSize = Size;

                if (mIsEndlessInHorizontal)
                {
                    // 왼쪽으로 벗어난 경우
                    if (cameraPosition.x < leftBottomLayerCenterPosition.x)
                    {
                        if (leftBottomLayerCenterPosition.x - cameraPosition.x > layerSize.x)
                        {
                            MoveTo(cameraPosition, leftBottomLayerCenterPosition, rightTopLayerCenterPosition);
                            return;
                        }

                        Transform newLeftBottomLayer = mRightBottomLayer;
                        Transform newLeftTopLayer = mRightTopLayer;

                        newLeftBottomLayer.position = new Vector3(leftBottomLayerPosition.x - layerSize.x, leftBottomLayerPosition.y);
                        newLeftTopLayer.position = new Vector3(leftBottomLayerPosition.x - layerSize.x, rightTopLayerPosition.y);

                        mRightBottomLayer = mLeftBottomLayer;
                        mRightTopLayer = mLeftTopLayer;

                        mLeftBottomLayer = newLeftBottomLayer;
                        mLeftTopLayer = newLeftTopLayer;
                    }
                    //  오른쪽으로 벗어난 경우
                    else if (cameraPosition.x > rightTopLayerCenterPosition.x)
                    {
                        if (cameraPosition.x - rightTopLayerCenterPosition.x > layerSize.x)
                        {
                            MoveTo(cameraPosition, leftBottomLayerCenterPosition, rightTopLayerCenterPosition);
                            return;
                        }

                        Transform newRightBottomLayer = mLeftBottomLayer;
                        Transform newRightTopLayer = mLeftTopLayer;

                        newRightBottomLayer.position = new Vector3(rightTopLayerPosition.x + layerSize.x, leftBottomLayerPosition.y);
                        newRightTopLayer.position = new Vector3(rightTopLayerPosition.x + layerSize.x, rightTopLayerPosition.y);

                        mLeftBottomLayer = mRightBottomLayer;
                        mLeftTopLayer = mRightTopLayer;

                        mRightBottomLayer = newRightBottomLayer;
                        mRightTopLayer = newRightTopLayer;
                    }

                    leftBottomLayerPosition = mLeftBottomLayer.position;
                    leftBottomLayerCenterPosition = leftBottomLayerPosition + centerLocalPosition;
                    rightTopLayerPosition = mRightTopLayer.position;
                    rightTopLayerCenterPosition = rightTopLayerPosition + centerLocalPosition;
                }

                if (mIsEndlessInVertical)
                {
                    // 하단으로 벗어난 경우
                    if (cameraPosition.y < leftBottomLayerCenterPosition.y)
                    {
                        if (leftBottomLayerCenterPosition.y - cameraPosition.y > layerSize.y)
                        {
                            MoveTo(cameraPosition, leftBottomLayerCenterPosition, rightTopLayerCenterPosition);
                            return;
                        }

                        Transform newLeftBottomLayer = mLeftTopLayer;
                        Transform newRightBottomLayer = mRightTopLayer;

                        newLeftBottomLayer.position = new Vector3(leftBottomLayerPosition.x, leftBottomLayerPosition.y - layerSize.y);
                        newRightBottomLayer.position = new Vector3(rightTopLayerPosition.x, leftBottomLayerPosition.y - layerSize.y);

                        mLeftTopLayer = mLeftBottomLayer;
                        mRightTopLayer = mRightBottomLayer;

                        mLeftBottomLayer = newLeftBottomLayer;
                        mRightBottomLayer = newRightBottomLayer;
                    }
                    // 상단으로 벗어난 경우
                    else if (cameraPosition.y > rightTopLayerCenterPosition.y)
                    {
                        if (cameraPosition.y - rightTopLayerCenterPosition.y > layerSize.y)
                        {
                            MoveTo(cameraPosition, leftBottomLayerCenterPosition, rightTopLayerCenterPosition);
                            return;
                        }

                        Transform newLeftTopLayer = mLeftBottomLayer;
                        Transform newRightTopLayer = mRightBottomLayer;

                        newLeftTopLayer.position = new Vector3(leftBottomLayerPosition.x, rightTopLayerPosition.y + layerSize.y);
                        newRightTopLayer.position = new Vector3(rightTopLayerPosition.x, rightTopLayerPosition.y + layerSize.y);

                        mLeftBottomLayer = mLeftTopLayer;
                        mRightBottomLayer = mRightTopLayer;

                        mLeftTopLayer = newLeftTopLayer;
                        mRightTopLayer = newRightTopLayer;
                    }
                }
            }
        }

        private void MoveTo(Vector2 cameraPosition, Vector2 leftBottomLayerCenterPosition, Vector2 rightTopLayerCenterPosition)
        {
            Vector2 prevCameraPosition = new Vector2(Random.Range(leftBottomLayerCenterPosition.x, rightTopLayerCenterPosition.x), Random.Range(leftBottomLayerCenterPosition.y, rightTopLayerCenterPosition.y));
            Vector2 movement = cameraPosition - prevCameraPosition;
            Vector2 leftBottomLayerPosition = mLeftBottomLayer.position;
            mLeftBottomLayer.position = leftBottomLayerPosition + movement;
            Vector2 rightBottomLayerPosition = mRightBottomLayer.position;
            mRightBottomLayer.position = rightBottomLayerPosition + movement;
            Vector2 rightTopLayerPosition = mRightTopLayer.position;
            mRightTopLayer.position = rightTopLayerPosition + movement;
            Vector2 leftTopLayerPosition = mLeftTopLayer.position;
            mLeftTopLayer.position = leftTopLayerPosition + movement;
        }
    }
}