﻿using UnityEngine;
using System.Collections.Generic;

namespace CameraManagement
{
    public class CameraTarget : MonoBehaviour
    {
        private static List<CameraBoundaryLine> mCameraBoundaryList;

        static CameraTarget()
        {
            mCameraBoundaryList = new List<CameraBoundaryLine>();
        }

        public static void AddBoundary(CameraBoundaryLine line)
        {
            mCameraBoundaryList.Add(line);
        }

        public static void RemoveBoundary(CameraBoundaryLine line)
        {
            mCameraBoundaryList.Remove(line);
        }

        [SerializeField]
        private float mInertiaLimit = 1.0f;
        public float InertialLimit
        {
            get { return mInertiaLimit; }
            set { mInertiaLimit = value; }
        }

        [SerializeField]
        private float mInertiaCondition = 1.0f;

        [SerializeField]
        private float mInertiaSpeed = 1.0f;
        
        public float Inertia { get; private set; }

        private EHorizontalDirection mInertiaDirection;

        private float mInertiaMovement;

        private float mPrevPosition_x;

        [SerializeField]
        private Vector2 mLeftBottomLimit = -Vector2.one;
        public Vector2 LeftBottomLimit
        {
            get { return mLeftBottomLimit; }
            set { mLeftBottomLimit = value; }
        }

        [SerializeField]
        private Vector2 mRightTopLimit = Vector2.one;
        public Vector2 RightTopLimit
        {
            get { return mRightTopLimit; }
            set { mRightTopLimit = value; }
        }

        public void Awake()
        {
            mPrevPosition_x = transform.position.x;
        }

        public void FixedUpdate()
        {
            Vector2 position = transform.position;

            if (mPrevPosition_x + Mathf.Epsilon < position.x)
            {
                if (mInertiaDirection == EHorizontalDirection.Right)
                {
                    if (mInertiaMovement > mInertiaCondition)
                    {
                        Inertia += mInertiaSpeed * Time.fixedDeltaTime;
                    }
                    else
                    {
                        mInertiaMovement += position.x - mPrevPosition_x;
                    }
                }
                else
                {
                    mInertiaDirection = EHorizontalDirection.Right;
                    mInertiaMovement = 0.0f;
                    Inertia = 0.0f;
                }
            }
            else if (mPrevPosition_x - Mathf.Epsilon > position.x)
            {
                if (mInertiaDirection == EHorizontalDirection.Left)
                {
                    if (mInertiaMovement > mInertiaCondition)
                    {
                        Inertia -= mInertiaSpeed * Time.fixedDeltaTime;
                    }
                    else
                    {
                        mInertiaMovement += mPrevPosition_x - position.x;
                    }
                }
                else
                {
                    mInertiaDirection = EHorizontalDirection.Left;
                    mInertiaMovement = 0.0f;
                    Inertia = 0.0f;
                }
            }

            mPrevPosition_x = position.x;
            Inertia = Mathf.Clamp(Inertia, -mInertiaLimit, mInertiaLimit);
        }

        public Vector2 GetDestination(float verticalSize, float horizontalSize)
        {
            Vector2 position = transform.position;

            bool bottomOverlapped = false;
            float bottom_y = position.y - verticalSize;
            bool topOverlapped = false;
            float top_y = position.y + verticalSize;

            bool leftOverlapped = false;
            float left_x = position.x - horizontalSize;
            bool rightOverlapped = false;
            float right_x = position.x + horizontalSize;

            mCameraBoundaryList.ForEach(delegate (CameraBoundaryLine line)
            {
                if (line.IsVerticalOverlapped(position, verticalSize))
                {
                    float overlap_y = line.GetY(position.x);
                    if(overlap_y > position.y)
                    {
                        if (overlap_y < top_y)
                        {
                            topOverlapped = true;
                            top_y = overlap_y;
                        }
                    }
                    else
                    {
                        if(overlap_y > bottom_y)
                        {
                            bottomOverlapped = true;
                            bottom_y = overlap_y;
                        }
                    }
                }

                if (line.IsHorizontalOverlapped(position, horizontalSize))
                {
                    float overlap_x = line.GetX(position.y);
                    if(overlap_x > position.x)
                    {
                        if(overlap_x < right_x)
                        {
                            rightOverlapped = true;
                            right_x = overlap_x;
                        }
                    }
                    else
                    {
                        if(overlap_x > left_x)
                        {
                            leftOverlapped = true;
                            left_x = overlap_x;
                        }
                    }
                }
            });

            float destination_y;
            float destination_x;

            if(bottomOverlapped && topOverlapped)
            {
                destination_y = (bottom_y + top_y) * 0.5f;
            }
            else if(bottomOverlapped)
            {
                destination_y = (bottom_y + verticalSize);
            }
            else if(topOverlapped)
            {
                destination_y = (top_y - verticalSize);
            }
            else
            {
                destination_y = position.y;
            }

            if(leftOverlapped && rightOverlapped)
            {
                destination_x = (left_x + right_x) * 0.5f;
            }
            else if(leftOverlapped)
            {
                destination_x = (left_x + horizontalSize);
            }
            else if(rightOverlapped)
            {
                destination_x = (right_x - horizontalSize);
            }
            else
            {
                destination_x = position.x;
            }

            return new Vector2(destination_x + Inertia, destination_y);
        }
    }
}