﻿using UnityEngine;

namespace CameraManagement
{
    public class ParallexBackground : MonoBehaviour
    {
        private ParallexLayer[] mLayers;

        [SerializeField]
        private Vector2 mMinWorldAnchor = -Vector2.one;
        public Vector2 MinWorldAnchor { get { return mMinWorldAnchor; } }

        [SerializeField]
        private Vector2 mMaxWorldAnchor = Vector2.one;
        public Vector2 MaxWorldAnchor { get { return mMaxWorldAnchor; } }

        public void Awake()
        {
            mLayers = GetComponentsInChildren<ParallexLayer>(true);
        }

        public void FixedUpdate()
        {
            CameraController camera = CameraController.Main;
            if(camera != null)
            {
                Vector2 cameraPosition = camera.transform.position;
                Vector2 worldCenterPosition = (mMinWorldAnchor + mMaxWorldAnchor) * 0.5f;
                Vector2 worldCenterLocalPositionToCamera = worldCenterPosition - cameraPosition;
                Vector2 worldSize = (mMaxWorldAnchor - mMinWorldAnchor);

                for(int indexOfLayers = 0; indexOfLayers < mLayers.Length; indexOfLayers++)
                {
                    Vector2 layerCenterLocalPosition = mLayers[indexOfLayers].CenterLocalAnchor;
                    Vector2 layerSize = mLayers[indexOfLayers].Size;

                    if (mLayers[indexOfLayers].ParallexType == EParallexType.Automatic)
                    {
                        Vector2 layerCenterPositionToCamera = (layerSize * worldCenterLocalPositionToCamera) / worldSize;
                        mLayers[indexOfLayers].transform.position = cameraPosition + layerCenterPositionToCamera - layerCenterLocalPosition;
                    }
                    else if(mLayers[indexOfLayers].ParallexType == EParallexType.Customize)
                    {
                        Vector2 layerCenterPositionToCamera = worldCenterLocalPositionToCamera * mLayers[indexOfLayers].ParallexRatio;
                        mLayers[indexOfLayers].transform.position = cameraPosition + layerCenterPositionToCamera - layerCenterLocalPosition;
                    }
                    else if(mLayers[indexOfLayers].ParallexType == EParallexType.Endless)
                    {
                        Vector2 layerCenterPositionToCamera = worldCenterLocalPositionToCamera * mLayers[indexOfLayers].ParallexRatio;
                        mLayers[indexOfLayers].transform.position = cameraPosition + layerCenterPositionToCamera - layerCenterLocalPosition;
                        mLayers[indexOfLayers].UpdateEndlessLayer(cameraPosition);
                    }
                }
            }
        }
    }
}
