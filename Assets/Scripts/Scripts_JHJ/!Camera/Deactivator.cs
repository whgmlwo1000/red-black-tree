﻿using UnityEngine;
using System.Collections;
using CameraManagement;

namespace RedBlackTree
{
    public class Deactivator : MonoBehaviour
    {
        public void FixedUpdate()
        {
            if(!StateManager.Pause.State)
            {
                Vector2 curPosition = transform.position;
                Vector2 cameraPosition = CameraController.Main.Position;

                if(Vector2.Distance(curPosition, cameraPosition) > ValueConstants.DeactivatorDistance)
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }
}