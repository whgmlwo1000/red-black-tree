﻿using UnityEngine;
using UnityEditor;

namespace CameraManagement
{
    [CustomEditor(typeof(ParallexBackground))]
    public class ParallexBackgroundEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SerializedProperty minWorldAnchorProp = serializedObject.FindProperty("mMinWorldAnchor");
            SerializedProperty maxWorldAnchorProp = serializedObject.FindProperty("mMaxWorldAnchor");

            minWorldAnchorProp.vector2Value = EditorGUILayout.Vector2Field(new GUIContent("Min World Anchor", "좌측 하단 월드 앵커"), minWorldAnchorProp.vector2Value);
            maxWorldAnchorProp.vector2Value = EditorGUILayout.Vector2Field(new GUIContent("Max World Anchor", "우측 상단 월드 앵커"), maxWorldAnchorProp.vector2Value);

            minWorldAnchorProp.vector2Value = Vector2.Min(minWorldAnchorProp.vector2Value, maxWorldAnchorProp.vector2Value);

            serializedObject.ApplyModifiedProperties();
        }

        public void OnSceneGUI()
        {
            ParallexBackground background = target as ParallexBackground;

            Vector2 minWorldAnchor = background.MinWorldAnchor;
            Vector2 maxWorldAnchor = background.MaxWorldAnchor;

            Handles.DrawSolidRectangleWithOutline(new Rect(background.MinWorldAnchor, background.MaxWorldAnchor - background.MinWorldAnchor), new Color(0.5f, 0.5f, 1.0f, 0.3f), new Color(0.2f, 0.2f, 1.0f, 0.7f));

            Handles.DrawLine(new Vector3(minWorldAnchor.x, (minWorldAnchor.y + maxWorldAnchor.y) * 0.5f), new Vector3(maxWorldAnchor.x, (minWorldAnchor.y + maxWorldAnchor.y) * 0.5f));
            Handles.DrawLine(new Vector3((minWorldAnchor.x + maxWorldAnchor.x) * 0.5f, minWorldAnchor.y), new Vector3((minWorldAnchor.x + maxWorldAnchor.x) * 0.5f, maxWorldAnchor.y));
        }
    }
}