﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

namespace CameraManagement
{
    [CustomEditor(typeof(CameraController))]
    public class CameraControllerEditor : Editor
    {
        private GUIContent mVerticalSizeContent = new GUIContent("Vertical Size", "카메라의 OrthographicSize");
        private GUIContent mSmoothnessContent = new GUIContent("Smoothness", "카메라 이동시 부드러움 정도");
        private GUIContent mTargetContent = new GUIContent("Target", "카메라가 따라다니는 대상");
        private GUIContent mBlackOutContent = new GUIContent("Black Out Panel", "Black Out Overlay Panel");
        private GUIContent mWhiteOutContent = new GUIContent("White Out Panel", "White Out Overlay Panel");
        private GUIContent mBlackOutPanelContent = new GUIContent("Black Out Intensity");
        private GUIContent mWhiteOutPanelContent = new GUIContent("White Out Intensity");


        public override void OnInspectorGUI()
        {
            CameraController controller = target as CameraController;
            Camera[] cameras = controller.GetComponentsInChildren<Camera>(true);
            CameraComponent[] components = controller.GetComponentsInChildren<CameraComponent>(true);

            SerializedProperty verticalSizeProp = serializedObject.FindProperty("mVerticalSize");
            SerializedProperty smoothnessProp = serializedObject.FindProperty("mSmoothness");
            SerializedProperty targetProp = serializedObject.FindProperty("mTarget");
            SerializedProperty blackOutPanelProp = serializedObject.FindProperty("mBlackOutPanel");
            SerializedProperty whiteOutPanelProp = serializedObject.FindProperty("mWhiteOutPanel");

            verticalSizeProp.floatValue = EditorGUILayout.FloatField(mVerticalSizeContent, verticalSizeProp.floatValue);
            verticalSizeProp.floatValue = Mathf.Max(verticalSizeProp.floatValue, 0.0f);
            for(int indexOfCamera = 0; indexOfCamera < cameras.Length; indexOfCamera++)
            {
                cameras[indexOfCamera].orthographicSize = verticalSizeProp.floatValue;
            }

            smoothnessProp.floatValue = EditorGUILayout.FloatField(mSmoothnessContent, smoothnessProp.floatValue);
            smoothnessProp.floatValue = Mathf.Max(smoothnessProp.floatValue, 0.0f);
            targetProp.objectReferenceValue = EditorGUILayout.ObjectField(mTargetContent, targetProp.objectReferenceValue, typeof(CameraTarget), true);

            blackOutPanelProp.objectReferenceValue = EditorGUILayout.ObjectField(mBlackOutContent, blackOutPanelProp.objectReferenceValue, typeof(Image), true);
            whiteOutPanelProp.objectReferenceValue = EditorGUILayout.ObjectField(mWhiteOutContent, whiteOutPanelProp.objectReferenceValue, typeof(Image), true);

            if(blackOutPanelProp.objectReferenceValue != null)
            {
                Image blackOutPanel = blackOutPanelProp.objectReferenceValue as Image;
                blackOutPanel.color = new Color(0.0f, 0.0f, 0.0f, EditorGUILayout.Slider(mBlackOutPanelContent, blackOutPanel.color.a, 0.0f, 1.0f));
            }

            if(whiteOutPanelProp.objectReferenceValue != null)
            {
                Image whiteOutPanel = whiteOutPanelProp.objectReferenceValue as Image;
                whiteOutPanel.color = new Color(1.0f, 1.0f, 1.0f, EditorGUILayout.Slider(mWhiteOutPanelContent, whiteOutPanel.color.a, 0.0f, 1.0f));
            }

            controller.Awake();
            for(int indexOfComp = 0; indexOfComp < components.Length; indexOfComp++)
            {
                components[indexOfComp].Awake();
            }
            controller.LetterBox(EditorGUILayout.Slider(new GUIContent("Letter Box"), controller.LetterBoxVerticalRatio, 0.0f, 1.0f));

            serializedObject.ApplyModifiedProperties();
        }

        public void OnSceneGUI()
        {
            CameraController controller = target as CameraController;
            controller.Awake();
            for(int indexOfCamera = 0; indexOfCamera < controller.Objects.Length; indexOfCamera++)
            {
                controller.Objects[indexOfCamera].Awake();
            }

            Vector2 cameraPosition = controller.transform.position;
            float verticalSize = controller.VerticalSize;
            float horizontalSize = controller.HorizontalSize;

            Handles.DrawLine(new Vector3(cameraPosition.x, cameraPosition.y - verticalSize), new Vector3(cameraPosition.x, cameraPosition.y + verticalSize));
            Handles.DrawLine(new Vector3(cameraPosition.x - horizontalSize, cameraPosition.y), new Vector3(cameraPosition.x + horizontalSize, cameraPosition.y));
        }
    }
}