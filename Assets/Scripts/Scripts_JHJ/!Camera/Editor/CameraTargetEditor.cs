﻿using UnityEngine;
using UnityEditor;

namespace CameraManagement
{
    [CustomEditor(typeof(CameraTarget))]
    public class CameraTargetEditor : Editor
    {
        private GUIContent mLeftBottomLimitContent = new GUIContent("Left Bottom Limitation", "카메라가 벗어나서는 안되는 구역의 좌측 하단 로컬 좌표");
        private GUIContent mRightTopLimitContent = new GUIContent("Right Top Limitation", "카메라가 벗어나서는 안되는 구역의 우측 상단 로컬 좌표");
        private GUIContent mInertiaLimitContent = new GUIContent("Inertia Limitation", "적용되는 관성의 한계값");
        private GUIContent mInertiaConditionContent = new GUIContent("Inertia Condition", "관성이 적용되기 시작하는 최소의 움직임");
        private GUIContent mInertiaSpeedContent = new GUIContent("Inertia Speed", "관성의 속도");

        public override void OnInspectorGUI()
        {
            SerializedProperty leftBottomLimitProp = serializedObject.FindProperty("mLeftBottomLimit");
            SerializedProperty rightTopLimitProp = serializedObject.FindProperty("mRightTopLimit");
            SerializedProperty inertiaLimitProp = serializedObject.FindProperty("mInertiaLimit");
            SerializedProperty inertiaConditionProp = serializedObject.FindProperty("mInertiaCondition");
            SerializedProperty inertiaSpeedProp = serializedObject.FindProperty("mInertiaSpeed");

            Vector2 leftBottomLimit = EditorGUILayout.Vector2Field(mLeftBottomLimitContent, leftBottomLimitProp.vector2Value);
            Vector2 rightTopLimit = EditorGUILayout.Vector2Field(mRightTopLimitContent, rightTopLimitProp.vector2Value);
            leftBottomLimitProp.vector2Value = Vector2.Min(leftBottomLimit, rightTopLimit);
            rightTopLimitProp.vector2Value = rightTopLimit;

            inertiaLimitProp.floatValue = EditorGUILayout.FloatField(mInertiaLimitContent, inertiaLimitProp.floatValue);
            inertiaLimitProp.floatValue = Mathf.Max(inertiaLimitProp.floatValue, 0.0f);

            inertiaConditionProp.floatValue = EditorGUILayout.FloatField(mInertiaConditionContent, inertiaConditionProp.floatValue);
            inertiaConditionProp.floatValue = Mathf.Max(inertiaConditionProp.floatValue, 0.0f);

            inertiaSpeedProp.floatValue = EditorGUILayout.FloatField(mInertiaSpeedContent, inertiaSpeedProp.floatValue);
            inertiaSpeedProp.floatValue = Mathf.Max(inertiaSpeedProp.floatValue, 0.0f);

            serializedObject.ApplyModifiedProperties();
        }

        public void OnSceneGUI()
        {
            CameraTarget cameraTarget = target as CameraTarget;

            Vector2 targetPosition = cameraTarget.transform.position;
            Vector2 leftBottomLimit = targetPosition + cameraTarget.LeftBottomLimit;
            Vector2 rightTopLimit = targetPosition + cameraTarget.RightTopLimit;
            float inertiaLimit = cameraTarget.InertialLimit;

            Handles.DrawSolidRectangleWithOutline(new Rect(leftBottomLimit, rightTopLimit - leftBottomLimit)
                , new Color(1.0f, 0.5f, 0.5f, 0.2f), new Color(1.0f, 0.1f, 0.1f, 0.2f));

            Handles.DrawLine(new Vector3(targetPosition.x - inertiaLimit, targetPosition.y)
                , new Vector3(targetPosition.x + inertiaLimit, targetPosition.y));

            Handles.RadiusHandle(Quaternion.identity, new Vector3(targetPosition.x + cameraTarget.Inertia, targetPosition.y), 0.5f);
        }
    }
}