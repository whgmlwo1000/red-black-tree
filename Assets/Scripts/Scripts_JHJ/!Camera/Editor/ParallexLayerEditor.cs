﻿using UnityEngine;
using UnityEditor;

namespace CameraManagement
{
    [CustomEditor(typeof(ParallexLayer))]
    public class ParallexLayerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            ParallexLayer layer = target as ParallexLayer;

            SerializedProperty parallexTypeProp = serializedObject.FindProperty("mParallexType");
            SerializedProperty parallexRatioProp = serializedObject.FindProperty("mParallexRatio");
            SerializedProperty minAnchorProp = serializedObject.FindProperty("mMinAnchor");
            SerializedProperty maxAnchorProp = serializedObject.FindProperty("mMaxAnchor");
            SerializedProperty isEndlessInHorizontalProp = serializedObject.FindProperty("mIsEndlessInHorizontal");
            SerializedProperty isEndlessInVerticalProp = serializedObject.FindProperty("mIsEndlessInVertical");
            SerializedProperty layerProp = serializedObject.FindProperty("mLayer");

            EParallexType parallexType = (EParallexType)parallexTypeProp.intValue;
            EParallexType newParallexType = (EParallexType)EditorGUILayout.EnumPopup(new GUIContent("Parallex Type", "패럴렉스 타입"), parallexType);
            parallexTypeProp.intValue = (int)newParallexType;

            Transform minAnchor = minAnchorProp.objectReferenceValue as Transform;
            if(minAnchor == null)
            {
                minAnchor = layer.transform.Find("AnchorMin");
                if (minAnchor == null)
                {
                    minAnchor = new GameObject("Min Anchor").transform;
                    minAnchor.SetParent(layer.transform);
                    minAnchor.localPosition = new Vector3(-1.0f, -1.0f, 0.0f);
                    minAnchor.localRotation = Quaternion.identity;
                    minAnchor.localScale = Vector3.one;
                }
                else
                {
                    minAnchor.name = "Min Anchor";
                }
                minAnchorProp.objectReferenceValue = minAnchor;
            }
            EditorGUILayout.ObjectField(new GUIContent("Min Anchor", "좌측 하단 앵커"), minAnchorProp.objectReferenceValue, typeof(Transform), true);

            Transform maxAnchor = maxAnchorProp.objectReferenceValue as Transform;
            if (maxAnchor == null)
            {
                maxAnchor = layer.transform.Find("AnchorMax");
                if (maxAnchor == null)
                {
                    maxAnchor = new GameObject("Max Anchor").transform;
                    maxAnchor.SetParent(layer.transform);
                    maxAnchor.localPosition = new Vector3(1.0f, 1.0f, 0.0f);
                    maxAnchor.localRotation = Quaternion.identity;
                    maxAnchor.localScale = Vector3.one;
                }
                else
                {
                    maxAnchor.name = "Max Anchor";
                }
                maxAnchorProp.objectReferenceValue = maxAnchor;
            }
            EditorGUILayout.ObjectField(new GUIContent("Max Anchor", "우측 상단 앵커"), maxAnchorProp.objectReferenceValue, typeof(Transform), true);

            if (parallexType == EParallexType.Automatic)
            {
            }
            else if(parallexType == EParallexType.Customize)
            {
                parallexRatioProp.vector2Value = EditorGUILayout.Vector2Field(new GUIContent("Parallex Velocity", "X 축, Y 축 패럴렉스 속도"), parallexRatioProp.vector2Value);
            }
            else if(parallexType == EParallexType.Endless)
            {
                parallexRatioProp.vector2Value = EditorGUILayout.Vector2Field(new GUIContent("Parallex Velocity", "X 축, Y 축 패럴렉스 속도"), parallexRatioProp.vector2Value);
                isEndlessInHorizontalProp.boolValue = EditorGUILayout.Toggle(new GUIContent("Is Endless In Horizontal", "수평 무제한"), isEndlessInHorizontalProp.boolValue);
                isEndlessInVerticalProp.boolValue = EditorGUILayout.Toggle(new GUIContent("Is Endless In Vertical", "수직 무제한"), isEndlessInVerticalProp.boolValue);
                layerProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Layer", "반복되는 대상"), layerProp.objectReferenceValue, typeof(Transform), true);
            }

            serializedObject.ApplyModifiedProperties();
        }

        public void OnSceneGUI()
        {
            ParallexLayer layer = target as ParallexLayer;

            Transform minAnchor = layer.MinAnchor;
            Transform maxAnchor = layer.MaxAnchor;
            if(minAnchor != null && maxAnchor != null)
            {
                Vector3 minAnchorPosition = minAnchor.position;
                Vector3 maxAnchorPosition = maxAnchor.position;
                float scale = 1.0f;

                Handles.TransformHandle(ref minAnchorPosition, Quaternion.identity, ref scale);
                Handles.TransformHandle(ref maxAnchorPosition, Quaternion.identity, ref scale);

                minAnchorPosition = Vector3.Min(minAnchorPosition, maxAnchorPosition);

                minAnchor.position = minAnchorPosition;
                maxAnchor.position = maxAnchorPosition;

                Handles.DrawSolidRectangleWithOutline(new Rect(minAnchorPosition, maxAnchorPosition - minAnchorPosition), new Color(0.5f, 0.5f, 1.0f, 0.3f), new Color(0.0f, 0.0f, 1.0f, 0.7f));
                Handles.DrawLine(new Vector3(minAnchorPosition.x, (minAnchorPosition.y + maxAnchorPosition.y) * 0.5f), new Vector3(maxAnchorPosition.x, (minAnchorPosition.y + maxAnchorPosition.y) * 0.5f));
                Handles.DrawLine(new Vector3((minAnchorPosition.x + maxAnchorPosition.x) * 0.5f, minAnchorPosition.y), new Vector3((minAnchorPosition.x + maxAnchorPosition.x) * 0.5f, maxAnchorPosition.y));
            }
        }
    }
}