﻿using UnityEngine;
using UnityEditor;

namespace CameraManagement
{
    [CustomEditor(typeof(CameraBoundaryLine))]
    public class CameraBoundaryLineEditor : Editor
    {
        private GUIContent mStartPointContent = new GUIContent("Start Point", "Line 시작점");
        private GUIContent mEndPointContent = new GUIContent("End Point", "Line 끝점");

        public override void OnInspectorGUI()
        {
            CameraBoundaryLine line = target as CameraBoundaryLine;
            SerializedProperty startPointProp = serializedObject.FindProperty("mStartPoint");
            SerializedProperty endPointProp = serializedObject.FindProperty("mEndPoint");

            Transform startPoint = startPointProp.objectReferenceValue as Transform;
            if(startPoint == null)
            {
                startPoint = new GameObject("Start Point").transform;
                startPoint.SetParent(line.transform);
                startPoint.localPosition = new Vector3(-1.0f, 0.0f, 0.0f);
                startPoint.localRotation = Quaternion.identity;
                startPoint.localScale = Vector3.one;
                startPointProp.objectReferenceValue = startPoint;
                serializedObject.ApplyModifiedProperties();
            }

            Transform endPoint = endPointProp.objectReferenceValue as Transform;
            if(endPoint == null)
            {
                endPoint = new GameObject("End Point").transform;
                endPoint.SetParent(line.transform);
                endPoint.localPosition = new Vector3(1.0f, 0.0f, 0.0f);
                endPoint.localRotation = Quaternion.identity;
                endPoint.localScale = Vector3.one;
                endPointProp.objectReferenceValue = endPoint;
                serializedObject.ApplyModifiedProperties();
            }

            EditorGUILayout.ObjectField(mStartPointContent, startPointProp.objectReferenceValue, typeof(Transform), true);
            EditorGUILayout.ObjectField(mEndPointContent, endPointProp.objectReferenceValue, typeof(Transform), true);
        }

        public void OnSceneGUI()
        {
            CameraBoundaryLine line = target as CameraBoundaryLine;

            if(line.StartPoint != null && line.EndPoint != null)
            {
                Handles.DrawLine(line.StartPoint.position, line.EndPoint.position);

                Vector3 startPointPosition = line.StartPoint.position;
                Vector3 endPointPosition = line.EndPoint.position;
                float scale = 1.0f;

                Handles.TransformHandle(ref startPointPosition, Quaternion.identity, ref scale);
                Handles.TransformHandle(ref endPointPosition, Quaternion.identity, ref scale);

                line.StartPoint.position = startPointPosition;
                line.EndPoint.position = endPointPosition;
            }
        }
    }
}