﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using ObjectManagement;

namespace CameraManagement
{
    public enum ECameraType
    {
        None = -1,

        /// <summary>
        /// 아무것도 비추지 않는 빈 공간
        /// </summary>
        Void,
        Object,
        UI, 

        Count
    }

    public enum EHorizontalDirection
    {
        Left = -1, Right = 1
    }

    public class CameraController : Handler<ECameraType, CameraComponent>
    {
        public static CameraController Main { get; private set; }

        [SerializeField]
        private float mVerticalSize = 12.0f;
        // 카메라 확대 및 축소 배율
        private float mZoomMagnification = 1.0f;
        public float LetterBoxVerticalRatio { get; private set; } = 1.0f;

        public float VerticalSize
        {
            get { return mVerticalSize; }
            set
            {
                value = Mathf.Max(value, 0.0f);
                mVerticalSize = value;
                SetOrthographicSize(mVerticalSize * LetterBoxVerticalRatio / mZoomMagnification);
            }
        }

        public float HorizontalSize
        {
            get
            {
                Camera camera = Objects[0].Camera;
                return mVerticalSize / camera.pixelHeight * camera.pixelWidth;
            }
            set
            {
                value = Mathf.Max(value, 0.0f);
                Camera camera = GetCamera(ECameraType.Void);
                mVerticalSize = value / camera.pixelWidth * camera.pixelHeight;
                SetOrthographicSize(mVerticalSize * LetterBoxVerticalRatio / mZoomMagnification);
            }
        }

        [SerializeField]
        private float mSmoothness = 10.0f;
        public float Smoothness
        {
            get { return mSmoothness; }
            set
            {
                mSmoothness = Mathf.Max(value, 1.0f);
            }
        }

        [SerializeField]
        private CameraTarget mTarget = null;
        public CameraTarget Target
        {
            get { return mTarget; }
            set { mTarget = value; }
        }

        #region Shake Field
        public bool IsShakeTriggered { get; private set; }
        private float mShakeRemainTime;
        private float mShakePower;
        private Vector2 mShakeDstPosition;
        private float mShakeCycleRemainTime;
        [SerializeField]
        private float mShakeCycle = 0.02f;
        #endregion
        #region BlackOut Field
        [SerializeField]
        private Image mBlackOutPanel = null;

        public bool IsBlackOutTriggered { get; private set; }
        private float mBlackOutRemainTime;
        private float mBlackOutTime;
        private float mBlackOutSrcIntensity;
        private float mBlackOutDstIntensity;
        #endregion
        #region WhiteOutField

        [SerializeField]
        private Image mWhiteOutPanel = null;

        public bool IsWhiteOutTriggered { get; private set; }
        private float mWhiteOutRemainTime;
        private float mWhiteOutTime;
        private float mWhiteOutSrcIntensity;
        private float mWhiteOutDstIntensity;

        #endregion
        #region LetterBox Field
        public bool IsLetterBoxTriggered { get; private set; }
        private float mLetterBoxRemainTime;
        private float mLetterBoxTime;
        private Rect mLetterBoxSrcRect;
        private Rect mLetterBoxDstRect;
        private float mLetterBoxSrcVerticalRatio;
        private float mLetterBoxDstVerticalRatio;
        #endregion
        #region Zoom Field
        public bool IsZoomTriggered { get; private set; }
        private float mZoomTime;
        private float mZoomRemainTime;
        private float mZoomSrcMagnification;
        private float mZoomDstMagnification;
        #endregion

        public Vector2 Position { get; set; }

        public override void Awake()
        {
            Application.targetFrameRate = 60;
            Main = this;
            base.Awake();

            Position = transform.position;
        }

        public void Start()
        {
            VerticalSize = mVerticalSize;
        }

        public void FixedUpdate()
        {
            if (mTarget != null)
            {
                Vector2 destination = mTarget.GetDestination(VerticalSize, HorizontalSize);

                // Target의 실제 좌표
                Vector2 targetPosition = mTarget.transform.position;
                // Target Area 의 좌측 하단 좌표
                Vector2 leftBottomLimit = targetPosition + mTarget.LeftBottomLimit;
                // TargetArea 의 우측 상단 좌표
                Vector2 rightTopLimit = targetPosition + mTarget.RightTopLimit;
                // 이전 카메라 좌표
                Vector2 prevPosition = Position;
                // 다음 카메라 위치
                Vector2 nextPosition = prevPosition + (destination - prevPosition) / mSmoothness;
                nextPosition = new Vector2(Mathf.Clamp(nextPosition.x, leftBottomLimit.x, rightTopLimit.x)
                    , Mathf.Clamp(nextPosition.y, leftBottomLimit.y, rightTopLimit.y));
                Position = nextPosition;
            }
            Vector2 position = Position + ExecuteShake();

            transform.position = position;
        }

        public void LateUpdate()
        {
            ExecuteBlackOut();
            ExecuteWhiteOut();
            ExecuteLetterBoxAndZoom();
        }

        public void OnDestroy()
        {
            Main = null;
        }

        /// <summary>
        /// 전달된 타입의 카메라 반환 메소드
        /// </summary>
        public Camera GetCamera(ECameraType type)
        {
            return Get(type).Camera;
        }

        /// <summary>
        /// time 동안 power의 강도로 카메라를 흔드는 메소드
        /// </summary>
        public void Shake(float time, float power)
        {
            IsShakeTriggered = true;

            mShakeRemainTime = time;
            mShakeCycleRemainTime = mShakeCycle;

            mShakePower = power;
        }

        /// <summary>
        /// time 에 걸쳐서 intensity 만큼 화면을 Black Out 시키는 메소드
        /// </summary>
        public void BlackOut(float time, float intensity)
        {
            IsBlackOutTriggered = true;

            mBlackOutTime = time;
            mBlackOutRemainTime = mBlackOutTime;

            mBlackOutSrcIntensity = mBlackOutPanel.color.a;
            mBlackOutDstIntensity = intensity;
        }

        /// <summary>
        /// 화면을 즉시 intensity로 암전시키는 메소드
        /// </summary>
        public void BlackOut(float intensity)
        {
            mBlackOutPanel.color = new Color(0.0f, 0.0f, 0.0f, intensity);
        }

        /// <summary>
        /// time 에 걸쳐서 intensity 만큼 화면을 White Out 시키는 메소드
        /// </summary>
        public void WhiteOut(float time, float intensity)
        {
            IsWhiteOutTriggered = true;

            mWhiteOutTime = time;
            mWhiteOutRemainTime = mWhiteOutTime;

            mWhiteOutSrcIntensity = mWhiteOutPanel.color.a;
            mWhiteOutDstIntensity = intensity;
        }

        /// <summary>
        /// 즉시 intensity 만큼 화면을 White Out 시키는 메소드
        /// </summary>
        public void WhiteOut(float intensity)
        {
            mWhiteOutPanel.color = new Color(1.0f, 1.0f, 1.0f, intensity);
        }

        /// <summary>
        /// time 에 걸쳐서 verticalRatio을 가진 Letter Box 로 만드는 메소드
        /// </summary>
        public void LetterBox(float time, float verticalRatio)
        {
            IsLetterBoxTriggered = true;

            mLetterBoxTime = time;
            mLetterBoxRemainTime = mLetterBoxTime;

            Camera camera = GetCamera(ECameraType.Object);
            mLetterBoxSrcRect = camera.rect;
            mLetterBoxDstRect = new Rect(mLetterBoxSrcRect.x, (1.0f - verticalRatio) * 0.5f, mLetterBoxSrcRect.width, verticalRatio);

            mLetterBoxSrcVerticalRatio = LetterBoxVerticalRatio;
            mLetterBoxDstVerticalRatio = mLetterBoxDstRect.height / mLetterBoxSrcRect.height;
        }

        /// <summary>
        /// 즉시 verticalRatio 를 가지는 LetterBox 로 만드는 메소드
        /// </summary>
        public void LetterBox(float verticalRatio)
        {
            Camera camera = GetCamera(ECameraType.Object);
            Rect cameraRect = camera.rect;
            cameraRect = new Rect(cameraRect.x, (1.0f - verticalRatio) * 0.5f, cameraRect.width, verticalRatio);
            // Void 카메라를 제외한 모든 카메라에 대해
            for (ECameraType type = ECameraType.Void + 1; type < ECameraType.Count - 1; type++)
            {
                // Rect 를 갱신한다.
                GetCamera(type).rect = cameraRect;
            }

            LetterBoxVerticalRatio = verticalRatio;
            VerticalSize = mVerticalSize;
        }

        /// <summary>
        /// time 에 걸쳐서 magnification의 배율로 설정하는 메소드
        /// </summary>
        /// <param name="time"></param>
        /// <param name="magnification"></param>
        public void Zoom(float time, float magnification)
        {
            IsZoomTriggered = true;

            mZoomTime = time;
            mZoomRemainTime = mZoomTime;

            mZoomSrcMagnification = mZoomMagnification;
            mZoomDstMagnification = magnification;
        }

        /// <summary>
        /// 즉시 magnification의 배율로 설정하는 메소드
        /// </summary>
        public void Zoom(float magnification)
        {
            mZoomMagnification = magnification;
            VerticalSize = mVerticalSize;
        }

        /// <summary>
        /// Shake 기능이 활성화되면 매 프레임마다 Shake 좌표를 반환하는 메소드
        /// </summary>
        private Vector2 ExecuteShake()
        {
            Vector2 shakePosition = Vector2.zero;

            if (IsShakeTriggered)
            {
                // Cycle 시간이 남았다면
                if (mShakeCycleRemainTime > 0.0f)
                {
                    mShakeCycleRemainTime -= Time.fixedDeltaTime;
                    float shakeCycleHalf = mShakeCycle * 0.5F;
                    // Cycle 시간이 주기의 반보다 크다면
                    if (mShakeCycleRemainTime > shakeCycleHalf)
                    {
                        shakePosition = Vector2.Lerp(Vector2.zero, mShakeDstPosition,
                            // (주기 - 남은 시간) / (주기 x 0.5)
                            (mShakeCycle - mShakeCycleRemainTime) / shakeCycleHalf);
                    }
                    // Cycle 시간이 주기의 반보다 작다면
                    else
                    {
                        shakePosition = Vector2.Lerp(mShakeDstPosition, Vector2.zero,
                            // (주기 x 0.5 - 남은 시간) / (주기 x 0.5)
                            (shakeCycleHalf - mShakeCycleRemainTime) / shakeCycleHalf);
                    }
                }
                // Cycle 시간이 0이하라면
                else
                {
                    // cycle 남는 시간은 0 이하의 값을 가지기 때문에 주기에 [-] 연산을 취함
                    mShakeRemainTime -= mShakeCycle - mShakeCycleRemainTime;
                    // Shake 시간이 남았다면
                    if (mShakeRemainTime > 0.0f)
                    {
                        mShakeCycleRemainTime = mShakeCycle;
                        mShakeDstPosition = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
                        mShakeDstPosition.Normalize();
                        mShakeDstPosition *= mShakePower;
                    }
                    // Shake 시간이 남지 않았다면
                    else
                    {
                        // Shake 기능을 끝낸다.
                        IsShakeTriggered = false;
                    }
                }
            }

            return shakePosition;
        }

        /// <summary>
        /// BlackOut 기능이 활성화되면 매 프레임마다 BlackOut 기능을 수행하는 메소드
        /// </summary>
        private void ExecuteBlackOut()
        {
            if (IsBlackOutTriggered)
            {
                // BlackOut 시간이 남아있다면
                if (mBlackOutRemainTime > 0.0f)
                {
                    // 남은 시간을 감소
                    mBlackOutRemainTime -= Time.deltaTime;
                    // BlackOut 패널의 알파값 갱신
                    mBlackOutPanel.color = new Color(0.0f, 0.0f, 0.0f,
                        Mathf.Lerp(mBlackOutSrcIntensity, mBlackOutDstIntensity,
                        (mBlackOutTime - mBlackOutRemainTime) / mBlackOutTime));
                }
                // BlackOut 시간이 남아있지 않다면
                else
                {
                    // BlackOut 패널의 알파값 갱신
                    mBlackOutPanel.color = new Color(0.0f, 0.0f, 0.0f, mBlackOutDstIntensity);
                    // BlackOut 완료
                    IsBlackOutTriggered = false;
                }
            }
        }
        /// <summary>
        /// WhiteOut 기능이 활성화되면 매 프레임마다 WhiteOut 기능을 수행하는 메소드
        /// </summary>
        private void ExecuteWhiteOut()
        {
            // WhiteOut 시간이 남아있다면
            if (mWhiteOutRemainTime > 0.0f)
            {
                // 남은 시간을 감소
                mWhiteOutRemainTime -= Time.deltaTime;
                // WhiteOut 패널의 알파값 갱신
                mWhiteOutPanel.color = new Color(1.0f, 1.0f, 1.0f,
                    Mathf.Lerp(mWhiteOutSrcIntensity, mWhiteOutDstIntensity,
                    (mWhiteOutTime - mWhiteOutRemainTime) / mWhiteOutTime));
            }
            // WhiteOut 시간이 남아닜지 않다면
            else
            {
                // Whiteout 패널의 알파값 갱신
                mWhiteOutPanel.color = new Color(1.0f, 1.0f, 1.0f, mWhiteOutDstIntensity);
                // WhiteOut 완료
                IsWhiteOutTriggered = false;
            }
        }

        /// <summary>
        /// LetterBox 기능이 활성화되면 매 프레임마다 LetterBox 기능을 수행하는 메소드
        /// 반드시 Zoom 기능을 수행한 뒤에 선언되어야 한다.
        /// </summary>
        private void ExecuteLetterBoxAndZoom()
        {
            if (IsLetterBoxTriggered)
            {
                // LetterBox 시간이 남았다면
                if (mLetterBoxRemainTime > 0.0f)
                {
                    // 남은 시간을 감소
                    mLetterBoxRemainTime -= Time.deltaTime;
                    // Lerp 연산을 하기위한 Delta값
                    float delta = (mLetterBoxTime - mLetterBoxRemainTime) / mLetterBoxTime;
                    // 현재 Rect
                    Rect curRect = GetCamera(ECameraType.Object).rect;
                    // 갱신할 Rect
                    Rect nextRect = new Rect(curRect.x,
                        Mathf.Lerp(mLetterBoxSrcRect.y, mLetterBoxDstRect.y, delta),
                        curRect.width,
                        Mathf.Lerp(mLetterBoxSrcRect.height, mLetterBoxDstRect.height, delta));

                    // Void 카메라를 제외한 모든 카메라에 대해
                    for (ECameraType type = ECameraType.Void + 1; type < ECameraType.Count - 1; type++)
                    {
                        // Rect를 갱신한다.
                        GetCamera(type).rect = nextRect;
                    }
                    // Rect 만 갱신하면 화면에 비춰지는 요소들이 작아져 보이기 때문에 VerticalSize를 갱신한다.
                    LetterBoxVerticalRatio = Mathf.Lerp(mLetterBoxSrcVerticalRatio, mLetterBoxDstVerticalRatio, delta);
                }
                // LetterBox 시간이 남아있지 않다면
                else
                {
                    // 현재 Rect
                    Rect curRect = GetCamera(ECameraType.Object).rect;
                    // 갱신할 Rect
                    Rect nextRect = new Rect(curRect.x, mLetterBoxDstRect.y, curRect.width, mLetterBoxDstRect.height);

                    // Void 카메라를 제외한 모든 카메라에 대해
                    for (ECameraType type = ECameraType.Void + 1; type < ECameraType.Count - 1; type++)
                    {
                        // Rect 를 갱신한다.
                        GetCamera(type).rect = nextRect;
                    }

                    LetterBoxVerticalRatio = mLetterBoxDstVerticalRatio;
                    // LetterBox 기능 완료
                    IsLetterBoxTriggered = false;
                }
            }
            if (IsZoomTriggered)
            {
                // Zoom 기능 수행 시간이 남았다면
                if (mZoomRemainTime > 0.0f)
                {
                    // 시간 감소
                    mZoomRemainTime -= Time.deltaTime;
                    // VerticalSize를 갱신
                    mZoomMagnification = Mathf.Lerp(mZoomSrcMagnification, mZoomDstMagnification, (mZoomTime - mZoomRemainTime) / mZoomTime);
                }
                // Zoom 기능 수행 시간이 끝났다면
                else
                {
                    mZoomMagnification = mZoomDstMagnification;
                    // Zoom 기능 완료
                    IsZoomTriggered = false;
                }
            }

            if (IsLetterBoxTriggered || IsZoomTriggered)
            {
                VerticalSize = mVerticalSize;
            }
        }

        private void SetOrthographicSize(float size)
        {
            for (int indexOfObject = 0; indexOfObject < Objects.Length; indexOfObject++)
            {
                Objects[indexOfObject].Camera.orthographicSize = size;
            }
        }
    }
}
