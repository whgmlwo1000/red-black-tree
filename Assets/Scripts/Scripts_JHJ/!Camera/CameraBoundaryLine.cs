﻿using UnityEngine;
using System.Collections;


namespace CameraManagement
{
    public class CameraBoundaryLine : MonoBehaviour
    {
        [SerializeField]
        private Transform mStartPoint = null;
        public Transform StartPoint { get { return mStartPoint; } }

        [SerializeField]
        private Transform mEndPoint = null;
        public Transform EndPoint { get { return mEndPoint; } }


        public void OnEnable()
        {
            CameraTarget.AddBoundary(this);
        }

        public void OnDisable()
        {
            CameraTarget.RemoveBoundary(this);
        }

        public bool IsVerticalOverlapped(Vector2 centerPosition, float verticalSize)
        {
            Vector2 startPosition = mStartPoint.position;
            Vector2 endPosition = mEndPoint.position;

            float min_y, max_y;

            if (startPosition.y < endPosition.y)
            {
                min_y = startPosition.y;
                max_y = endPosition.y;
            }
            else
            {
                min_y = endPosition.y;
                max_y = startPosition.y;
            }

            return (centerPosition.x - startPosition.x) * (centerPosition.x - endPosition.x) < -Mathf.Epsilon
                && (centerPosition.y - verticalSize - max_y) * (centerPosition.y + verticalSize - min_y) < -Mathf.Epsilon;
        }

        public bool IsHorizontalOverlapped(Vector2 centerPosition, float horizontalSize)
        {
            Vector2 startPosition = mStartPoint.position;
            Vector2 endPosition = mEndPoint.position;

            float min_x, max_x;

            if(startPosition.x < endPosition.x)
            {
                min_x = startPosition.x;
                max_x = endPosition.x;
            }
            else
            {
                min_x = endPosition.x;
                max_x = startPosition.x;
            }

            return (centerPosition.y - startPosition.y) * (centerPosition.y - endPosition.y) < -Mathf.Epsilon
                && (centerPosition.x - horizontalSize - max_x) * (centerPosition.x + horizontalSize - min_x) < -Mathf.Epsilon;
        }

        public float GetX(float y)
        {
            Vector2 startPosition = mStartPoint.position;
            Vector2 endPosition = mEndPoint.position;

            return (y - startPosition.y) * (endPosition.x - startPosition.x) / (endPosition.y - startPosition.y) + startPosition.x;
        }

        public float GetY(float x)
        {
            Vector2 startPosition = mStartPoint.position;
            Vector2 endPosition = mEndPoint.position;

            return (x - startPosition.x) * (endPosition.y - startPosition.y) / (endPosition.x - startPosition.x) + startPosition.y;
        }
    }
}