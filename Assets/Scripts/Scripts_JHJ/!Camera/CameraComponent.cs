﻿using UnityEngine;
using ObjectManagement;

namespace CameraManagement
{
    [RequireComponent(typeof(Camera))]
    public class CameraComponent : MonoBehaviour, IHandleable<ECameraType>
    {
        [SerializeField]
        private ECameraType mType = ECameraType.None;
        public ECameraType Type { get { return mType; } }

        public Camera Camera { get; private set; }

        public void Awake()
        {
            Camera = GetComponent<Camera>();
        }
    }
}