﻿using UnityEngine;
using System.Collections.Generic;

namespace TouchManagement
{
    public delegate void OnTouch(Touch touch, Vector2 worldPosition);

    /// <summary>
    /// 감지된 터치를 수신할 수 있는 인터페이스
    /// </summary>
    public interface ITouchReceiver
    {
        bool enabled { get; set; }

        int Depth { get; }
        bool IsActive { get; }
        bool GetTouchIn(Vector2 touchWorldPosition);

        event System.Action OnWarmUp;
        event OnTouch OnTouchStart;
        event OnTouch OnTouchStay;
        event OnTouch OnTouchEnd;

        void CallOnWarmUp();
        void CallOnTouchStart(Touch touch, Vector2 worldPosition);
        void CallOnTouchStay(Touch touch, Vector2 worldPosition);
        void CallOnTouchEnd(Touch touch, Vector2 worldPosition);

    }

    /// <summary>
    /// 감지된 터치를 송신할 수 있는 클래스
    /// </summary>
    public class TouchSender : MonoBehaviour
    {
        // 터치 리시버를 우선순위 별로 등록한 사전
        private static Dictionary<int, List<ITouchReceiver>> mReceiverDict = new Dictionary<int, List<ITouchReceiver>>();
        private static List<int> mKeyList = new List<int>();
        // 최대 동시 터치 가능한 리시버 저장 배열
        private static List<ITouchReceiver>[] mReceivers = new List<ITouchReceiver>[10]
        { new List<ITouchReceiver>(), new List<ITouchReceiver>(), new List<ITouchReceiver>(), new List<ITouchReceiver>(), new List<ITouchReceiver>(),
        new List<ITouchReceiver>(), new List<ITouchReceiver>(), new List<ITouchReceiver>(), new List<ITouchReceiver>(), new List<ITouchReceiver>() };
#if UNITY_EDITOR || UNITY_STANDALONE
        // 마우스로 입력된 터치 리시버를 저장할 리스트
        private static List<ITouchReceiver> mMouseReceiver = new List<ITouchReceiver>();
#endif
        // 터치 혹은 마우스 입력이 끝난 리시버를 임시로 저장할 리스트
        private static List<ITouchReceiver> mRemoveReceiverList = new List<ITouchReceiver>();
        // 등록될 리시버가 저장된 큐
        private static Queue<ITouchReceiver> mSaveReceiverQueue = new Queue<ITouchReceiver>();

        // 터치 좌표의 기준점이 되는 카메라
        [SerializeField]
        private Camera mActiveCamera = null;

        public void Update()
        {
            // 등록될 리시버가 큐에 존재하는 경우
            while (mSaveReceiverQueue.Count > 0)
            {
                ITouchReceiver receiver = mSaveReceiverQueue.Dequeue();
                if (!mReceiverDict.ContainsKey(-receiver.Depth))
                {
                    mReceiverDict.Add(-receiver.Depth, new List<ITouchReceiver>());
                    mKeyList.Add(-receiver.Depth);
                    mKeyList.Sort();
                }

                mReceiverDict[-receiver.Depth].Add(receiver);
            }

            CheckTouch();
        }

        public void OnDestroy()
        {
            mSaveReceiverQueue.Clear();

            // 리스트를 제거하지 말고, 리스트를 클리어
            mKeyList.ForEach(delegate (int key)
            {
                mReceiverDict[key].Clear();
            });

            // 터치 리시버 배열 클리어
            for (int fingerId = 0; fingerId < mReceivers.Length; fingerId++)
            {
                mReceivers[fingerId].Clear();
            }
        }

        /// <summary>
        /// ITouchReceiver를 TouchSender에 등록하기 위해 호출되어야 하는 메소드
        /// </summary>
        public static void AddReceiver(ITouchReceiver receiver)
        {
            mSaveReceiverQueue.Enqueue(receiver);
        }

        // 마우스 이전 좌표
        private Vector2 mMousePrevPosition;
        // 마우스 입력 도중 지나간 시간으로 누적되진 않는다.
        private float mMouseDeltaTime;

        private void CheckTouch()
        {
            // 등록된 모든 ITouchReceiver에 대해
            mKeyList.ForEach(delegate (int key)
            {
                // 한번 데워놓는다.
                mReceiverDict[key].ForEach(delegate (ITouchReceiver receiver)
                {
                    if (receiver.IsActive)
                    {
                        receiver.CallOnWarmUp();
                    }
                });
            });

#if UNITY_EDITOR || UNITY_STANDALONE
            // 마우스가 존재한다면 마우스에 대한 입력 처리 시작
            if (Input.mousePresent)
            {
                Vector2 mousePosition = Input.mousePosition;
                Vector2 mouseWorldPosition = mActiveCamera.ScreenToWorldPoint(mousePosition);
                // 마우스 버튼이 눌린 순간
                if (Input.GetMouseButtonDown(0))
                {
                    bool isBlocked = false;
                    // 모든 우선순위별 ITouchReceiver에 대해 
                    mKeyList.ForEach(delegate (int key)
                    {
                        if (!isBlocked)
                        {
                            mReceiverDict[key].ForEach(delegate (ITouchReceiver receiver)
                            {
                                if (receiver.IsActive && receiver.GetTouchIn(mouseWorldPosition))
                                {
                                    isBlocked = true;
                                    mMouseReceiver.Add(receiver);

                                    mMousePrevPosition = mousePosition;
                                    mMouseDeltaTime = Time.deltaTime;

                                    Touch touch = new Touch();
                                    touch.position = mousePosition;
                                    touch.phase = TouchPhase.Began;
                                    touch.deltaPosition = Vector2.zero;
                                    touch.fingerId = -1;
                                    touch.deltaTime = mMouseDeltaTime;

                                    receiver.CallOnTouchStart(touch, mouseWorldPosition);
                                }
                            });
                        }
                    });
                }
                // 마우스 입력에 등록된 ITouchReceiver가 존재하는 경우
                else if (mMouseReceiver.Count > 0)
                {
                    mRemoveReceiverList.Clear();

                    mMouseReceiver.ForEach(delegate (ITouchReceiver receiver)
                    {
                        // 마우스 입력이 감지되고 있는 경우
                        if (receiver.IsActive && Input.GetMouseButton(0))
                        {
                            mMouseDeltaTime += Time.deltaTime;

                            Touch touch = new Touch();
                            touch.position = mousePosition;
                            Vector2 deltaPosition = mousePosition - mMousePrevPosition;
                            touch.deltaPosition = deltaPosition;
                            if (Mathf.Abs(deltaPosition.x) > Mathf.Epsilon || Mathf.Abs(deltaPosition.y) > Mathf.Epsilon)
                            {
                                touch.phase = TouchPhase.Moved;
                            }
                            else
                            {
                                touch.phase = TouchPhase.Stationary;
                            }

                            touch.fingerId = -1;
                            touch.deltaTime = mMouseDeltaTime;

                            mMousePrevPosition = mousePosition;

                            receiver.CallOnTouchStay(touch, mouseWorldPosition);
                        }
                        // 마우스 입력이 감지되지 않았거나 비활성화된 경우에는 End 처리를 수행
                        else
                        {
                            mMouseDeltaTime += Time.deltaTime;

                            Touch touch = new Touch();
                            touch.position = mousePosition;
                            touch.deltaPosition = mousePosition - mMousePrevPosition;
                            touch.phase = TouchPhase.Ended;

                            touch.fingerId = -1;
                            touch.deltaTime = mMouseDeltaTime;

                            receiver.CallOnTouchEnd(touch, mouseWorldPosition);
                            mRemoveReceiverList.Add(receiver);
                        }
                    });

                    mRemoveReceiverList.ForEach(delegate (ITouchReceiver receiver)
                    {
                        mMouseReceiver.Remove(receiver);
                    });
                }
            }
#endif

            // 현재 입력된 모든 터치에 대해
            for (int indexOfTouch = 0; indexOfTouch < Input.touchCount; indexOfTouch++)
            {
                // 터치 구조체 할당
                Touch touch = Input.GetTouch(indexOfTouch);
                // 터치 좌표를 World 좌표로 변환
                Vector2 touchedWorldPosition = mActiveCamera.ScreenToWorldPoint(touch.position);
                if (touch.fingerId < mReceivers.Length)
                {
                    bool isBlocked = false;
                    // 터치가 시작되는 경우
                    if (touch.phase == TouchPhase.Began)
                    {
                        mKeyList.ForEach(delegate (int key)
                        {
                            if (!isBlocked)
                            {
                                mReceiverDict[key].ForEach(delegate (ITouchReceiver receiver)
                                {
                                    if (receiver.IsActive && receiver.GetTouchIn(touchedWorldPosition))
                                    {
                                        isBlocked = true;
                                        mReceivers[touch.fingerId].Add(receiver);
                                        receiver.CallOnTouchStart(touch, touchedWorldPosition);
                                    }
                                });
                            }
                        });
                    }
                    // 터치된 ITouchReceiver가 존재하는 경우
                    else if (mReceivers[touch.fingerId].Count > 0)
                    {
                        mRemoveReceiverList.Clear();

                        mReceivers[touch.fingerId].ForEach(delegate (ITouchReceiver receiver)
                        {
                            // 터치가 진행되는 경우
                            if (receiver.IsActive && (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary))
                            {
                                receiver.CallOnTouchStay(touch, touchedWorldPosition);
                            }
                            else
                            {
                                receiver.CallOnTouchEnd(touch, touchedWorldPosition);
                                mRemoveReceiverList.Add(receiver);
                            }
                        });

                        mRemoveReceiverList.ForEach(delegate (ITouchReceiver receiver)
                        {
                            mReceivers[touch.fingerId].Remove(receiver);
                        });

                    }
                }
            }
        }
    }
}