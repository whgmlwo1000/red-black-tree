﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace TouchManagement
{
    public class TouchBox : MonoBehaviour, ITouchReceiver
    {
        [SerializeField]
        private int mDepth = 0;
        public int Depth { get { return mDepth; } }

        public virtual bool IsActive { get { return gameObject.activeInHierarchy && enabled; } }

        [SerializeField]
        private Transform mLeftBottomAnchor = null;
        public Transform LeftBottomAnchor { get { return mLeftBottomAnchor; } }
        [SerializeField]
        private Transform mRightTopAnchor = null;
        public Transform RightTopAnchor { get { return mRightTopAnchor; } }

        private event System.Action mOnWarmUp;
        public event System.Action OnWarmUp
        {
            add { mOnWarmUp += value; }
            remove { mOnWarmUp -= value; }
        }

        private event OnTouch mOnTouchStart;
        public event OnTouch OnTouchStart
        {
            add { mOnTouchStart += value; }
            remove { mOnTouchStart -= value; }
        }
        private event OnTouch mOnTouchStay;
        public event OnTouch OnTouchStay
        {
            add { mOnTouchStay += value; }
            remove { mOnTouchStay -= value; }
        }

        private event OnTouch mOnTouchEnd;
        public event OnTouch OnTouchEnd
        {
            add { mOnTouchEnd += value; }
            remove { mOnTouchEnd -= value; }
        }


        public void Start()
        {
            TouchSender.AddReceiver(this);
            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        public void OnDestroy()
        {
            SceneManager.activeSceneChanged -= OnActiveSceneChanged;
        }

        public bool GetTouchIn(Vector2 touchWorldPosition)
        {
            Vector2 leftBottomPosition = mLeftBottomAnchor.position;
            Vector2 rightTopPosition = mRightTopAnchor.position;

            // 지정된 범위 내에 존재하는 경우
            if (leftBottomPosition.x - Mathf.Epsilon <= touchWorldPosition.x && touchWorldPosition.x <= rightTopPosition.x + Mathf.Epsilon
                && leftBottomPosition.y - Mathf.Epsilon <= touchWorldPosition.y && touchWorldPosition.y <= rightTopPosition.y + Mathf.Epsilon)
            {
                return true;
            }
            return false;
        }

        public void CallOnWarmUp()
        {
            mOnWarmUp?.Invoke();
        }

        public void CallOnTouchStart(Touch touch, Vector2 worldPosition)
        {
            mOnTouchStart?.Invoke(touch, worldPosition);
        }

        public void CallOnTouchStay(Touch touch, Vector2 worldPosition)
        {
            mOnTouchStay?.Invoke(touch, worldPosition);
        }

        public void CallOnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            mOnTouchEnd?.Invoke(touch, worldPosition);
        }

        public void OnActiveSceneChanged(Scene srcScene, Scene dstScene)
        {
            TouchSender.AddReceiver(this);
        }
    }
}