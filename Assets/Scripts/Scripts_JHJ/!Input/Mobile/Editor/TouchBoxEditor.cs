﻿using UnityEditor;
using UnityEngine;

namespace TouchManagement
{
    [CustomEditor(typeof(TouchBox))]
    [CanEditMultipleObjects]
    public class TouchBoxEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            TouchBox box = target as TouchBox;

            SerializedProperty leftBottomAnchorProp = serializedObject.FindProperty("mLeftBottomAnchor");
            SerializedProperty rightTopAnchorProp = serializedObject.FindProperty("mRightTopAnchor");

            Transform leftBottomAnchor = leftBottomAnchorProp.objectReferenceValue as Transform;
            Transform rightTopAnchor = rightTopAnchorProp.objectReferenceValue as Transform;

            if (leftBottomAnchor == null || rightTopAnchor == null)
            {
                if (box.GetComponentInParent<Canvas>() != null)
                {
                    leftBottomAnchor = new GameObject("Anchor_LeftBottom", typeof(RectTransform)).transform;
                    rightTopAnchor = new GameObject("Anchor_RightTop", typeof(RectTransform)).transform;

                    leftBottomAnchor.SetParent(box.transform);
                    rightTopAnchor.SetParent(box.transform);

                    var boxRectTrans = box.GetComponent<RectTransform>();
                    leftBottomAnchor.localPosition = new Vector2(boxRectTrans.rect.xMin, boxRectTrans.rect.yMin);
                    rightTopAnchor.localPosition = new Vector2(boxRectTrans.rect.xMax, boxRectTrans.rect.yMax);
                }
                else
                {
                    leftBottomAnchor = new GameObject("Anchor_LeftBottom").transform;
                    rightTopAnchor = new GameObject("Anchor_RightTop").transform;
                    leftBottomAnchor.SetParent(box.transform);
                    rightTopAnchor.SetParent(box.transform);
                    leftBottomAnchor.localPosition = new Vector3(-1.0f, -1.0f, 0.0f);
                    rightTopAnchor.localPosition = new Vector3(1.0f, 1.0f, 0.0f);
                }
                leftBottomAnchor.localRotation = Quaternion.identity;
                leftBottomAnchor.localScale = Vector3.one;
                rightTopAnchor.localRotation = Quaternion.identity;
                rightTopAnchor.localScale = Vector3.one;

                leftBottomAnchorProp.objectReferenceValue = leftBottomAnchor;
                rightTopAnchorProp.objectReferenceValue = rightTopAnchor;
            }

            leftBottomAnchorProp.objectReferenceValue = EditorGUILayout.ObjectField("Left Bottom Anchor", leftBottomAnchor, typeof(Transform), true);
            rightTopAnchorProp.objectReferenceValue = EditorGUILayout.ObjectField("Right Top Anchor", rightTopAnchor, typeof(Transform), true);

            SerializedProperty depthProp = serializedObject.FindProperty("mDepth");

            depthProp.intValue = EditorGUILayout.IntField(new GUIContent("Depth", "터치 중첩시 우선 입력되는 깊이"), depthProp.intValue);

            serializedObject.ApplyModifiedProperties();
        }

        private void OnSceneGUI()
        {
            TouchBox box = target as TouchBox;

            if (box.LeftBottomAnchor != null && box.RightTopAnchor != null)
            {
                Transform leftBottomAnchor = box.LeftBottomAnchor;
                Transform rightTopAnchor = box.RightTopAnchor;

                Vector3 leftBottomPosition = leftBottomAnchor.position;
                Vector3 rightTopPosition = rightTopAnchor.position;
                float scale = 1.0f;
                Handles.TransformHandle(ref leftBottomPosition, Quaternion.identity, ref scale);
                Handles.TransformHandle(ref rightTopPosition, Quaternion.identity, ref scale);


                if (leftBottomPosition.x > rightTopPosition.x)
                {
                    leftBottomAnchor.position = new Vector3(rightTopPosition.x, leftBottomPosition.y);
                    leftBottomPosition = leftBottomAnchor.position;
                }
                if (leftBottomPosition.y > rightTopPosition.y)
                {
                    leftBottomAnchor.position = new Vector3(leftBottomPosition.x, rightTopPosition.y);
                    leftBottomPosition = leftBottomAnchor.position;
                }
                else
                {
                    leftBottomAnchor.position = leftBottomPosition;
                    rightTopAnchor.position = rightTopPosition;
                }


                Vector2 size = new Vector2(rightTopPosition.x - leftBottomPosition.x, rightTopPosition.y - leftBottomPosition.y);

                Handles.DrawSolidRectangleWithOutline(new Rect(leftBottomPosition, size), new Color(1.0f, 0.0f, 1.0f, 0.25f), new Color(1.0f, 0.0f, 1.0f, 0.6f));
                Handles.Label(new Vector3(rightTopPosition.x, leftBottomPosition.y), "Depth : " + box.Depth);
                Handles.Label(leftBottomPosition, "LeftBottom Anchor");
                Handles.Label(rightTopPosition, "RightTop Anchor");
            }
        }
    }
}