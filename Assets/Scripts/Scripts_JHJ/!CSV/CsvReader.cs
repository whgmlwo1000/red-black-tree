﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Csv
{
    public static class CsvReader
    {
        private static string SPLIT = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
        private static string LINE_SPLIT = @"\r\n|\n\r|\n|\r";
        private static char[] TRIM_CHARS = { '\"' };

        public static Dictionary<string, Dictionary<string, string>> Read(string file, bool isPath)
        {
            Dictionary<string, Dictionary<string, string>> data = new Dictionary<string, Dictionary<string, string>>();

            if (isPath)
            {
                try
                {
                    StreamReader reader = new StreamReader(file);

                    string[] lines = Regex.Split(reader.ReadToEnd(), LINE_SPLIT);

                    if (lines.Length <= 1)
                    {
                        return data;
                    }

                    string[] header = Regex.Split(lines[0], SPLIT);

                    for (int row = 1; row < lines.Length; row++)
                    {
                        string[] values = Regex.Split(lines[row], SPLIT);
                        if (values.Length == 0 || values[0] == "")
                        {
                            continue;
                        }

                        Dictionary<string, string> entry = new Dictionary<string, string>();

                        for (int column = 1; column < header.Length && column < values.Length; column++)
                        {
                            string value = values[column].TrimStart(TRIM_CHARS).TrimEnd(TRIM_CHARS).Replace("\\", "").Replace("  ", "\n");
                            entry.Add(header[column], value);
                        }

                        data.Add(values[0], entry);
                    }

                    reader.Close();
                    return data;
                }
                catch (IOException)
                {
                    return null;
                }
            }
            else
            {
                string[] lines = Regex.Split(file, LINE_SPLIT);

                if (lines.Length <= 1)
                {
                    return data;
                }

                string[] header = Regex.Split(lines[0], SPLIT);

                for (int row = 1; row < lines.Length; row++)
                {
                    string[] values = Regex.Split(lines[row], SPLIT);
                    if (values.Length == 0 || values[0] == "")
                    {
                        continue;
                    }

                    Dictionary<string, string> entry = new Dictionary<string, string>();

                    for (int column = 1; column < header.Length && column < values.Length; column++)
                    {
                        string value = values[column].TrimStart(TRIM_CHARS).TrimEnd(TRIM_CHARS).Replace("\\", "").Replace("  ", "\n");
                        entry.Add(header[column], value);
                    }

                    data.Add(values[0], entry);
                }
                return data;
            }
        }
    }
}
