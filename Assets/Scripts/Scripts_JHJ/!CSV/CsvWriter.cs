﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Csv
{
    public static class CsvWriter
    {
        private static char SPLIT_CHAR = ',';

        public static void Write(Dictionary<string, Dictionary<string, string>> data, string filePath)
        {
            //FileStream writer = new FileStream(filePath, FileMode.Create);
            StreamWriter writer = new StreamWriter(filePath);

            Dictionary<string, Dictionary<string, string>>.Enumerator rowEnumerator = data.GetEnumerator();

            // 내부 요소가 존재한다면,
            if (rowEnumerator.MoveNext())
            {
                Dictionary<string, string>.Enumerator columnEnumerator = rowEnumerator.Current.Value.GetEnumerator();
                // 내용을 담을 content
                StringBuilder content = new StringBuilder();
                // header 저장
                while (columnEnumerator.MoveNext())
                {
                    content.Append(SPLIT_CHAR);
                    content.Append(columnEnumerator.Current.Key);
                }

                content.Append('\n');

                rowEnumerator = data.GetEnumerator();
                // value 저장
                while (rowEnumerator.MoveNext())
                {
                    // 태그 삽입
                    content.Append(rowEnumerator.Current.Key);

                    // value 삽입
                    columnEnumerator = rowEnumerator.Current.Value.GetEnumerator();
                    while (columnEnumerator.MoveNext())
                    {
                        content.Append(SPLIT_CHAR);
                        if (columnEnumerator.Current.Value.Contains(","))
                        {
                            content.Append('"');
                            content.Append(columnEnumerator.Current.Value.Replace("\n", "  "));
                            content.Append('"');
                        }
                        else
                        {
                            content.Append(columnEnumerator.Current.Value.Replace("\n", "  "));
                        }
                    }

                    content.Append('\n');
                }

                writer.Write(content.ToString());
            }

            writer.Close();
        }
    }
}