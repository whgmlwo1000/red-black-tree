﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace RedBlackTree
{

    public class ItemTooltip : MonoBehaviour
    {
        public enum ETooltipState
        {
            DEACTIVE,       // 비활성화
            ACTIVATING,     // 활성화되는중
            ACTIVE,         // 활성화
            DEACTIVATING    // 비활성화되는중
        }

        public ETooltipState State
        {
            get { return mState; }
        }

        [System.Serializable]
        protected class TextInfo
        {
            public Text Text;
            public string Key;
        }

        [SerializeField]
        protected float mMinHeightSize = 500f;
        protected float mAdditionalHeightSize;
        protected float mDstHeightSize;
        protected float mSrcHeightSize;
        [SerializeField]
        private float mWidthSize = 1000f;

        [SerializeField]
        protected RectTransform mPanel;

        [SerializeField]
        protected TextInfo[] mTextInfo;

        protected ETooltipState mState;

        [SerializeField]
        protected float mPopUpTime = 0.2f;
        protected float mRemainPopUpTime;

        public void PopUp()
        {
            gameObject.SetActive(true);
            mState = ETooltipState.ACTIVATING;
            mRemainPopUpTime = mPopUpTime;
            mPanel.gameObject.SetActive(true);

            mSrcHeightSize = mPanel.sizeDelta.y;

            mAdditionalHeightSize = 0.0f;
            for (int i = 0; i < mTextInfo.Length; i++)
            {
                string script = GetFormattedScript(mTextInfo[i].Key);
                mTextInfo[i].Text.text = script;
                int countOfLines = (int)(script.Length * 0.5f / (mTextInfo[i].Text.rectTransform.sizeDelta.x / mTextInfo[i].Text.fontSize));
                mAdditionalHeightSize += countOfLines * mTextInfo[i].Text.fontSize;
            }

            mDstHeightSize = mMinHeightSize + mAdditionalHeightSize;
        }

        public void PopDown()
        {
            mState = ETooltipState.DEACTIVATING;
            mRemainPopUpTime = mPopUpTime;
            mSrcHeightSize = mPanel.sizeDelta.y;
            for (int i = 0; i < mTextInfo.Length; i++)
            {
                mTextInfo[i].Text.gameObject.SetActive(false);
            }
        }

        protected string GetFormattedScript(string key)
        {
            if (DataManager.HasTag(key))
            {
                return DataManager.GetScript(key);
            }
            return "None";
        }

        protected virtual void Awake()
        {
            mState = ETooltipState.DEACTIVE;
            mPanel.sizeDelta = new Vector2(mWidthSize, 0.0f);
            gameObject.SetActive(false);
            for (int i = 0; i < mTextInfo.Length; i++)
            {
                mTextInfo[i].Text.gameObject.SetActive(false);
            }
        }

        protected void Update()
        {
            switch (mState)
            {
            case ETooltipState.ACTIVATING:
                if (mRemainPopUpTime > 0.0f)
                {
                    mRemainPopUpTime -= Time.deltaTime;
                    mPanel.sizeDelta = new Vector2(mWidthSize, Mathf.Lerp(mDstHeightSize, mSrcHeightSize, mRemainPopUpTime / mPopUpTime));
                }
                else
                {
                    mPanel.sizeDelta = new Vector2(mWidthSize, mDstHeightSize);
                    mState = ETooltipState.ACTIVE;
                    for (int i = 0; i < mTextInfo.Length; i++)
                    {
                        mTextInfo[i].Text.gameObject.SetActive(true);
                    }
                }
                break;
            case ETooltipState.DEACTIVATING:
                if (mRemainPopUpTime > 0.0f)
                {
                    mRemainPopUpTime -= Time.deltaTime;
                    mPanel.sizeDelta = new Vector2(mWidthSize, Mathf.Lerp(0.0f, mSrcHeightSize, mRemainPopUpTime / mPopUpTime));
                }
                else
                {
                    mPanel.sizeDelta = new Vector2(mWidthSize, 0.0f);
                    mState = ETooltipState.DEACTIVE;
                    gameObject.SetActive(false);
                }
                break;
            }
        }
    }

}