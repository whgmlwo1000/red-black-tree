﻿using UnityEngine;


namespace ArchSpirit
{
    public class DungeonProceeder_Stop : State<string>
    {
        private DungeonProceeder mProceeder;

        public DungeonProceeder_Stop(DungeonProceeder proceeder) : base("Stop")
        {
            mProceeder = proceeder;
        }

        public override void FixedUpdate()
        {
            mProceeder.DeltaPosition = Vector2.zero;
        }
    }
}