﻿using UnityEngine;

namespace ArchSpirit
{
    public class DungeonProceeder_Proceed : State<string>
    {
        private DungeonProceeder mProceeder;

        public DungeonProceeder_Proceed(DungeonProceeder proceeder) : base("Proceed")
        {
            mProceeder = proceeder;
        }

        public override void FixedUpdate()
        {
            mProceeder.DeltaPosition = new Vector2(mProceeder.Speed * Time.fixedDeltaTime, 0.0f);
            mProceeder.transform.Translate(mProceeder.DeltaPosition, Space.World);
        }
    }
}