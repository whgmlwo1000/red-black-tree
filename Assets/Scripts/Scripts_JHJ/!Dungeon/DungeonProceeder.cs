﻿using UnityEngine;

namespace ArchSpirit
{
    public class DungeonProceeder : StateBase<string>
    {
        public static DungeonProceeder Main { get; private set; } = null;

        [SerializeField]
        private float mSpeed = 10.0f;
        public float Speed
        {
            get { return mSpeed; }
        }

        public Vector2 DeltaPosition { get; set; }

        public override void Awake()
        {
            base.Awake();

            if(Main == null)
            {
                Main = this;

                AddStates(new DungeonProceeder_Stop(this)
                    , new DungeonProceeder_Proceed(this));
                State = "Proceed";
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public override void FixedUpdate()
        {
            if (!StateManager.Pause.State)
            {
                base.FixedUpdate();
            }
        }

        public override void Update()
        {
            if (!StateManager.Pause.State)
            {
                base.Update();
            }
        }

        public void OnDestroy()
        {
            if (Main == this)
            {
                Main = null;
            }
        }
    }
}