﻿using UnityEngine;
using UnityEditor;

namespace ArchSpirit
{
    [CustomEditor(typeof(DungeonProceeder))]
    public class DungeonProceederEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SerializedProperty speedProp = serializedObject.FindProperty("mSpeed");

            speedProp.floatValue = Mathf.Max(EditorGUILayout.FloatField("Speed", speedProp.floatValue), speedProp.floatValue);

            serializedObject.ApplyModifiedProperties();
        }
    }
}