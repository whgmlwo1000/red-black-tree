﻿using UnityEngine;
using UnityEngine.UI;
using TouchManagement;

namespace ArchSpirit
{
    [RequireComponent(typeof(TouchBox))]
    public class DPad : MonoBehaviour
    {
        [SerializeField]
        private PlayerCommand mPlayerCommand = null;

        private TouchBox mTouchBox;

        [SerializeField]
        private float mRadius = 150.0f;

        [SerializeField]
        private Image mDPadImage = null;
        [SerializeField]
        private Image mStickImage = null;

        private int mTouchId = -10;
        private Vector2 mStartPosition;

        public void Awake()
        {
            mTouchBox = GetComponent<TouchBox>();

            mDPadImage.gameObject.SetActive(false);
            mStickImage.gameObject.SetActive(false);

            mTouchBox.OnTouchStart += OnTouchStart;
            mTouchBox.OnTouchStay += OnTouchStay;
            mTouchBox.OnTouchEnd += OnTouchEnd;
        }

        private void OnTouchStart(Touch touch, Vector2 worldPosition)
        {
            if (mTouchId == -10)
            {
                mTouchId = touch.fingerId;
                mStartPosition = touch.position;

                mDPadImage.gameObject.SetActive(true);
                mDPadImage.rectTransform.anchoredPosition = mStartPosition;
                mStickImage.rectTransform.localPosition = Vector2.zero;

                mPlayerCommand.MoveDirection = Vector2.zero;
            }
        }

        private void OnTouchStay(Touch touch, Vector2 worldPosition)
        {
            if (mTouchId == touch.fingerId)
            {
                Vector2 curPosition = touch.position;
                Vector2 deltaPosition = curPosition - mStartPosition;

                deltaPosition = Vector2.ClampMagnitude(deltaPosition, mRadius);
                mPlayerCommand.MoveDirection = deltaPosition / mRadius;

                mStickImage.rectTransform.localPosition = deltaPosition;
            }
        }

        private void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            if (mTouchId == touch.fingerId)
            {
                mTouchId = -10;
                mDPadImage.gameObject.SetActive(false);

                mPlayerCommand.MoveDirection = Vector2.zero;
            }
        }
    }
}