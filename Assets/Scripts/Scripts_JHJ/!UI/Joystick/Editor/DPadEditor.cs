﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

namespace ArchSpirit
{
    [CustomEditor(typeof(DPad))]
    public class DPadEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SerializedProperty playerCommandProp = serializedObject.FindProperty("mPlayerCommand");
            SerializedProperty radiusProp = serializedObject.FindProperty("mRadius");
            SerializedProperty dPadImageProp = serializedObject.FindProperty("mDPadImage");
            SerializedProperty stickImageProp = serializedObject.FindProperty("mStickImage");

            playerCommandProp.objectReferenceValue = EditorGUILayout.ObjectField("Player Command", playerCommandProp.objectReferenceValue, typeof(PlayerCommand), false);
            radiusProp.floatValue = Mathf.Max(EditorGUILayout.FloatField("DPad Radius", radiusProp.floatValue), 0.0f);
            dPadImageProp.objectReferenceValue = EditorGUILayout.ObjectField("DPad Image", dPadImageProp.objectReferenceValue, typeof(Image), true);
            stickImageProp.objectReferenceValue = EditorGUILayout.ObjectField("DPad Stick Image", stickImageProp.objectReferenceValue, typeof(Image), true);

            serializedObject.ApplyModifiedProperties();
        }
    }
}