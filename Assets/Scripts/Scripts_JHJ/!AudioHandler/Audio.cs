﻿using UnityEngine;
using ObjectManagement;

[RequireComponent(typeof(AudioSource))]
public class Audio<EAudioFlags> : MonoBehaviour, IHandleable<EAudioFlags>
{
    public delegate void Action();

    public EAudioFlags Type { get { return mType; } }
    [SerializeField]
    private EAudioFlags mType = default;

    public AudioSource AudioSource { get; private set; }

    public void Awake()
    {
        AudioSource = GetComponent<AudioSource>();
    }
}
