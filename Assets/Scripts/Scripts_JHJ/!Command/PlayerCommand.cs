﻿using UnityEngine;

namespace ArchSpirit
{
    [CreateAssetMenu(fileName = "New PlayerCommand", menuName = "Player/Command", order = 1000)]
    public class PlayerCommand : ScriptableObject
    {
        public Vector2 MoveDirection;
    }
}