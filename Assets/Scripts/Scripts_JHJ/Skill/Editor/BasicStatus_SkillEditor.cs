﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_Skill))]
    public class BasicStatus_SkillEditor : Editor
    {
        public sealed override void OnInspectorGUI()
        {
            SetProperties();
            serializedObject.ApplyModifiedProperties();
        }

        public virtual void SetProperties()
        {

        }
    }
}