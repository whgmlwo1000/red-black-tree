﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(SkillTable_Monster))]
    public class SkillTable_MonsterEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SerializedProperty activeSkillProps = serializedObject.FindProperty("mActiveSkills");
            SerializedProperty passiveSkillProps = serializedObject.FindProperty("mPassiveSkills");

            int countOfSkill = (int)EActiveSkill_Monster.Count;
            if(activeSkillProps.arraySize != countOfSkill)
            {
                activeSkillProps.arraySize = countOfSkill;
                serializedObject.ApplyModifiedProperties();
            }

            countOfSkill = (int)EPassiveSkill_Monster.Count;
            if(passiveSkillProps.arraySize != countOfSkill)
            {
                passiveSkillProps.arraySize = countOfSkill;
                serializedObject.ApplyModifiedProperties();
            }

            EditorGUILayout.LabelField(new GUIContent("Active Skills"), EditorStyles.boldLabel);

            for (EActiveSkill_Monster e = EActiveSkill_Monster.None + 1; e < EActiveSkill_Monster.Count; e++)
            {
                SerializedProperty skillProp = activeSkillProps.GetArrayElementAtIndex((int)e);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent(e.ToString()), GUILayout.MaxWidth(150));
                skillProp.objectReferenceValue = EditorGUILayout.ObjectField(skillProp.objectReferenceValue, typeof(BasicStatus_ActiveSkill_Monster), false);
                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.Space();
            EditorGUILayout.LabelField(new GUIContent("Passive Skills"), EditorStyles.boldLabel);

            for (EPassiveSkill_Monster e = EPassiveSkill_Monster.None + 1; e < EPassiveSkill_Monster.Count; e++)
            {
                SerializedProperty skillProp = passiveSkillProps.GetArrayElementAtIndex((int)e);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent(e.ToString()), GUILayout.MaxWidth(150));
                skillProp.objectReferenceValue = EditorGUILayout.ObjectField(skillProp.objectReferenceValue, typeof(BasicStatus_PassiveSkill_Monster), false);
                EditorGUILayout.EndHorizontal();
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}