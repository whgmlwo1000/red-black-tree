﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public enum ESkillCategory_Monster
    {
        None = -1,

        FireStraight = 0,
        GlobalHeal = 1,
        GlobalEnhance = 2, 
        Blink = 3,
    }

    public abstract class Skill_Monster : Skill
    {
        public abstract ESkillCategory_Monster Category { get; }

        public new BasicStatus_Skill_Monster BasicStatus { get; }

        public Skill_Monster(BasicStatus_Skill_Monster basicStatus) : base(basicStatus)
        {
            BasicStatus = basicStatus;
        }
    }
}