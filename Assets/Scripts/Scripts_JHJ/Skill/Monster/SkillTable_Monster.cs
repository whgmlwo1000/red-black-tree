﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New SkillTable_Monster", menuName = "Monster/Skill Table", order = 1000)]
    public class SkillTable_Monster : ScriptableObject
    {
        [SerializeField]
        private BasicStatus_ActiveSkill_Monster[] mActiveSkills = new BasicStatus_ActiveSkill_Monster[(int)EActiveSkill_Monster.Count];
        [SerializeField]
        private BasicStatus_PassiveSkill_Monster[] mPassiveSkills = new BasicStatus_PassiveSkill_Monster[(int)EPassiveSkill_Monster.Count];

        public ActiveSkill_Monster[] CreateActiveSkills()
        {
            ActiveSkill_Monster[] skills = new ActiveSkill_Monster[(int)EActiveSkill_Monster.Count];

            skills[(int)EActiveSkill_Monster.FireStraight] = 
                new ActiveSkill_Monster_FireStraight(mActiveSkills[(int)EActiveSkill_Monster.FireStraight] as BasicStatus_ActiveSkill_Monster_FireStraight);
            skills[(int)EActiveSkill_Monster.GlobalEnhance] =
                new ActiveSkill_Monster_GlobalEnhance(mActiveSkills[(int)EActiveSkill_Monster.GlobalEnhance] as BasicStatus_ActiveSkill_Monster_GlobalEnhance);
            skills[(int)EActiveSkill_Monster.GlobalHeal] =
                new ActiveSkill_Monster_GlobalHeal(mActiveSkills[(int)EActiveSkill_Monster.GlobalHeal] as BasicStatus_ActiveSkill_Monster_GlobalHeal);
            skills[(int)EActiveSkill_Monster.Blink] =
                new ActiveSkill_Monster_Blink(mActiveSkills[(int)EActiveSkill_Monster.Blink] as BasicStatus_ActiveSkill_Monster_Blink);

            return skills;
        }

        public PassiveSkill_Monster[] CreatePassiveSkills()
        {
            PassiveSkill_Monster[] skills = new PassiveSkill_Monster[(int)EPassiveSkill_Monster.Count];

            return skills;
        }
    }
}