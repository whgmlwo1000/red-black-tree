﻿using UnityEngine;

namespace RedBlackTree
{
    public enum EPassiveSkill_Monster
    {
        None = -1, 

        Count
    }

    public abstract class PassiveSkill_Monster : Skill_Monster
    {
        public abstract EPassiveSkill_Monster Type { get; }

        public new BasicStatus_PassiveSkill_Monster BasicStatus { get; }

        public PassiveSkill_Monster(BasicStatus_PassiveSkill_Monster basicStatus) : base(basicStatus)
        {
            BasicStatus = basicStatus;
        }
    }
}