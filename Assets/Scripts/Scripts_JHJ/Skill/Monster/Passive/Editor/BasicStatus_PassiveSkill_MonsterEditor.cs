﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_PassiveSkill_Monster))]
    public class BasicStatus_PassiveSkill_MonsterEditor : BasicStatus_Skill_MonsterEditor
    {
    }
}