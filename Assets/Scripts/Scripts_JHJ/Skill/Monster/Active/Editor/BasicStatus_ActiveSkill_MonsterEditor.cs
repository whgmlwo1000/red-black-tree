﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_ActiveSkill_Monster))]
    public class BasicStatus_ActiveSkill_MonsterEditor : BasicStatus_Skill_MonsterEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();
            SerializedProperty coolDownProp = serializedObject.FindProperty("CoolDown");

            coolDownProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(new GUIContent("Cool Down"), coolDownProp.floatValue), 0.0f);
        }
    }
}