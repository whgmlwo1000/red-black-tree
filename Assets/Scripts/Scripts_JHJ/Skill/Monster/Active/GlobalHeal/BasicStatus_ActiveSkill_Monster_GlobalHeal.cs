﻿using UnityEngine;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_ActiveSkill_Monster_FireStraight", menuName = "Monster/Active Skill/Global Heal", order = 1000)]
    public class BasicStatus_ActiveSkill_Monster_GlobalHeal : BasicStatus_ActiveSkill_Monster
    {
        public int HealValue;
    }
}