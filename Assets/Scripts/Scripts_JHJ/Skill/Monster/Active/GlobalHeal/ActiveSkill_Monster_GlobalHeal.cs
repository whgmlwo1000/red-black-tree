﻿using UnityEngine;

namespace RedBlackTree
{
    public class ActiveSkill_Monster_GlobalHeal : ActiveSkill_Monster
    {
        public override EActiveSkill_Monster Type { get { return EActiveSkill_Monster.GlobalHeal; } }
        public override ESkillCategory_Monster Category { get { return ESkillCategory_Monster.GlobalHeal; } }

        public new BasicStatus_ActiveSkill_Monster_GlobalHeal BasicStatus { get; }

        public int HealValue { get { return BasicStatus.HealValue; } }

        public ActiveSkill_Monster_GlobalHeal(BasicStatus_ActiveSkill_Monster_GlobalHeal basicStatus) : base(basicStatus)
        {
            BasicStatus = basicStatus;
        }
    }
}