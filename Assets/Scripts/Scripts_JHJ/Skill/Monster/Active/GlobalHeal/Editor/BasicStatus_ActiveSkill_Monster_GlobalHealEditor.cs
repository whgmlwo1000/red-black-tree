﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_ActiveSkill_Monster_GlobalHeal))]
    public class BasicStatus_ActiveSkill_Monster_GlobalHealEditor : BasicStatus_ActiveSkill_MonsterEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            SerializedProperty healValueProp = serializedObject.FindProperty("HealValue");

            healValueProp.intValue = Mathf.Max(EditorGUILayout.IntField(new GUIContent("Heal Value"), healValueProp.intValue), 0);
        }
    }
}