﻿using UnityEngine;

namespace RedBlackTree
{
    public class ActiveSkill_Monster_GlobalEnhance : ActiveSkill_Monster
    {
        public override EActiveSkill_Monster Type { get { return EActiveSkill_Monster.GlobalEnhance; } }
        public override ESkillCategory_Monster Category { get { return ESkillCategory_Monster.GlobalEnhance; } }

        public new BasicStatus_ActiveSkill_Monster_GlobalEnhance BasicStatus { get; }

        public float EnhanceRate { get { return BasicStatus.EnhanceRate; } }

        public ActiveSkill_Monster_GlobalEnhance(BasicStatus_ActiveSkill_Monster_GlobalEnhance basicStatus) : base(basicStatus)
        {
            BasicStatus = basicStatus;
        }
    }
}