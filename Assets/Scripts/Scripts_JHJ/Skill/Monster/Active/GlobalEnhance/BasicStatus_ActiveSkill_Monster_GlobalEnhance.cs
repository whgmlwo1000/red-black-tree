﻿using UnityEngine;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_ActiveSkill_Monster_GlobalEnhance", menuName = "Monster/Active Skill/Global Enhance", order = 1000)]
    public class BasicStatus_ActiveSkill_Monster_GlobalEnhance : BasicStatus_ActiveSkill_Monster
    {
        public float EnhanceRate;
    }
}