﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_ActiveSkill_Monster_GlobalEnhance))]
    public class BasicStatus_ActiveSkill_Monster_GlobalEnhanceEditor : BasicStatus_ActiveSkill_MonsterEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            SerializedProperty enhanceRateProp = serializedObject.FindProperty("EnhanceRate");
            enhanceRateProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(new GUIContent("Enhance Rate"), enhanceRateProp.floatValue), 0.0f);
        }
    }
}