﻿using UnityEngine;

namespace RedBlackTree
{
    public class ActiveSkill_Monster_Blink : ActiveSkill_Monster
    {
        public override ESkillCategory_Monster Category { get { return ESkillCategory_Monster.Blink; } }
        public override EActiveSkill_Monster Type { get { return EActiveSkill_Monster.Blink; } }

        public new BasicStatus_ActiveSkill_Monster_Blink BasicStatus { get; }

        public ActiveSkill_Monster_Blink(BasicStatus_ActiveSkill_Monster_Blink basicStatus) : base(basicStatus)
        {
            BasicStatus = basicStatus;
        }
    }
}