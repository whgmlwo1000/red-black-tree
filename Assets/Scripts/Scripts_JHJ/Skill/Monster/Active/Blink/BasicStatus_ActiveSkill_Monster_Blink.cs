﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_ActiveSkill_Monster_FireStraight", menuName = "Monster/Active Skill/Blink", order = 1000)]
    public class BasicStatus_ActiveSkill_Monster_Blink : BasicStatus_ActiveSkill_Monster
    {

    }
}