﻿using UnityEngine;

namespace RedBlackTree
{
    public class ActiveSkill_Monster_FireStraight : ActiveSkill_Monster
    {
        public override EActiveSkill_Monster Type { get { return EActiveSkill_Monster.FireStraight; } }
        public override ESkillCategory_Monster Category { get { return ESkillCategory_Monster.FireStraight; } }

        public new BasicStatus_ActiveSkill_Monster_FireStraight BasicStatus { get; }

        public EProjectile ProjectileType { get { return BasicStatus.ProjectileType; } }

        public ActiveSkill_Monster_FireStraight(BasicStatus_ActiveSkill_Monster_FireStraight basicStatus) : base(basicStatus)
        {
            BasicStatus = basicStatus;
        }
    }
}