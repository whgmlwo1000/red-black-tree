﻿using UnityEngine;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_ActiveSkill_Monster_FireStraight", menuName = "Monster/Active Skill/Fire Straight", order = 1000)]
    public class BasicStatus_ActiveSkill_Monster_FireStraight : BasicStatus_ActiveSkill_Monster
    {
        public EProjectile ProjectileType;
    }
}