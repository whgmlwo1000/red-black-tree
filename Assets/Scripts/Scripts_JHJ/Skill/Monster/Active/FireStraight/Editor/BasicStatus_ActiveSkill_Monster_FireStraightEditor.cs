﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_ActiveSkill_Monster_FireStraight))]
    public class BasicStatus_ActiveSkill_Monster_FireStraightEditor : BasicStatus_ActiveSkill_MonsterEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            SerializedProperty projectileTypeProp = serializedObject.FindProperty("ProjectileType");

            EProjectile projectileType = (EProjectile)projectileTypeProp.intValue;
            projectileType = (EProjectile)EditorGUILayout.EnumPopup(new GUIContent("Projectile Type"), projectileType);
            projectileTypeProp.intValue = (int)projectileType;
        }
    }
}