﻿using UnityEngine;

namespace RedBlackTree
{
    public enum EActiveSkill_Monster
    {
        None = -1, 

        /// <summary>
        /// 직선 투사체 발사
        /// </summary>
        FireStraight = 0, 
        /// <summary>
        /// 전역 힐
        /// </summary>
        GlobalHeal = 1, 
        /// <summary>
        /// 전역 강화
        /// </summary>
        GlobalEnhance = 2, 
        /// <summary>
        /// 점멸
        /// </summary>
        Blink = 3, 

        Count
    }

    public abstract class ActiveSkill_Monster : Skill_Monster
    {
        public abstract EActiveSkill_Monster Type { get; }
        
        public new BasicStatus_ActiveSkill_Monster BasicStatus { get; }

        public float CoolDown { get { return BasicStatus.CoolDown; } }

        public float CoolDown_Remain { get; private set; }

        public ActiveSkill_Monster(BasicStatus_ActiveSkill_Monster basicStatus) : base(basicStatus)
        {
            BasicStatus = basicStatus;
        }
        public virtual bool CanUse { get { return CoolDown_Remain <= Mathf.Epsilon; } }

        public void OnUse()
        {
            CoolDown_Remain = CoolDown;
        }

        public virtual void Update()
        {
            if(!StateManager.Pause.State && CoolDown_Remain > 0.0f)
            {
                CoolDown_Remain -= Time.deltaTime;
            }
        }
    }
}