﻿using UnityEngine;

namespace RedBlackTree
{
    public abstract class Skill
    {
        public BasicStatus_Skill BasicStatus { get; }

        public Skill(BasicStatus_Skill basicStatus)
        {
            BasicStatus = basicStatus;
        }
    }
}