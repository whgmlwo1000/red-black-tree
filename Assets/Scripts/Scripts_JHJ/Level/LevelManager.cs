﻿using UnityEngine;
using System.Collections.Generic;

namespace RedBlackTree
{
    public static class LevelManager
    {
        private static Dictionary<int, float> mFruitTreePossibilityIncrementDict = new Dictionary<int, float>();
        private static int mFruitTreePossibilityIncrementKey;

        public static float FruitTreePossibilityIncrement
        {
            get
            {
                float sum = 0.0f;
                for (int indexOfKey = 0; indexOfKey < mFruitTreePossibilityIncrementKey; indexOfKey++)
                {
                    sum += mFruitTreePossibilityIncrementDict[indexOfKey];
                }

                return sum;
            }
        }

        private static Dictionary<int, float> mBoxPossibilityIncrementDict = new Dictionary<int, float>();
        private static int mBoxPossibilityIncrementKey;

        public static float BoxPossibilityIncrement
        {
            get
            {
                float sum = 0.0f;
                for(int indexOfKey = 0; indexOfKey < mBoxPossibilityIncrementKey; indexOfKey++)
                {
                    sum += mBoxPossibilityIncrementDict[indexOfKey];
                }
                return sum;
            }
        }

        public static int AddFruitTreePossibilityIncrement()
        {
            mFruitTreePossibilityIncrementDict.Add(mFruitTreePossibilityIncrementKey, 0.0f);
            return mFruitTreePossibilityIncrementKey++;
        }

        public static void SetFruitTreePossibilityIncrement(int key, float possibility)
        {
            mFruitTreePossibilityIncrementDict[key] = possibility;
        }

        public static float GetFruitTreePossibilityIncrement(int key)
        {
            return mFruitTreePossibilityIncrementDict[key];
        }

        public static int AddBoxPossibilityIncrement()
        {
            mBoxPossibilityIncrementDict.Add(mBoxPossibilityIncrementKey, 0.0f);
            return mBoxPossibilityIncrementKey++;
        }

        public static void SetBoxPossibilityIncrement(int key, float possibility)
        {
            mBoxPossibilityIncrementDict[key] = possibility;
        }

        public static float GetBoxPossibilityIncrement(int key)
        {
            return mBoxPossibilityIncrementDict[key];
        }
    }
}