﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class BasicStatus_Item : ScriptableObject
    {
        public Sprite Icon;

        public string NameTag;
        public string OptionTag;

        public int MaxLevel;
        public int MaxProficiency;
    }
}