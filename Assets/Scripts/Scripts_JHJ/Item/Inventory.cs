﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace RedBlackTree
{
    /// <summary>
    /// 액티브 아이템, 패시브 아이템, 영혼, 악마의 영혼을 담을 수 있는 인벤토리
    /// 인벤토리에 아이템 추가, 아이템 삭제
    /// 인덱스 기반 인벤토리 내 아이템 검색, 
    /// 모든 아이템 정보 중 지정된 타입 반환, 
    /// 액티브 아이템 장착
    /// 인벤토리 내 아이템 위치 이동
    /// </summary>
    [CreateAssetMenu(fileName = "New Inventory", menuName = "Inventory", order = 1000)]
    public class Inventory : ScriptableObject
    {
        [SerializeField]
        private ActiveItemTable mActiveItemTable = null;
        [SerializeField]
        private PassiveItemTable mPassiveItemTable = null;

        private int mSouls;

        public int Souls
        {
            get { return mSouls; }
            set { mSouls = Mathf.Max(value, 0); }
        }

        public int DemonSouls
        {
            get { return PlayerData.Current.DemonSouls; }
            set { PlayerData.Current.DemonSouls = value; }
        }

        private ActiveItem[] mActiveItems;
        private PassiveItem[] mPassiveItems;

        private ActiveItem[] mMountedActiveItems = new ActiveItem[ValueConstants.ActiveItemMountSpace];

        // 습득된 아이템 저장 리스트
        private List<ActiveItem> mAddedActiveItemList = new List<ActiveItem>();
        public int CountOfAddedActiveItems { get { return mAddedActiveItemList.Count; } }

        private List<PassiveItem> mAddedPassiveItemList = new List<PassiveItem>();
        public int CountOfAddedPassiveItems { get { return mAddedPassiveItemList.Count; } }

        public void OnEnable()
        {
            CreateItems();

            StateManager.Scene.AddEnterEvent(EScene.Scene_Sanctuary, Clear);
        }

        public void CreateItems()
        {
            if (mActiveItemTable != null && mPassiveItemTable != null)
            {
                mActiveItems = mActiveItemTable.CreateItems();
                mPassiveItems = mPassiveItemTable.CreateItems();
                Clear();
            }
        }

        public void Clear()
        {
            mAddedActiveItemList.ForEach(delegate (ActiveItem item)
            {
                item.Reset();
            });
            mAddedActiveItemList.Clear();

            mAddedPassiveItemList.ForEach(delegate (PassiveItem item)
            {
                item.Reset();
            });
            mAddedPassiveItemList.Clear();
            Souls = 0;
        }

        public void Add(EActiveItem activeItemType, int count)
        {
            count = Mathf.Max(count, 0);

            ActiveItem activeItem = mActiveItems[(int)activeItemType];
            if (!activeItem.IsAcquired)
            {
                mAddedActiveItemList.Add(activeItem);
            }
            activeItem.Proficiency += count;
        }

        public void Add(EPassiveItem passiveItemType, int count)
        {
            count = Mathf.Max(count, 0);

            PassiveItem passiveItem = mPassiveItems[(int)passiveItemType];
            if (!passiveItem.IsAcquired)
            {
                mAddedPassiveItemList.Add(passiveItem);
            }
            passiveItem.Proficiency += count;
        }

        public void RemoveActiveItemAt(int index, int count)
        {
            if(mAddedActiveItemList.Count <= 0)
            {
                return;
            }

            index = Mathf.Clamp(index, 0, mAddedActiveItemList.Count - 1);
            ActiveItem item = mAddedActiveItemList[index];
            item.Proficiency -= count;
            if(!item.IsAcquired)
            {
                mAddedActiveItemList.RemoveAt(index);
            }
        }

        public void RemovePassiveItemAt(int index, int count)
        {
            if(mAddedPassiveItemList.Count <= 0)
            {
                return;
            }

            index = Mathf.Clamp(index, 0, mAddedPassiveItemList.Count - 1);
            PassiveItem item = mAddedPassiveItemList[index];
            item.Proficiency -= count;
            if(!item.IsAcquired)
            {
                mAddedPassiveItemList.RemoveAt(index);
            }
        }

        public void Remove(EActiveItem activeItemType, int count)
        {
            count = Mathf.Max(count, 1);

            ActiveItem activeItem = mActiveItems[(int)activeItemType];
            bool isAcquired = activeItem.IsAcquired;
            activeItem.Proficiency -= count;

            if(isAcquired && !activeItem.IsAcquired)
            {
                mAddedActiveItemList.Remove(activeItem);
            }
        }

        public void Remove(EPassiveItem passiveItemType, int count)
        {
            count = Mathf.Max(count, 1);

            PassiveItem passiveItem = mPassiveItems[(int)passiveItemType];
            bool isAcquired = passiveItem.IsAcquired;
            passiveItem.Proficiency -= count;

            if(isAcquired && !passiveItem.IsAcquired)
            {
                mAddedPassiveItemList.Remove(passiveItem);
            }
        }

        public ActiveItem GetAddedActiveItemAt(int index)
        {
            if(mAddedActiveItemList.Count <= 0 || index >= mAddedActiveItemList.Count)
            {
                return null;
            }

            return mAddedActiveItemList[index];
        }

        public PassiveItem GetAddedPassiveItemAt(int index)
        {
            if(mAddedPassiveItemList.Count <= 0 || index >= mAddedPassiveItemList.Count)
            {
                return null;
            }

            return mAddedPassiveItemList[index];
        }

        public ActiveItem Get(EActiveItem activeItem)
        {
            return mActiveItems[(int)activeItem];
        }

        public PassiveItem Get(EPassiveItem passiveItem)
        {
            return mPassiveItems[(int)passiveItem];
        }

        public void MountActiveItem(int indexOfInventory, int indexOfMount)
        {
            if(mAddedActiveItemList.Count < 0)
            {
                return;
            }

            indexOfInventory = Mathf.Clamp(indexOfInventory, 0, mAddedActiveItemList.Count - 1);
            indexOfMount = Mathf.Clamp(indexOfMount, 0, mMountedActiveItems.Length - 1);

            if(mMountedActiveItems[indexOfMount] != null)
            {
                mMountedActiveItems[indexOfMount].UnMount();
            }

            ActiveItem item = mAddedActiveItemList[indexOfInventory];
            mMountedActiveItems[indexOfMount] = item;
            item.Mount();
        }

        public void MoveActiveItem(int srcIndex, int dstIndex)
        {
            if(mAddedActiveItemList.Count <= 0)
            {
                return;
            }

            srcIndex = Mathf.Clamp(srcIndex, 0, mAddedActiveItemList.Count - 1);
            dstIndex = Mathf.Clamp(dstIndex, 0, mAddedActiveItemList.Count - 1);

            ActiveItem item = mAddedActiveItemList[srcIndex];
            mAddedActiveItemList.RemoveAt(srcIndex);
            mAddedActiveItemList.Insert(dstIndex, item);
        }

        public void MovePassiveItem(int srcIndex, int dstIndex)
        {
            if(mAddedPassiveItemList.Count <= 0)
            {
                return;
            }

            srcIndex = Mathf.Clamp(srcIndex, 0, mAddedPassiveItemList.Count - 1);
            dstIndex = Mathf.Clamp(dstIndex, 0, mAddedPassiveItemList.Count - 1);

            PassiveItem item = mAddedPassiveItemList[srcIndex];
            mAddedPassiveItemList.RemoveAt(srcIndex);
            mAddedPassiveItemList.Insert(dstIndex, item);
        }
    }
}