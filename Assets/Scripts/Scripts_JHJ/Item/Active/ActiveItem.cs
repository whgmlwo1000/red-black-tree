﻿using UnityEngine;

namespace RedBlackTree
{
    public enum EActiveItem
    {
        None = -1, 

        Count
    }

    public class ActiveItem : Item
    {
        public new BasicStatus_ActiveItem BasicStatus { get; }

        public EActiveItem Type { get; }

        public bool IsMounted { get; private set; }

        public virtual float CoolTime
        {
            get { return BasicStatus.CoolTime; }
        }

        public ActiveItem(EActiveItem type, BasicStatus_ActiveItem basicStatus) : base(basicStatus)
        {
            Type = type;
            BasicStatus = basicStatus;
        }

        public void Mount()
        {
            IsMounted = true;
        }

        public void UnMount()
        {
            IsMounted = false;
        }
    }
}