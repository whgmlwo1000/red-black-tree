﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(ActiveItemTable))]
    public class ActiveItemTableEditor : Editor
    {
        public sealed override void OnInspectorGUI()
        {
            SetProperties();
            serializedObject.ApplyModifiedProperties();
        }

        public virtual void SetProperties()
        {
            ActiveItemTable table = target as ActiveItemTable;

            SerializedProperty basicStatusProps = serializedObject.FindProperty("mBasicStatus");

            EditorGUILayout.LabelField(new GUIContent(string.Format("Count : {0}", (int)EActiveItem.Count)), EditorStyles.boldLabel);
            EditorGUILayout.Space();
            EditorGUILayout.LabelField(new GUIContent("Basic Status"), EditorStyles.boldLabel);
            for (int indexOfStatus = 0; indexOfStatus < basicStatusProps.arraySize; indexOfStatus++)
            {
                SerializedProperty statusProp = basicStatusProps.GetArrayElementAtIndex(indexOfStatus);
                statusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent(((EActiveItem)indexOfStatus).ToString())
                    , statusProp.objectReferenceValue, typeof(BasicStatus_ActiveItem), false);
            }

            EditorGUILayout.Space();
        }
    }
}