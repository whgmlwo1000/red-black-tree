﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_ActiveItem))]
    public class BasicStatus_ActiveItemEditor : BasicStatus_ItemEditor
    {

    }
}