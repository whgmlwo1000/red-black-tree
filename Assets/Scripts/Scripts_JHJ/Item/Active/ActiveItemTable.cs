﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New ActiveItemTable", menuName = "Item/Active Item Table", order = 1000)]
    public class ActiveItemTable : ScriptableObject
    {
        [SerializeField]
        private BasicStatus_ActiveItem[] mBasicStatus = new BasicStatus_ActiveItem[(int)EActiveItem.Count];

        public ActiveItem[] CreateItems()
        {
            ActiveItem[] activeItems = new ActiveItem[(int)EActiveItem.Count];

            return activeItems;
        }
    }
}