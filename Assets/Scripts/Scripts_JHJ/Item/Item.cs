﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Item
    {
        public BasicStatus_Item BasicStatus { get; }

        private bool mIsAcquired;
        public bool IsAcquired
        {
            get { return mIsAcquired; }
            set
            {
                mIsAcquired = value;
                if (mIsAcquired && mLevel <= 0)
                {
                    mLevel = 1;
                    mProficiency = 1;
                    OnLevelChanged?.Invoke();
                }
                else if (!mIsAcquired && mLevel > 0)
                {
                    mLevel = 0;
                    mProficiency = 0;
                    OnLevelChanged?.Invoke();
                }
            }
        }

        public System.Action OnLevelChanged;

        public Sprite Icon { get { return BasicStatus.Icon; } }

        public virtual string Name
        {
            get
            {
                if (DataManager.HasTag(BasicStatus.NameTag))
                {
                    return string.Format(DataManager.GetScript(BasicStatus.NameTag), Mathf.Clamp(Level, 1, MaxLevel));
                }
                return StringConstants.None;
            }
        }

        public virtual string Option
        {
            get
            {
                if (DataManager.HasTag(BasicStatus.OptionTag))
                {
                    return DataManager.GetScript(BasicStatus.OptionTag);
                }
                return StringConstants.None;
            }
        }

        public int MaxLevel
        {
            get { return BasicStatus.MaxLevel; }
        }

        public int MaxProficiency
        {
            get { return BasicStatus.MaxProficiency; }
        }

        private int mLevel;
        public int Level
        {
            get { return mLevel; }
            set
            {
                if (value < 1)
                {
                    mIsAcquired = false;
                    value = 0;
                    mProficiency = 0;
                }
                else if (!mIsAcquired)
                {
                    mIsAcquired = true;
                    mProficiency = 0;
                }

                mLevel = Mathf.Min(value, MaxLevel);
                OnLevelChanged?.Invoke();
            }
        }

        private int mProficiency;
        public int Proficiency
        {
            get { return mProficiency; }
            set
            {
                if(IsAcquired)
                {
                    while (Level > 0 && value < 0)
                    {
                        Level--;
                        value += MaxProficiency;
                    }
                    while (Level < MaxLevel && value >= MaxProficiency)
                    {
                        Level++;
                        value -= MaxProficiency;
                    }
                    mProficiency = Mathf.Clamp(value, 0, MaxProficiency - 1);
                }
                else if(value-- > 0)
                {
                    mIsAcquired = true;
                    mLevel = 1;
                    while(value >= MaxProficiency)
                    {
                        Level++;
                        value -= MaxProficiency - 1;
                    }
                    mProficiency = value;
                }
            }
        }

        public Item(BasicStatus_Item basicStatus)
        {
            BasicStatus = basicStatus;
        }

        public virtual void Reset()
        {
            Level = 0;
        }
    }
}