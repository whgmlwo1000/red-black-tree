﻿using UnityEngine;

namespace RedBlackTree
{
    public class PassiveItem_PurificationAmulet : PassiveItem
    {
        public new BasicStatus_PassiveItem_PurificationAmulet BasicStatus { get; }

        public override string Option
        {
            get { return string.Format(base.Option, SoulsIncrease * 100.0); }
        }

        public float SoulsIncrease
        {
            get { return GetSoulsIncrease(Level); }
        }


        public PassiveItem_PurificationAmulet(BasicStatus_PassiveItem_PurificationAmulet basicStatus) : base(EPassiveItem.PurificationAmulet, basicStatus)
        {
            BasicStatus = basicStatus;
        }

        public float GetSoulsIncrease(int level)
        {
            return BasicStatus.SoulsIncreases[Mathf.Clamp(level, 1, MaxLevel) - 1];
        }
    }
}