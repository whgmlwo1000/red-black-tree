﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_PassiveItem_PurificationAmulet))]
    public class BasicStatus_PassiveItem_PurificationAmuletEditor : BasicStatus_PassiveItemEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            BasicStatus_PassiveItem_PurificationAmulet basicStatus = target as BasicStatus_PassiveItem_PurificationAmulet;

            SerializedProperty soulsIncreaseProps = serializedObject.FindProperty("SoulsIncreases");

            if (soulsIncreaseProps.arraySize != basicStatus.MaxLevel)
            {
                soulsIncreaseProps.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }

            for (int indexOfProp = 0; indexOfProp < soulsIncreaseProps.arraySize; indexOfProp++)
            {
                EditorGUILayout.Space();
                SerializedProperty soulIncreaseProp = soulsIncreaseProps.GetArrayElementAtIndex(indexOfProp);

                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}", indexOfProp + 1)), EditorStyles.boldLabel);
                soulIncreaseProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Souls Increase", "영혼 습득 증가율"), soulIncreaseProp.floatValue);
                soulIncreaseProp.floatValue = Mathf.Max(soulIncreaseProp.floatValue, 0.0f);
            }
        }
    }
}