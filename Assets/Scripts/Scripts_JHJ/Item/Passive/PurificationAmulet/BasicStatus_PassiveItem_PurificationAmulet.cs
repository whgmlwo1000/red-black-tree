﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_PassiveItem_PurificationAmulet", menuName = "Item/PassiveItem/Purification Amulet Basic Status", order = 1000)]
    public class BasicStatus_PassiveItem_PurificationAmulet : BasicStatus_PassiveItem
    {
        public float[] SoulsIncreases;
    }
}