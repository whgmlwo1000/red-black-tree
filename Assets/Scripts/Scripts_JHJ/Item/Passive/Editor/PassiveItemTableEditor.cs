﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(PassiveItemTable))]
    public class PassiveItemTableEditor : Editor
    {
        public sealed override void OnInspectorGUI()
        {
            SetProperties();
            serializedObject.ApplyModifiedProperties();
        }

        public virtual void SetProperties()
        {
            PassiveItemTable table = target as PassiveItemTable;

            SerializedProperty basicStatusProps = serializedObject.FindProperty("mBasicStatus");

            EditorGUILayout.LabelField(new GUIContent(string.Format("Count : {0}", (int)EPassiveItem.Count)), EditorStyles.boldLabel);
            EditorGUILayout.Space();
            EditorGUILayout.LabelField(new GUIContent("Basic Status"), EditorStyles.boldLabel);
            for (int indexOfStatus = 0; indexOfStatus < basicStatusProps.arraySize; indexOfStatus++)
            {
                SerializedProperty statusProp = basicStatusProps.GetArrayElementAtIndex(indexOfStatus);
                statusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent(((EPassiveItem)indexOfStatus).ToString())
                    , statusProp.objectReferenceValue, typeof(BasicStatus_PassiveItem), false);
            }

            EditorGUILayout.Space();
        }
    }
}