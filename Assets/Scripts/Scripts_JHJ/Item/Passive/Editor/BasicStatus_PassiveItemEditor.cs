﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_PassiveItem))]
    public class BasicStatus_PassiveItemEditor : BasicStatus_ItemEditor
    {

    }
}