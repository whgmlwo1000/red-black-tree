﻿using UnityEngine;

namespace RedBlackTree
{
    public class PassiveItem_FrigarHandMirror : PassiveItem
    {
        public new BasicStatus_PassiveItem_FrigarHandMirror BasicStatus { get; }

        public override string Option { get { return string.Format(base.Option, ReflectOffenseIncrease * 100.0); } }

        public float ReflectOffenseIncrease
        {
            get { return GetReflectOffenseIncrease(Level); }
        }

        public PassiveItem_FrigarHandMirror(BasicStatus_PassiveItem_FrigarHandMirror basicStatus) : base(EPassiveItem.FrigarHandMirror, basicStatus)
        {
            BasicStatus = basicStatus;
        }

        public float GetReflectOffenseIncrease(int level)
        {
            return BasicStatus.ReflectOffenseIncreases[Mathf.Clamp(level, 1, MaxLevel) - 1];
        }
    }
}