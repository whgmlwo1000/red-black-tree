﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_PassiveItem_FrigarHandMirror", menuName = "Item/PassiveItem/Frigar Hand Mirror Basic Status", order = 1000)]
    public class BasicStatus_PassiveItem_FrigarHandMirror : BasicStatus_PassiveItem
    {
        public float[] ReflectOffenseIncreases;
    }
}