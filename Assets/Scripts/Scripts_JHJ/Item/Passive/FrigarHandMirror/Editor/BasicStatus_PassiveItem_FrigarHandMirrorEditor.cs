﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_PassiveItem_FrigarHandMirror))]
    public class BasicStatus_PassiveItem_FrigarHandMirrorEditor : BasicStatus_PassiveItemEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            BasicStatus_PassiveItem_FrigarHandMirror basicStatus = target as BasicStatus_PassiveItem_FrigarHandMirror;

            SerializedProperty reflectOffenseIncreaseProps = serializedObject.FindProperty("ReflectOffenseIncreases");

            if (reflectOffenseIncreaseProps.arraySize != basicStatus.MaxLevel)
            {
                reflectOffenseIncreaseProps.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }

            for (int indexOfProp = 0; indexOfProp < reflectOffenseIncreaseProps.arraySize; indexOfProp++)
            {
                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}", indexOfProp + 1)), EditorStyles.boldLabel);
                SerializedProperty reflectOffenseIncreaseProp = reflectOffenseIncreaseProps.GetArrayElementAtIndex(indexOfProp);

                reflectOffenseIncreaseProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Reflect Offense Increase", "투사체 반사 공격력 증가율"), reflectOffenseIncreaseProp.floatValue);
                reflectOffenseIncreaseProp.floatValue = Mathf.Max(reflectOffenseIncreaseProp.floatValue, 0.0f);
                EditorGUILayout.Space();
            }
        }
    }
}