﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_PassiveItem_Elixir", menuName = "Item/PassiveItem/Elixir Basic Status", order = 1000)]
    public class BasicStatus_PassiveItem_Elixir : BasicStatus_PassiveItem
    {
        public float[] LifeRegenerationRates;
    }
}