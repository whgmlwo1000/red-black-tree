﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_PassiveItem_Elixir))]
    public class BasicStatus_PassiveItem_ElixirEditor : BasicStatus_PassiveItemEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            EditorGUILayout.Space();

            BasicStatus_PassiveItem_Elixir basicStatus = target as BasicStatus_PassiveItem_Elixir;

            SerializedProperty lifeRegenerationRateProps = serializedObject.FindProperty("LifeRegenerationRates");

            if(lifeRegenerationRateProps.arraySize != basicStatus.MaxLevel)
            {
                lifeRegenerationRateProps.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }
            
            for(int indexOfProp = 0; indexOfProp < lifeRegenerationRateProps.arraySize; indexOfProp++)
            {
                EditorGUILayout.Space();

                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}", indexOfProp + 1)), EditorStyles.boldLabel);

                SerializedProperty lifeRegenerationRateProp = lifeRegenerationRateProps.GetArrayElementAtIndex(indexOfProp);
                lifeRegenerationRateProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Life Reneration Rate", "부활시 생명력 회복 비율"), lifeRegenerationRateProp.floatValue);
                lifeRegenerationRateProp.floatValue = Mathf.Clamp(lifeRegenerationRateProp.floatValue, 0.0f, 1.0f);
            }
        }
    }
}