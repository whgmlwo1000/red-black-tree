﻿using UnityEngine;

namespace RedBlackTree
{
    public class PassiveItem_Elixir : PassiveItem
    {
        public new BasicStatus_PassiveItem_Elixir BasicStatus { get; }

        public override string Option
        {
            get { return string.Format(base.Option, LifeRegenerationRate * 100.0); }
        }

        public float LifeRegenerationRate
        {
            get { return GetLifeRegenerationRate(Level); }
        }

        public PassiveItem_Elixir(BasicStatus_PassiveItem_Elixir basicStatus) : base(EPassiveItem.Elixir, basicStatus)
        {
            BasicStatus = basicStatus;
        }

        public float GetLifeRegenerationRate(int level)
        {
            return BasicStatus.LifeRegenerationRates[Mathf.Clamp(level, 1, MaxLevel) - 1];
        }
    }
}