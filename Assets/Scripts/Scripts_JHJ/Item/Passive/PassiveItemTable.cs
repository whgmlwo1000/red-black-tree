﻿using UnityEngine;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New PassiveItemTable", menuName = "Item/Passive Item Table", order = 1000)]
    public class PassiveItemTable : ScriptableObject
    {
        [SerializeField]
        private BasicStatus_PassiveItem[] mBasicStatus = new BasicStatus_PassiveItem[(int)EPassiveItem.Count];
        
        public PassiveItem[] CreateItems()
        {
            PassiveItem[] passiveItems = new PassiveItem[(int)EPassiveItem.Count];

            passiveItems[(int)EPassiveItem.AffluenceSeed] 
                = new PassiveItem_AffluenceSeed(mBasicStatus[(int)EPassiveItem.AffluenceSeed] as BasicStatus_PassiveItem_AffluenceSeed);
            passiveItems[(int)EPassiveItem.FlameFairy]
                = new PassiveItem_ElementalFairy(EPassiveItem.FlameFairy, mBasicStatus[(int)EPassiveItem.FlameFairy] as BasicStatus_PassiveItem_ElementalFairy);
            passiveItems[(int)EPassiveItem.LightFairy]
                = new PassiveItem_ElementalFairy(EPassiveItem.LightFairy, mBasicStatus[(int)EPassiveItem.LightFairy] as BasicStatus_PassiveItem_ElementalFairy);
            passiveItems[(int)EPassiveItem.TyrSword]
                = new PassiveItem_TyrSword(mBasicStatus[(int)EPassiveItem.TyrSword] as BasicStatus_PassiveItem_TyrSword);
            passiveItems[(int)EPassiveItem.TyrShield]
                = new PassiveItem_TyrShield(mBasicStatus[(int)EPassiveItem.TyrShield] as BasicStatus_PassiveItem_TyrShield);
            passiveItems[(int)EPassiveItem.PurificationAmulet]
                = new PassiveItem_PurificationAmulet(mBasicStatus[(int)EPassiveItem.PurificationAmulet] as BasicStatus_PassiveItem_PurificationAmulet);
            passiveItems[(int)EPassiveItem.ExorcismAmulet]
                = new PassiveItem_ExorcismAmulet(mBasicStatus[(int)EPassiveItem.ExorcismAmulet] as BasicStatus_PassiveItem_ExorcismAmulet);
            passiveItems[(int)EPassiveItem.Elixir]
                = new PassiveItem_Elixir(mBasicStatus[(int)EPassiveItem.Elixir] as BasicStatus_PassiveItem_Elixir);
            passiveItems[(int)EPassiveItem.FrigarHandMirror]
                = new PassiveItem_FrigarHandMirror(mBasicStatus[(int)EPassiveItem.FrigarHandMirror] as BasicStatus_PassiveItem_FrigarHandMirror);
            passiveItems[(int)EPassiveItem.GreedNecklace]
                = new PassiveItem_GreedNecklace(mBasicStatus[(int)EPassiveItem.GreedNecklace] as BasicStatus_PassiveItem_GreedNecklace);

            return passiveItems;
        }
    }
}