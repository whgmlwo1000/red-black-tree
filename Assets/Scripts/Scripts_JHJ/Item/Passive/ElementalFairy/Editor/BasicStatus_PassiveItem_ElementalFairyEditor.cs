﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_PassiveItem_ElementalFairy))]
    public class BasicStatus_PassiveItem_ElementalFairyEditor : BasicStatus_PassiveItemEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            BasicStatus_PassiveItem_ElementalFairy basicStatus = target as BasicStatus_PassiveItem_ElementalFairy;

            SerializedProperty projectileTermProps = serializedObject.FindProperty("ProjectileTerms");
            SerializedProperty projectileOffenseProps = serializedObject.FindProperty("ProjectileOffenses");

            if(projectileTermProps.arraySize != basicStatus.MaxLevel || projectileOffenseProps.arraySize != basicStatus.MaxLevel)
            {
                projectileTermProps.arraySize = basicStatus.MaxLevel;
                projectileOffenseProps.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }

            for(int indexOfProp = 0; indexOfProp < projectileTermProps.arraySize; indexOfProp++)
            {
                EditorGUILayout.Space();

                SerializedProperty termProp = projectileTermProps.GetArrayElementAtIndex(indexOfProp);
                SerializedProperty offenseProp = projectileOffenseProps.GetArrayElementAtIndex(indexOfProp);

                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}", indexOfProp + 1)), EditorStyles.boldLabel);
                termProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Projectile Term"), termProp.floatValue);
                termProp.floatValue = Mathf.Max(termProp.floatValue, 0.0f);
                offenseProp.intValue = EditorGUILayout.IntField(new GUIContent("Projectile Offense"), offenseProp.intValue);
                offenseProp.intValue = Mathf.Max(offenseProp.intValue, 0);
            }
        }
    }
}