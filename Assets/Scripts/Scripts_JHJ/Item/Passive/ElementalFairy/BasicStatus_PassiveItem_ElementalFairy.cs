﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_PassiveItem_ElementalFairy", menuName = "Item/PassiveItem/Elemental Fairy Basic Status", order = 1000)]
    public class BasicStatus_PassiveItem_ElementalFairy : BasicStatus_PassiveItem
    {
        public float[] ProjectileTerms;
        public int[] ProjectileOffenses;
    }
}