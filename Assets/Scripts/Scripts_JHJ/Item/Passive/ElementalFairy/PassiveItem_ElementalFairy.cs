﻿using UnityEngine;

namespace RedBlackTree
{
    public class PassiveItem_ElementalFairy : PassiveItem
    {
        public new BasicStatus_PassiveItem_ElementalFairy BasicStatus { get; }

        public override string Option
        {
            get { return string.Format(base.Option, ProjectileOffense, ProjectileTerm); }
        }

        public float ProjectileTerm
        {
            get { return GetProjectileTerm(Level); }
        }

        public int ProjectileOffense
        {
            get { return GetProjectileOffense(Level); }
        }

        public PassiveItem_ElementalFairy(EPassiveItem type, BasicStatus_PassiveItem_ElementalFairy basicStatus) : base(type, basicStatus)
        {
            BasicStatus = basicStatus;
        }

        public float GetProjectileTerm(int level)
        {
            return BasicStatus.ProjectileTerms[Mathf.Clamp(level, 1, MaxLevel) - 1];
        }

        public int GetProjectileOffense(int level)
        {
            return BasicStatus.ProjectileOffenses[Mathf.Clamp(level, 1, MaxLevel) - 1];
        }
    }
}