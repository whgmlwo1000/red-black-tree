﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_PassiveItem_GreedNecklace", menuName = "Item/PassiveItem/Necklace Of Greed Basic Status", order = 1000)]
    public class BasicStatus_PassiveItem_GreedNecklace : BasicStatus_PassiveItem
    {
        public float[] BoxPossibilityIncrements;
    }
}