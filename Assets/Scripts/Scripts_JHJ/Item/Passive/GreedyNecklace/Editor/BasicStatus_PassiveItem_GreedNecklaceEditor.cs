﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_PassiveItem_GreedNecklace))]
    public class BasicStatus_PassiveItem_GreedNecklaceEditor : BasicStatus_PassiveItemEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            BasicStatus_PassiveItem_GreedNecklace basicStatus = target as BasicStatus_PassiveItem_GreedNecklace;

            SerializedProperty boxPossibilityIncrementProps = serializedObject.FindProperty("BoxPossibilityIncrements");

            if (boxPossibilityIncrementProps.arraySize != basicStatus.MaxLevel)
            {
                boxPossibilityIncrementProps.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }

            for (int indexOfProp = 0; indexOfProp < boxPossibilityIncrementProps.arraySize; indexOfProp++)
            {
                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}", indexOfProp + 1)), EditorStyles.boldLabel);
                SerializedProperty boxPossibilityIncrementProp = boxPossibilityIncrementProps.GetArrayElementAtIndex(indexOfProp);

                boxPossibilityIncrementProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Box Possibility Increment", "박스 발견 확률 증가량"), boxPossibilityIncrementProp.floatValue);
                boxPossibilityIncrementProp.floatValue = Mathf.Clamp(boxPossibilityIncrementProp.floatValue, 0.0f, 1.0f);
                EditorGUILayout.Space();
            }
        }
    }
}