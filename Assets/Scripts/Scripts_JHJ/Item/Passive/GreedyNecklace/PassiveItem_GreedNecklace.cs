﻿using UnityEngine;

namespace RedBlackTree
{
    public class PassiveItem_GreedNecklace : PassiveItem
    {
        public new BasicStatus_PassiveItem_GreedNecklace BasicStatus { get; }

        public override string Option
        {
            get { return string.Format(base.Option, BoxPossibilityIncrement * 100.0); }
        }

        public float BoxPossibilityIncrement
        {
            get { return GetBoxPossibilityIncrement(Level); }
        }

        public int BoxPossibilityIncrementKey { get; }

        public PassiveItem_GreedNecklace(BasicStatus_PassiveItem_GreedNecklace basicStatus) : base(EPassiveItem.GreedNecklace, basicStatus)
        {
            BasicStatus = basicStatus;
            BoxPossibilityIncrementKey = LevelManager.AddBoxPossibilityIncrement();
            OnLevelChanged += OnGreedNecklaceLevelChanged;
        }

        public void OnGreedNecklaceLevelChanged()
        {
            if(IsAcquired)
            {
                LevelManager.SetBoxPossibilityIncrement(BoxPossibilityIncrementKey, BoxPossibilityIncrement);
            }
            else
            {
                LevelManager.SetBoxPossibilityIncrement(BoxPossibilityIncrementKey, 0.0f);
            }
        }

        public float GetBoxPossibilityIncrement(int level)
        {
            return BasicStatus.BoxPossibilityIncrements[Mathf.Clamp(level, 1, MaxLevel) - 1];
        }
    }
}