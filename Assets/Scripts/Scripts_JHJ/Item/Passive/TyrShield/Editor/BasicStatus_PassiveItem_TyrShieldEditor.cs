﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_PassiveItem_TyrShield))]
    public class BasicStatus_PassiveItem_TyrShieldEditor : BasicStatus_PassiveItemEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            BasicStatus_PassiveItem_TyrShield basicStatus = target as BasicStatus_PassiveItem_TyrShield;

            SerializedProperty damageDecrementProps = serializedObject.FindProperty("DamageDecrements");

            if (damageDecrementProps.arraySize != basicStatus.MaxLevel)
            {
                damageDecrementProps.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }

            for (int indexOfProp = 0; indexOfProp < damageDecrementProps.arraySize; indexOfProp++)
            {
                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}", indexOfProp + 1)), EditorStyles.boldLabel);
                SerializedProperty damageDecrementProp = damageDecrementProps.GetArrayElementAtIndex(indexOfProp);

                damageDecrementProp.intValue = EditorGUILayout.IntField(new GUIContent("Damage Decrement", "피해 감소량"), damageDecrementProp.intValue);
                damageDecrementProp.intValue = Mathf.Max(damageDecrementProp.intValue, 0);
                EditorGUILayout.Space();
            }
        }
    }
}