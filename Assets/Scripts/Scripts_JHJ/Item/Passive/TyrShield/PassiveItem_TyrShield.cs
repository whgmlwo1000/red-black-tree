﻿using UnityEngine;

namespace RedBlackTree
{
    public class PassiveItem_TyrShield : PassiveItem
    {
        public new BasicStatus_PassiveItem_TyrShield BasicStatus { get; }

        public override string Option
        {
            get { return string.Format(base.Option, DamageDecrement); }
        }

        public int DamageDecrement
        {
            get { return GetDamageDecrement(Level); }
        }

        public PassiveItem_TyrShield(BasicStatus_PassiveItem_TyrShield basicStatus) : base(EPassiveItem.TyrShield, basicStatus)
        {
            BasicStatus = basicStatus;
        }

        public int GetDamageDecrement(int level)
        {
            return BasicStatus.DamageDecrements[Mathf.Clamp(level, 1, MaxLevel) - 1];
        }
    }
}