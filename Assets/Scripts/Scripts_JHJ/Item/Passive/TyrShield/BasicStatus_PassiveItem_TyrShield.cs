﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_PassiveItem_TyrShield", menuName = "Item/PassiveItem/Tyr's Shield Basic Status", order = 1000)]
    public class BasicStatus_PassiveItem_TyrShield : BasicStatus_PassiveItem
    {
        public int[] DamageDecrements;
    }
}