﻿using UnityEngine;

namespace RedBlackTree
{
    public class PassiveItem_TyrSword : PassiveItem
    {
        public new BasicStatus_PassiveItem_TyrSword BasicStatus { get; }

        public override string Option
        {
            get { return string.Format(base.Option, OffenseIncrement); }
        }

        public int OffenseIncrement
        {
            get { return GetOffenseIncrement(Level); }
        }

        public PassiveItem_TyrSword(BasicStatus_PassiveItem_TyrSword basicStatus) : base(EPassiveItem.TyrSword, basicStatus)
        { 
            BasicStatus = basicStatus;
        }

        public int GetOffenseIncrement(int level)
        {
            return BasicStatus.OffenseIncrements[Mathf.Clamp(level, 1, MaxLevel) - 1];
        }
    }
}