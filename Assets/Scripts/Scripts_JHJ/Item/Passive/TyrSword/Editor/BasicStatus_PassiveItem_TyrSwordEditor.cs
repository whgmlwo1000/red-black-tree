﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_PassiveItem_TyrSword))]
    public class BasicStatus_PassiveItem_TyrSwordEditor : BasicStatus_PassiveItemEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            BasicStatus_PassiveItem_TyrSword basicStatus = target as BasicStatus_PassiveItem_TyrSword;

            SerializedProperty offenseIncrementProps = serializedObject.FindProperty("OffenseIncrements");

            if(offenseIncrementProps.arraySize != basicStatus.MaxLevel)
            {
                offenseIncrementProps.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }

            for(int indexOfProp = 0; indexOfProp < offenseIncrementProps.arraySize; indexOfProp++)
            {
                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}", indexOfProp + 1)), EditorStyles.boldLabel);
                SerializedProperty offenseIncrementProp = offenseIncrementProps.GetArrayElementAtIndex(indexOfProp);

                offenseIncrementProp.intValue = EditorGUILayout.IntField(new GUIContent("Offense Increment", "공격력 증가량"), offenseIncrementProp.intValue);
                offenseIncrementProp.intValue = Mathf.Max(offenseIncrementProp.intValue, 0);
                EditorGUILayout.Space();
            }
        }
    }
}