﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_PassiveItem_TyrSword", menuName = "Item/PassiveItem/Tyr's Sword Basic Status", order = 1000)]
    public class BasicStatus_PassiveItem_TyrSword : BasicStatus_PassiveItem
    {
        public int[] OffenseIncrements;
    }
}