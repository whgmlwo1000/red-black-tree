﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_PassiveItem_AffluenceSeed", menuName = "Item/PassiveItem/Affluence Seed Basic Status", order = 1000)]
    public class BasicStatus_PassiveItem_AffluenceSeed : BasicStatus_PassiveItem
    {
        public float[] FruitTreePossibilityIncrements;
    }
}