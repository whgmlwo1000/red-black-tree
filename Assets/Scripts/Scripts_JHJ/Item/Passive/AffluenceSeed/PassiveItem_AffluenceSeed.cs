﻿using UnityEngine;

namespace RedBlackTree
{
    public class PassiveItem_AffluenceSeed : PassiveItem
    {
        public new BasicStatus_PassiveItem_AffluenceSeed BasicStatus { get; }

        public override string Option
        {
            get { return string.Format(base.Option, FruitTreePossibilityIncrement * 100.0); }
        }

        public int FruitTreePossibilityIncrementKey { get; }

        public float FruitTreePossibilityIncrement
        {
            get
            {
                return GetFruitPossibilityIncrement(Level);
            }
        }

        public PassiveItem_AffluenceSeed(BasicStatus_PassiveItem_AffluenceSeed basicStatus) : base(EPassiveItem.AffluenceSeed, basicStatus)
        {
            BasicStatus = basicStatus;
            FruitTreePossibilityIncrementKey = LevelManager.AddFruitTreePossibilityIncrement();

            OnLevelChanged += OnAffluenceSeedLevelChanged;
        }

        public float GetFruitPossibilityIncrement(int level)
        {
            return BasicStatus.FruitTreePossibilityIncrements[Mathf.Clamp(level, 1, MaxLevel) - 1];
        }

        public void OnAffluenceSeedLevelChanged()
        {
            if (IsAcquired)
            {
                LevelManager.SetFruitTreePossibilityIncrement(FruitTreePossibilityIncrementKey, FruitTreePossibilityIncrement);
            }
            else
            {
                LevelManager.SetFruitTreePossibilityIncrement(FruitTreePossibilityIncrementKey, 0.0f);
            }
        }
    }
}