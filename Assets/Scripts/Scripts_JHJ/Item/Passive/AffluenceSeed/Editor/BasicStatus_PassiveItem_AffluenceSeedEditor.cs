﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_PassiveItem_AffluenceSeed))]
    public class BasicStatus_PassiveItem_AffluenceSeedEditor : BasicStatus_PassiveItemEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            BasicStatus_PassiveItem_AffluenceSeed basicStatus = target as BasicStatus_PassiveItem_AffluenceSeed;

            SerializedProperty fruitTreePossibilityIncrementProps = serializedObject.FindProperty("FruitTreePossibilityIncrements");

            if (fruitTreePossibilityIncrementProps.arraySize != basicStatus.MaxLevel)
            {
                fruitTreePossibilityIncrementProps.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }

            for (int indexOfProp = 0; indexOfProp < fruitTreePossibilityIncrementProps.arraySize; indexOfProp++)
            {
                EditorGUILayout.Space();
                SerializedProperty possibilityProp = fruitTreePossibilityIncrementProps.GetArrayElementAtIndex(indexOfProp);

                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}", indexOfProp + 1)), EditorStyles.boldLabel);
                possibilityProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Fruit Tree Possibility Increment", "열매 나무 생성 확률 증가량"), possibilityProp.floatValue);
                possibilityProp.floatValue = Mathf.Clamp(possibilityProp.floatValue, 0.0f, 1.0f);
            }
        }
    }
}