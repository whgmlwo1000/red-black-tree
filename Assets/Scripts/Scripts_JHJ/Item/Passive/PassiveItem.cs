﻿using UnityEngine;

namespace RedBlackTree
{
    public enum EPassiveItem
    {
        None = -1, 
        /// <summary>
        /// 풍요의 씨앗
        /// </summary>
        AffluenceSeed, 

        /// <summary>
        /// 불꽃의 요정
        /// </summary>
        FlameFairy, 

        /// <summary>
        /// 빛의 요정
        /// </summary>
        LightFairy, 

        /// <summary>
        /// 티르의 검
        /// </summary>
        TyrSword, 

        /// <summary>
        /// 티르의 방패
        /// </summary>
        TyrShield, 

        /// <summary>
        /// 정화의 부적
        /// </summary>
        PurificationAmulet, 

        /// <summary>
        /// 퇴마의 부적
        /// </summary>
        ExorcismAmulet, 

        /// <summary>
        /// 엘릭서
        /// </summary>
        Elixir, 

        /// <summary>
        /// 프리가의 손거울
        /// </summary>
        FrigarHandMirror, 

        /// <summary>
        /// 탐욕의 목걸이
        /// </summary>
        GreedNecklace, 

        Count
    }

    public class PassiveItem : Item
    {
        public new BasicStatus_PassiveItem BasicStatus { get; }

        public EPassiveItem Type { get; }

        public PassiveItem(EPassiveItem type, BasicStatus_PassiveItem basicStatus) : base(basicStatus)
        {
            Type = type;
            BasicStatus = basicStatus;
        }
    }
}