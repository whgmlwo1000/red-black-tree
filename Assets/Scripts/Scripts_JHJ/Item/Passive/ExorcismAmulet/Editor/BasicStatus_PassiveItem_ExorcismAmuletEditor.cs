﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_PassiveItem_ExorcismAmulet))]
    public class BasicStatus_PassiveItem_ExorcismAmuletEditor : BasicStatus_PassiveItemEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            BasicStatus_PassiveItem_ExorcismAmulet basicStatus = target as BasicStatus_PassiveItem_ExorcismAmulet;

            SerializedProperty demonSoulsAdditionPossibilityProps = serializedObject.FindProperty("DemonSoulsAdditionPossibilities");

            if (demonSoulsAdditionPossibilityProps.arraySize != basicStatus.MaxLevel)
            {
                demonSoulsAdditionPossibilityProps.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }

            for (int indexOfProp = 0; indexOfProp < demonSoulsAdditionPossibilityProps.arraySize; indexOfProp++)
            {
                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}", indexOfProp + 1)), EditorStyles.boldLabel);
                SerializedProperty demonSoulsIncreaseProp = demonSoulsAdditionPossibilityProps.GetArrayElementAtIndex(indexOfProp);

                demonSoulsIncreaseProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Demon Souls Addition Possibility", "악마의 영혼 추가 습득 확률"), demonSoulsIncreaseProp.floatValue);
                demonSoulsIncreaseProp.floatValue = Mathf.Clamp(demonSoulsIncreaseProp.floatValue, 0.0f, 1.0f);
                EditorGUILayout.Space();
            }
        }
    }
}