﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_PassiveItem_ExorcismAmulet", menuName = "Item/PassiveItem/Exorcism Amulet Basic Status", order = 1000)]
    public class BasicStatus_PassiveItem_ExorcismAmulet : BasicStatus_PassiveItem
    {
        public float[] DemonSoulsAdditionPossibilities;
    }
}