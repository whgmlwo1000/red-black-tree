﻿using UnityEngine;

namespace RedBlackTree
{
    public class PassiveItem_ExorcismAmulet : PassiveItem
    {
        public new BasicStatus_PassiveItem_ExorcismAmulet BasicStatus { get; }

        public override string Option
        {
            get { return string.Format(base.Option, DemonSoulsAdditionPossibility * 100.0); }
        }

        public float DemonSoulsAdditionPossibility
        {
            get { return GetDemonSoulsAdditionPossibility(Level); }
        }

        public PassiveItem_ExorcismAmulet(BasicStatus_PassiveItem_ExorcismAmulet basicStatus) : base(EPassiveItem.ExorcismAmulet, basicStatus)
        {
            BasicStatus = basicStatus;
        }

        public float GetDemonSoulsAdditionPossibility(int level)
        {
            return BasicStatus.DemonSoulsAdditionPossibilities[Mathf.Clamp(level, 1, MaxLevel) - 1];
        }
    }
}