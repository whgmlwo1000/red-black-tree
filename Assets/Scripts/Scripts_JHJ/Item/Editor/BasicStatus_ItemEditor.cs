﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_Item))]
    public class BasicStatus_ItemEditor : Editor
    {
        private GUIContent mIconContent = new GUIContent("Icon", "아이콘 스프라이트");
        private GUIContent mNameTagContent = new GUIContent("Name Tag", "이름 태그");
        private GUIContent mOptionTagContent = new GUIContent("Option Tag", "옵션 태그");
        private GUIContent mMaxLevelContent = new GUIContent("Max Level", "최대 레벨");
        private GUIContent mMaxProficiencyContent = new GUIContent("Max Proficiency", "최대 숙달량");

        public override void OnInspectorGUI()
        {
            SetProperties();

            serializedObject.ApplyModifiedProperties();
        }

        public virtual void SetProperties()
        {
            SerializedProperty iconProp = serializedObject.FindProperty("Icon");
            SerializedProperty nameTagProp = serializedObject.FindProperty("NameTag");
            SerializedProperty optionTagProp = serializedObject.FindProperty("OptionTag");
            SerializedProperty maxLevelProp = serializedObject.FindProperty("MaxLevel");
            SerializedProperty maxProficiencyProp = serializedObject.FindProperty("MaxProficiency");

            iconProp.objectReferenceValue = EditorGUILayout.ObjectField(mIconContent, iconProp.objectReferenceValue, typeof(Sprite), false);
            nameTagProp.stringValue = EditorGUILayout.TextField(mNameTagContent, nameTagProp.stringValue);

            if(DataManager.HasTag(nameTagProp.stringValue))
            {
                EditorGUILayout.HelpBox(DataManager.GetScript(nameTagProp.stringValue), MessageType.None);
            }
            else
            {
                EditorGUILayout.HelpBox(StringConstants.None, MessageType.None);
            }

            optionTagProp.stringValue = EditorGUILayout.TextField(mOptionTagContent, optionTagProp.stringValue);

            if(DataManager.HasTag(optionTagProp.stringValue))
            {
                EditorGUILayout.HelpBox(DataManager.GetScript(optionTagProp.stringValue), MessageType.None);
            }
            else
            {
                EditorGUILayout.HelpBox(StringConstants.None, MessageType.None);
            }

            maxProficiencyProp.intValue = Mathf.Max(EditorGUILayout.IntField(mMaxProficiencyContent, maxProficiencyProp.intValue), 1);
            maxLevelProp.intValue = Mathf.Max(EditorGUILayout.IntField(mMaxLevelContent, maxLevelProp.intValue), 1);
        }
    }
}