﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Inventory))]
    public class InventoryEditor : Editor
    {
        private const string ActiveItemTable = "Active Item Table";
        private const string PassiveItemTable = "Passive Item Table";

        private const string ActiveItemInventorySizeString = "Show Active Item Inventory : {0}";
        private const string PassiveItemInventorySizeString = "Show Passive Item Inventory : {0}";

        private const string ItemSlotString = "{0}. {1}";
        private const string ProficiencyString = "Proficiency : {0}";
        private const string LevelString = "Level : {0}";
        private const string BasicStatusString = "Basic Status";
        private const string ActiveItemType = "Active Item Type";
        private const string PassiveItemType = "Passive Item Type";
        private const string AddActiveItem = "Add Active Item";
        private const string RemoveActiveItem = "Remove Active Item";
        private const string AddPassiveItem = "Add Passive Item";
        private const string RemovePassiveItem = "RemovePassiveItem";
        private const string ClearItems = "Clear Items";
        private const string Souls = "Souls";
        private const string DemonSouls = "Demon Souls";
        private bool mShowActiveItems = false;
        private bool mShowPassiveItems = false;
        private EPassiveItem mPassiveItemType = EPassiveItem.None;
        private EActiveItem mActiveItemType = EActiveItem.None;

        public override void OnInspectorGUI()
        {
            SetProperties();

            serializedObject.ApplyModifiedProperties();
        }

        public virtual void SetProperties()
        {
            Inventory inventory = target as Inventory;

            SerializedProperty activeItemTableProp = serializedObject.FindProperty("mActiveItemTable");
            SerializedProperty passiveItemTableProp = serializedObject.FindProperty("mPassiveItemTable");

            bool isTableAssigned = activeItemTableProp.objectReferenceValue != null && passiveItemTableProp.objectReferenceValue != null;

            activeItemTableProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent(ActiveItemTable), activeItemTableProp.objectReferenceValue, typeof(ActiveItemTable), false);
            passiveItemTableProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent(PassiveItemTable), passiveItemTableProp.objectReferenceValue, typeof(PassiveItemTable), false);

            if (activeItemTableProp.objectReferenceValue != null && passiveItemTableProp.objectReferenceValue != null)
            {
                if (!isTableAssigned)
                {
                    serializedObject.ApplyModifiedProperties();
                    inventory.CreateItems();
                }

                EditorGUILayout.Space();

                GUILayout.BeginHorizontal();
                mActiveItemType = (EActiveItem)EditorGUILayout.EnumPopup(ActiveItemType, mActiveItemType);
                if (GUILayout.Button(AddActiveItem))
                {
                    inventory.Add(mActiveItemType, 1);
                }
                else if (GUILayout.Button(RemoveActiveItem))
                {
                    inventory.Remove(mActiveItemType, 1);
                }
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                mPassiveItemType = (EPassiveItem)EditorGUILayout.EnumPopup(PassiveItemType, mPassiveItemType);
                if (GUILayout.Button(AddPassiveItem))
                {
                    inventory.Add(mPassiveItemType, 1);
                }
                else if (GUILayout.Button(RemovePassiveItem))
                {
                    inventory.Remove(mPassiveItemType, 1);
                }
                GUILayout.EndHorizontal();

                EditorGUILayout.Space();

                inventory.Souls = EditorGUILayout.IntField(new GUIContent(Souls), inventory.Souls);
                inventory.DemonSouls = EditorGUILayout.IntField(new GUIContent(DemonSouls), inventory.DemonSouls);

                mShowActiveItems = EditorGUILayout.Toggle(new GUIContent(string.Format(ActiveItemInventorySizeString, inventory.CountOfAddedActiveItems))
                    , mShowActiveItems);
                EditorGUILayout.Space();
                if (mShowActiveItems)
                {
                    for (int indexOfInventory = 0; indexOfInventory < inventory.CountOfAddedActiveItems; indexOfInventory++)
                    {
                        ActiveItem item = inventory.GetAddedActiveItemAt(indexOfInventory);
                        EditorGUILayout.LabelField(new GUIContent(string.Format(ItemSlotString, indexOfInventory, item.Type.ToString())));
                        EditorGUILayout.LabelField(new GUIContent(string.Format(LevelString, item.Level)));
                        EditorGUILayout.LabelField(new GUIContent(string.Format(ProficiencyString, item.Proficiency)));
                        EditorGUILayout.ObjectField(new GUIContent(BasicStatusString), item.BasicStatus, typeof(BasicStatus_ActiveItem), false);
                        EditorGUILayout.Space();
                    }
                }

                mShowPassiveItems = EditorGUILayout.Toggle(new GUIContent(string.Format(PassiveItemInventorySizeString, inventory.CountOfAddedPassiveItems))
                    , mShowPassiveItems);
                if (mShowPassiveItems)
                {
                    for (int indexOfInventory = 0; indexOfInventory < inventory.CountOfAddedPassiveItems; indexOfInventory++)
                    {
                        PassiveItem item = inventory.GetAddedPassiveItemAt(indexOfInventory);
                        EditorGUILayout.LabelField(new GUIContent(string.Format(ItemSlotString, indexOfInventory, item.Type.ToString())));
                        EditorGUILayout.LabelField(new GUIContent(string.Format(LevelString, item.Level)));
                        EditorGUILayout.LabelField(new GUIContent(string.Format(ProficiencyString, item.Proficiency)));
                        EditorGUILayout.ObjectField(new GUIContent(BasicStatusString), item.BasicStatus, typeof(BasicStatus_PassiveItem), false);
                        EditorGUILayout.Space();
                    }
                }

                EditorGUILayout.Space();

                if (GUILayout.Button(ClearItems))
                {
                    inventory.Clear();
                }
            }
        }
    }
}