﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(PlayerStatus))]
    public class PlayerStatusEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            PlayerStatus status = target as PlayerStatus;

            SerializedProperty inventoryProp = serializedObject.FindProperty("mInventory");
            SerializedProperty maxPromotionProp = serializedObject.FindProperty("mMaxPromotion");
            SerializedProperty statusCoefficientProps = serializedObject.FindProperty("mStatusCoefficients");
            SerializedProperty statusConstantProps = serializedObject.FindProperty("mStatusConstants");

            inventoryProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Inventory", "인벤토리"), inventoryProp.objectReferenceValue, typeof(Inventory), false);
            maxPromotionProp.intValue = EditorGUILayout.IntField(new GUIContent("Max Promotion", "최대 증가 횟수"), maxPromotionProp.intValue);
            maxPromotionProp.intValue = Mathf.Max(maxPromotionProp.intValue, 0);

            EditorGUILayout.Space();

            if(statusCoefficientProps.arraySize != (int)EPlayerStatus_Coefficient.Count)
            {
                statusCoefficientProps.arraySize = (int)EPlayerStatus_Coefficient.Count;
                serializedObject.ApplyModifiedProperties();
            }

            if(statusConstantProps.arraySize != (int)EPlayerStatus_Constant.Count)
            {
                statusConstantProps.arraySize = (int)EPlayerStatus_Constant.Count;
                serializedObject.ApplyModifiedProperties();
            }

            for(int indexOfStatus = 0; indexOfStatus < statusCoefficientProps.arraySize; indexOfStatus++)
            {
                SerializedProperty statusCoefficientProp = statusCoefficientProps.GetArrayElementAtIndex(indexOfStatus);

                statusCoefficientProp.floatValue = EditorGUILayout.FloatField(new GUIContent(((EPlayerStatus_Coefficient)indexOfStatus).ToString()), statusCoefficientProp.floatValue);
                statusCoefficientProp.floatValue = Mathf.Max(statusCoefficientProp.floatValue, 0.0f);
            }

            for(int indexOfStatus = 0; indexOfStatus < statusConstantProps.arraySize; indexOfStatus++)
            {
                SerializedProperty statusConstantProp = statusConstantProps.GetArrayElementAtIndex(indexOfStatus);

                statusConstantProp.floatValue = EditorGUILayout.FloatField(new GUIContent(((EPlayerStatus_Constant)indexOfStatus).ToString()), statusConstantProp.floatValue);
                statusConstantProp.floatValue = Mathf.Max(statusConstantProp.floatValue, 0.0f);
            }

            if(status.Inventory != null)
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField(new GUIContent("Current Status"), EditorStyles.boldLabel);

                for(EPlayerStatus_Coefficient e = EPlayerStatus_Coefficient.None + 1; e < EPlayerStatus_Coefficient.Count; e++)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent(string.Format("{0} Status : {1}", e, status.GetStatus(e))));
                    EditorGUILayout.LabelField(new GUIContent(string.Format("{0} Promotion : {1}", e, status.GetPromotion(e) * status.GetCoefficient(e))));
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent(string.Format("Promotion Cost : {0}", status.GetPromotionCost(e))));
                    if (status.CanPromote(e))
                    {
                        if(GUILayout.Button("Promote"))
                        {
                            status.Promote(e);
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                }

                for(EPlayerStatus_Constant e = EPlayerStatus_Constant.None + 1; e < EPlayerStatus_Constant.Count; e++)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent(string.Format("{0} Status : {1}", e, status.GetStatus(e))));
                    EditorGUILayout.LabelField(new GUIContent(string.Format("{0} Promotion : {1}", e, status.GetPromotion(e) * status.GetConstant(e))));
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent(string.Format("Promotion Cost : {0}", status.GetPromotionCost(e))));
                    if (status.CanPromote(e))
                    {
                        if (GUILayout.Button("Promote"))
                        {
                            status.Promote(e);
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.Space();

                EditorGUILayout.LabelField(new GUIContent(string.Format("Total Promotion Cost : {0}", status.PromotionCost)));
                status.Inventory.DemonSouls = EditorGUILayout.IntField(new GUIContent("Demon Souls In Inventory"), status.Inventory.DemonSouls);

                EditorGUILayout.Space();

                if(GUILayout.Button("Reset Promotion"))
                {
                    status.ResetPromotion();
                }
                if(GUILayout.Button("Apply Promotion"))
                {
                    status.ApplyPromotion();
                }
                if(GUILayout.Button("Reset Status"))
                {
                    status.Reset();
                }
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}