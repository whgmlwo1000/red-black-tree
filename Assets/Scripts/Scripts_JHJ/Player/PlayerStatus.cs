﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

namespace RedBlackTree
{
    public enum EPlayerStatus_Coefficient
    {
        None = -1,
        /// <summary>
        /// 거인 최대 생명력 추가 계수
        /// </summary>
        MaxLife,
        /// <summary>
        /// 투사체 반사시 공격력 추가 계수
        /// </summary>
        ReflectOffense,
        /// <summary>
        /// 요정 공격력 추가 계수
        /// </summary>
        FairyOffense,
        /// <summary>
        /// 아이템 공격력 추가 계수
        /// </summary>
        ItemOffense,
        Count
    }

    public enum EPlayerStatus_Constant
    {
        None = -1,
        /// <summary>
        /// 치명타 확률 추가 상수
        /// </summary>
        CriticalChance,

        Count
    }

    [CreateAssetMenu(fileName = "New PlayerStatus", menuName = "Player/Status", order = 1000)]
    public class PlayerStatus : ScriptableObject
    {
        [SerializeField]
        private Inventory mInventory = null;
        public Inventory Inventory { get { return mInventory; } }

        [SerializeField]
        private int mMaxPromotion = 12;
        public int MaxPromotion { get { return mMaxPromotion; } }

        [SerializeField]
        private float[] mStatusCoefficients = new float[(int)EPlayerStatus_Coefficient.Count];
        [SerializeField]
        private float[] mStatusConstants = new float[(int)EPlayerStatus_Constant.Count];

        private int[] mStatusCoefficientPromotions = new int[(int)EPlayerStatus_Coefficient.Count];
        private int[] mStatusConstantPromotions = new int[(int)EPlayerStatus_Constant.Count];

        public int PromotionCost { get; private set; }

        /// <summary>
        /// Status 전체 초기화
        /// </summary>
        public void Reset()
        {
            for (EPlayerStatus_Coefficient e = EPlayerStatus_Coefficient.None + 1; e < EPlayerStatus_Coefficient.Count; e++)
            {
                PlayerData.Current.SetLevel(e, 0);
                mStatusCoefficientPromotions[(int)e] = 0;
            }

            for (EPlayerStatus_Constant e = EPlayerStatus_Constant.None + 1; e < EPlayerStatus_Constant.Count; e++)
            {
                PlayerData.Current.SetLevel(e, 0);
                mStatusConstantPromotions[(int)e] = 0;
            }
            PromotionCost = 0;
        }

        /// <summary>
        /// Status Promotion 리셋
        /// </summary>
        public void ResetPromotion()
        {
            for(EPlayerStatus_Coefficient e = EPlayerStatus_Coefficient.None + 1; e < EPlayerStatus_Coefficient.Count; e++)
            {
                mStatusCoefficientPromotions[(int)e] = 0;
            }
            for(EPlayerStatus_Constant e = EPlayerStatus_Constant.None + 1; e < EPlayerStatus_Constant.Count; e++)
            {
                mStatusConstantPromotions[(int)e] = 0;
            }
            PromotionCost = 0;
        }

        /// <summary>
        /// Status Promotion 적용
        /// </summary>
        public void ApplyPromotion()
        {
            for(EPlayerStatus_Coefficient e = EPlayerStatus_Coefficient.None + 1; e < EPlayerStatus_Coefficient.Count; e++)
            {
                PlayerData.Current.SetLevel(e, PlayerData.Current.GetLevel(e) + mStatusCoefficientPromotions[(int)e]);
                mStatusCoefficientPromotions[(int)e] = 0;
            }

            for(EPlayerStatus_Constant e = EPlayerStatus_Constant.None + 1; e < EPlayerStatus_Constant.Count; e++)
            {
                PlayerData.Current.SetLevel(e, PlayerData.Current.GetLevel(e) + mStatusConstantPromotions[(int)e]);
                mStatusConstantPromotions[(int)e] = 0;
            }

            Inventory.DemonSouls -= PromotionCost;
            PromotionCost = 0;
        }

        /// <summary>
        /// Status 실제 레벨 반환
        /// </summary>
        public int GetLevel(EPlayerStatus_Coefficient type)
        {
            return PlayerData.Current.GetLevel(type);
        }
        /// <summary>
        /// Status 승급 횟수 반환
        /// </summary>
        public int GetPromotion(EPlayerStatus_Coefficient type)
        {
            return mStatusCoefficientPromotions[(int)type];
        }
        /// <summary>
        /// Status 실제 레벨 반환
        /// </summary>
        public int GetLevel(EPlayerStatus_Constant type)
        {
            return PlayerData.Current.GetLevel(type);
        }
        /// <summary>
        /// Status 승급 횟수 반환
        /// </summary>
        public int GetPromotion(EPlayerStatus_Constant type)
        {
            return mStatusConstantPromotions[(int)type];
        }

        /// <summary>
        /// Status 계수 단위 반환
        /// </summary>
        public float GetCoefficient(EPlayerStatus_Coefficient type)
        {
            return mStatusCoefficients[(int)type];
        }
        /// <summary>
        /// Status 상수 단위 반환
        /// </summary>
        public float GetConstant(EPlayerStatus_Constant type)
        {
            return mStatusConstants[(int)type];
        }
        /// <summary>
        /// 현재 실제 Level을 결과로 실제 Status 수치를 반환
        /// </summary>
        public float GetStatus(EPlayerStatus_Coefficient type)
        {
            return 1.0f + (GetLevel(type) * GetCoefficient(type));
        }
        /// <summary>
        /// 현재 실제 Level 을 결과로 실제 Status 수치를 반환
        /// </summary>
        public float GetStatus(EPlayerStatus_Constant type)
        {
            return GetLevel(type) * GetConstant(type);
        }

        public int GetPromotionCost(EPlayerStatus_Coefficient type)
        {
            return GetLevel(type) + GetPromotion(type) + 1;
        }

        public int GetPromotionCost(EPlayerStatus_Constant type)
        {
            return GetLevel(type) + GetPromotion(type) + 1;
        }

        /// <summary>
        /// 현재 승급 가능한 Status 라면 true 를 반환
        /// </summary>
        public bool CanPromote(EPlayerStatus_Coefficient type)
        {
            if (GetLevel(type) + GetPromotion(type) < mMaxPromotion 
                && GetPromotionCost(type) + PromotionCost <= Inventory.DemonSouls)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// 현재 승급 가능한 Status라면 true를 반환
        /// </summary>
        public bool CanPromote(EPlayerStatus_Constant type)
        {
            if(GetLevel(type) + GetPromotion(type) < mMaxPromotion 
                && GetPromotionCost(type) + PromotionCost <= Inventory.DemonSouls)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Status 승급
        /// </summary>
        public void Promote(EPlayerStatus_Coefficient type)
        {
            PromotionCost += GetLevel(type) + GetPromotion(type) + 1;
            mStatusCoefficientPromotions[(int)type]++;
        }
        /// <summary>
        /// Status 승급
        /// </summary>
        public void Promote(EPlayerStatus_Constant type)
        {
            PromotionCost += GetLevel(type) + GetPromotion(type) + 1;
            mStatusConstantPromotions[(int)type]++;
        }
    }
}