﻿using UnityEngine;

namespace RedBlackTree
{
    [System.Serializable]
    public class PlayerData
    {
        #region Static Method
        private static PlayerData mCurrent;
        /// <summary>
        /// 현재 사용중인 플레이어 데이터
        /// </summary>
        public static PlayerData Current
        {
            get
            {
                if (mCurrent == null)
                {
                    CurrentKey = 0;
                }
                return mCurrent;
            }
            private set { mCurrent = value; }
        }

        private static int mCurrentKey = -1;
        /// <summary>
        /// 현재 사용중인 플레이어 데이터의 키값
        /// 가리키는 데이터가 없을 경우에는 -1 값을 저장
        /// </summary>
        public static int CurrentKey
        {
            get { return mCurrentKey; }
            set
            {
                mCurrentKey = value;
                string keyString = mCurrentKey.ToString();
                if (PlayerPrefs.HasKey(keyString))
                {
                    Current = JsonUtility.FromJson<PlayerData>(PlayerPrefs.GetString(keyString));
                }
                else
                {
                    Current = new PlayerData();
                }
            }
        }

        /// <summary>
        /// 전달된 키값을 가진 플레이어 데이터의 존재 여부 반환 메소드
        /// </summary>
        public static bool HasKey(int key)
        {
            string keyString = key.ToString();
            return PlayerPrefs.HasKey(keyString);
        }

        /// <summary>
        /// 전달된 키값을 가진 플레이어 데이터 객체 반환 메소드
        /// 단, Json 파일을 파싱하는 작업이 포함되어 있으므로 캐싱해서 사용하는 것을 권장
        /// </summary>
        public static PlayerData GetPlayerData(int key)
        {
            string keyString = key.ToString();
            return JsonUtility.FromJson<PlayerData>(PlayerPrefs.GetString(keyString));
        }

        /// <summary>
        /// 전달된 키값을 가진 플레이어 데이터를 삭제하는 메소드
        /// </summary>
        public static void DeletePlayerData(int key)
        {
            PlayerPrefs.DeleteKey(key.ToString());
        }

        /// <summary>
        /// Current가 참조하는 플레이어 데이터 객체를 저장하는 메소드
        /// </summary>
        public static void Save()
        {
            PlayerPrefs.SetString(mCurrentKey.ToString(), JsonUtility.ToJson(Current));
            PlayerPrefs.Save();
#if (UNITY_IOS || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX)
#elif UNITY_ANDROID
            
#endif
        }
#endregion

        [SerializeField]
        private int mDemonSouls = 0;
        public int DemonSouls
        {
            get { return mDemonSouls; }
            set { mDemonSouls = Mathf.Max(value, 0); }
        }

        [SerializeField]
        private int[] mStatusCoefficientLevels = new int[(int)EPlayerStatus_Coefficient.Count];
        public int GetLevel(EPlayerStatus_Coefficient type)
        {
            return mStatusCoefficientLevels[(int)type];
        }
        public void SetLevel(EPlayerStatus_Coefficient type, int level)
        {
            mStatusCoefficientLevels[(int)type] = Mathf.Max(level, 0);
        }


        [SerializeField]
        private int[] mStatusConstantLevels = new int[(int)EPlayerStatus_Constant.Count];
        public int GetLevel(EPlayerStatus_Constant type)
        {
            return mStatusConstantLevels[(int)type];
        }
        public void SetLevel(EPlayerStatus_Constant type, int level)
        {
            mStatusConstantLevels[(int)type] = Mathf.Max(level, 0);
        }

        [SerializeField]
        private bool[] mOpenedFairySkins = new bool[(int)ESkin_Fairy.Count];
        public bool IsOpenedFairySkin(ESkin_Fairy skin)
        {
            return mOpenedFairySkins[(int)skin];
        }
        public void OpenFairySkin(ESkin_Fairy skin)
        {
            mOpenedFairySkins[(int)skin] = true;
            Save();
        }

        [SerializeField]
        private ESkin_Fairy mFairySkin = ESkin_Fairy.Normal;
        public ESkin_Fairy FairySkin
        {
            get { return mFairySkin; }
            set
            {
                mFairySkin = value;
                Save();
            }
        }

        [SerializeField]
        private bool[] mCitizenRescue = new bool[(int)ECitizen.Count];
        public bool IsRescued(ECitizen citizen)
        {
            return mCitizenRescue[(int)citizen];
        }
        public void SetRescue(ECitizen citizen, bool isRescued)
        {
            mCitizenRescue[(int)citizen] = isRescued;
            Save();
        }

        [SerializeField]
        private int mRound = 1;
        /// <summary>
        /// 회차 수
        /// </summary>
        public int Round
        {
            get { return mRound; }
            set { mRound = Mathf.Max(value, 1); }
        }

        [SerializeField]
        private bool mIsClearTutorial = false;
        public bool IsClearTutorial {
            get { return mIsClearTutorial; }
            set {
                mIsClearTutorial = value;
                Save();
            }
        }
        [SerializeField]
        UIPlayerMenuSetting.Volumes mVolumes = new UIPlayerMenuSetting.Volumes();
        public UIPlayerMenuSetting.Volumes Volumes {
            get {
                return mVolumes;
            }
            set {
                mVolumes = value;
                Save();
            }
        }

        [SerializeField]
        bool mVibration = true;
        public bool Vibration {
            get {
                return mVibration;
            }
            set {
                mVibration = value;
                Save();
            }
        }


        public void Reset()
        {
            DemonSouls = 0;
            IsClearTutorial = false;

            for (EPlayerStatus_Constant status = EPlayerStatus_Constant.None + 1; status < EPlayerStatus_Constant.Count; status++)
            {
                SetLevel(status, 0);
            }
            for(EPlayerStatus_Coefficient status = EPlayerStatus_Coefficient.None + 1; status < EPlayerStatus_Coefficient.Count; status++)
            {
                SetLevel(status, 0);
            }
            for(ECitizen citizen = ECitizen.None + 1; citizen < ECitizen.Count; citizen++)
            {
                SetRescue(citizen, false);
            }
        }
    }
}