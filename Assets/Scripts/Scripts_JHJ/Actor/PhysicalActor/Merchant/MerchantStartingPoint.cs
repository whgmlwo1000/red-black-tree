﻿using UnityEngine;

namespace RedBlackTree
{
    public class MerchantStartingPoint : MonoBehaviour
    {
        public static MerchantStartingPoint Main { get; private set; }

        public void Awake()
        {
            Main = this;
        }
        public void OnDestroy()
        {
            Main = null;
        }
    }
}