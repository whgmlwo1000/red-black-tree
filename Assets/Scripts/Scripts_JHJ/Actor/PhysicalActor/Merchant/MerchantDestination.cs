﻿using UnityEngine;

namespace RedBlackTree
{
    public class MerchantDestination : MonoBehaviour
    {
        public static MerchantDestination Main { get; private set; } = null;

        public void Awake()
        {
            Main = this;
        }

        public void OnDestroy()
        {
            Main = null;
        }
    }
}