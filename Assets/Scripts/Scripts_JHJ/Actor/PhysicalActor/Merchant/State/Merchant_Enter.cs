﻿using UnityEngine;

namespace RedBlackTree
{
    public class Merchant_Enter : State<EStateType_Merchant>
    {
        private Merchant mMerchant;

        public Merchant_Enter(Merchant merchant) : base(EStateType_Merchant.Enter)
        {
            mMerchant = merchant;
        }

        public override void Start()
        {
            mMerchant.MainAnimator.SetInteger("state", (int)Type);
        }

        public override void FixedUpdate()
        {
            mMerchant.FollowCamera();

            if(!mMerchant.CheckArrival())
            {
                mMerchant.MoveToDestination();
            }
        }
    }
}