﻿using UnityEngine;

namespace RedBlackTree
{
    public class Merchant_Exit : State<EStateType_Merchant>
    {
        private Merchant mMerchant;

        public Merchant_Exit(Merchant merchant) : base(EStateType_Merchant.Exit)
        {
            mMerchant = merchant;
        }

        public override void Start()
        {
            mMerchant.MainAnimator.SetInteger("state", (int)Type);
        }

        public override void FixedUpdate()
        {
            mMerchant.FollowCamera();
        }
    }
}