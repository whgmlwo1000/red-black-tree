﻿using UnityEngine;

namespace RedBlackTree
{
    public class Merchant_Sell : State<EStateType_Merchant>
    {
        private Merchant mMerchant;

        public Merchant_Sell(Merchant merchant) : base(EStateType_Merchant.Sell)
        {
            mMerchant = merchant;
        }

        public override void Start()
        {
            mMerchant.MainAnimator.SetInteger("state", (int)Type);
        }

        public override void Update()
        {
            mMerchant.CheckSoldOut();
        }

        public override void FixedUpdate()
        {
            mMerchant.FollowCamera();
        }
    }
}