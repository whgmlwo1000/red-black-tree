﻿using UnityEngine;
using UnityEngine.SceneManagement;
using ObjectManagement;
using CameraManagement;

namespace RedBlackTree
{
    public enum EStateType_Merchant
    {
        None = -1, 

        Enter, 
        Sell, 
        Exit, 
    }

    public enum EMerchant
    {
        None = -1, 

        Midgard, 
    }

    public enum ETriggerType_Merchant
    {
        None = -1 , 

        DestinationTrigger, 
    }

    public enum EAudioType_Merchant
    {
        None = -1, 

        Appear = 0, 
        Leave = 1, 
    }

    [RequireComponent(typeof(TriggerHandler_Merchant), typeof(AudioHandler_Merchant))]
    public class Merchant : APhysicalActor<EStateType_Merchant>, IPoolable<EMerchant>
    {
        public override EActorType ActorType { get { return EActorType.Merchant; } }

        public bool IsWorldCanvas { get { return false; } }

        public bool IsPooled { get { return !gameObject.activeSelf; } }

        public TriggerHandler_Merchant TriggerHandler { get; private set; }
        public AudioHandler_Merchant AudioHandler { get; private set; }

        [SerializeField]
        private EMerchant mType = EMerchant.None;
        public EMerchant Type { get { return mType; } }

        private Vector2 mPrevCameraPosition;
        private ProductStand mProductStand;

        [SerializeField]
        private float mSpeedRate = 1.0f;

        private bool mIsDisplayed = false;

        public SellerRemainTimeHandler RemainTimeHandler { get; private set; }

        public override void Awake()
        {
            base.Awake();

            TriggerHandler = GetComponent<TriggerHandler_Merchant>();
            AudioHandler = GetComponent<AudioHandler_Merchant>();
            mProductStand = GetComponentInChildren<ProductStand>(true);
            RemainTimeHandler = GetComponentInChildren<SellerRemainTimeHandler>(true);

            StateManager.AddOnExitAbyss(OnActiveSceneChanged);

            SetStates(new Merchant_Enter(this)
                , new Merchant_Sell(this)
                , new Merchant_Exit(this));
        }

        public override void Start()
        {
            base.Start();

            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        /// <summary>
        /// 카메라의 이동과 함께 움직이는 메소드
        /// </summary>
        public void FollowCamera()
        {
            Vector2 cameraPosition = CameraController.Main.Position;
            Vector2 curPosition = transform.position;
            transform.position = curPosition + (cameraPosition - mPrevCameraPosition);
            mPrevCameraPosition = cameraPosition;
        }

        /// <summary>
        /// 목적지로 이동하는 메소드
        /// </summary>
        public void MoveToDestination()
        {
            Vector2 destination = MerchantDestination.Main.transform.position;
            Vector2 curPosition = transform.position;
            Velocity = (destination - curPosition) * mSpeedRate;
        }

        /// <summary>
        /// 목적지에 도착했는지 검사하는 메소드
        /// </summary>
        public bool CheckArrival()
        {
            Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Merchant.DestinationTrigger, out int count);
            for (int indexOfCollider = 0; indexOfCollider < count; indexOfCollider++)
            {
                MerchantDestination dst = colliders[indexOfCollider].GetComponent<MerchantDestination>();
                if (dst != null)
                {
                    Velocity = Vector2.zero;
                    State = EStateType_Merchant.Sell;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 가판대에 상품이 매진되었는지 검사하는 메소드
        /// </summary>
        public bool CheckSoldOut()
        {
            if (mIsDisplayed && mProductStand.IsSoldOut(StateManager.Scene.State == EScene.Scene_Sanctuary ?
                InventoryBag.Inventory.DemonSouls : InventoryBag.Inventory.Souls))
            {
                mProductStand.Deactivate();
                State = EStateType_Merchant.Exit;
                return true;
            }
            return false;
        }

        /// <summary>
        /// 상인을 생성하는 메소드
        /// </summary>
        public void Spawn()
        {
            mPrevCameraPosition = CameraController.Main.Position;
            transform.position = MerchantStartingPoint.Main.transform.position;
            gameObject.SetActive(true);
            State = EStateType_Merchant.Enter;
            MainAnimator.SetBool("display", false);
            mIsDisplayed = false;

            PlayAppearAudio();
        }

        /// <summary>
        /// Exit 상태로 전이하는 메소드
        /// </summary>
        public void Leave()
        {
            mProductStand.Deactivate();
            State = EStateType_Merchant.Exit;
        }

        /// <summary>
        /// 애니메이션 상에서 가판대 생성 준비 애니메이션이 끝나면 호출되는 메소드
        /// </summary>
        private void Display_Animation()
        {
            MainAnimator.SetBool("display", true);
            mIsDisplayed = true;
            mProductStand.Activate();
        }

        /// <summary>
        /// 애니메이션 상에서 Exit 애니메이션이 끝날때 비활성화 시키기 위해 호출되는 메소드
        /// </summary>
        private void Deactivate_Animation()
        {
            gameObject.SetActive(false);
        }

        private void OnActiveSceneChanged()
        {
            mProductStand.Deactivate();
            Deactivate_Animation();
        }

        private void OnActiveSceneChanged(Scene src, Scene dst)
        {
            OnActiveSceneChanged();
        }

        private void PlayAppearAudio()
        {
            AudioHandler.Get(EAudioType_Merchant.Appear).AudioSource.Play();
        }

        private void PlayLeaveAudio_Animation()
        {
            AudioHandler.Get(EAudioType_Merchant.Leave).AudioSource.Play();
        }
    }
}