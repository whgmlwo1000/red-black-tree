﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Merchant))]
    public class MerchantEditor : APhysicalActorEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            EditorGUILayout.Space();

            SerializedProperty typeProp = serializedObject.FindProperty("mType");
            SerializedProperty speedRateProp = serializedObject.FindProperty("mSpeedRate");

            EMerchant type = (EMerchant)typeProp.intValue;
            type = (EMerchant)EditorGUILayout.EnumPopup(new GUIContent("Merchant Type"), type);
            typeProp.intValue = (int)type;

            speedRateProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Speed Rate"), speedRateProp.floatValue);
            speedRateProp.floatValue = Mathf.Max(speedRateProp.floatValue, 0.0f);

            return mainTransform;
        }
    }
}