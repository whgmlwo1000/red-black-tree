﻿using UnityEditor;
using ObjectManagement;

namespace RedBlackTree
{
    [CustomEditor(typeof(MerchantPool))]
    public class MerchantPoolEditor : PoolEditor<EMerchant>
    {

    }
}