﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class Fruit_Throw : State<EStateType_Fruit>
    {
        private Fruit mFruit;
        private float mRemainTime_Wait;
        private float mPrevCameraPosition_x;

        public Fruit_Throw(Fruit fruit) : base(EStateType_Fruit.Throw)
        {
            mFruit = fruit;
        }

        public override void Start()
        {
            mFruit.MainAnimator.SetInteger("state", (int)Type);
            mRemainTime_Wait = ValueConstants.FruitWaitTime;
            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }

        public override void Update()
        {
            if(mRemainTime_Wait > 0.0f)
            {
                mRemainTime_Wait -= Time.deltaTime;
            }
            else
            {
                mFruit.Contract();
            }
        }

        public override void FixedUpdate()
        {
            mPrevCameraPosition_x = mFruit.FollowCamera(mPrevCameraPosition_x);
            if(mFruit.Detect())
            {
                mFruit.Activate();
            }
        }
    }
}