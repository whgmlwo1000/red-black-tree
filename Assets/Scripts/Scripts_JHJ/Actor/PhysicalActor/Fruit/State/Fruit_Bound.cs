﻿using UnityEngine;

namespace RedBlackTree
{
    public class Fruit_Bound : State<EStateType_Fruit>
    {
        private Fruit mFruit;
        private float mRemainTime_Generating;
        private bool mIsGenerating;
        private Transform mRenderTransform;

        public Fruit_Bound(Fruit fruit) : base(EStateType_Fruit.Bound)
        {
            mFruit = fruit;
        }

        public override void Start()
        {
            mFruit.MainAnimator.SetInteger("state", (int)Type);
            mIsGenerating = true;
            mRemainTime_Generating = 1.0f;
            mRenderTransform = mFruit.MainRendererHandler.Get(0).transform;
            mRenderTransform.localScale = new Vector3(0.0f, 0.0f, 1.0f);
        }

        public override void End()
        {
            if(mIsGenerating)
            {
                mRenderTransform.localScale = Vector3.one;
            }
        }

        public override void Update()
        {
            if(mIsGenerating)
            {
                mRemainTime_Generating -= Time.deltaTime;
                if(mRemainTime_Generating > 0.0f)
                {
                    mRenderTransform.localScale = Vector3.Lerp(Vector3.one, new Vector3(0.0f, 0.0f, 1.0f), mRemainTime_Generating);
                }
                else
                {
                    mIsGenerating = false;
                    mRenderTransform.localScale = Vector3.one;
                }
            }
        }
    }
}