﻿using UnityEngine;

namespace RedBlackTree
{
    public class Fruit_Activate : State<EStateType_Fruit>
    {
        private Fruit mFruit;

        public Fruit_Activate(Fruit fruit) : base(EStateType_Fruit.Activate)
        {
            mFruit = fruit;
        }

        public override void Start()
        {
            mFruit.MainAnimator.SetInteger("state", (int)Type);
        }
    }
}