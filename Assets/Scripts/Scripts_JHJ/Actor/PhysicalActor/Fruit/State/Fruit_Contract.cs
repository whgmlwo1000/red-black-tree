﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class Fruit_Contract : State<EStateType_Fruit>
    {
        private Fruit mFruit;
        private float mPrevCameraPosition_x;

        public Fruit_Contract(Fruit fruit) : base(EStateType_Fruit.Contract)
        {
            mFruit = fruit;
        }

        public override void Start()
        {
            mFruit.MainAnimator.SetInteger("state", (int)Type);
            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }

        public override void FixedUpdate()
        {
            mPrevCameraPosition_x = mFruit.FollowCamera(mPrevCameraPosition_x);
        }
    }
}