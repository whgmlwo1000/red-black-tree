﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class InsaneWalnut_Activate : Fruit_Activate
    {
        private InsaneWalnut mFruit;
        private int mRemainCount_Bump;
        private float mRemainTime_Bump;

        private float mPrevCameraPosition_x;

        public InsaneWalnut_Activate(InsaneWalnut fruit) : base(fruit)
        {
            mFruit = fruit;
        }

        public override void Start()
        {
            base.Start();
            mRemainCount_Bump = mFruit.Status.BumpCount;
            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();

            mPrevCameraPosition_x = mFruit.FollowCamera(mPrevCameraPosition_x);

            if(mRemainTime_Bump > 0.0f)
            {
                mRemainTime_Bump -= Time.deltaTime;
            }
            else if (mFruit.Affect())
            {
                mFruit.AudioHandler.Get(EAudioType_Fruit.Dealing_Crush).AudioSource.Play();
                if (--mRemainCount_Bump > 0)
                {
                    mRemainTime_Bump = ValueConstants.BumpTerm;
                }
                else
                {
                    mFruit.Contract();
                    return;
                }
            }
        }
    }
}