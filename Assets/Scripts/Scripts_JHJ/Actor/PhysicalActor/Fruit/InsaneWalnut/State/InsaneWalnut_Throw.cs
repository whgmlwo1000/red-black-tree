﻿using UnityEngine;

namespace RedBlackTree
{
    public class InsaneWalnut_Throw : Fruit_Throw
    {
        private InsaneWalnut mFruit;

        public InsaneWalnut_Throw(InsaneWalnut fruit) : base(fruit)
        {
            mFruit = fruit;
        }

        public override void Start()
        {
            mFruit.Activate();
        }
    }
}