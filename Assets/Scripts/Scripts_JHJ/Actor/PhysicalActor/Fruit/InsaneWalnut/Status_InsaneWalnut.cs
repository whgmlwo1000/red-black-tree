﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Status_InsaneWalnut : Status_Fruit
    {
        private BasicStatus_InsaneWalnut mBasicStatus;

        public new BasicStatus_InsaneWalnut BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }

        public int BumpCount { get { return BasicStatus.BumpCount; } }
        public int Damage { get { return BasicStatus.Damages[Level - 1]; } }
    }
}