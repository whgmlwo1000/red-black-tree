﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(InsaneWalnut))]
    public class InsaneWalnutEditor : FruitEditor
    {
        public override Transform SetProperties()
        {
            Transform main = base.SetProperties();

            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");

            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Basic Status"),
                basicStatusProp.objectReferenceValue, typeof(BasicStatus_InsaneWalnut), false);
            return main;
        }
    }
}