﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_InsaneWalnut))]
    public class BasicStatus_InsaneWalnutEditor : BasicStatus_FruitEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            EditorGUILayout.Space();

            SerializedProperty bumpCountProp = serializedObject.FindProperty("BumpCount");
            SerializedProperty damageProps = serializedObject.FindProperty("Damages");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Bump Count"), GUILayout.MaxWidth(100));
            bumpCountProp.intValue = Mathf.Max(EditorGUILayout.IntField(bumpCountProp.intValue, GUILayout.MaxWidth(50)), 1);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            BasicStatus_InsaneWalnut basicStatus = target as BasicStatus_InsaneWalnut;
            if(damageProps.arraySize != basicStatus.MaxLevel)
            {
                damageProps.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }

            for(int indexOfProp = 0; indexOfProp < damageProps.arraySize; indexOfProp++)
            {
                SerializedProperty damageProp = damageProps.GetArrayElementAtIndex(indexOfProp);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent("Level {0}. Damage"), GUILayout.MaxWidth(150));
                damageProp.intValue = Mathf.Max(EditorGUILayout.IntField(damageProp.intValue, GUILayout.MaxWidth(50)), 0);
                EditorGUILayout.EndHorizontal();
            }
        }
    }
}