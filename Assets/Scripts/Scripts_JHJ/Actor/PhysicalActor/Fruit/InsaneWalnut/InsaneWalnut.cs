﻿using UnityEngine;

namespace RedBlackTree
{
    public class InsaneWalnut : Fruit
    {
        private Status_InsaneWalnut mStatus;
        public new Status_InsaneWalnut Status
        {
            get { return mStatus; }
            protected set
            {
                value.BasicStatus = mBasicStatus;
                base.Status = mStatus = value;
            }
        }

        [SerializeField]
        private BasicStatus_InsaneWalnut mBasicStatus = null;

        public override void Awake()
        {
            base.Awake();

            Status = ScriptableObject.CreateInstance<Status_InsaneWalnut>();

            SetState(new InsaneWalnut_Throw(this));
            SetState(new InsaneWalnut_Activate(this));
        }

        public bool Affect()
        {
            bool isAffected = false;
            Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Fruit.Affect, out int count);

            for(int indexOfCollider = 0; indexOfCollider < count; indexOfCollider++)
            {
                IDamagedActor actor = colliders[indexOfCollider].GetComponentInParent<IDamagedActor>();
                if(actor != null && actor.CanDamageShield)
                {
                    isAffected = true;
                    actor.Damage.Set(true
                        , EDamageType.Shield
                        , Status.Damage
                        , 0.0f
                        , false
                        , this
                        , CenterTransform.position);
                    actor.InvokeDamage();
                }
            }

            return isAffected || TriggerHandler.IsTriggered(ETriggerType_Fruit.Detect);
        }

        protected override void OnGenerate()
        {
            base.OnGenerate();

            MainColliderHandler.Get(EMainColliderType.HitBox_00).gameObject.SetActive(false);
        }

        protected override void OnActivate()
        {
            base.OnActivate();

            MainColliderHandler.Get(EMainColliderType.HitBox_00).gameObject.SetActive(true);
        }
    }
}