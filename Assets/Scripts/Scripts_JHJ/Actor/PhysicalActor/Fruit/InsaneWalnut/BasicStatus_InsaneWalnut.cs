﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_InsaneWalnut", menuName = "Fruit/Insane Walnut Basic Status", order = 1000)]
    public class BasicStatus_InsaneWalnut : BasicStatus_Fruit
    {
        public int BumpCount;
        public int[] Damages;
    }
}