﻿using UnityEngine;

namespace RedBlackTree
{
    [RequireComponent(typeof(Animator))]
    public class FruitOutline : MonoBehaviour
    { 
        public Fruit Fruit { get; private set; }
        public Animator MainAnimator { get; private set; }

        public void Awake()
        {
            Fruit = GetComponentInParent<Fruit>();
            MainAnimator = GetComponent<Animator>();

            Fruit.onGenerate += OnGenerate;
            Fruit.onThrow += OnThrow;
        }

        public void OnGenerate()
        {
            MainAnimator.SetBool("active", true);
        }

        public void OnThrow()
        {
            MainAnimator.SetBool("active", false);
        }
    }
}