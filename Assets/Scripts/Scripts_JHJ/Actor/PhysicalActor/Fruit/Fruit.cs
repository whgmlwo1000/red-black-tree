﻿using UnityEngine;
using UnityEngine.SceneManagement;
using ObjectManagement;
using TouchManagement;
using CameraManagement;

namespace RedBlackTree
{
    public enum EFruit
    {
        None = -1,
        /// <summary>
        /// 천상의 열매
        /// </summary>
        HeavenFruit = 0,
        /// <summary>
        /// 심연의 멜론
        /// </summary>
        AbyssMelon = 1,
        /// <summary>
        /// 정화의 열매
        /// </summary>
        PurifiedFruit = 2,
        /// <summary>
        /// 그림자골 열매
        /// </summary>
        ShadowValleyFruit = 3,
        /// <summary>
        /// 폭탄 열매
        /// </summary>
        BombFruit = 4, 

        /// <summary>
        /// 식인 토마토
        /// </summary>
        CannibalTomato = 5, 

        /// <summary>
        /// 미친 호박
        /// </summary>
        CrazyPumpkin = 6, 

        /// <summary>
        /// 정신없는 호두
        /// </summary>
        InsaneWalnut = 7, 

        /// <summary>
        /// 황금사과
        /// </summary>
        GoldenApple = 8, 

        Count
    }

    public enum EFruitCategory
    {
        None = -1,
        Dealing = 0,
        Supporting = 1,
        Count
    }

    public enum EStateType_Fruit
    {
        /// <summary>
        /// 나무에 매달린 상태
        /// </summary>
        Bound,
        /// <summary>
        /// 던져짐
        /// </summary>
        Throw,

        /// <summary>
        /// 수축
        /// </summary>
        Contract, 

        /// <summary>
        /// 활성화중
        /// </summary>
        Activate,
    }

    public enum ETriggerType_Fruit
    {
        Detect = 0, 
        Affect = 1
    }

    public enum EAudioType_Fruit
    {
        None = -1, 
        Throw = 0, 

        Dealing_Crush = 1, 
        Dealing_Enchant = 2, 
        Dealing_Explode = 3, 
        Dealing_MeleeAttack = 4, 

        Supporting_Enchant = 10, 
        Supporting_Recover = 11
    }

    [RequireComponent(typeof(TriggerHandler_Fruit), typeof(AudioHandler_Fruit))]
    [RequireComponent(typeof(TouchBox), typeof(Deactivator))]
    public abstract class Fruit : APhysicalActor<EStateType_Fruit>, IPoolable<EFruit>
    {
        [SerializeField]
        private EFruitCategory mCategory = EFruitCategory.None;
        public EFruitCategory Category { get { return mCategory; } }

        public override EActorType ActorType { get { return EActorType.Fruit; } }

        [SerializeField]
        private EFruit mType = EFruit.None;
        public EFruit Type { get { return mType; } }

        public ITouchReceiver Receiver { get; private set; }
        [SerializeField]
        private PlayerCommand mCommand = null;
        public PlayerCommand Command { get { return mCommand; } }

        public bool IsPooled { get { return !gameObject.activeSelf; } }

        public Status_Fruit Status { get; protected set; }

        public bool IsWorldCanvas { get { return false; } }

        public TriggerHandler_Fruit TriggerHandler { get; private set; }
        public AudioHandler_Fruit AudioHandler { get; private set; }

        private float mSumOfDeltaTime;
        private Vector2 mTouchStartPosition;

        public event System.Action onGenerate;
        public event System.Action onThrow;
        public event System.Action onContract;
        public event System.Action onActivate;
        public event System.Action onDeactivate;

        public override void Awake()
        {
            base.Awake();

            TriggerHandler = GetComponent<TriggerHandler_Fruit>();
            AudioHandler = GetComponent<AudioHandler_Fruit>();

            Receiver = GetComponent<ITouchReceiver>();
            Receiver.OnTouchStart += OnTouchStart;
            Receiver.OnTouchStay += OnTouchStay;

            StateManager.AddOnExitAbyss(OnActiveSceneChanged);

            SetStates(new Fruit_Bound(this)
                , new Fruit_Throw(this)
                , new Fruit_Contract(this)
                , new Fruit_Activate(this));
        }

        public override void Start()
        {
            base.Start();

            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        public void GenerateOn(Vector2 position, EHorizontalDirection direction)
        {
            transform.position = position;
            HorizontalDirection = direction;
            gameObject.SetActive(true);
            Receiver.enabled = true;
            MainRigidbody.constraints = RigidbodyConstraints2D.FreezeAll;

            float weightSum = Status.WeightSum_Level;
            float selection = Random.Range(0.0f, weightSum);

            for (int indexOfWeights = 0; indexOfWeights < Status.MaxLevel; indexOfWeights++)
            {
                float weight = Status.GetWeight_Level(indexOfWeights);
                if (selection < weight)
                {
                    Status.Level = indexOfWeights + 1;
                    break;
                }
                else
                {
                    selection -= weight;
                }
            }

            MainAnimator.SetInteger("level", Status.Level);

            OnGenerate();
            State = EStateType_Fruit.Bound;
        }

        public void Throw(Vector2 force)
        {
            Receiver.enabled = false;
            MainRigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;

            OnThrow(force);
            State = EStateType_Fruit.Throw;
        }

        public void Contract()
        {
            OnContract();
            State = EStateType_Fruit.Contract;
        }

        public void Activate()
        {
            OnActivate();
            State = EStateType_Fruit.Activate;
        }

        public void Deactivate()
        {
            OnDeactivate();
            gameObject.SetActive(false);
        }

        public float FollowCamera(float prevCameraPosition_x)
        {
            float cameraPosition_x = CameraController.Main.Position.x;
            if (StateManager.Scene.State != EScene.Scene_Sanctuary)
            {
                Vector2 curPosition = MainRigidbody.position;
                float cameraDeltaPosition_x = cameraPosition_x - prevCameraPosition_x;
                MainRigidbody.position = new Vector2(curPosition.x + cameraDeltaPosition_x, curPosition.y);
            }
            return cameraPosition_x;
        }

        public virtual bool Detect()
        {
            Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Fruit.Detect, out int count);
            for (int indexOfCollider = 0; indexOfCollider < count; indexOfCollider++)
            {
                IDamaged damaged = colliders[indexOfCollider].GetComponentInParent<IDamaged>();
                if (damaged != null)
                {
                    return true;
                }
            }
            return false;
        }

        private void OnTouchStart(Touch touch, Vector2 worldPosition)
        {
            mTouchStartPosition = worldPosition;
            mSumOfDeltaTime = 0.0f;
        }

        private void OnTouchStay(Touch touch, Vector2 worldPosition)
        {
            mSumOfDeltaTime += Time.unscaledDeltaTime;
            float length = Vector2.Distance(mTouchStartPosition, worldPosition);
            if (length > ValueConstants.FruitThrowDistance)
            {
                Command.IsThrown = true;
                Throw((worldPosition - mTouchStartPosition) / mSumOfDeltaTime);
            }
        }

        public void InvokeOnGenerate()
        {
            onGenerate?.Invoke();
        }
        public void InvokeOnThrow()
        {
            onThrow?.Invoke();
        }
        public void InvokeOnContract()
        {
            onContract?.Invoke();
        }
        public void InvokeOnActivate()
        {
            onActivate?.Invoke();
        }
        public void InvokeOnDeactivate()
        {
            onDeactivate?.Invoke();
        }


        protected virtual void OnGenerate()
        {
            InvokeOnGenerate();
        }
        protected virtual void OnThrow(Vector2 force)
        {
            PlayThrowAudio();
            // Supporting 열매라면 Giant에게 유도되어 날라간다.
            if (mCategory == EFruitCategory.Supporting)
            {
                Vector2 direction;
                // 성역 내에서는 허수아비를 표적으로 해서 날아간다.
                if(StateManager.Scene.State != EScene.Scene_Sanctuary)
                {
                    Vector2 giantPosition = Giant.Main.CenterTransform.position;
                    Vector2 fruitPosition = MainRigidbody.position;
                    direction = giantPosition - fruitPosition;
                }
                else
                {
                    Vector2 scareCrowPosition = Scarecrow.Main.CenterTransform.position;
                    Vector2 fruitPosition = MainRigidbody.position;
                    direction = scareCrowPosition - fruitPosition;
                }
                
                force = new Vector2(direction.x / 0.5f, direction.y / 0.5f - Physics2D.gravity.y * 0.5f * 0.5f);
                Velocity = force;
            }
            else
            {
                Velocity = force / Status.Weight;
            }

            InvokeOnThrow();
        }
        protected virtual void OnContract()
        {
            MainRigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
            InvokeOnContract();
        }
        protected virtual void OnActivate()
        {
            InvokeOnActivate();
        }
        protected virtual void OnDeactivate()
        {
            InvokeOnDeactivate();
        }

        private void OnActiveSceneChanged()
        {
            Deactivate();
        }

        private void OnActiveSceneChanged(Scene src, Scene dst)
        {
            OnActiveSceneChanged();
        }

        private void Deactivate_Animation()
        {
            Deactivate();
        }

        private void Activate_Animation()
        {
            Activate();
        }

        private void PlayThrowAudio()
        {
            AudioHandler.Get(EAudioType_Fruit.Throw).AudioSource.Play();
        }
    }
}