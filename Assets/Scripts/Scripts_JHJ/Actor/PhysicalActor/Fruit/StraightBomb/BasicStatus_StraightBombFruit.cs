﻿using UnityEngine;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_StraightBombFruit", menuName = "Fruit/Straight Bomb Basic Status", order = 1000)]
    public class BasicStatus_StraightBombFruit : BasicStatus_Fruit
    {
        public int BombCount;
        public float Speed;
        public int[] Damages;
    }
}