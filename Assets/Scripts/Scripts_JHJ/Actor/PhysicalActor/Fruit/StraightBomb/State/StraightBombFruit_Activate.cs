﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class StraightBombFruit_Activate : Fruit_Activate
    {
        private StraightBombFruit mFruit;
        private float mPrevCameraPosition_x;

        public StraightBombFruit_Activate(StraightBombFruit fruit) : base(fruit)
        {
            mFruit = fruit;
        }

        public override void Start()
        {
            base.Start();
            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();

            if (mFruit.IsTerrainTriggered)
            {
                mFruit.Contract();
            }
            else
            {
                mPrevCameraPosition_x = mFruit.FollowCamera(mPrevCameraPosition_x);
            }
        }
    }
}