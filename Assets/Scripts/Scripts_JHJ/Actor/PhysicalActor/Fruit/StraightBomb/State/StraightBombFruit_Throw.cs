﻿using UnityEngine;


namespace RedBlackTree
{
    public class StraightBombFruit_Throw : Fruit_Throw
    {
        private StraightBombFruit mFruit;
        public StraightBombFruit_Throw(StraightBombFruit fruit) : base(fruit)
        {
            mFruit = fruit;
        }

        public override void Start()
        {
            mFruit.Activate();
        }
    }
}