﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_StraightBombFruit))]
    public class BasicStatus_StraightBombFruitEditor : BasicStatus_FruitEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            EditorGUILayout.Space();

            SerializedProperty bombCountProp = serializedObject.FindProperty("BombCount");
            SerializedProperty speedProp = serializedObject.FindProperty("Speed");
            SerializedProperty damageProps = serializedObject.FindProperty("Damages");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Bomb Count"), GUILayout.MaxWidth(100));
            bombCountProp.intValue = Mathf.Max(EditorGUILayout.IntField(bombCountProp.intValue, GUILayout.MaxWidth(50)), 1);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Speed"), GUILayout.MaxWidth(100));
            speedProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(speedProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            BasicStatus_StraightBombFruit basicStatus = target as BasicStatus_StraightBombFruit;
            if (damageProps.arraySize != basicStatus.MaxLevel)
            {
                damageProps.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }

            for(int indexOfProp = 0; indexOfProp < damageProps.arraySize; indexOfProp++)
            {
                SerializedProperty damageProp = damageProps.GetArrayElementAtIndex(indexOfProp);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}. Damage", indexOfProp + 1)), GUILayout.MaxWidth(120));
                damageProp.intValue = Mathf.Max(EditorGUILayout.IntField(damageProp.intValue, GUILayout.MaxWidth(50)), 0);
                EditorGUILayout.EndHorizontal();
            }
        }
    }
}