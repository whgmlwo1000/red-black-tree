﻿using UnityEngine;

namespace RedBlackTree
{
    public class StraightBombFruit : Fruit
    {
        private Status_StraightBombFruit mStatus;
        public new Status_StraightBombFruit Status
        {
            get { return mStatus; }
            protected set
            {
                value.BasicStatus = mBasicStatus;
                base.Status = mStatus = value;
            }
        }

        public bool IsTerrainTriggered
        {
            get { return TriggerHandler.IsTriggered(ETriggerType_Fruit.Detect); }
        }

        [SerializeField]
        private BasicStatus_StraightBombFruit mBasicStatus = null;

        private int mRemainCountOfBomb;
        public bool IsReady { get; private set; }

        public override void Awake()
        {
            base.Awake();

            Status = ScriptableObject.CreateInstance<Status_StraightBombFruit>();

            SetState(new StraightBombFruit_Throw(this));
            SetState(new StraightBombFruit_Activate(this));
        }

        protected override void OnThrow(Vector2 force)
        {
            Velocity = force.normalized * Status.Speed;
            InvokeOnThrow();
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            IsReady = true;
            MainAnimator.SetBool("ready", true);
            mRemainCountOfBomb = Status.BombCount;
        }

        private void Bomb_Animation()
        {
            PlayExplodeAudio();
            Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Fruit.Affect, out int count);
            for(int indexOfCollider = 0; indexOfCollider < count; indexOfCollider++)
            {
                IDamaged damaged = colliders[indexOfCollider].GetComponentInParent<IDamaged>();
                if(damaged != null)
                {
                    damaged.Damage.Set(true,
                        EDamageType.Shield,
                        Status.Damage,
                        0.0f,
                        false,
                        this,
                        CenterTransform.position);
                    damaged.InvokeDamage();
                }
            }

            mRemainCountOfBomb--;
        }

        private void EndOfBomb_Animation()
        {
            if(mRemainCountOfBomb <= 0)
            {
                Contract();
            }
        }

        private void EndOfReady_Animation()
        {
            IsReady = false;
            MainAnimator.SetBool("ready", false);
        }

        private void PlayExplodeAudio()
        {
            AudioHandler.Get(EAudioType_Fruit.Dealing_Explode).AudioSource.Play();
        }
    }
}