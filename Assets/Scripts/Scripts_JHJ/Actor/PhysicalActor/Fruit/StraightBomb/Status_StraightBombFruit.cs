﻿using UnityEngine;

namespace RedBlackTree
{
    public class Status_StraightBombFruit : Status_Fruit
    {
        private BasicStatus_StraightBombFruit mBasicStatus;
        public new BasicStatus_StraightBombFruit BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }

        public float Speed { get { return BasicStatus.Speed; } }
        public int BombCount { get { return BasicStatus.BombCount; } }
        public int Damage { get { return BasicStatus.Damages[Level - 1]; } }
    }
}