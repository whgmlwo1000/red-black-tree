﻿using UnityEngine;

namespace RedBlackTree
{
    public class Status_Fruit : ScriptableObject
    {
        public BasicStatus_Fruit BasicStatus { get; set; }

        public float Weight { get { return BasicStatus.Weight; } }

        public int MaxLevel { get { return BasicStatus.MaxLevel; } }

        private int mLevel = 1;
        public int Level
        {
            get { return mLevel; }
            set { mLevel = Mathf.Clamp(value, 1, MaxLevel); }
        }

        public float WeightSum_Level
        {
            get
            {
                float weightSum = 0.0f;
                for(int indexOfWeight = 0; indexOfWeight < BasicStatus.Weights_Level.Length; indexOfWeight++)
                {
                    weightSum += GetWeight_Level(indexOfWeight);
                }
                return weightSum;
            }
        }

        public float GetWeight_Level(int index)
        {
            return BasicStatus.Weights_Level[index];
        }

    }
}