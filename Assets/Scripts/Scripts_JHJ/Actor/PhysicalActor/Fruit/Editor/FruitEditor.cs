﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Fruit))]
    public class FruitEditor : APhysicalActorEditor
    {
        public override Transform SetProperties()
        {
            SerializedProperty fruitTypeProp = serializedObject.FindProperty("mType");
            SerializedProperty fruitCategoryProp = serializedObject.FindProperty("mCategory");
            SerializedProperty commandProp = serializedObject.FindProperty("mCommand");

            EFruitCategory fruitCategory = (EFruitCategory)fruitCategoryProp.intValue;
            fruitCategory = (EFruitCategory)EditorGUILayout.EnumPopup(new GUIContent("Fruit Category", "열매 유형"), fruitCategory);
            fruitCategoryProp.intValue = (int)fruitCategory;

            EFruit fruitType = (EFruit)fruitTypeProp.intValue;
            fruitType = (EFruit)EditorGUILayout.EnumPopup(new GUIContent("Fruit Type", "열매 고유 타입"), fruitType);
            fruitTypeProp.intValue = (int)fruitType;

            commandProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Player Command", "플레이어 커맨드"), commandProp.objectReferenceValue, typeof(PlayerCommand), false);

            serializedObject.ApplyModifiedProperties();

            return base.SetProperties();
        }
    }
}