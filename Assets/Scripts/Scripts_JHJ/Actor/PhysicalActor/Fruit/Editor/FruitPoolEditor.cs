﻿using UnityEngine;
using UnityEditor;
using ObjectManagement;

namespace RedBlackTree
{
    [CustomEditor(typeof(FruitPool))]
    public class FruitPoolEditor : PoolEditor<EFruit>
    {
    }
}