﻿using UnityEngine;
using UnityEditor;


namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_Fruit))]
    public class BasicStatus_FruitEditor : Editor
    {
        public sealed override void OnInspectorGUI()
        {
            SetProperties();
            serializedObject.ApplyModifiedProperties();
        }

        public virtual void SetProperties()
        {
            SerializedProperty weightProp = serializedObject.FindProperty("Weight");
            SerializedProperty maxLevelProp = serializedObject.FindProperty("MaxLevel");
            SerializedProperty weightOfLevelProps = serializedObject.FindProperty("Weights_Level");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Fruit Weight", "과일 무게"), GUILayout.MaxWidth(100));
            weightProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(weightProp.floatValue, GUILayout.MaxWidth(50)), 1.0f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Max Level", "최대 레벨"), GUILayout.MaxWidth(100));
            maxLevelProp.intValue = Mathf.Max(EditorGUILayout.IntField(maxLevelProp.intValue, GUILayout.MaxWidth(50)), 1);
            EditorGUILayout.EndHorizontal();
            if (weightOfLevelProps.arraySize != maxLevelProp.intValue)
            {
                weightOfLevelProps.arraySize = maxLevelProp.intValue;
                serializedObject.ApplyModifiedProperties();
            }

            for (int indexOfWeight = 0; indexOfWeight < weightOfLevelProps.arraySize; indexOfWeight++)
            {
                SerializedProperty weightOfLevelProp = weightOfLevelProps.GetArrayElementAtIndex(indexOfWeight);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}'s Weight", indexOfWeight + 1)), GUILayout.MaxWidth(100));
                weightOfLevelProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(weightOfLevelProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);
                EditorGUILayout.EndHorizontal();
            }
        }
    }
}