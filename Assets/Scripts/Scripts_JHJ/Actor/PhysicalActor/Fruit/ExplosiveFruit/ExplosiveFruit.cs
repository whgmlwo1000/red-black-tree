﻿using UnityEngine;

namespace RedBlackTree
{
    public class ExplosiveFruit : Fruit
    {
        private Status_ExplosiveFruit mExplosiveFruitStatus;
        public new Status_ExplosiveFruit Status
        {
            get { return mExplosiveFruitStatus; }
            protected set { base.Status = mExplosiveFruitStatus = value; }
        }

        public override void Awake()
        {
            base.Awake();

            SetState(new ExplosiveFruit_Activate(this));
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            MainRigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }
}