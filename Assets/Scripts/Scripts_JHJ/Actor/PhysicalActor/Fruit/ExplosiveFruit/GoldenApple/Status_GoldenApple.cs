﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Status_GoldenApple : Status_ExplosiveFruit
    {
        private BasicStatus_GoldenApple mBasicStatus;
        public new BasicStatus_GoldenApple BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }

        public float InvincibleTime { get { return BasicStatus.InvincibleTimes[Level - 1]; } }
    }
}