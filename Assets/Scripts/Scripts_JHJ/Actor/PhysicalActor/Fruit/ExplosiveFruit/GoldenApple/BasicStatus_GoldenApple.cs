﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_GoldenApple", menuName = "Fruit/Golden Apple Basic Status", order = 1000)]
    public class BasicStatus_GoldenApple : BasicStatus_ExplosiveFruit
    {
        public float[] InvincibleTimes;
    }
}