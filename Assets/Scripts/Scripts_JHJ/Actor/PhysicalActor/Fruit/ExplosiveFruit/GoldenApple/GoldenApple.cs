﻿using UnityEngine;

namespace RedBlackTree
{
    public class GoldenApple : ExplosiveFruit
    {
        private Status_GoldenApple mStatus;
        public new Status_GoldenApple Status
        {
            get { return mStatus; }
            protected set
            {
                value.BasicStatus = mBasicStatus;
                base.Status = mStatus = value;
            }
        }

        [SerializeField]
        private BasicStatus_GoldenApple mBasicStatus = null;

        public override void Awake()
        {
            base.Awake();

            Status = ScriptableObject.CreateInstance<Status_GoldenApple>();
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            PlayEnchantAudio();
        }
        private void SetInvincible_Animation()
        {
            Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Fruit.Affect, out int count);

            if (StateManager.Scene.State != EScene.Scene_Sanctuary)
            {
                for (int indexOfCollider = 0; indexOfCollider < count; indexOfCollider++)
                {
                    Giant giant = colliders[indexOfCollider].GetComponentInParent<Giant>();
                    if (giant != null)
                    {
                        giant.SetInvincible(Status.InvincibleTime);
                    }
                }
            }
            else
            {
                for (int indexOfCollider = 0; indexOfCollider < count; indexOfCollider++)
                {
                    Scarecrow scarecrow = colliders[indexOfCollider].GetComponentInParent<Scarecrow>();
                    if (scarecrow != null)
                    {
                        scarecrow.SetInvincible(Status.InvincibleTime);
                    }
                }
            }
        }

        private void PlayEnchantAudio()
        {
            AudioHandler.Get(EAudioType_Fruit.Supporting_Enchant).AudioSource.Play();
        }
    }
}