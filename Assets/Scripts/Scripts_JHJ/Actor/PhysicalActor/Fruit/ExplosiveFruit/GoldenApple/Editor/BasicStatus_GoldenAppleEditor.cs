﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_GoldenApple))]
    public class BasicStatus_GoldenAppleEditor : BasicStatus_ExplosiveFruitEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            SerializedProperty invincibleTimeProps = serializedObject.FindProperty("InvincibleTimes");

            BasicStatus_GoldenApple basicStatus = target as BasicStatus_GoldenApple;

            if(invincibleTimeProps.arraySize != basicStatus.MaxLevel)
            {
                invincibleTimeProps.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }

            EditorGUILayout.Space();

            for(int indexOfProp = 0; indexOfProp < invincibleTimeProps.arraySize; indexOfProp++)
            {
                SerializedProperty invincibleTimeProp = invincibleTimeProps.GetArrayElementAtIndex(indexOfProp);

                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}. Invincible Time", indexOfProp + 1)), GUILayout.MaxWidth(150));
                invincibleTimeProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(invincibleTimeProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);

                EditorGUILayout.EndHorizontal();
            }
        }
    }
}