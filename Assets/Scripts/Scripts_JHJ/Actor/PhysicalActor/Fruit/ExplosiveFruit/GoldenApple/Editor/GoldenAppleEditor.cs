﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(GoldenApple))]
    public class GoldenAppleEditor : ExplosiveFruitEditor
    {
        public override Transform SetProperties()
        {
            Transform main = base.SetProperties();

            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");
            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Basic Status")
                , basicStatusProp.objectReferenceValue, typeof(BasicStatus_GoldenApple), false);

            return main;
        }
    }
}