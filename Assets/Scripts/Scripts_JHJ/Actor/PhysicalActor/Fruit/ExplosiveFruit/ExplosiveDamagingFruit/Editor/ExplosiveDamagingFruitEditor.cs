﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(ExplosiveDamagingFruit))]
    public class ExplosiveDamagingFruitEditor : ExplosiveFruitEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");
            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Basic Status", "열매 기본 스테이터스")
                , basicStatusProp.objectReferenceValue, typeof(BasicStatus_ExplosiveDamagingFruit), false);
            serializedObject.ApplyModifiedProperties();

            return mainTransform;
        }
    }
}