﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Status_ExplosiveDamagingFruit : Status_ExplosiveFruit
    {
        private BasicStatus_ExplosiveDamagingFruit mBasicStatus;
        public new BasicStatus_ExplosiveDamagingFruit BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }

        public EDamageType DamageType { get { return BasicStatus.DamageType; } }
        public int Damage { get { return BasicStatus.Damages[Level - 1]; } }

        public float Force { get { return BasicStatus.Forces[Level - 1]; } }

        public float ShakeTime { get { return BasicStatus.ShakeTime; } }
        public float ShakePower { get { return BasicStatus.ShakePower; } }
    }
}