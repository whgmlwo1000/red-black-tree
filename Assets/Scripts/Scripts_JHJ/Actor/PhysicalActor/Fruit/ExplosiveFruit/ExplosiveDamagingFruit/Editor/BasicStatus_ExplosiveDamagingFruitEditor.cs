﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_ExplosiveDamagingFruit))]
    public class BasicStatus_ExplosiveDamagingFruitEditor : BasicStatus_ExplosiveFruitEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            BasicStatus_ExplosiveDamagingFruit basicStatus = target as BasicStatus_ExplosiveDamagingFruit;
            
            SerializedProperty damageTypeProp = serializedObject.FindProperty("DamageType");
            SerializedProperty damagesProp = serializedObject.FindProperty("Damages");
            SerializedProperty forcesProp = serializedObject.FindProperty("Forces");
            SerializedProperty shakeTimeProp = serializedObject.FindProperty("ShakeTime");
            SerializedProperty shakePowerProp = serializedObject.FindProperty("ShakePower");

            if(damagesProp.arraySize != basicStatus.MaxLevel || forcesProp.arraySize != basicStatus.MaxLevel)
            {
                damagesProp.arraySize = basicStatus.MaxLevel;
                forcesProp.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Damage Type", "데미지의 타입"), GUILayout.MaxWidth(100));
            EDamageType damageType = (EDamageType)damageTypeProp.intValue;
            damageType = (EDamageType)EditorGUILayout.EnumPopup(damageType, GUILayout.MaxWidth(100));
            damageTypeProp.intValue = (int)damageType;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            for(int indexOfDamage = 0; indexOfDamage < basicStatus.MaxLevel; indexOfDamage++)
            {
                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}", indexOfDamage + 1)), EditorStyles.boldLabel);

                EditorGUILayout.BeginHorizontal();

                SerializedProperty damageProp = damagesProp.GetArrayElementAtIndex(indexOfDamage);
                SerializedProperty forceProp = forcesProp.GetArrayElementAtIndex(indexOfDamage);

                EditorGUILayout.LabelField(new GUIContent("Damage", "데미지 수치"), GUILayout.MaxWidth(100));
                damageProp.intValue = Mathf.Max(EditorGUILayout.IntField(damageProp.intValue, GUILayout.MaxWidth(50)), 0);

                EditorGUILayout.LabelField(new GUIContent("Force", "피격당하는 대상이 밀려나는 정도의 수치"), GUILayout.MaxWidth(100));
                forceProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(forceProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);

                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Shake Time", "피격시 진동 시간"), GUILayout.MaxWidth(100));
            shakeTimeProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(shakeTimeProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);
            EditorGUILayout.LabelField(new GUIContent("Shake Power", "피격시 진동 세기"), GUILayout.MaxWidth(100));
            shakePowerProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(shakePowerProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);
            EditorGUILayout.EndHorizontal();
            serializedObject.ApplyModifiedProperties();
        }
    }
}