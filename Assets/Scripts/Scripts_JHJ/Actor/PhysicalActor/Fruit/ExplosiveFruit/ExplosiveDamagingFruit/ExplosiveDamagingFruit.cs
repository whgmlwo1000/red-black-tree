﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class ExplosiveDamagingFruit : ExplosiveFruit
    {

        [SerializeField]
        private BasicStatus_ExplosiveDamagingFruit mBasicStatus = null;

        private Status_ExplosiveDamagingFruit mExplosiveDamagingFruitStatus;
        public new Status_ExplosiveDamagingFruit Status
        {
            get { return mExplosiveDamagingFruitStatus; }
            protected set { base.Status = mExplosiveDamagingFruitStatus = value; }
        }

        public override void Awake()
        {
            base.Awake();

            Status = ScriptableObject.CreateInstance<Status_ExplosiveDamagingFruit>();
            Status.BasicStatus = mBasicStatus;
        }

        public override bool Detect()
        {
            Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Fruit.Detect, out int count);
            for (int indexOfCollider = 0; indexOfCollider < count; indexOfCollider++)
            {
                IDamagedActor actor = colliders[indexOfCollider].GetComponentInParent<IDamagedActor>();
                if (actor != null && actor.CanDamageShield)
                {
                    return true;
                }
            }
            return false;
        }

        protected override void OnActivate()
        {
            base.OnActivate();

            PlayExplodeAudio();
        }

        private void Damage_Animation()
        {
            Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Fruit.Affect, out int count);
            for(int indexOfCollder = 0; indexOfCollder < count; indexOfCollder++)
            {
                IDamaged damaged = colliders[indexOfCollder].GetComponentInParent<IDamaged>();
                if (damaged != null)
                {
                    damaged.Damage.Set(isImportant: true
                        , type: Status.DamageType
                        , value: Status.Damage
                        , srcForce: Status.Force
                        , isCritical: false
                        , srcActor: this
                        , srcPosition: CenterTransform.position);
                    if(damaged.InvokeDamage())
                    {
                        CameraController.Main.Shake(Status.ShakeTime, Status.ShakePower);
                        Vibration.Vibrate((long)(Status.ShakeTime * 1000f));
                    }
                }
            }
        }

        private void PlayExplodeAudio()
        {
            AudioHandler.Get(EAudioType_Fruit.Dealing_Explode).AudioSource.Play();
        }
    }
}