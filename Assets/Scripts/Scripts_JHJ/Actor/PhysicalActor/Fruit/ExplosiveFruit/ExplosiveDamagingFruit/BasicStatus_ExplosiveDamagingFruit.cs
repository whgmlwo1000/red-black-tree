﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_ExplosiveDamagingFruit", menuName = "Fruit/Explosive Damaging Fruit Basic Status", order = 1000)]
    public class BasicStatus_ExplosiveDamagingFruit : BasicStatus_ExplosiveFruit
    {
        public EDamageType DamageType;
        public int[] Damages;
        public float[] Forces;

        public float ShakePower;
        public float ShakeTime;
    }
}