﻿using UnityEngine;

namespace RedBlackTree
{
    public class ExplosiveHealingFruit : ExplosiveFruit
    {
        [SerializeField]
        private BasicStatus_ExplosiveHealingFruit mBasicStatus = null;

        private Status_ExplosiveHealingFruit mExplosiveHealingFruitStatus;
        public new Status_ExplosiveHealingFruit Status
        {
            get { return mExplosiveHealingFruitStatus; }
            protected set { base.Status = mExplosiveHealingFruitStatus = value; }
        }

        public override void Awake()
        {
            base.Awake();

            Status = ScriptableObject.CreateInstance<Status_ExplosiveHealingFruit>();
            Status.BasicStatus = mBasicStatus;
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            PlayRecoverAudio();
        }

        public void Heal_Animation()
        {
            Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Fruit.Affect, out int count);
            for(int indexOfCollider = 0; indexOfCollider < count; indexOfCollider++)
            {
                IDamaged damaged = colliders[indexOfCollider].GetComponentInParent<IDamaged>();
                if(damaged != null)
                {
                    damaged.Heal.Set(true, EDamageType.Life, Status.Heal, Status.HealRate);
                    damaged.InvokeHeal();
                }
            }
        }

        private void PlayRecoverAudio()
        {
            AudioHandler.Get(EAudioType_Fruit.Supporting_Recover).AudioSource.Play();
        }
    }
}