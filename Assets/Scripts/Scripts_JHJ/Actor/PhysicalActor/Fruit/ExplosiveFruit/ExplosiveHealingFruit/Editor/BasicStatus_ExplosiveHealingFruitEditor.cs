﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_ExplosiveHealingFruit))]
    public class BasicStatus_ExplosiveHealingFruitEditor : BasicStatus_ExplosiveFruitEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();
            BasicStatus_ExplosiveHealingFruit basicStatus = target as BasicStatus_ExplosiveHealingFruit;

            SerializedProperty healProps = serializedObject.FindProperty("Heals");
            SerializedProperty healRateProps = serializedObject.FindProperty("HealRates");

            if (healProps.arraySize != basicStatus.MaxLevel || healRateProps.arraySize != basicStatus.MaxLevel)
            {
                healProps.arraySize = basicStatus.MaxLevel;
                healRateProps.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }

            EditorGUILayout.Space();
            for (int indexOfHeal = 0; indexOfHeal < basicStatus.MaxLevel; indexOfHeal++)
            {
                SerializedProperty healValueProp = healProps.GetArrayElementAtIndex(indexOfHeal);
                SerializedProperty healRateProp = healRateProps.GetArrayElementAtIndex(indexOfHeal);

                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}", indexOfHeal + 1)), EditorStyles.boldLabel);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent("Heal", "치유량"), GUILayout.MaxWidth(50));
                healValueProp.intValue = Mathf.Max(EditorGUILayout.IntField(healValueProp.intValue, GUILayout.MaxWidth(50)), 0);
                EditorGUILayout.LabelField(new GUIContent("Heal Rate", "최대 체력 대비 치유율"), GUILayout.MaxWidth(70));
                healRateProp.floatValue = Mathf.Clamp01(EditorGUILayout.FloatField(healRateProp.floatValue, GUILayout.MaxWidth(50)));
                EditorGUILayout.EndHorizontal();
            }
            
            serializedObject.ApplyModifiedProperties();
        }
    }
}