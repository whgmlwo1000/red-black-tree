﻿using UnityEngine;

namespace RedBlackTree
{
    public class Status_ExplosiveHealingFruit : Status_ExplosiveFruit
    {
        private BasicStatus_ExplosiveHealingFruit mBasicStatus;
        public new BasicStatus_ExplosiveHealingFruit BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }

        public int Heal { get { return BasicStatus.Heals[Level - 1]; } }

        public float HealRate { get { return BasicStatus.HealRates[Level - 1]; } }
    }
}