﻿using UnityEngine;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_ExplosiveHealingFruit", menuName = "Fruit/Explosive Healing Fruit Basic Status", order = 1000)]
    public class BasicStatus_ExplosiveHealingFruit : BasicStatus_ExplosiveFruit
    {
        public int[] Heals;
        public float[] HealRates;
    }
}