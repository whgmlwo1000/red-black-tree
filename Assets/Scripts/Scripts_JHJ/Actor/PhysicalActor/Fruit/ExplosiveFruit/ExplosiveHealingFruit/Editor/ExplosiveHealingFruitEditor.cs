﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(ExplosiveHealingFruit))]
    public class ExplosiveHealingFruitEditor : ExplosiveFruitEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");
            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Basic Status", "폭발하는 치유 열매 정적 수치 스테이터스"), basicStatusProp.objectReferenceValue, typeof(BasicStatus_ExplosiveHealingFruit), false);

            serializedObject.ApplyModifiedProperties();

            return mainTransform;
        }
    }
}