﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class ExplosiveFruit_Activate : Fruit_Activate
    {
        private ExplosiveFruit mFruit;
        private float mPrevCameraPosition_x;
        public ExplosiveFruit_Activate(ExplosiveFruit fruit) : base(fruit)
        {
            mFruit = fruit;
        }

        public override void Start()
        {
            base.Start();
            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();
            mPrevCameraPosition_x = mFruit.FollowCamera(mPrevCameraPosition_x);
        }
    }
}