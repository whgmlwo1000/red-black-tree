﻿using UnityEngine;

namespace RedBlackTree
{
    public class Status_ExplosiveFruit : Status_Fruit
    {
        private BasicStatus_ExplosiveFruit mBasicStatus;
        public new BasicStatus_ExplosiveFruit BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }
    }
}