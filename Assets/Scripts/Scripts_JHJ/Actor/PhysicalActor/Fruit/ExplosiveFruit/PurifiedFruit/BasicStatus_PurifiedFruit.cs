﻿using UnityEngine;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_PurifiedFruit", menuName = "Fruit/Purified Fruit Basic Status", order = 1000)]
    public class BasicStatus_PurifiedFruit : BasicStatus_ExplosiveFruit
    {

    }
}