﻿using UnityEngine;

namespace RedBlackTree
{
    public class Status_PurifiedFruit : Status_ExplosiveFruit
    {
        private BasicStatus_PurifiedFruit mBasicStatus;
        public new BasicStatus_PurifiedFruit BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }
    }
}