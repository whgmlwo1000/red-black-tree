﻿using UnityEngine;

namespace RedBlackTree
{

    public class PurifiedFruit : ExplosiveFruit
    {
        private Status_PurifiedFruit mStatus;
        public new Status_PurifiedFruit Status
        {
            get { return mStatus; }
            protected set
            {
                value.BasicStatus = mBasicStatus;
                base.Status = mStatus = value;
            }
        }

        [SerializeField]
        private BasicStatus_PurifiedFruit mBasicStatus = null;

        public override void Awake()
        {
            base.Awake();

            Status = ScriptableObject.CreateInstance<Status_PurifiedFruit>();
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            PlayEnchantAudio();
        }

        public override bool Detect()
        {
            Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Fruit.Detect, out int count);
            for (int indexOfCollider = 0; indexOfCollider < count; indexOfCollider++)
            {
                IDamagedActor actor = colliders[indexOfCollider].GetComponentInParent<IDamagedActor>();
                if (actor != null && actor.CanDamageShield)
                {
                    return true;
                }
            }
            return false;
        }

        private void Purify_Animation()
        {
            if (StateManager.Scene.State != EScene.Scene_Sanctuary)
            {
                Monster.MonsterList.ForEach(delegate (Monster monster)
                {
                    monster.Damage.Set(true
                        , EDamageType.Shield
                        , monster.Status.Shield
                        , 0.0f
                        , true
                        , this
                        , CenterTransform.position);
                    monster.InvokeDamage();
                });
            }
            else
            {
                Scarecrow.Main.Damage.Set(true
                    , EDamageType.Shield
                    , Scarecrow.Main.Status.MaxShield
                    , 0.0f
                    , true
                    , this
                    , CenterTransform.position);
                Scarecrow.Main.InvokeDamage();
            }
        }

        private void PlayEnchantAudio()
        {
            AudioHandler.Get(EAudioType_Fruit.Dealing_Enchant).AudioSource.Play();
        }
    }
}