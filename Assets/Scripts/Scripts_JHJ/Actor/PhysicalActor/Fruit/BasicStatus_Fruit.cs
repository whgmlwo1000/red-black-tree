﻿using UnityEngine;

namespace RedBlackTree
{
    public class BasicStatus_Fruit : ScriptableObject
    {
        public float Weight;

        public int MaxLevel;
        public float[] Weights_Level;
    }
}