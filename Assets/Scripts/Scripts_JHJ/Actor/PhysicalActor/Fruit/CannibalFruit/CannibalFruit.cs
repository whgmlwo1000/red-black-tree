﻿using UnityEngine;

namespace RedBlackTree
{
    public class CannibalFruit : Fruit
    {
        private Status_CannibalFruit mStatus;
        public new Status_CannibalFruit Status
        {
            get { return mStatus; }
            protected set
            {
                value.BasicStatus = mBasicStatus;
                base.Status = mStatus = value;
            }
        }

        [SerializeField]
        private BasicStatus_CannibalFruit mBasicStatus = null;

        public IDamagedActor Target { get; private set; }
        public bool IsReady { get; private set; }

        public override void Awake()
        {
            base.Awake();

            Status = ScriptableObject.CreateInstance<Status_CannibalFruit>();

            StateManager.Pause.AddEnterEvent(true, OnPauseEnter);
            StateManager.Pause.AddExitEvent(true, OnPauseExit);

            SetState(new CannibalFruit_Activate(this));
        }

        public override bool Detect()
        {
            Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Fruit.Detect, out int count);
            for (int indexOfCollider = 0; indexOfCollider < count; indexOfCollider++)
            {
                IDamagedActor actor = colliders[indexOfCollider].GetComponentInParent<IDamagedActor>();
                if (actor != null && actor.CanDamageShield)
                {
                    Target = actor;
                    return true;
                }
            }
            return false;
        }

        protected override void OnThrow(Vector2 force)
        {
            base.OnThrow(force);
            HorizontalDirection = EHorizontalDirection.Right;
            MainAnimator.SetBool("reveal", false);
        }

        protected override void OnActivate()
        {
            base.OnActivate();
            IsReady = true;
            MainAnimator.SetBool("ready", true);
            MainRigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
        }

        private void CompleteToReady_Animation()
        {
            IsReady = false;
            MainAnimator.SetBool("ready", false);

            transform.position = Target.CenterTransform.position;
        }

        private void CompleteToReveal_Animation()
        {
            MainAnimator.SetBool("reveal", true);
        }

        private void OnPauseEnter()
        {
            if(gameObject.activeInHierarchy && State == EStateType_Fruit.Activate)
            {
                AudioHandler.Get(EAudioType_Fruit.Dealing_MeleeAttack).AudioSource.Stop();
            }
        }

        private void OnPauseExit()
        {
            if (gameObject.activeInHierarchy && State == EStateType_Fruit.Activate)
            {
                AudioHandler.Get(EAudioType_Fruit.Dealing_MeleeAttack).AudioSource.Play();
            }
        }
    }
}