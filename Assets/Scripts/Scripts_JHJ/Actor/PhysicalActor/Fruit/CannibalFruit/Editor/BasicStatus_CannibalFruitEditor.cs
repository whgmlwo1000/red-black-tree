﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_CannibalFruit))]
    public class BasicStatus_CannibalFruitEditor : BasicStatus_FruitEditor
    {
        
        public override void SetProperties()
        {
            base.SetProperties();

            BasicStatus_CannibalFruit basicStatus = target as BasicStatus_CannibalFruit;

            SerializedProperty activeTimeProp = serializedObject.FindProperty("ActiveTime");
            SerializedProperty damageProps = serializedObject.FindProperty("Damages");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Active Time"), GUILayout.MaxWidth(100));
            activeTimeProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(activeTimeProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);
            EditorGUILayout.EndHorizontal();

            if(damageProps.arraySize != basicStatus.MaxLevel)
            {
                damageProps.arraySize = basicStatus.MaxLevel;
                serializedObject.ApplyModifiedProperties();
            }

            for(int indexOfProp = 0; indexOfProp < damageProps.arraySize; indexOfProp++)
            {
                SerializedProperty damageProp = damageProps.GetArrayElementAtIndex(indexOfProp);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent(string.Format("Level {0}. Damage", indexOfProp + 1)), GUILayout.MaxWidth(150));
                damageProp.intValue = Mathf.Max(EditorGUILayout.IntField(damageProp.intValue, GUILayout.MaxWidth(50)), 0);
                EditorGUILayout.EndHorizontal();
            }
        }
    }
}