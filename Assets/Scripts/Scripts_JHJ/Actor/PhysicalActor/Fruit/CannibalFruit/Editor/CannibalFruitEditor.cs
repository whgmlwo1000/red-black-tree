﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(CannibalFruit))]
    public class CannibalFruitEditor : FruitEditor
    {
        public override Transform SetProperties()
        {
            Transform main = base.SetProperties();

            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");

            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Basic Status"), basicStatusProp.objectReferenceValue, typeof(BasicStatus_CannibalFruit), false);

            return main;
        }
    }
}