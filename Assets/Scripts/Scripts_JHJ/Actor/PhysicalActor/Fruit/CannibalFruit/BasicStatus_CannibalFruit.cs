﻿using UnityEngine;
using System.Collections;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_CannibalFruit", menuName = "Fruit/Cannibal Fruit Basic Status", order = 1000)]
    public class BasicStatus_CannibalFruit : BasicStatus_Fruit
    {
        public float ActiveTime;
        public int[] Damages;
    }
}