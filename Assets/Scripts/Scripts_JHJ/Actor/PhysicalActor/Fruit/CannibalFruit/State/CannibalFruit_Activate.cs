﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class CannibalFruit_Activate : Fruit_Activate
    {
        private CannibalFruit mFruit;
        private float mRemainTime;
        private float mRemainTime_Attack;
        private float mPrevCameraPosition_x;

        public CannibalFruit_Activate(CannibalFruit fruit) : base(fruit)
        {
            mFruit = fruit;
        }

        public override void Start()
        {
            base.Start();

            mFruit.AudioHandler.Get(EAudioType_Fruit.Dealing_MeleeAttack).AudioSource.Play();

            mRemainTime = mFruit.Status.ActiveTime;
            mRemainTime_Attack = ValueConstants.DotDamageTerm;
            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }

        public override void End()
        {
            mFruit.AudioHandler.Get(EAudioType_Fruit.Dealing_MeleeAttack).AudioSource.Stop();
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();

            if(mFruit.IsReady)
            {
                mPrevCameraPosition_x = mFruit.FollowCamera(mPrevCameraPosition_x);
            }
            else
            {
                mFruit.transform.position = mFruit.Target.CenterTransform.position;
            }
        }

        public override void Update()
        {
            base.Update();

            if (mFruit.Target.Status.Shield == 0)
            {
                mFruit.Contract();
            }
            else if (!mFruit.IsReady)
            {
                if (mRemainTime > 0.0f)
                {
                    mRemainTime -= Time.deltaTime;
                    if (mRemainTime_Attack > 0.0f)
                    {
                        mRemainTime_Attack -= Time.deltaTime;
                    }
                    else
                    {
                        mFruit.Target.Damage.Set(true
                            , EDamageType.Shield
                            , mFruit.Status.Damage
                            , 0.0f
                            , false
                            , mFruit
                            , mFruit.CenterTransform.position);
                        mFruit.Target.InvokeDamage();
                        mRemainTime_Attack = ValueConstants.DotDamageTerm;
                    }
                }
                else
                {
                    mFruit.Contract();
                }
            }
        }
    }
}