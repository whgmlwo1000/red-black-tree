﻿using UnityEngine;

namespace RedBlackTree
{
    public class Status_CannibalFruit : Status_Fruit
    {
        private BasicStatus_CannibalFruit mBasicStatus;
        public new BasicStatus_CannibalFruit BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }

        public int Damage { get { return BasicStatus.Damages[Level - 1]; } }
        public float ActiveTime { get { return BasicStatus.ActiveTime; } }
    }
}