﻿using UnityEngine;
using ObjectManagement;
using UnityEngine.SceneManagement;
using CameraManagement;

namespace RedBlackTree
{
    public enum EAcquisition
    {
        None = -1,

        Souls,
        DemonSouls,

        _PassiveItem_ = 100,
        /// <summary>
        /// 풍요의 씨앗
        /// </summary>
        AffluenceSeed,

        /// <summary>
        /// 불꽃의 요정
        /// </summary>
        FlameFairy,

        /// <summary>
        /// 빛의 요정
        /// </summary>
        LightFairy,

        /// <summary>
        /// 티르의 검
        /// </summary>
        TyrSword,

        /// <summary>
        /// 티르의 방패
        /// </summary>
        TyrShield,

        /// <summary>
        /// 정화의 부적
        /// </summary>
        PurificationAmulet,

        /// <summary>
        /// 퇴마의 부적
        /// </summary>
        ExorcismAmulet,

        /// <summary>
        /// 엘릭서
        /// </summary>
        Elixir,

        /// <summary>
        /// 프리가의 손거울
        /// </summary>
        FrigarHandMirror,

        /// <summary>
        /// 탐욕의 목걸이
        /// </summary>
        GreedNecklace,
    }

    public enum EStateType_Acquisition
    {
        Prepare = 0,
        Start = 1,
        End = 2,
    }

    public enum ETriggerType_Acquisition
    {
        Acquired,
    }

    public enum EAudioType_Acquisition
    {

    }

    [RequireComponent(typeof(TriggerHandler_Acquisition), typeof(AudioHandler_Acquisition))]
    public class Acquisition : APhysicalActor<EStateType_Acquisition>, IPoolable<EAcquisition>
    {
        public override EActorType ActorType { get { return EActorType.Acquisition; } }

        [SerializeField]
        private EAcquisition mType = EAcquisition.None;
        public EAcquisition Type { get { return mType; } }

        public bool IsPooled { get { return !gameObject.activeSelf; } }

        public TriggerHandler_Acquisition TriggerHandler { get; private set; }
        public AudioHandler_Acquisition AudioHandler { get; private set; }

        public bool IsWorldCanvas { get { return false; } }

        public event System.Action OnActivate;

        private Vector2 mPrevCameraPosition;

        public override void Awake()
        {
            base.Awake();

            TriggerHandler = GetComponent<TriggerHandler_Acquisition>();
            AudioHandler = GetComponent<AudioHandler_Acquisition>();

            StateManager.AddOnExitAbyss(OnActiveSceneChanged);

            SetStates(new Acquisition_Prepare(this)
                , new Acquisition_Start(this)
                , new Acquisition_End(this));
        }

        public override void Start()
        {
            base.Start();

            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();

            if(!StateManager.Pause.State)
            {
                Vector2 curPosition = MainRigidbody.position;
                Vector2 cameraPosition = CameraController.Main.Position;
                MainRigidbody.position = curPosition + (cameraPosition - mPrevCameraPosition);
                mPrevCameraPosition = cameraPosition;
            }
        }

        public void MoveToBag()
        {
            Vector2 curPosition = MainRigidbody.position;
            Transform destination = InventoryBag.Destination;
            Vector2 dstPosition = transform.position;
            if (destination != null)
            {
                dstPosition = InventoryBag.Destination.position;
            }

            Vector2 direction = dstPosition - curPosition;
            direction.Normalize();
            Velocity *= 0.9f;
            AddVelocity(ValueConstants.MaxSpeedOfAcquisition, direction * ValueConstants.MaxSpeedOfAcquisition * Time.fixedDeltaTime * 5.0f);
        }

        public void Activate(Vector2 position)
        {
            mPrevCameraPosition = CameraController.Main.Position;
            transform.position = position;
            gameObject.SetActive(true);
            Velocity = Vector2.zero;
            State = EStateType_Acquisition.Prepare;
            OnActivate?.Invoke();
        }

        private void Start_Animation()
        {
            State = EStateType_Acquisition.Start;
        }

        protected virtual void End_Animation()
        {
            gameObject.SetActive(false);
        }

        private void OnActiveSceneChanged()
        {
            gameObject.SetActive(false);
        }
        private void OnActiveSceneChanged(Scene src, Scene dst)
        {
            gameObject.SetActive(false);
        }
    }
}