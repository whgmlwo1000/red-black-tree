﻿using UnityEngine;

namespace RedBlackTree
{
    public enum EAudioType_InventoryBag
    {
        None = -1, 

        Item_0 = 0, 
        Item_1, 
        Item_End, 

        Souls_0 = 10, 
        Souls_End
    }

    [RequireComponent(typeof(Animator), typeof(AudioHandler_InventoryBag))]
    public class InventoryBag : MonoBehaviour
    {
        private static InventoryBag mMain = null;

        public static Transform Destination
        {
            get
            {
                if(mMain != null)
                {
                    return mMain.mCenterTransform;
                }

                return null;
            }
        }

        public static Inventory Inventory
        {
            get
            {
                if (mMain != null)
                {
                    return mMain.mInventory;
                }
                else if (Fairy.Main != null)
                {
                    return Fairy.Main.Inventory;
                }
                return null;
            }
        }

        public AudioHandler_InventoryBag AudioHandler { get; private set; }

        public static void PushItem()
        {
            mMain.AudioHandler.Get((EAudioType_InventoryBag)Random.Range((int)EAudioType_InventoryBag.Item_0
                , (int)EAudioType_InventoryBag.Item_End)).AudioSource.Play();
            PlayAnimation();
        }

        public static void PushSouls()
        {
            mMain.AudioHandler.Get((EAudioType_InventoryBag)Random.Range((int)EAudioType_InventoryBag.Souls_0
                , (int)EAudioType_InventoryBag.Souls_End)).AudioSource.Play();
            PlayAnimation();
        }

        private static void PlayAnimation()
        {
            mMain.mAnimator.SetTrigger("push");
        }

        [SerializeField]
        private Inventory mInventory = null;

        [SerializeField]
        private Transform mCenterTransform = null;

        private Animator mAnimator;

        public void Awake()
        {
            mMain = this;
            mAnimator = GetComponent<Animator>();
            AudioHandler = GetComponent<AudioHandler_InventoryBag>();
        }

        public void OnDestroy()
        {
            mMain = null;
        }
    }
}