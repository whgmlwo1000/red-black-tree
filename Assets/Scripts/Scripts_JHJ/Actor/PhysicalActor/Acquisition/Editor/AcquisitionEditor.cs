﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Acquisition))]
    public class AcquisitionEditor : APhysicalActorEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            SerializedProperty typeProp = serializedObject.FindProperty("mType");

            EAcquisition type = (EAcquisition)typeProp.intValue;
            type = (EAcquisition)EditorGUILayout.EnumPopup(new GUIContent("Acquisition Type", "획득물 고유 타입"), type);
            typeProp.intValue = (int)type;

            return mainTransform;
        }
    }
}