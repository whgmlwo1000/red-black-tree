﻿using UnityEngine;
using UnityEditor;
using ObjectManagement;

namespace RedBlackTree
{
    [CustomEditor(typeof(AcquisitionPool))]
    public class AcquisitionPoolEditor : PoolEditor<EAcquisition>
    {

    }
}