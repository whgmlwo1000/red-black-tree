﻿using UnityEngine;

namespace RedBlackTree
{
    public class Acquisition_Souls : Acquisition
    {
        private int mSouls;

        public override void Awake()
        {
            base.Awake();

            OnActivate += OnSoulsAcquired;
        }

        public void SetParams(int souls)
        {
            mSouls = souls;
        }

        protected override void End_Animation()
        {
            InventoryBag.PushSouls();
            base.End_Animation();
        }

        private void OnSoulsAcquired()
        {
            Inventory inventory = InventoryBag.Inventory;
            PassiveItem_PurificationAmulet item = inventory.Get(EPassiveItem.PurificationAmulet) as PassiveItem_PurificationAmulet;
            if (item != null && item.IsAcquired)
            {
                inventory.Souls += (int)(mSouls * (1.0f + item.SoulsIncrease));
            }
            else
            {
                inventory.Souls += mSouls;
            }
        }
    }
}