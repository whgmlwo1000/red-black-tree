﻿using UnityEngine;

namespace RedBlackTree
{
    public class Acquisition_Prepare : State<EStateType_Acquisition>
    {
        private Acquisition mAcquisition;
        public Acquisition_Prepare(Acquisition acquisition) : base(EStateType_Acquisition.Prepare)
        {
            mAcquisition = acquisition;
        }

        public override void Start()
        {
            mAcquisition.MainAnimator.SetInteger("state", (int)Type);
        }
    }
}