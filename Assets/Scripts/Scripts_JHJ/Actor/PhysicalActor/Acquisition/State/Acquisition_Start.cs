﻿using UnityEngine;

namespace RedBlackTree
{
    public class Acquisition_Start : State<EStateType_Acquisition>
    {
        private Acquisition mAcquisition;
        public Acquisition_Start(Acquisition acquisition) : base(EStateType_Acquisition.Start)
        {
            mAcquisition = acquisition;
        }

        public override void Start()
        {
            mAcquisition.MainAnimator.SetInteger("state", (int)Type);
        }

        public override void FixedUpdate()
        {
            Collider2D[] colliders = mAcquisition.TriggerHandler.GetColliders(ETriggerType_Acquisition.Acquired, out int count);
            for (int indexOfCollider = 0; indexOfCollider < count; indexOfCollider++)
            {
                InventoryBag destination = colliders[indexOfCollider].GetComponentInParent<InventoryBag>();
                if (destination != null)
                {
                    mAcquisition.State = EStateType_Acquisition.End;
                    return;
                }
            }

            mAcquisition.MoveToBag();
        }
    }
}