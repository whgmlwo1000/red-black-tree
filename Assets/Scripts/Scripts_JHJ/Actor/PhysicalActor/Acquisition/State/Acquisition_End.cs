﻿using UnityEngine;

namespace RedBlackTree
{
    public class Acquisition_End : State<EStateType_Acquisition>
    {
        private Acquisition mAcquisition;
        public Acquisition_End(Acquisition acquisition) : base(EStateType_Acquisition.End)
        {
            mAcquisition = acquisition;
        }

        public override void Start()
        {
            mAcquisition.MainAnimator.SetInteger("state", (int)Type);
            mAcquisition.Velocity = Vector2.zero;
        }
    }
}