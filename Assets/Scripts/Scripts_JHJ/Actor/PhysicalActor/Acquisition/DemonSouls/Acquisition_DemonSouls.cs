﻿using UnityEngine;

namespace RedBlackTree
{
    public class Acquisition_DemonSouls : Acquisition
    {
        public override void Awake()
        {
            base.Awake();

            OnActivate += OnDemonSoulsAcquired;
        }

        protected override void End_Animation()
        {
            InventoryBag.PushSouls();
            base.End_Animation();
        }

        private void OnDemonSoulsAcquired()
        {
            Inventory inventory = InventoryBag.Inventory;
            PassiveItem_ExorcismAmulet item = inventory.Get(EPassiveItem.ExorcismAmulet) as PassiveItem_ExorcismAmulet;
            if (item != null && item.IsAcquired)
            {
                float selection = Random.Range(0.0f, 1.0f);
                if(selection < item.DemonSoulsAdditionPossibility)
                {
                    inventory.DemonSouls += 2;
                    return;
                }
            }
            inventory.DemonSouls += 1;
        }
    }
}