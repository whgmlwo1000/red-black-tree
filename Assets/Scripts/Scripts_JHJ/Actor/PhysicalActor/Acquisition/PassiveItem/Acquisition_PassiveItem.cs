﻿using UnityEngine;

namespace RedBlackTree
{
    public class Acquisition_PassiveItem : Acquisition
    {
        [SerializeField]
        private EPassiveItem mPassiveItem = EPassiveItem.None;

        public override void Awake()
        {
            base.Awake();

            OnActivate += OnPassiveItemAcquired;
        }

        protected override void End_Animation()
        {
            InventoryBag.PushItem();
            base.End_Animation();
        }

        private void OnPassiveItemAcquired()
        {
            InventoryBag.Inventory.Add(mPassiveItem, 1);
        }
    }
}