﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Acquisition_PassiveItem))]
    public class Acquisition_PassiveItemEditor : AcquisitionEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            SerializedProperty passiveItemProp = serializedObject.FindProperty("mPassiveItem");

            EPassiveItem passiveItem = (EPassiveItem)passiveItemProp.intValue;
            passiveItem = (EPassiveItem)EditorGUILayout.EnumPopup(new GUIContent("Passive Item Type", "패시브 아이템 고유 타입"), passiveItem);
            passiveItemProp.intValue = (int)passiveItem;

            serializedObject.ApplyModifiedProperties();

            return mainTransform;
        }
    }
}