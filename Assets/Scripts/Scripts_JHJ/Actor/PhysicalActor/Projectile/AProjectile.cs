﻿using UnityEngine;
using UnityEngine.SceneManagement;
using ObjectManagement;
using CameraManagement;

namespace RedBlackTree
{
    public enum EProjectile
    {
        None = -1,
        /// <summary>
        /// 요정 빔
        /// </summary>
        FairyBeam = 0,

        /// <summary>
        /// 일반 몬스터가 발사하는 투사체
        /// </summary>
        MonsterNormalProjectile = 1, 

        /// <summary>
        /// 화염 정령이 쏘는 투사체
        /// </summary>
        FlameFairy = 7, 

        /// <summary>
        /// 빛의 정령이 쏘는 투사체
        /// </summary>
        LightFairy = 8, 


        Count
    }

    public interface IProjectile : IPhysicalActor, IPoolable<EProjectile>
    {
        bool IsActivated { get; }
        bool CanActivate { get; }
        bool CanDeactivate { get; }
        bool CanReflect { get; }

        void Activate(Transform parent, Vector2 position);
        void Deactivate();
        void ShutDown();

        void OnReflectBy(IActor actor, float offenseIncrease);

        void OnActivate();
        void OnDeactivate();
        void OnShutDown();
    }


    public abstract class AProjectile<EStateType> : APhysicalActor<EStateType>, IProjectile
        where EStateType : System.Enum
    {
        [SerializeField]
        private EProjectile mType = EProjectile.None;
        public EProjectile Type { get { return mType; } }

        public sealed override EActorType ActorType { get { return EActorType.Projectile; } }

        public virtual bool IsPooled { get { return !gameObject.activeSelf; } }

        public bool IsActivated { get; private set; }

        public virtual bool CanActivate { get { return IsPooled; } }
        public virtual bool CanDeactivate { get { return IsActivated; } }
        public virtual bool CanReflect { get { return false; } }

        public bool IsWorldCanvas { get { return false; } }

        public Transform Parent { get; private set; }
        private bool mIsCameraParent;
        private Vector2 mPrevParentPosition;
        private float mPrevCameraPosition_x;
        private float mTimeTaken;

        public override void Awake()
        {
            base.Awake();

            StateManager.AddOnExitAbyss(OnActiveSceneChanged);
        }

        public override void Start()
        {
            base.Start();

            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        public override void Update()
        {
            base.Update();

            if(!StateManager.Pause.State)
            {
                if(mTimeTaken < ValueConstants.ProjectileTime)
                {
                    mTimeTaken += Time.deltaTime;
                }
                else
                {
                    Deactivate();
                }
            }
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();

            if(!StateManager.Pause.State)
            {
                if (mIsCameraParent)
                {
                    float curCameraPosition_x = CameraController.Main.transform.position.x;
                    float deltaCameraPosition_x = curCameraPosition_x - mPrevCameraPosition_x;
                    mPrevCameraPosition_x = curCameraPosition_x;
                    Vector2 curPosition = MainRigidbody.position;
                    MovePosition(new Vector2(curPosition.x + deltaCameraPosition_x, curPosition.y));
                }
                else
                {
                    Vector2 curParentPosition = Parent.position;
                    Vector2 curPosition = MainRigidbody.position;
                    MovePosition(curPosition + (curParentPosition - mPrevParentPosition));
                    mPrevParentPosition = curParentPosition;
                }
            }
        }

        public void Activate(Transform parent, Vector2 position)
        {
            Parent = parent;
            transform.position = position;

            if (Parent == null)
            {
                mIsCameraParent = true;
                mPrevCameraPosition_x = CameraController.Main.transform.position.x;
            }
            else
            {
                mIsCameraParent = false;
                mPrevParentPosition = Parent.position;
            }

            gameObject.SetActive(true);
            IsActivated = true;
            OnActivate();
        }

        public void Deactivate()
        {
            IsActivated = false;
            OnDeactivate();
        }

        public void ShutDown()
        {
            IsActivated = false;
            OnShutDown();
            gameObject.SetActive(false);
        }

        public virtual void OnActivate() 
        {
            mTimeTaken = 0.0f;
        }
        public virtual void OnDeactivate() { }
        public virtual void OnShutDown() { }

        private void OnActiveSceneChanged()
        {
            ShutDown();
        }

        private void OnActiveSceneChanged(Scene src, Scene dst)
        {
            OnActiveSceneChanged();
        }

        public virtual void OnReflectBy(IActor actor, float offenseIncrease)
        {
        }
    }
}