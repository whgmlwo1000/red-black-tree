﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class AProjectileEditor : APhysicalActorEditor
    {
        public override Transform SetProperties()
        {
            ShowProjectileType();
            EditorGUILayout.Space();
            return base.SetProperties();
        }

        /// <summary>
        /// 투사체의 고유한 타입을 출력하고 설정하는 메소드
        /// </summary>
        public void ShowProjectileType()
        {
            IProjectile projectile = target as IProjectile;
            SerializedProperty projectileTypeProp = serializedObject.FindProperty("mType");
            EProjectile projectileType = projectile.Type;

            projectileType = (EProjectile)EditorGUILayout.EnumPopup(new GUIContent("Projectile Type", "투사체 고유 타입"), projectileType, EditorStyles.boldLabel);
            projectileTypeProp.intValue = (int)projectileType;
            serializedObject.ApplyModifiedProperties();
        }
    }
}