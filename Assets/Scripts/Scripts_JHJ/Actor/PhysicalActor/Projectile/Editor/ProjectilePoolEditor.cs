﻿using UnityEngine;
using UnityEditor;
using ObjectManagement;

namespace RedBlackTree
{
    [CustomEditor(typeof(ProjectilePool))]
    public class ProjectilePoolEditor : PoolEditor<EProjectile>
    {

    }
}