﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Projectile_FairyBeam))]
    [CanEditMultipleObjects]
    public class Projectile_FairyBeamEditor : AProjectileEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            EditorGUILayout.Space();

            SerializedProperty shakeTimeProp = serializedObject.FindProperty("mShakeTime");
            SerializedProperty shakePowerProp = serializedObject.FindProperty("mShakePower");

            shakeTimeProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Camera Shake Time", "피격시 카메라 흔들림 시간"), shakeTimeProp.floatValue);
            shakePowerProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Camera Shake Power", "피격시 카메라 흔들림 강도"), shakePowerProp.floatValue);

            EditorGUILayout.Space();

            SerializedProperty rendererWidthErrorProp = serializedObject.FindProperty("mRendererWidthError");
            SerializedProperty triggerDistanceErrorProp = serializedObject.FindProperty("mTriggerDistanceError");

            rendererWidthErrorProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Renderer Width Error", "실제 목표 지점 거리와 렌더러 가로 길이 오차"), rendererWidthErrorProp.floatValue);
            triggerDistanceErrorProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Trigger Distance Error", "실제 목표 지점 거리와 트리거 거리 오차"), triggerDistanceErrorProp.floatValue);

            serializedObject.ApplyModifiedProperties();

            return mainTransform;
        }
    }
}