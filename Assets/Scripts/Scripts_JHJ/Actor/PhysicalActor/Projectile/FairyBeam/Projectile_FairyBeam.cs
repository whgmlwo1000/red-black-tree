﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public enum EStateType_Projectile_FairyBeam
    {
        None = -1,

        Inactive = 0,
        Active = 1,

        Count
    }

    public enum ETriggerType_Projectile_FairyBeam
    {
        None = -1,

        Attack = 0,

        Count
    }

    public enum EAudioType_Projectile_FairyBeam
    {
        None = -1,

        Count
    }

    [RequireComponent(typeof(TriggerHandler_Projectile_FairyBeam), typeof(AudioHandler_Projectile_FairyBeam), typeof(AttackHandler_Projectile_FairyBeam))]
    public class Projectile_FairyBeam : AProjectile<EStateType_Projectile_FairyBeam>
    {

        public TriggerHandler_Projectile_FairyBeam TriggerHandler { get; private set; }
        public AudioHandler_Projectile_FairyBeam AudioHandler { get; private set; }
        public AttackHandler_Projectile_FairyBeam AttackHandler { get; private set; }

        public int Offense { get; private set; }
        public EDamageType DamageType { get; private set; }
        public IActor SrcActor { get; private set; }


        public float CriticalChance { get; private set; }
        public float CriticalMagnification { get; private set; }

        public Vector2 DstPosition { get; private set; }

        private Collider2D[] mResults;
        private RaycastHit2D[] mRayResults;

        [SerializeField]
        private float mShakePower = 0.05f;
        [SerializeField]
        private float mShakeTime = 0.05f;

        [SerializeField]
        private float mRendererWidthError = 0.0f;

        [SerializeField]
        private float mTriggerDistanceError = 0.0f;

        public override void Awake()
        {
            base.Awake();

            TriggerHandler = GetComponent<TriggerHandler_Projectile_FairyBeam>();
            AudioHandler = GetComponent<AudioHandler_Projectile_FairyBeam>();
            AttackHandler = GetComponent<AttackHandler_Projectile_FairyBeam>();

            mResults = new Collider2D[20];
            mRayResults = new RaycastHit2D[20];

            SetStates(new Projectile_FairyBeam_Inactive(this)
                , new Projectile_FairyBeam_Active(this));
        }

        public void SetParams(EDamageType damageType
            , int offense
            , float criticalChance
            , float criticalMag
            , IActor srcActor
            , Vector2 dstPosition)
        {
            DamageType = damageType;
            Offense = offense;
            CriticalChance = criticalChance;
            CriticalMagnification = criticalMag;
            SrcActor = srcActor;
            DstPosition = dstPosition;
        }

        /// <summary>
        /// 공격 검사 후 적용 메소드
        /// </summary>
        public void AttackOn(ETriggerType_Projectile_FairyBeam triggerType)
        {
            CapsuleCollider2D trigger = TriggerHandler.GetTrigger(triggerType) as CapsuleCollider2D;
            Vector2 srcPosition = AttackHandler.Get(triggerType).transform.position;

            bool isDamaged = false;
            bool isCritical = false;

            Vector2 triggerPosition = trigger.transform.position;
            Vector2 triggerSize = trigger.size;

            // 범위에 존재하는 모든 몬스터 객체에 대해
            int countOfResults = Physics2D.OverlapCapsuleNonAlloc(triggerPosition, triggerSize, trigger.direction, Angle, mResults, (int)(ELayerFlags.Monster | ELayerFlags.Box));

            for (int indexOfResult = 0; indexOfResult < countOfResults; indexOfResult++)
            {
                IDamaged damagable = mResults[indexOfResult].GetComponentInParent<IDamaged>();
                if (damagable != null)
                {
                    float selection = Random.Range(0.0f, 1.0f);
                    if (selection < CriticalChance)
                    {
                        isCritical = true;
                        damagable.Damage.Set(isImportant: true
                            , type: DamageType
                            , value: (int)(Offense * CriticalMagnification)
                            , srcForce: 0.0f
                            , isCritical: true
                            , srcActor: SrcActor
                            , srcPosition: srcPosition);
                    }
                    else
                    {
                        damagable.Damage.Set(isImportant: true
                            , type: DamageType
                            , value: Offense
                            , srcForce: 0.0f
                            , isCritical: false
                            , srcActor: SrcActor
                            , srcPosition: srcPosition);
                    }

                    if (damagable.InvokeDamage())
                    {
                        isDamaged = true;
                    }
                }
            }

            // 데미지를 받은 대상이 존재한다면 카메라를 흔듬
            if (isDamaged)
            {
                if (isCritical)
                {
                    CameraController.Main.Shake(mShakeTime, mShakePower * 5.0f);
                    Vibration.Vibrate((long)(mShakeTime * 1000f));

                    Effect criticalEffect = EffectPool.Get(EEffectType.CriticalFairyBeam);
                    if(criticalEffect != null)
                    {
                        criticalEffect.Activate(CameraController.Main.transform, TriggerHandler.Get(ETriggerType_Projectile_FairyBeam.Attack).transform.position);
                    }
                }
                else
                {
                    CameraController.Main.Shake(mShakeTime, mShakePower);
                }

                Effect effect = EffectPool.Get(EEffectType.FairyBeam);
                if (effect != null)
                {
                    effect.Activate(CameraController.Main.transform, TriggerHandler.Get(ETriggerType_Projectile_FairyBeam.Attack).transform.position);
                }
            }
        }

        public override void OnActivate()
        {
            base.OnActivate();
            Vector2 srcPosition = transform.position;
            // 목표 지점
            Vector2 dstPosition = DstPosition;
            // 방향 벡터
            Vector2 diff = dstPosition - srcPosition;
            // 시작지점부터 목표지점까지의 길이
            float magnitude = diff.magnitude;
            // 투사체 방향 설정
            SetDirection(diff, false);
            int countOfHit = Physics2D.RaycastNonAlloc(srcPosition, diff, mRayResults, magnitude, (int)ELayerFlags.Terrain);
            if (countOfHit > 0)
            {
                magnitude = mRayResults[0].distance;
            }
            // 렌더러의 Width길이 설정
            SpriteRenderer renderer = MainRendererHandler.GetRenderer(0);
            float rendererHeight = renderer.size.y;
            renderer.size = new Vector2(magnitude + mRendererWidthError, rendererHeight);

            // 트리거의 위치를 목표지점으로 설정
            Transform attackTriggerTransform = TriggerHandler.Get(ETriggerType_Projectile_FairyBeam.Attack).transform;
            attackTriggerTransform.localPosition = new Vector3(magnitude + mTriggerDistanceError, 0.0f);

            // Active 상태 전이
            State = EStateType_Projectile_FairyBeam.Active;
        }

        public override void OnDeactivate()
        {
            base.OnDeactivate();

            State = EStateType_Projectile_FairyBeam.Inactive;
        }

        public override void OnShutDown()
        {
            base.OnShutDown();
            State = EStateType_Projectile_FairyBeam.Inactive;
        }
    }
}