﻿using UnityEngine;


namespace RedBlackTree
{
    public class Projectile_FairyBeam_Inactive : State<EStateType_Projectile_FairyBeam>
    {
        private Projectile_FairyBeam mProjectile;
        public Projectile_FairyBeam_Inactive(Projectile_FairyBeam projectile) : base(EStateType_Projectile_FairyBeam.Inactive)
        {
            mProjectile = projectile;
        }

        public override void Start()
        {
            mProjectile.MainAnimator.SetInteger("state", (int)Type);
            mProjectile.gameObject.SetActive(false);
        }
    }
}