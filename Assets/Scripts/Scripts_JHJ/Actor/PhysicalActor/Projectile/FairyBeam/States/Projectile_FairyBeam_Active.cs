﻿using UnityEngine;

namespace RedBlackTree
{
    public class Projectile_FairyBeam_Active : State<EStateType_Projectile_FairyBeam>
    {
        private Projectile_FairyBeam mProjectile;
        public Projectile_FairyBeam_Active(Projectile_FairyBeam projectile) : base(EStateType_Projectile_FairyBeam.Active)
        {
            mProjectile = projectile;
        }

        public override void Start()
        {
            mProjectile.MainAnimator.SetInteger("state", (int)Type);
        }
    }
}