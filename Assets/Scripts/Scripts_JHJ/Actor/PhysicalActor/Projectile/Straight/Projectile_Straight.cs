﻿using UnityEngine;
using System.Collections;

namespace RedBlackTree
{
    public enum EStateType_Projectile_Straight
    {
        None = -1,

        Inactive,
        Active,
        End,

        Count
    }

    public enum ETriggerType_Projectile_Straight
    {
        None = -1,

        DetectGiant,
        AttackGiant,
        DetectMonster,
        AttackMonster,
        ScreenZone, 

        Count
    }

    public enum EAttackType_Projectile_Straight
    {
        Attacker
    }

    public enum EAudioType_Projectile_Straight
    {
        None = -1,

        Count
    }

    [RequireComponent(typeof(TriggerHandler_Projectile_Straight), typeof(AudioHandler_Projectile_Straight), typeof(AttackHandler_Projectile_Straight))]
    public class Projectile_Straight : AProjectile<EStateType_Projectile_Straight>
    {
        public TriggerHandler_Projectile_Straight TriggerHandler { get; private set; }
        public AudioHandler_Projectile_Straight AudioHandler { get; private set; }
        public AttackHandler_Projectile_Straight AttackHandler { get; private set; }

        public override bool CanReflect { get { return SrcActor.ActorType == EActorType.Monster; } }

        /// <summary>
        /// 도착 지점
        /// </summary>
        public Vector2 DstPosition { get; private set; }

        [SerializeField]
        private float mSpeed = 10.0f;
        public float Speed { get { return mSpeed; } }

        public EDamageType DamageType { get; private set; }
        public int Offense { get; private set; }
        public float Force { get; private set; }

        public float CriticalChance { get; private set; }
        public float CriticalMagnification { get; private set; }

        public IActor SrcActor { get; private set; }

        private ETriggerType_Projectile_Straight mDetectType;
        private ETriggerType_Projectile_Straight mAttackType;

        public override void Awake()
        {
            base.Awake();

            TriggerHandler = GetComponent<TriggerHandler_Projectile_Straight>();
            AudioHandler = GetComponent<AudioHandler_Projectile_Straight>();
            AttackHandler = GetComponent<AttackHandler_Projectile_Straight>();

            SetStates(new Projectile_Straight_Inactive(this)
                , new Projectile_Straight_Active(this)
                , new Projectile_Straight_End(this));
        }

        public override void OnActivate()
        {
            base.OnActivate();

            Vector2 srcPosition = transform.position ;
            transform.position = new Vector3(srcPosition.x, srcPosition.y, Random.Range(-1.0f, 1.0f));
            Vector2 dstPosition = DstPosition;
            Vector2 direction = dstPosition - srcPosition;
            direction.Normalize();
            SetDirection(direction, true);
            Velocity = direction * Speed;

            State = EStateType_Projectile_Straight.Active;
        }

        public override void OnDeactivate()
        {
            base.OnDeactivate();
            Velocity = Vector2.zero;
            State = EStateType_Projectile_Straight.End;
        }

        public override void OnShutDown()
        {
            base.OnShutDown();
            State = EStateType_Projectile_Straight.Inactive;
        }

        public override void OnReflectBy(IActor actor, float offenseIncrease)
        {
            SrcActor = actor;
            SetDirection(-Direction, true);
            Velocity = -Velocity;
            Offense = (int)(Offense * offenseIncrease);

            if (SrcActor.ActorType == EActorType.Giant)
            {
                DamageType = EDamageType.Shield;
                mDetectType = ETriggerType_Projectile_Straight.DetectMonster;
                mAttackType = ETriggerType_Projectile_Straight.AttackMonster;
            }
            else
            {
                DamageType = EDamageType.Life;
                mDetectType = ETriggerType_Projectile_Straight.DetectGiant;
                mAttackType = ETriggerType_Projectile_Straight.AttackGiant;
            }
        }

        public void SetParams(IActor srcActor, Vector2 dstPosition, int offense, float force, float criticalChance, float criticalMagnification)
        {
            SrcActor = srcActor;
            DstPosition = dstPosition;
            Offense = offense;
            Force = force;
            CriticalChance = criticalChance;
            CriticalMagnification = criticalMagnification;

            // 물리 레이어 매트릭스에 설정되어 있는 트리거 되는 대상을 변경
            if (SrcActor.ActorType == EActorType.Fairy || SrcActor.ActorType == EActorType.Giant)
            {
                DamageType = EDamageType.Shield;

                mDetectType = ETriggerType_Projectile_Straight.DetectMonster;
                mAttackType = ETriggerType_Projectile_Straight.AttackMonster;
            }
            // 그외는 플레이어를 때리는 요소
            else
            {
                DamageType = EDamageType.Life;
                mDetectType = ETriggerType_Projectile_Straight.DetectGiant;
                mAttackType = ETriggerType_Projectile_Straight.AttackGiant;
            }
        }

        public bool Detect()
        {
            if(TriggerHandler.IsTriggered(ETriggerType_Projectile_Straight.ScreenZone))
            {
                return true;
            }
            else if(mDetectType != ETriggerType_Projectile_Straight.DetectGiant)
            {
                Collider2D[] colliders = TriggerHandler.GetColliders(mDetectType, out int count);

                for (int indexOfCollider = 0; indexOfCollider < count; indexOfCollider++)
                {
                    IDamagedActor damagedActor = colliders[indexOfCollider].GetComponentInParent<IDamagedActor>();
                    if (damagedActor != null
                        && (damagedActor.CanDamageShield))
                    {
                        return true;
                    }
                }
            }
            else
            {
                return TriggerHandler.IsTriggered(mDetectType);
            }
            return false;
        }

        public void Attack_Animation()
        {
            Collider2D[] colliders = TriggerHandler.GetColliders(mAttackType, out int countOfColliders);

            for (int indexOfColliders = 0; indexOfColliders < countOfColliders; indexOfColliders++)
            {
                IDamaged damaged = colliders[indexOfColliders].GetComponentInParent<IDamaged>();
                if (damaged != null)
                {
                    float selection = Random.Range(0.0f, 1.0f);
                    if (selection < CriticalChance)
                    {
                        damaged.Damage.Set(isImportant: true
                            , type: DamageType
                            , value: (int)(Offense * CriticalMagnification)
                            , srcForce: Force
                            , isCritical: true
                            , srcActor: SrcActor
                            , srcPosition: AttackHandler.Get(EAttackType_Projectile_Straight.Attacker).transform.position);
                    }
                    else
                    {
                        damaged.Damage.Set(isImportant: true
                        , type: DamageType
                        , value: Offense
                        , srcForce: Force
                        , isCritical: false
                        , srcActor: SrcActor
                        , srcPosition: AttackHandler.Get(EAttackType_Projectile_Straight.Attacker).transform.position);
                    }
                    damaged.InvokeDamage();
                }
            }
        }
    }
}