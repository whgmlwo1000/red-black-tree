﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace RedBlackTree
{
    [CustomEditor(typeof(Projectile_Straight))]
    public class Projectile_StraightEditor : AProjectileEditor
    {
        public override Transform SetProperties()
        {
            Transform main = base.SetProperties();

            SerializedProperty speedProp = serializedObject.FindProperty("mSpeed");
            speedProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(new GUIContent("Speed"), speedProp.floatValue), 0.0f);

            return main;
        }
    }
}