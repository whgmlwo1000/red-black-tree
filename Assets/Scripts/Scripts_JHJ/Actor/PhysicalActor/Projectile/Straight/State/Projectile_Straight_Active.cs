﻿using UnityEngine;

namespace RedBlackTree
{
    public class Projectile_Straight_Active : State<EStateType_Projectile_Straight>
    {
        private Projectile_Straight mProjectile;
        public Projectile_Straight_Active(Projectile_Straight projectile) : base(EStateType_Projectile_Straight.Active)
        {
            mProjectile = projectile;
        }

        public override void Start()
        {
            mProjectile.MainAnimator.SetInteger("state", (int)Type);
        }

        public override void FixedUpdate()
        {
            if(mProjectile.Detect())
            {
                mProjectile.Deactivate();
            }
        }
    }
}