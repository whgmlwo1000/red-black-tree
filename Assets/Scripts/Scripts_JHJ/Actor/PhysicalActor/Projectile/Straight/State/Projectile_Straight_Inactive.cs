﻿using UnityEngine;

namespace RedBlackTree
{
    public class Projectile_Straight_Inactive : State<EStateType_Projectile_Straight>
    {
        private Projectile_Straight mProjectile;

        public Projectile_Straight_Inactive(Projectile_Straight projectile) : base(EStateType_Projectile_Straight.Inactive)
        {
            mProjectile = projectile;
        }

        public override void Start()
        {
            mProjectile.MainAnimator.SetInteger("state", (int)Type);
            mProjectile.gameObject.SetActive(false);
        }
    }
}