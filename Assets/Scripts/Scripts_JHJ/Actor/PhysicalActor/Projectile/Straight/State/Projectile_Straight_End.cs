﻿using UnityEngine;

namespace RedBlackTree
{
    public class Projectile_Straight_End : State<EStateType_Projectile_Straight>
    {
        private Projectile_Straight mProjectile;

        public Projectile_Straight_End(Projectile_Straight projectile) : base(EStateType_Projectile_Straight.End)
        {
            mProjectile = projectile;
        }

        public override void Start()
        {
            mProjectile.MainAnimator.SetInteger("state", (int)Type);
        }
    }
}