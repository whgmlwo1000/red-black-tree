﻿using UnityEngine;
using ObjectManagement;
using UnityEngine.SceneManagement;
using TouchManagement;
using CameraManagement;

namespace RedBlackTree
{
    public enum EBlessedFairy
    {
        None = -1, 
        BlessedFairy, 


    }

    public enum EStateType_BlessedFairy
    {
        Activate 
    }

    public enum EAudioType_BlessedFairy
    {
        None = -1, 
        Move = 0, 
    }

    [RequireComponent(typeof(AudioHandler_BlessedFairy), typeof(TouchBox), typeof(Deactivator))]
    public class BlessedFairy : APhysicalActor<EStateType_BlessedFairy>, IPoolable<EBlessedFairy>
    {
        public override EActorType ActorType { get { return EActorType.BlessedFairy; } }

        public bool IsWorldCanvas { get { return false; } }

        public bool IsPooled { get { return !gameObject.activeSelf; } }

        [SerializeField]
        private EBlessedFairy mType = EBlessedFairy.None;
        public EBlessedFairy Type { get { return mType; } }

        public AudioHandler_BlessedFairy AudioHandler { get; private set; }
        public ITouchReceiver TouchReceiver { get; private set; }

        [SerializeField]
        private BasicStatus_BlessedFairy mBasicStatus = null;

        public float Speed { get { return mBasicStatus.Speed; } }

        public float CycleTime { get { return mBasicStatus.CycleTime; } }

        public float CycleSpeed { get { return mBasicStatus.CycleSpeed; } }

        public int HealValue { get { return mBasicStatus.HealValue; } }

        public override void Awake()
        {
            base.Awake();

            TouchReceiver = GetComponent<ITouchReceiver>();
            AudioHandler = GetComponent<AudioHandler_BlessedFairy>();

            TouchReceiver.OnTouchStart += OnTouchStart;

            StateManager.AddOnExitAbyss(OnActiveSceneChanged);

            StateManager.Pause.AddEnterEvent(true, OnPauseEnter);
            StateManager.Pause.AddExitEvent(true, OnPauseExit);

            SetStates(new BlessedFairy_Activate(this));
        }

        public override void Start()
        {
            base.Start();

            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        public void Spawn()
        {
            transform.position = VerticalCast.Position;
            gameObject.SetActive(true);

            State = EStateType_BlessedFairy.Activate;
        }

        public float FollowCamera(float prevCameraPosition_x)
        {
            float cameraPosition_x = CameraController.Main.Position.x;
            Vector2 curPosition = MainRigidbody.position;
            float cameraDeltaPosition_x = cameraPosition_x - prevCameraPosition_x;
            MainRigidbody.position = new Vector2(curPosition.x + cameraDeltaPosition_x, curPosition.y);
            return cameraPosition_x;
        }

        private void OnTouchStart(Touch touch, Vector2 worldPosition)
        {
            Giant.Main.Heal.Set(true
                , EDamageType.Life
                , HealValue
                , 0.0f);
            Giant.Main.InvokeHeal();

            Effect effect = EffectPool.Get(EEffectType.BlessedFairy);
            if(effect != null)
            {
                effect.Activate(null, worldPosition);
            }
        }

        private void OnActiveSceneChanged()
        {
            gameObject.SetActive(false);
        }
        private void OnActiveSceneChanged(Scene src, Scene dst)
        {
            OnActiveSceneChanged();
        }

        private void OnPauseEnter()
        {
            if(gameObject.activeInHierarchy)
            {
                AudioHandler.Get(EAudioType_BlessedFairy.Move).AudioSource.Stop();
            }
        }

        private void OnPauseExit()
        {
            if(gameObject.activeInHierarchy)
            {
                AudioHandler.Get(EAudioType_BlessedFairy.Move).AudioSource.Play();
            }
        }
    }
}