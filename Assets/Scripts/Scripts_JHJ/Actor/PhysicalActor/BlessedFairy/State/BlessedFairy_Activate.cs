﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class BlessedFairy_Activate : State<EStateType_BlessedFairy>
    {
        private BlessedFairy mFairy;
        private EVerticalDirection mVerticalDirection;
        private float mRemainTime_Cycle;
        private float mPrevCameraPosition_x;

        public BlessedFairy_Activate(BlessedFairy fairy) : base(EStateType_BlessedFairy.Activate)
        {
            mFairy = fairy;
        }

        public override void Start()
        {
            mFairy.AudioHandler.Get(EAudioType_BlessedFairy.Move).AudioSource.Play();
            mRemainTime_Cycle = mFairy.CycleTime;

            int selection = (int)Random.Range(0.0f, 2.0f);
            if(selection ==0)
            {
                mVerticalDirection = EVerticalDirection.Down;
                mFairy.Velocity = new Vector2(-mFairy.Speed, -mFairy.CycleSpeed * mFairy.CycleTime * 0.5f);
            }
            else
            {
                mVerticalDirection = EVerticalDirection.Up;
                mFairy.Velocity = new Vector2(-mFairy.Speed, mFairy.CycleSpeed * mFairy.CycleTime * 0.5f);
            }

            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }

        public override void End()
        {
            mFairy.AudioHandler.Get(EAudioType_BlessedFairy.Move).AudioSource.Stop();
        }

        public override void FixedUpdate()
        {
            Vector2 curVelocity = mFairy.Velocity;

            if(mRemainTime_Cycle > 0.0f)
            {
                mRemainTime_Cycle -= Time.fixedDeltaTime;
                
                if(mVerticalDirection == EVerticalDirection.Down)
                {
                    mFairy.Velocity = new Vector2(curVelocity.x, curVelocity.y + mFairy.CycleSpeed * Time.fixedDeltaTime);
                }
                else
                {
                    mFairy.Velocity = new Vector2(curVelocity.x, curVelocity.y - mFairy.CycleSpeed * Time.fixedDeltaTime);
                }
            }
            else
            {
                mRemainTime_Cycle += mFairy.CycleTime - Time.fixedDeltaTime;

                if (mVerticalDirection == EVerticalDirection.Down)
                {
                    mVerticalDirection = EVerticalDirection.Up;
                    mFairy.Velocity = new Vector2(curVelocity.x, curVelocity.y - mFairy.CycleSpeed * Time.fixedDeltaTime);
                }
                else
                {
                    mVerticalDirection = EVerticalDirection.Down;
                    mFairy.Velocity = new Vector2(curVelocity.x, curVelocity.y + mFairy.CycleSpeed * Time.fixedDeltaTime);
                }
            }

            mPrevCameraPosition_x = mFairy.FollowCamera(mPrevCameraPosition_x);
        }
    }
}