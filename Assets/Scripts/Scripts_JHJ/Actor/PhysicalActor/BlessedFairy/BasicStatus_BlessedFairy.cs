﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_BlessedFairy", menuName = "Support Fairy/Blessed Fairy Basic Status", order = 1000)]
    public class BasicStatus_BlessedFairy : ScriptableObject
    {
        public float Speed;
        public float CycleTime;
        public float CycleSpeed;

        public int HealValue;
    }
}