﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BlessedFairy))]
    public class BlessedFairyEditor : APhysicalActorEditor
    {
        public override Transform SetProperties()
        {
            SerializedProperty typeProp = serializedObject.FindProperty("mType");
            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Type", GUILayout.MaxWidth(100));
            EBlessedFairy type = (EBlessedFairy)typeProp.intValue;
            type = (EBlessedFairy)EditorGUILayout.EnumPopup(type, GUILayout.MaxWidth(150));
            typeProp.intValue = (int)type;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Basic Status", GUILayout.MaxWidth(100));
            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(basicStatusProp.objectReferenceValue, typeof(BasicStatus_BlessedFairy), false);
            EditorGUILayout.EndHorizontal();

            return base.SetProperties();
        }
    }
}