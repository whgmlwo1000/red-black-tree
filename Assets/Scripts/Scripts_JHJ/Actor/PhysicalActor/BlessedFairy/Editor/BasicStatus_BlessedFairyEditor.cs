﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_BlessedFairy))]
    public class BasicStatus_BlessedFairyEditor : Editor
    {
        public sealed override void OnInspectorGUI()
        {
            SetProperties();
            serializedObject.ApplyModifiedProperties();
        }

        public virtual void SetProperties()
        {
            SerializedProperty speedProp = serializedObject.FindProperty("Speed");
            SerializedProperty cycleTimeProp = serializedObject.FindProperty("CycleTime");
            SerializedProperty cycleSpeedProp = serializedObject.FindProperty("CycleSpeed");
            SerializedProperty healValueProp = serializedObject.FindProperty("HealValue");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Speed", GUILayout.MaxWidth(100));
            speedProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(speedProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Cycle Time", GUILayout.MaxWidth(100));
            cycleTimeProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(cycleTimeProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Cycle Speed", GUILayout.MaxWidth(100));
            cycleSpeedProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(cycleSpeedProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Heal Value", GUILayout.MaxWidth(100));
            healValueProp.intValue = Mathf.Max(EditorGUILayout.IntField(healValueProp.intValue, GUILayout.MaxWidth(50)), 0);
            EditorGUILayout.EndHorizontal();
        }
    }
}