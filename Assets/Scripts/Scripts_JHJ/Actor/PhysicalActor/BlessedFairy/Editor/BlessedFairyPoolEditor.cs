﻿using UnityEngine;
using UnityEditor;
using ObjectManagement;

namespace RedBlackTree
{
    [CustomEditor(typeof(BlessedFairyPool))]
    public class BlessedFairyPoolEditor : PoolEditor<EBlessedFairy>
    {
    }
}