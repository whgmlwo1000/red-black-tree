﻿using UnityEngine;

namespace RedBlackTree
{
    public class ArrestedCitizen_Break : State<EStateType_ArrestedCitizen>
    {
        private ArrestedCitizen mCitizen;

        public ArrestedCitizen_Break(ArrestedCitizen citizen) : base(EStateType_ArrestedCitizen.Break)
        {
            mCitizen = citizen;
        }

        public override void Start()
        {
            mCitizen.MainAnimator.SetInteger("state", (int)Type);
            PlayerData.Current.SetRescue(mCitizen.Type, true);
        }
    }
}