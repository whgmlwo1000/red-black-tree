﻿using UnityEngine;

namespace RedBlackTree
{
    public class ArrestedCitizen_Arrested : State<EStateType_ArrestedCitizen>
    {
        private ArrestedCitizen mCitizen;
        public ArrestedCitizen_Arrested(ArrestedCitizen citizen) : base(EStateType_ArrestedCitizen.Arrested)
        {
            mCitizen = citizen;
        }

        public override void Start()
        {
            mCitizen.MainAnimator.SetInteger("state", (int)Type);
            mCitizen.MainRigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
            mCitizen.MainColliderHandler.Get(EMainColliderType.TerrainTrigger).gameObject.SetActive(false);
            mCitizen.TouchReceiver.enabled = true;
        }

        public override void End()
        {
            mCitizen.TouchReceiver.enabled = false;
        }
    }
}