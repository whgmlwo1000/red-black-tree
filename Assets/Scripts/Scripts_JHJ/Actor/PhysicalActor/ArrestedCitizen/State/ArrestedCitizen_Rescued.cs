﻿using UnityEngine;

namespace RedBlackTree
{
    public class ArrestedCitizen_Rescued : State<EStateType_ArrestedCitizen>
    {
        private ArrestedCitizen mCitizen;

        public ArrestedCitizen_Rescued(ArrestedCitizen citizen) : base(EStateType_ArrestedCitizen.Rescued)
        {
            mCitizen = citizen;
        }

        public override void Start()
        {
            mCitizen.MainAnimator.SetInteger("state", (int)Type);
        }
    }
}