﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(ArrestedCitizen))]
    public class ArrestedCitizenEditor : APhysicalActorEditor
    {
        public override Transform SetProperties()
        {
            SerializedProperty typeProp = serializedObject.FindProperty("mType");
            SerializedProperty upAnchorProp = serializedObject.FindProperty("mUpAnchor");
            SerializedProperty downAnchorProp = serializedObject.FindProperty("mDownAnchor");

            ECitizen type = (ECitizen)typeProp.intValue;
            type = (ECitizen)EditorGUILayout.EnumPopup(new GUIContent("Type"), type);
            typeProp.intValue = (int)type;

            upAnchorProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Up Anchor"), upAnchorProp.objectReferenceValue, typeof(Transform), true);
            downAnchorProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Down Anchor"), downAnchorProp.objectReferenceValue, typeof(Transform), true);

            return base.SetProperties();
        }
    }
}