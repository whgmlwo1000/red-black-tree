﻿using UnityEngine;
using UnityEngine.SceneManagement;
using ObjectManagement;
using TouchManagement;

namespace RedBlackTree
{
    public enum EStateType_ArrestedCitizen
    {
        // 구속된 상태
        Arrested, 
        // 부숴지는 상태
        Break, 
        // 구출 상태
         Rescued, 
    }

    public enum EAudioType_ArrestedCitizen
    {
        None = -1, 

        Rescue = 0, 
        Thank = 1
    }

    [RequireComponent(typeof(AudioHandler_ArrestedCitizen), typeof(Deactivator), typeof(TouchBox))]
    public class ArrestedCitizen : APhysicalActor<EStateType_ArrestedCitizen>, IPoolable<ECitizen>
    {
        public override EActorType ActorType { get { return EActorType.ArrestedCitizen; } }

        public bool IsWorldCanvas { get { return false; } }

        public bool IsPooled { get { return !gameObject.activeSelf; } }

        [SerializeField]
        private ECitizen mType = ECitizen.None;
        public ECitizen Type { get { return mType; } }

        [SerializeField]
        private Transform mUpAnchor = null;
        public Transform UpAnchor { get { return mUpAnchor; } }

        [SerializeField]
        private Transform mDownAnchor = null;
        public Transform DownAnchor { get { return mDownAnchor; } }

        public AudioHandler_ArrestedCitizen AudioHandler { get; private set; }

        public ITouchReceiver TouchReceiver { get; private set; }

        public override void Awake()
        {
            base.Awake();
            TouchReceiver = GetComponent<ITouchReceiver>();
            AudioHandler = GetComponent<AudioHandler_ArrestedCitizen>();
            TouchReceiver.OnTouchEnd += OnTouchEnd;
            StateManager.AddOnExitAbyss(OnActiveSceneChanged);

            SetStates(new ArrestedCitizen_Arrested(this)
                , new ArrestedCitizen_Break(this)
                , new ArrestedCitizen_Rescued(this));
        }

        public override void Start()
        {
            base.Start();

            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        public void Spawn(Vector2 position, EVerticalDirection verticalDirection)
        {
            if (verticalDirection == EVerticalDirection.Down)
            {
                Vector2 downAnchorLocalPosition = mDownAnchor.localPosition;
                transform.position = new Vector3(position.x, position.y - downAnchorLocalPosition.y);
            }
            else if(verticalDirection == EVerticalDirection.Up)
            {
                Vector2 upAnchorLocalPosition = mUpAnchor.localPosition;
                transform.position = new Vector3(position.x, position.y - upAnchorLocalPosition.y);
            }

            gameObject.SetActive(true);
            State = EStateType_ArrestedCitizen.Arrested;
        }

        private void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            if(TouchReceiver.GetTouchIn(worldPosition))
            {
                PlayRescueAudio();
                State = EStateType_ArrestedCitizen.Break;
                Effect effect = EffectPool.Get(EEffectType.InsaneWalnut);
                if(effect != null)
                {
                    effect.Activate(null, worldPosition);
                }
            }
        }

        private void OnActiveSceneChanged()
        {
            gameObject.SetActive(false);
        }

        private void OnActiveSceneChanged(Scene src, Scene dst)
        {
            OnActiveSceneChanged();
        }

        private void OnBreakEnd_Animation()
        {
            State = EStateType_ArrestedCitizen.Rescued;
        }

        private void OnStartFalling_Animation()
        {
            MainRigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
            MainRigidbody.WakeUp();
            MainColliderHandler.Get(EMainColliderType.TerrainTrigger).gameObject.SetActive(true);
        }

        private void PlayRescueAudio()
        {
            AudioHandler.Get(EAudioType_ArrestedCitizen.Rescue).AudioSource.Play();
        }

        private void PlayThankAudio_Animation()
        {
            AudioHandler.Get(EAudioType_ArrestedCitizen.Thank).AudioSource.Play();
        }
    }
}