﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_Product))]
    public class BasicStatus_ProductEditor : Editor
    {
        public sealed override void OnInspectorGUI()
        {
            SetProperties();

            serializedObject.ApplyModifiedProperties();
        }

        public virtual void SetProperties()
        {
            SerializedProperty soulsPriceProp = serializedObject.FindProperty("SoulsPrice");
            SerializedProperty demonSoulsPriceProp = serializedObject.FindProperty("DemonSoulsPrice");

            soulsPriceProp.intValue = EditorGUILayout.IntField(new GUIContent("Souls Price", "영혼 기준 가격"), soulsPriceProp.intValue);
            soulsPriceProp.intValue = Mathf.Max(soulsPriceProp.intValue, 0);

            demonSoulsPriceProp.intValue = EditorGUILayout.IntField(new GUIContent("Demon Souls Price", "악마의 영혼 기준 가격"), demonSoulsPriceProp.intValue);
            demonSoulsPriceProp.intValue = Mathf.Max(demonSoulsPriceProp.intValue, 0);
        }
    }
}