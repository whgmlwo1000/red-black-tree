﻿using UnityEngine;
using UnityEditor;
using ObjectManagement;

namespace RedBlackTree
{
    [CustomEditor(typeof(ProductPool))]
    public class ProductPoolEditor : PoolEditor<EProduct>
    {
    }
}