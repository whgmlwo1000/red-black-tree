﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Product))]
    public class ProductEditor : APhysicalActorEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            SerializedProperty typeProp = serializedObject.FindProperty("mType");
            SerializedProperty priceHolderProp = serializedObject.FindProperty("mPriceHolder");

            EProduct type = (EProduct)typeProp.intValue;
            type = (EProduct)EditorGUILayout.EnumPopup(new GUIContent("Product Type"), type);
            typeProp.intValue = (int)type;

            priceHolderProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Price Holder"), priceHolderProp.objectReferenceValue, typeof(Transform), true);
            if(priceHolderProp.objectReferenceValue == null)
            {
                Product product = target as Product;
                Transform priceHolder = new GameObject("Price Holder").transform;
                priceHolder.SetParent(product.transform);
                priceHolder.localPosition = Vector3.zero;
                priceHolder.localRotation = Quaternion.identity;
                priceHolder.localScale = Vector3.one;
                priceHolderProp.objectReferenceValue = priceHolder;
            }

            return mainTransform;
        }
    }
}