﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Status_Product_ActiveItem : Status_Product
    {
        private BasicStatus_Product_ActiveItem mBasicStatus;
        public new BasicStatus_Product_ActiveItem BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }

        public EActiveItem ActiveItem { get { return BasicStatus.ActiveItem; } }
    }
}