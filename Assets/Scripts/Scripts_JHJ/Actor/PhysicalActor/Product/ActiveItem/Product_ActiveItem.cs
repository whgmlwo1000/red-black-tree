﻿using UnityEngine;

namespace RedBlackTree
{
    public class Product_ActiveItem : Product
    {
        [SerializeField]
        private BasicStatus_Product_ActiveItem mBasicStatus = null;
        private Status_Product_ActiveItem mStatus;
        public new Status_Product_ActiveItem Status
        {
            get { return mStatus; }
            protected set
            {
                value.BasicStatus = mBasicStatus;
                base.Status = mStatus = value;
            }
        }

        public override void Awake()
        {
            base.Awake();

            Status = ScriptableObject.CreateInstance<Status_Product_ActiveItem>();
        }

        protected override void OnBought()
        {
            base.OnBought();
            InventoryBag.Inventory.Add(Status.ActiveItem, 1);
        }
    }
}