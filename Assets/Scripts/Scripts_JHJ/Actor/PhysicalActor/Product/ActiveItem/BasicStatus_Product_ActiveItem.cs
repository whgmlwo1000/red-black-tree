﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_Product_ActiveItem", menuName = "Product/Active Item Basic Status", order = 1000)]
    public class BasicStatus_Product_ActiveItem : BasicStatus_Product
    {
        public EActiveItem ActiveItem = EActiveItem.None;
    }
}