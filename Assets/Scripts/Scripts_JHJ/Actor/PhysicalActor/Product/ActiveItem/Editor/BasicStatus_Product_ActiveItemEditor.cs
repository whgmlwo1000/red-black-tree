﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_Product_ActiveItem))]
    public class BasicStatus_Product_ActiveItemEditor : BasicStatus_ProductEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            SerializedProperty activeItemProp = serializedObject.FindProperty("ActiveItem");

            EActiveItem activeItem = (EActiveItem)activeItemProp.intValue;
            activeItem = (EActiveItem)EditorGUILayout.EnumPopup(new GUIContent("Active Item"), activeItem);
            activeItemProp.intValue = (int)activeItem;
        }
    }
}