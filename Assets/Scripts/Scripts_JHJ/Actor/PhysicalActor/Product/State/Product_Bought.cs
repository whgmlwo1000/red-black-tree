﻿using UnityEngine;

namespace RedBlackTree
{
    public class Product_Bought : State<EStateType_Product>
    {
        private Product mProduct;
        public Product_Bought(Product product) : base(EStateType_Product.Bought)
        {
            mProduct = product;
        }

        public override void Start()
        {
            mProduct.MainAnimator.SetInteger("state", (int)Type);
        }

        public override void FixedUpdate()
        {
            if (!mProduct.CheckAcquired())
            {
                mProduct.FollowCamera();
                mProduct.MoveToBag();
            }
        }
    }
}