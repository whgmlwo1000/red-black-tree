﻿using UnityEngine;

namespace RedBlackTree
{
    public class Product_Displayed : State<EStateType_Product>
    {
        private Product mProduct;
        public Product_Displayed(Product product) : base(EStateType_Product.Displayed)
        {
            mProduct = product;
        }

        public override void Start()
        {
            mProduct.MainAnimator.SetInteger("state", (int)Type);
        }

        public override void FixedUpdate()
        {
            mProduct.FollowParent();
            mProduct.ShowExpensive();
        }
    }
}