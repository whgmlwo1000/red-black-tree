﻿using UnityEngine;

namespace RedBlackTree
{
    public class Product_Acquired : State<EStateType_Product>
    {
        private Product mProduct;

        public Product_Acquired(Product product) : base(EStateType_Product.Acquired)
        {
            mProduct = product;
        }

        public override void Start()
        {
            mProduct.MainAnimator.SetInteger("state", (int)Type);
        }
    }
}