﻿using UnityEngine;
using UnityEngine.SceneManagement;
using ObjectManagement;
using TouchManagement;
using CameraManagement;

namespace RedBlackTree
{
    public enum EProduct
    {
        None = -1,


        _PassiveItem_ = 100,
        /// <summary>
        /// 풍요의 씨앗
        /// </summary>
        AffluenceSeed,

        /// <summary>
        /// 불꽃의 요정
        /// </summary>
        FlameFairy,

        /// <summary>
        /// 빛의 요정
        /// </summary>
        LightFairy,

        /// <summary>
        /// 티르의 검
        /// </summary>
        TyrSword,

        /// <summary>
        /// 티르의 방패
        /// </summary>
        TyrShield,

        /// <summary>
        /// 정화의 부적
        /// </summary>
        PurificationAmulet,

        /// <summary>
        /// 퇴마의 부적
        /// </summary>
        ExorcismAmulet,

        /// <summary>
        /// 엘릭서
        /// </summary>
        Elixir,

        /// <summary>
        /// 프리가의 손거울
        /// </summary>
        FrigarHandMirror,

        /// <summary>
        /// 탐욕의 목걸이
        /// </summary>
        GreedNecklace,

    }

    public enum EStateType_Product
    {
        /// <summary>
        /// 진열된 상태
        /// </summary>
        Displayed,
        /// <summary>
        /// 구매된 상태
        /// </summary>
        Bought,
        /// <summary>
        /// 습득된 상태
        /// </summary>
        Acquired,
    }

    public enum EAudioType_Product
    {
        None = -1,
        Sell = 0
    }

    public enum ETriggerType_Product
    {
        Acquired,
    }

    [RequireComponent(typeof(TriggerHandler_Product), typeof(AudioHandler_Product))]
    public class Product : APhysicalActor<EStateType_Product>, IPoolable<EProduct>
    {
        public override EActorType ActorType { get { return EActorType.Product; } }

        [SerializeField]
        private EProduct mType = EProduct.None;
        public EProduct Type { get { return mType; } }

        public bool IsPooled { get { return !gameObject.activeSelf; } }

        public Status_Product Status { get; protected set; }

        /// <summary>
        /// 현재 진입 장소에 따라서 Demon Souls를 이용해 살 것인지
        /// Souls 를 이용해 살 것인지 결정하여 결과를 반환하는 프로퍼티
        /// </summary>
        public bool CanBeBought
        {
            get
            {
                if (StateManager.Scene.State == EScene.Scene_Sanctuary)
                {
                    if (InventoryBag.Inventory.DemonSouls >= Status.DemonSoulsPrice)
                    {
                        return true;
                    }
                }
                else if (InventoryBag.Inventory.Souls >= Status.SoulsPrice)
                {
                    return true;
                }

                return false;
            }
        }

        public int Price
        {
            get
            {
                if (StateManager.Scene.State == EScene.Scene_Sanctuary)
                {
                    return Status.DemonSoulsPrice;
                }
                else
                {
                    return Status.SoulsPrice;
                }
            }
        }

        public ITouchReceiver TouchReceiver { get; private set; }

        public TriggerHandler_Product TriggerHandler { get; private set; }
        public AudioHandler_Product AudioHandler { get; private set; }

        public bool IsWorldCanvas { get { return false; } }

        [SerializeField]
        private Transform mPriceHolder = null;
        public Transform PriceHolder { get { return mPriceHolder; } }

        private Vector2 mPrevParentPosition;

        private Vector2 mPrevCameraPosition;

        private float mAccumulatedTouchTime;
        private Vector2 mTouchStartWorldPosition;
        private Vector2 mTouchStartPosition;

        private bool mIsShowExpensive = false;
        private float mShowExpensiveTime = 0.0f;

        private Price mPrice;

        public ProductStand Stand { get; private set; }

        public override void Awake()
        {
            base.Awake();

            TouchReceiver = GetComponent<ITouchReceiver>();

            TriggerHandler = GetComponent<TriggerHandler_Product>();
            AudioHandler = GetComponent<AudioHandler_Product>();

            TouchReceiver.OnTouchStart += OnTouchStart;
            TouchReceiver.OnTouchStay += OnTouchStay;
            TouchReceiver.OnTouchEnd += OnTouchEnd;

            SetStates(new Product_Displayed(this)
                , new Product_Bought(this)
                , new Product_Acquired(this));
        }

        public override void Start()
        {
            base.Start();
            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        /// <summary>
        /// 상품을 활성화
        /// </summary>
        public void Activate(ProductStand stand, Vector2 position)
        {
            transform.position = position;
            Stand = stand;
            mPrevParentPosition = Stand.transform.position;
            mShowExpensiveTime = 0.0f;
            gameObject.SetActive(true);
            State = EStateType_Product.Displayed;
            mPrice = PricePool.Get(EPrice.Normal);
            if (mPrice != null)
            {
                mPrice.Activate(this);
            }
        }

        /// <summary>
        /// 상품 비활성화 메소드
        /// </summary>
        public void Deactivate()
        {
            if (mPrice != null)
            {
                mPrice.Deactivate();
            }
            gameObject.SetActive(false);
        }

        /// <summary>
        /// 애니메이션 이벤트로 호출되는 비활성화 메소드
        /// </summary>
        public void Deactivate_Animation()
        {
            Deactivate();
        }

        public void MoveToBag()
        {
            Vector2 curPosition = MainRigidbody.position;
            Transform destination = InventoryBag.Destination;
            Vector2 dstPosition = transform.position;
            if (destination != null)
            {
                dstPosition = InventoryBag.Destination.position;
            }

            Vector2 direction = dstPosition - curPosition;
            direction.Normalize();

            Velocity *= 0.9f;
            AddVelocity(ValueConstants.MaxSpeedOfAcquisition, direction * ValueConstants.MaxSpeedOfAcquisition * Time.fixedDeltaTime * 5.0f);
        }

        /// <summary>
        /// Parent의 이동과 똑같이 움직이는 메소드
        /// </summary>
        public void FollowParent()
        {
            Vector2 curPosition = MainRigidbody.position;
            Vector2 curParentPosition = Stand.transform.position;
            MainRigidbody.position = curPosition + (curParentPosition - mPrevParentPosition);
            mPrevParentPosition = curParentPosition;
        }

        /// <summary>
        /// Camera의 이동과 똑같이 움직이는 메소드
        /// </summary>
        public void FollowCamera()
        {
            Vector2 curPosition = MainRigidbody.position;
            Vector2 curCameraPosition = CameraController.Main.Position;
            MainRigidbody.position = curPosition + (curCameraPosition - mPrevCameraPosition);
            mPrevCameraPosition = curCameraPosition;
        }

        /// <summary>
        /// Inventory Bag이 트리거되면 아이템 습득 메커니즘을 실행하는 메소드
        /// </summary>
        public bool CheckAcquired()
        {
            Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Product.Acquired, out int count);
            for (int indexOfCollider = 0; indexOfCollider < count; indexOfCollider++)
            {
                InventoryBag inventoryBag = colliders[indexOfCollider].GetComponentInParent<InventoryBag>();
                if (inventoryBag != null)
                {
                    InventoryBag.PushItem();
                    State = EStateType_Product.Acquired;
                    return true;
                }
            }
            return false;
        }

        public void BeBought()
        {
            mPrevCameraPosition = CameraController.Main.Position;
            if (StateManager.Scene.State == EScene.Scene_Sanctuary)
            {
                InventoryBag.Inventory.DemonSouls -= Status.DemonSoulsPrice;
            }
            else
            {
                InventoryBag.Inventory.Souls -= Status.SoulsPrice;
            }

            State = EStateType_Product.Bought;
            OnBought();
        }

        /// <summary>
        /// 구매할 수 있는 조건이 갖춰지지 않은 경우 구매를 호출하였다면, 
        /// 지정된 시간동안 구매가 불가능함을 표시하기 위해 진동하는 메소드
        /// </summary>
        public void ShowExpensive()
        {
            if (mIsShowExpensive)
            {
                if (mShowExpensiveTime > 0.0f)
                {
                    mShowExpensiveTime -= Time.deltaTime;
                    Transform rendererTransform = MainRendererHandler.Get(0).transform;
                    rendererTransform.localPosition = new Vector3(Random.Range(-ValueConstants.PowerOfExpensiveShow, ValueConstants.PowerOfExpensiveShow)
                        , Random.Range(-ValueConstants.PowerOfExpensiveShow, ValueConstants.PowerOfExpensiveShow));
                }
                else
                {
                    mIsShowExpensive = false;
                    Transform rendererTransform = MainRendererHandler.Get(0).transform;
                    rendererTransform.localPosition = Vector3.zero;
                }
            }
        }

        protected virtual void ShowTooltip()
        { }
        protected virtual void CloseTooltip()
        { }
        protected virtual void OnBought()
        {
            AudioHandler.Get(EAudioType_Product.Sell).AudioSource.Play();
        }

        /// <summary>
        /// 터치가 시작되는 경우
        /// 터치가 시작된 좌표들을 저장하고, 
        /// 툴팁을 출력하는 메소드
        /// </summary>
        private void OnTouchStart(Touch touch, Vector2 worldPosition)
        {
            if (State == EStateType_Product.Displayed)
            {
                mAccumulatedTouchTime = 0.0f;
                mTouchStartWorldPosition = worldPosition;
                mTouchStartPosition = touch.position;

                ShowTooltip();
            }
        }

        /// <summary>
        /// 터치가 지속되는 경우
        /// 스와이프 터치가 감지되는 경우, 아이템 구매가 가능한지 검사하고,
        /// 구매가 가능한 경우, 자원을 감소시키고 아이템 습득 과정을 호출하는 메소드
        /// 구매가 가능하지 않은 경우, 구매가 가능하지 않다는 표시를 호출하는 메소드
        /// </summary>
        private void OnTouchStay(Touch touch, Vector2 worldPosition)
        {
            if (State == EStateType_Product.Displayed)
            {
                mAccumulatedTouchTime += Time.deltaTime;
                Vector2 touchDirection = touch.position - mTouchStartPosition;
                float touchMagnitude = touchDirection.magnitude;
                if (touchMagnitude > ValueConstants.TouchDistanceOfBuying)
                {
                    if (CanBeBought)
                    {
                        Vector2 direction = worldPosition - mTouchStartWorldPosition;
                        Velocity = direction / mAccumulatedTouchTime;
                        if (mIsShowExpensive)
                        {
                            mIsShowExpensive = false;
                            MainRendererHandler.Get(0).transform.localPosition = Vector3.zero;
                        }
                        BeBought();

                        // 툴팁 종료...
                        CloseTooltip();
                    }
                    else
                    {
                        mIsShowExpensive = true;
                        mShowExpensiveTime = ValueConstants.ShowExpensiveTime;
                    }
                }
            }
        }

        /// <summary>
        /// 터치가 끝나는 경우
        /// 툴팁이 출력중이라면 툴팁을 종료하는 메소드
        /// </summary>
        private void OnTouchEnd(Touch touch, Vector2 worldPosition)
        {
            // 툴팁 종료
            CloseTooltip();
        }

        /// <summary>
        /// 활성화된 씬이 바뀌는 경우
        /// </summary>
        private void OnActiveSceneChanged(Scene src, Scene dst)
        {
            Deactivate();
        }
    }
}