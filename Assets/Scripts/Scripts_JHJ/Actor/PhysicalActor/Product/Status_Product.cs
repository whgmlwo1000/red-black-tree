﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Status_Product : ScriptableObject
    {
        public BasicStatus_Product BasicStatus { get; set; }

        public int SoulsPrice { get { return BasicStatus.SoulsPrice; } }
        public int DemonSoulsPrice { get { return BasicStatus.DemonSoulsPrice; } }
    }
}