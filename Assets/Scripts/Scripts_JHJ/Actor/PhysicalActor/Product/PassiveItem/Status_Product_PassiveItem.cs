﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Status_Product_PassiveItem : Status_Product
    {
        private BasicStatus_Product_PassiveItem mBasicStatus;
        public new BasicStatus_Product_PassiveItem BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }

        public EPassiveItem PassiveItem { get { return BasicStatus.PassiveItem; } }
    }
}