﻿using UnityEngine;

namespace RedBlackTree
{
    public class Product_PassiveItem : Product
    {
        [SerializeField]
        private BasicStatus_Product_PassiveItem mBasicStatus = null;
        private Status_Product_PassiveItem mStatus;
        public new Status_Product_PassiveItem Status
        {
            get { return mStatus; }
            protected set 
            {
                value.BasicStatus = mBasicStatus;
                base.Status = mStatus = value; 
            }
        }

        private ToolTip mTooltip;

        public override void Awake()
        {
            base.Awake();
            Status = ScriptableObject.CreateInstance<Status_Product_PassiveItem>();
        }

        protected override void ShowTooltip()
        {
            base.ShowTooltip();
            PassiveItem item = InventoryBag.Inventory.Get((EPassiveItem)(Type - EProduct._PassiveItem_ - 1));
            mTooltip = ToolTipPoll.PopUp(new ScriptBase(item.Name, item.Option), Stand.transform, new Vector2(-8.0f, 3.0f));
        }

        protected override void CloseTooltip()
        {
            base.CloseTooltip();
            mTooltip.PopDown();
        }

        protected override void OnBought()
        {
            base.OnBought();
            InventoryBag.Inventory.Add(Status.PassiveItem, 1);
        }
    }
}