﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName ="New BasicStatus_Product_PassiveItem", menuName = "Product/Passive Item Basic Status", order = 1000)]
    public class BasicStatus_Product_PassiveItem : BasicStatus_Product
    {
        public EPassiveItem PassiveItem = EPassiveItem.None;
    }
}