﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Product_PassiveItem))]
    public class Product_PassiveItemEditor : ProductEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");
            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Basic Status"), basicStatusProp.objectReferenceValue, typeof(BasicStatus_Product_PassiveItem), false);

            return mainTransform;
        }
    }
}