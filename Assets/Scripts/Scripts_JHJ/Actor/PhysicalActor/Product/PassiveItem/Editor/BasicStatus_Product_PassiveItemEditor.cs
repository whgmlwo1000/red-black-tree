﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_Product_PassiveItem))]
    public class BasicStatus_Product_PassiveItemEditor : BasicStatus_ProductEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            SerializedProperty passiveItemProp = serializedObject.FindProperty("PassiveItem");

            EPassiveItem passiveItem = (EPassiveItem)passiveItemProp.intValue;
            passiveItem = (EPassiveItem)EditorGUILayout.EnumPopup(new GUIContent("Passive Item"), passiveItem);
            passiveItemProp.intValue = (int)passiveItem;
        }
    }
}