﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class BasicStatus_Product : ScriptableObject
    {
        public int SoulsPrice = 100;
        public int DemonSoulsPrice = 1;
    }
}