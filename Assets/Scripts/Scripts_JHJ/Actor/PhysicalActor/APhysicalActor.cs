﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RedBlackTree
{
    public interface IPhysicalActor : IActor
    {
        Transform CenterTransform { get; }
        MainColliderHandler MainColliderHandler { get; }
        Rigidbody2D MainRigidbody { get; }
        Vector2 Velocity { get; set; }

        bool IsZeroFriction { get; }
        PhysicsMaterial2D MainPhysicsMaterial { get; set; }

        void SetHorizontalSpeed(float speed, EHorizontalDirection direction);

        void AddHorizontalSpeed(float maxSpeed, EHorizontalDirection direction, float speed);
        void SetVerticalSpeed(float speed, EVerticalDirection direction);
        void AddVerticalSpeed(float maxSpeed, EVerticalDirection direction, float speed);

        void AddVelocity(float maxSpeed, Vector2 acceleration);

        void AddResistance(float resistance);
        void MovePosition(Vector2 position);
    }

    [RequireComponent(typeof(Rigidbody2D))]
    public abstract class APhysicalActor<EStateType> : AActor<EStateType>, IPhysicalActor
        where EStateType : System.Enum
    {
        [SerializeField]
        private Transform mCenterTransform = null;
        public Transform CenterTransform { get { return mCenterTransform; } }
        [SerializeField]
        private MainColliderHandler mMainColliderHandler = null;
        /// <summary>
        /// 액터의 메인 콜라이더
        /// </summary>
        public MainColliderHandler MainColliderHandler { get { return mMainColliderHandler; } }
        /// <summary>
        /// 액터 리지드바디
        /// </summary>
        public Rigidbody2D MainRigidbody { get; private set; }

        private bool mIsPhysicsUpdated;
        /// <summary>
        /// 액터 속도
        /// </summary>
        public Vector2 Velocity
        {
            get { return MainRigidbody.velocity; }
            set
            {
                mIsPhysicsUpdated = true;
                MainRigidbody.velocity = value;
            }
        }

        private bool mIsAccelerated_x = false;
        private bool mIsAccelerated_y = false;

        /// <summary>
        /// 속도 설정으로 인해 현재 FixedUpdate의 마찰력이 0인 머터리얼 설정 여부
        /// </summary>
        public bool IsZeroFriction { get; private set; }

        private PhysicsMaterial2D mMainPhysicsMaterial;
        /// <summary>
        /// 액터의 물리 물체 정보
        /// </summary>
        public PhysicsMaterial2D MainPhysicsMaterial
        {
            get { return mMainPhysicsMaterial; }
            set
            {
                mMainPhysicsMaterial = value;
                if(!IsZeroFriction)
                {
                    MainRigidbody.sharedMaterial = mMainPhysicsMaterial;
                }
            }
        }

        public override void Awake()
        {
            base.Awake();
            MainRigidbody = GetComponent<Rigidbody2D>();
        }

        public override void FixedUpdate()
        {
            // 일시정지 상태인 경우 처리하지 않음
            if (!StateManager.Pause.State)
            {
                base.FixedUpdate();

                // 속도 설정이 호출된 경우
                if (mIsPhysicsUpdated)
                {
                    // 이번 Fixed Update의 마찰력을 0으로 만듬
                    mIsPhysicsUpdated = false;
                    if (!IsZeroFriction)
                    {
                        IsZeroFriction = true;
                        MainRigidbody.sharedMaterial = ReferenceConstants.ZeroFriction;
                    }
                }
                // 속도 설정 후의 FixedUpdate 다음 FixedUpdate인 경우
                else if (IsZeroFriction)
                {
                    // PhysicsMaterial을 원상태로 되돌림
                    IsZeroFriction = false;
                    MainRigidbody.sharedMaterial = mMainPhysicsMaterial;
                    mIsAccelerated_x = mIsAccelerated_y = false;
                }
            }
        }

        /// <summary>
        /// 기존 속도에 가속도를 가하는 메소드
        /// Status 객체의 MoveSpeed (최대속도)를 고려하여 작동
        /// </summary>
        public void AddVelocity(float maxSpeed, Vector2 acceleration)
        {
            Vector2 curVelocity = Velocity;

            float curSpeed_x = Mathf.Abs(curVelocity.x);
            float curSpeed_y = Mathf.Abs(curVelocity.y);

            float nextVelocity_x = curVelocity.x + acceleration.x;
            float nextVelocity_y = curVelocity.y + acceleration.y;

            float nextSpeed_x = Mathf.Abs(nextVelocity_x);
            float nextSpeed_y = Mathf.Abs(nextVelocity_y);

            // 현재 속도가 최대 속도 이하거나, 현재 속도가 최대 속도를 초과했을때 다음 속도가 반대 방향이라면, 
            if ((curSpeed_x <= maxSpeed + Mathf.Epsilon) || (nextVelocity_x * curVelocity.x < -Mathf.Epsilon))
            {
                nextVelocity_x = Mathf.Clamp(nextVelocity_x, -maxSpeed, maxSpeed);
                if (Mathf.Abs(acceleration.x) > Mathf.Epsilon)
                {
                    mIsAccelerated_x = true;
                }
            }
            // 현재 속도가 최대 속도를 초과했고, 다음 속도가 현재 속도와 같은 방향이면서, 다음 속도가 현재 속도보다 높다면, 
            else if (nextSpeed_x > curSpeed_x + Mathf.Epsilon)
            {
                nextVelocity_x = curVelocity.x;
            }

            if ((curSpeed_y <= maxSpeed + Mathf.Epsilon) || (nextVelocity_x * curVelocity.x < -Mathf.Epsilon))
            {
                nextVelocity_y = Mathf.Clamp(nextVelocity_y, -maxSpeed, maxSpeed);
                if (Mathf.Abs(acceleration.y) > Mathf.Epsilon)
                {
                    mIsAccelerated_y = true;
                }
            }
            else if (nextSpeed_y > curSpeed_y + Mathf.Epsilon)
            {
                nextVelocity_y = curVelocity.y;
            }

            Velocity = new Vector2(nextVelocity_x, nextVelocity_y);
        }

        public void AddResistance(float resistance)
        {
            Vector2 curVelocity = Velocity;
            float nextVelocity_x = curVelocity.x;
            float nextVelocity_y = curVelocity.y;

            // x 축 가속이 적용되지 않은 경우
            if (!mIsAccelerated_x)
            {
                // x 축 속도가 양수인 경우
                if (nextVelocity_x > Mathf.Epsilon)
                {
                    nextVelocity_x = Mathf.Max(0.0f, nextVelocity_x - resistance);
                }
                // x 축 속도가 음수인 경우
                else if (nextVelocity_x < -Mathf.Epsilon)
                {
                    nextVelocity_x = Mathf.Min(0.0f, nextVelocity_x + resistance);
                }
            }
            // x 축 가속이 적용된 경우
            else
            {
                mIsAccelerated_x = false;
            }

            // y 축 가속이 적용되지 않은 경우
            if (!mIsAccelerated_y)
            {
                // y 축 속도가 양수인 경우
                if (nextVelocity_y > Mathf.Epsilon)
                {
                    nextVelocity_y = Mathf.Max(0.0f, nextVelocity_y - resistance);
                }
                // y 축 속도가 음수인 경우
                else if (nextVelocity_y < -Mathf.Epsilon)
                {
                    nextVelocity_y = Mathf.Min(0.0f, nextVelocity_y + resistance);
                }
            }
            // y축 가속이 적용된 경우
            else
            {
                mIsAccelerated_y = false;
            }

            Velocity = new Vector2(nextVelocity_x, nextVelocity_y);
        }

        /// <summary>
        /// 액터의 수평 속도 설정 메소드
        /// </summary>
        public void SetHorizontalSpeed(float speed, EHorizontalDirection direction)
        {
            HorizontalDirection = direction;
            Velocity = new Vector2((float)direction * speed, Velocity.y);
        }
        public void AddHorizontalSpeed(float maxSpeed, EHorizontalDirection direction, float speed)
        {
            HorizontalDirection = direction;
            if (Mathf.Abs(speed) > Mathf.Epsilon)
            {
                Vector2 curVelocity = Velocity;
                float nextVelocity_x = curVelocity.x + (float)direction * speed;

                // 현재 속도가 최대 속도 이하거나, 현재 속도가 최대 속도를 초과했을때 다음 속도가 반대 방향이라면, 
                if ((curVelocity.x <= maxSpeed + Mathf.Epsilon) || (nextVelocity_x * curVelocity.x < -Mathf.Epsilon))
                {
                    nextVelocity_x = Mathf.Clamp(nextVelocity_x, -maxSpeed, maxSpeed);
                }
                // 현재 속도가 최대 속도를 초과했고, 다음 속도가 현재 속도와 같은 방향이면서, 다음 속도가 현재 속도보다 높다면, 
                else if (nextVelocity_x > curVelocity.x + Mathf.Epsilon)
                {
                    nextVelocity_x = curVelocity.x;
                }

                Velocity = new Vector2(nextVelocity_x, curVelocity.y);
            }
        }

        /// <summary>
        /// 액터의 수직 속도 설정 메소드
        /// </summary>
        public void SetVerticalSpeed(float speed, EVerticalDirection direction)
        {
            Velocity = new Vector2(Velocity.x, (float)direction * speed);
        }

        public void AddVerticalSpeed(float maxSpeed, EVerticalDirection direction, float speed)
        {
            if (Mathf.Abs(speed) > Mathf.Epsilon)
            {
                Vector2 curVelocity = Velocity;
                float nextVelocity_y = curVelocity.y + (float)direction * speed;

                // 현재 속도가 최대 속도 이하거나, 현재 속도가 최대 속도를 초과했을때 다음 속도가 반대 방향이라면, 
                if ((curVelocity.y <= maxSpeed + Mathf.Epsilon) || (nextVelocity_y * curVelocity.y < -Mathf.Epsilon))
                {
                    nextVelocity_y = Mathf.Clamp(nextVelocity_y, -maxSpeed, maxSpeed);
                }
                // 현재 속도가 최대 속도를 초과했고, 다음 속도가 현재 속도와 같은 방향이면서, 다음 속도가 현재 속도보다 높다면, 
                else if (nextVelocity_y > curVelocity.y + Mathf.Epsilon)
                {
                    nextVelocity_y = curVelocity.y;
                }

                Velocity = new Vector2(curVelocity.x, nextVelocity_y);
            }
        }

        /// <summary>
        /// 액터 물리 기반 이동 메소드
        /// Must Be Called In FixedUpdate
        /// </summary>
        /// <param name="position"></param>
        public void MovePosition(Vector2 position)
        {
            MainRigidbody.position = position;
            mIsPhysicsUpdated = true;
        }
    }
}