﻿using UnityEngine;
using UnityEditor;


namespace RedBlackTree
{
    public class BasicStatus_Damaged : ScriptableObject
    {
        public Sprite DefaultSprite;

        public string NameTag;
        public string FlavorTag;

        public int MaxLife;
        public int MaxShield;

        public float Weight;
    }
}