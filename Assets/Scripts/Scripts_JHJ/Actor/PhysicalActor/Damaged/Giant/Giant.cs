﻿using UnityEngine;
using System.Collections;
using CameraManagement;

namespace RedBlackTree
{
    public enum EStateType_Giant
    {
        None = -1,

        // 대기
        Wait = 0,
        // 이동
        Move = 1,
        // 피격
        Hit = 2,
        // 죽음
        Die = 3,

        // 방어
        Defense = 4,

        // 소형 몬스터 공격 준비
        Attack_Prepare_Small_00 = 8,
        // 소형 몬스터 공격
        Attack_Small_00 = 9,
        // 소형 몬스터 공격 준비
        Attack_Prepare_Small_01 = 10,
        // 소형 몬스터 공격
        Attack_Small_01 = 11,
        // 중형 몬스터 공격 준비
        Attack_Prepare_Medium_00 = 12,
        // 중형 몬스터 공격
        Attack_Medium_00 = 13,
        // 중형 몬스터 공격 준비
        Attack_Prepare_Medium_01 = 14,
        // 중형 몬스터 공격
        Attack_Medium_01 = 15,
        // 대형 몬스터 공격 준비
        Attack_Prepare_Large_00 = 16,
        // 대형 몬스터 공격
        Attack_Large_00 = 17,
        // 대형 몬스터 공격 준비
        Attack_Prepare_Large_01 = 18,
        // 대형 몬스터 공격
        Attack_Large_01 = 19,

        Count
    }

    public enum ETriggerType_Giant
    {
        None = -1,

        Attackable = 0,
        Attackable_Urgent = 1,

        Defense_Late = 2,
        Defense_Perfect = 3,

        Count
    }

    public enum EAudioType_Giant
    {
        None = -1,

        Move_0 = 0,
        Move_1,
        Move_End,

        Counter_0 = 10,
        Counter_1,
        Counter_2,
        Counter_3,
        Counter_End,

        Hit_0 = 20,
        Hit_1,
        Hit_End,

        Die = 30
    }

    [RequireComponent(typeof(TriggerHandler_Giant), typeof(AttackHandler_Giant), typeof(AudioHandler_Giant))]
    public class Giant : ADamagedActor<EStateType_Giant>
    {
        public static Giant Main { get; private set; } = null;

        public sealed override EActorType ActorType { get { return EActorType.Giant; } }

        public bool InvincibleForTest { get; set; }

        public float InvincibleTime { get; private set; }

        public override bool CanDamageLife
        {
            get { return base.CanDamageLife && State != EStateType_Giant.Die && !Status.IsInvincible && !InvincibleForTest; }
        }

        public override bool CanDamageShield
        {
            get { return false; }
        }

        public override bool CanKnockback
        {
            get { return base.CanKnockback && State != EStateType_Giant.Die; }
        }

        public override bool CanHealLife
        {
            get { return base.CanHealLife && State != EStateType_Giant.Die; }
        }

        public override bool CanHealShield
        {
            get { return false; }
        }

        public override int DamageDecrement
        {
            get
            {
                PassiveItem_TyrShield item = Inventory.Get(EPassiveItem.TyrShield) as PassiveItem_TyrShield;
                if(item != null && item.IsAcquired)
                {
                    return base.DamageDecrement + item.DamageDecrement;
                }
                return base.DamageDecrement;
            }
        }

        [SerializeField]
        private Status_Giant mStatus = null;
        public new Status_Giant Status
        {
            get { return mStatus; }
            set { base.Status = mStatus = value; }
        }

        [SerializeField]
        private PlayerCommand mCommand = null;
        public PlayerCommand Command
        {
            get { return mCommand; }
        }

        [SerializeField]
        private Inventory mInventory = null;
        public Inventory Inventory
        {
            get { return mInventory; }
        }

        [SerializeField]
        private PlayerStatus mPlayerStatus = null;
        public PlayerStatus PlayerStatus { get { return mPlayerStatus; } }

        [SerializeField]
        private Animator mDefenseLineAnimator = null;
        public Animator DefenseLineAnimator { get { return mDefenseLineAnimator; } }

        private float mDefenseRemainTime;

        public IAgent_Movable Agent { get; private set; }


        public TriggerHandler_Giant TriggerHandler { get; private set; }
        public AttackHandler_Giant AttackHandler { get; private set; }
        public AudioHandler_Giant AudioHandler { get; private set; }

        private ElementalFairy[] mElementalFairies;

        public override void Awake()
        {
            base.Awake();
            // 미리 할당시키기
            Vibration.isAndroid();

            Main = this;

            Agent = new Agent_Giant();
            Status = Status;
            Status.BasicStatus = Status.BasicStatus;
            Status.Life = Status.MaxLife;

            TriggerHandler = GetComponent<TriggerHandler_Giant>();
            AttackHandler = GetComponent<AttackHandler_Giant>();
            AudioHandler = GetComponent<AudioHandler_Giant>();

            mElementalFairies = new ElementalFairy[(int)EElementalFairy.Count];
            ElementalFairy[] fairies = GetComponentsInChildren<ElementalFairy>(true);
            for (int indexOfFairy = 0; indexOfFairy < fairies.Length; indexOfFairy++)
            {
                mElementalFairies[(int)fairies[indexOfFairy].ElementalType] = fairies[indexOfFairy];
            }

            SetStates(new Giant_Wait(this)
                , new Giant_Move(this)
                , new Giant_Hit(this)
                , new Giant_Die(this)
                , new Giant_Defense(this)
                , new Giant_Attack_Prepare(this, EStateType_Giant.Attack_Prepare_Small_00)
                , new Giant_Attack(this, EStateType_Giant.Attack_Small_00)
                , new Giant_Attack_Prepare(this, EStateType_Giant.Attack_Prepare_Small_01)
                , new Giant_Attack(this, EStateType_Giant.Attack_Small_01)
                , new Giant_Attack_Prepare(this, EStateType_Giant.Attack_Prepare_Medium_00)
                , new Giant_Attack(this, EStateType_Giant.Attack_Medium_00)
                , new Giant_Attack_Prepare(this, EStateType_Giant.Attack_Prepare_Medium_01)
                , new Giant_Attack(this, EStateType_Giant.Attack_Medium_01)
                , new Giant_Attack_Prepare(this, EStateType_Giant.Attack_Prepare_Large_00)
                , new Giant_Attack(this, EStateType_Giant.Attack_Large_00)
                , new Giant_Attack_Prepare(this, EStateType_Giant.Attack_Prepare_Large_01)
                , new Giant_Attack(this, EStateType_Giant.Attack_Large_01));
        }

        public override void Start()
        {
            base.Start();

            CameraController.Main.BlackOut(1.0f);
            CameraController.Main.BlackOut(1.0f, 0.0f);
        }

        public override void Update()
        {
            base.Update();

            if (!StateManager.Pause.State)
            {
                Agent.Update();

                for (EElementalFairy e = EElementalFairy.None + 1; e < EElementalFairy.Count; e++)
                {
                    int indexOfElementalFairy = (int)e;
                    PassiveItem_ElementalFairy item = Inventory.Get(mElementalFairies[indexOfElementalFairy].PassiveItemType) as PassiveItem_ElementalFairy;
                    // Elemental Fairy를 이용하는 아이템을 습득하였고, ElementalFairy가 비활성화 상태인 경우
                    if (item.IsAcquired && !mElementalFairies[indexOfElementalFairy].IsActivated)
                    {
                        mElementalFairies[indexOfElementalFairy].Activate(item);
                    }
                    // Elemental Fairy를 이용하는 아이텝을 습득하지 않은 상태고, Elemental Fairy가 활성화된 상태인 경우
                    else if (!item.IsAcquired && mElementalFairies[indexOfElementalFairy].IsActivated)
                    {
                        mElementalFairies[indexOfElementalFairy].Deactivate();
                    }
                }

                if (mDefenseRemainTime > 0.0f)
                {
                    mDefenseRemainTime -= Time.deltaTime;
                }
                else if(Input.GetKeyDown(KeyCode.Space) && State != EStateType_Giant.Die)
                {
                    State = EStateType_Giant.Defense;
                }

                if(Status.IsInvincible)
                {
                    if(InvincibleTime > 0.0f)
                    {
                        InvincibleTime -= Time.deltaTime;
                    }
                    else
                    {
                        Status.IsInvincible = false;
                    }
                }
            }
        }

        public virtual void OnDestroy()
        {
            Main = null;
        }

        public void SetInvincible(float time)
        {
            Status.IsInvincible = true;
            InvincibleTime = time;
        }

        protected override void OnDamageLife()
        {
            base.OnDamageLife();
            PlayHitAudio();
            if (Status.Life == 0)
            {
                /*
                 * 엘릭서 패시브 아이템을 소유하고 있다면
                 * Enhancement를 1 감소시키고, 엘릭서 패시브 아이템의 생명력 재생 비율만큼
                 * 생명력을 재생시킨다.
                 */
                PassiveItem_Elixir elixir = Inventory.Get(EPassiveItem.Elixir) as PassiveItem_Elixir;
                if (elixir != null && elixir.IsAcquired)
                {
                    Status.Life = (int)(Status.MaxLife * elixir.LifeRegenerationRate);
                    Inventory.Remove(EPassiveItem.Elixir, elixir.Level * elixir.MaxProficiency + elixir.Proficiency);
                }
                else
                {
                    State = EStateType_Giant.Die;
                }
            }
        }

        protected override void OnKnockback()
        {
            base.OnKnockback();
            if (State == EStateType_Giant.Die)
            {
                Velocity = Vector2.zero;
            }
            else
            {
                State = EStateType_Giant.Hit;
            }
        }

        public bool CheckDefenseCommand()
        {
            if (mDefenseRemainTime <= Mathf.Epsilon && Command.Giant_Defense)
            {
                State = EStateType_Giant.Defense;
                return true;
            }
            return false;
        }

        public void OnDied_Animation()
        {
            MainAnimator.SetBool("die", true);
        }

        public void Defense()
        {
            bool isDefensed = false;

            CircleCollider2D innerTrigger = TriggerHandler.GetTrigger(ETriggerType_Giant.Defense_Late) as CircleCollider2D;
            Vector2 triggerPosition = innerTrigger.transform.position;

            CircleCollider2D outerTrigger = TriggerHandler.GetTrigger(ETriggerType_Giant.Defense_Perfect) as CircleCollider2D;
            Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_Giant.Defense_Perfect, out int countOfColliders);
            for (int indexOfColliders = 0; indexOfColliders < countOfColliders; indexOfColliders++)
            {
                IProjectile projectile = colliders[indexOfColliders].GetComponentInParent<IProjectile>();
                if (projectile != null && projectile.CanReflect)
                {
                    Vector2 projectileCenterPosition = projectile.CenterTransform.position;
                    float distance = Vector2.Distance(triggerPosition, projectileCenterPosition);

                    if(distance >= innerTrigger.radius && distance <= outerTrigger.radius)
                    {
                        float offenseIncrease = PlayerStatus.GetStatus(EPlayerStatus_Coefficient.ReflectOffense);
                        PassiveItem_FrigarHandMirror item = Inventory.Get(EPassiveItem.FrigarHandMirror) as PassiveItem_FrigarHandMirror;
                        if(item != null && item.IsAcquired)
                        {
                            offenseIncrease += item.ReflectOffenseIncrease;
                        }
                        projectile.OnReflectBy(this, offenseIncrease);
                        isDefensed = true;
                    }
                }
            }

            if (isDefensed)
            {
                PlayCounterAudio();
                CameraController.Main.Shake(0.2f, 1.0f);
                mDefenseLineAnimator.SetTrigger("hit");
                Vibration.Vibrate(100);
            }
            else
            {
                mDefenseRemainTime = Status.DefenseCoolDown;
            }
        }

        public void AttackOn_Animation(ETriggerType_Giant triggerType)
        {
            Collider2D[] colliders = TriggerHandler.GetColliders(triggerType, out int countOfColliders);
            bool isDamaged = false;
            for (int i = 0; i < countOfColliders; i++)
            {
                IDamaged damagable = colliders[i].GetComponentInParent<IDamaged>();

                if (damagable != null)
                {
                    float selection = Random.Range(0.0f, 1.0f);
                    if (selection < Status.CriticalChance)
                    {
                        damagable.Damage.Set(isImportant: true
                            , type: EDamageType.Life
                            , value: (int)(Status.Offense * Status.CriticalMagnification)
                            , srcForce: Status.Force
                            , isCritical: true
                            , srcActor: this
                            , srcPosition: AttackHandler.Get(triggerType).transform.position);
                    }
                    else
                    {
                        damagable.Damage.Set(isImportant: true
                            , type: EDamageType.Life
                            , value: Status.Offense
                            , srcForce: Status.Force
                            , isCritical: false
                            , srcActor: this
                            , srcPosition: AttackHandler.Get(triggerType).transform.position);
                    }

                    if (damagable.InvokeDamage())
                    {
                        isDamaged = true;
                    }
                }
            }

            if (isDamaged)
            {
                CameraController.Main.Shake(0.2f, 1.0f);
            }
        }

        public void MakeRunDust_Animation()
        {
            Effect effect = EffectPool.Get(EEffectType.RunDust) as Effect;
            if (effect != null)
            {
                effect.Activate(transform, transform.position);
            }
        }

        public bool SetAttack(bool isAttacking)
        {
            ESize targetSize = GetDamagedSize(ETriggerType_Giant.Attackable_Urgent);

            int selection = (int)Random.Range(0.0f, 2.0f);

            if (targetSize == ESize.Small)
            {
                State = EStateType_Giant.Attack_Small_00 + selection * 2;
                return true;
            }
            else if (targetSize == ESize.Medium)
            {
                State = EStateType_Giant.Attack_Medium_00 + selection * 2;
                return true;
            }
            else if (targetSize == ESize.Large || targetSize == ESize.ExtraLarge)
            {
                State = EStateType_Giant.Attack_Large_00 + selection * 2;
                return true;
            }

            // 공격 시전중이 아니라면
            if (!isAttacking)
            {
                targetSize = GetDamagedSize(ETriggerType_Giant.Attackable);


                if (targetSize == ESize.Small)
                {
                    State = EStateType_Giant.Attack_Prepare_Small_00 + selection * 2;
                    return true;
                }
                else if (targetSize == ESize.Medium)
                {
                    State = EStateType_Giant.Attack_Prepare_Medium_00 + selection * 2;
                    return true;
                }
                else if (targetSize == ESize.Large || targetSize == ESize.ExtraLarge)
                {
                    State = EStateType_Giant.Attack_Prepare_Large_00 + selection * 2;
                    return true;
                }
            }

            return false;
        }

        private ESize GetDamagedSize(ETriggerType_Giant type)
        {
            Collider2D[] colliders = TriggerHandler.GetColliders(type, out int countOfColliders);

            for (int i = 0; i < countOfColliders; i++)
            {
                Monster actor = colliders[i].GetComponentInParent<Monster>();

                if (actor != null)
                {
                    return actor.Size;
                }
            }
            return ESize.None;
        }

        private EAudioType_Giant mMoveAudio = EAudioType_Giant.Move_0;
        private void PlayMoveAudio_Animation()
        {
            AudioHandler.Get(mMoveAudio).AudioSource.Play();
            mMoveAudio = mMoveAudio + 1 < EAudioType_Giant.Move_End ? mMoveAudio + 1 : EAudioType_Giant.Move_0;
        }

        private EAudioType_Giant mCounterAudio = EAudioType_Giant.Counter_0;
        private void PlayCounterAudio()
        {
            AudioHandler.Get(mCounterAudio).AudioSource.Play();
            mCounterAudio = mCounterAudio + 1 < EAudioType_Giant.Counter_End ? mCounterAudio + 1 : EAudioType_Giant.Counter_0;
        }

        private EAudioType_Giant mHitAudio = EAudioType_Giant.Hit_0;
        private void PlayHitAudio()
        {
            AudioHandler.Get(mHitAudio).AudioSource.Play();
            mHitAudio = mHitAudio + 1 < EAudioType_Giant.Hit_End ? mHitAudio + 1 : EAudioType_Giant.Hit_0;
        }

        private void PlayDieAudio_Animation()
        {
            AudioHandler.Get(EAudioType_Giant.Die).AudioSource.Play();
        }
    }
}