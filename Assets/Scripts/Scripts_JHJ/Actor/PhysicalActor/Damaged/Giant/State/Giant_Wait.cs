﻿using UnityEngine;

namespace RedBlackTree
{
    public class Giant_Wait : State<EStateType_Giant>
    {
        private Giant mGiant;

        public Giant_Wait(Giant giant) : base(EStateType_Giant.Wait)
        {
            mGiant = giant;
        }

        public override void Start()
        {
            mGiant.MainAnimator.SetInteger("state", (int)Type);
        }

        public override void Update()
        {
            // 오른쪽으로 이동하는 입력이 감지된 경우
            if (mGiant.Agent.MoveHorizontal > 0.0f)
            {
                // 상태를 Move로 전이
                mGiant.State = EStateType_Giant.Move;
            }
        }
    }
}