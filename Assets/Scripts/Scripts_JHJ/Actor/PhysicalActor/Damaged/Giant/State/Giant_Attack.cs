﻿using UnityEngine;

namespace RedBlackTree
{
    public class Giant_Attack : State<EStateType_Giant>
    {
        private Giant mGiant;

        private float mRemainTime;
        private bool mIsStarted = false;

        public Giant_Attack(Giant giant, EStateType_Giant state) : base(state)
        {
            mGiant = giant;
        }

        public override void Start()
        {
            mRemainTime = ValueConstants.OneFrameInAnimation;
            if (!mIsStarted)
            {
                mIsStarted = true;
                mGiant.MainAnimator.SetInteger("state", (int)Type);
            }
            else
            {
                mGiant.MainAnimator.SetTrigger("attack");
            }
        }

        public override void End()
        {
            mIsStarted = false;
        }

        public override void Update()
        {
            if (mGiant.CheckDefenseCommand())
            {
                return;
            }
        }

        public override void FixedUpdate()
        {
            mGiant.SetHorizontalSpeed(mGiant.Status.MoveSpeed, EHorizontalDirection.Right);

            

            // 한프레임 대기 후 공격 체크
            if (mRemainTime > 0.0f)
            {
                mRemainTime -= Time.deltaTime;
            }
            else if (mGiant.SetAttack(true))
            {
                return;
            }
        }
    }
}