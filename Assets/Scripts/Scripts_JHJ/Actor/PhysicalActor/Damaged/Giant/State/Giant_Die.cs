﻿using UnityEngine;
using UnityEngine.SceneManagement;
using CameraManagement;

namespace RedBlackTree
{
    public class Giant_Die : State<EStateType_Giant>
    {
        private Giant mGiant;
        private float mRemainTime;
        private bool mIsBlackOutTriggered = false;

        public Giant_Die(Giant giant) : base(EStateType_Giant.Die)
        {
            mGiant = giant;
        }

        public override void Start()
        {
            mGiant.MainAnimator.SetInteger("state", (int)Type);
            mGiant.Velocity = Vector2.zero;
            mGiant.MainColliderHandler.Get(EMainColliderType.HitBox_00).gameObject.SetActive(false);
            mRemainTime = ValueConstants.MonsterDieWaitTime;
            mIsBlackOutTriggered = false;
        }

        public override void Update()
        {
           if(!mIsBlackOutTriggered)
            {
                if(mRemainTime > 0.0f)
                {
                    mRemainTime -= Time.deltaTime;
                }
                else
                {
                    mIsBlackOutTriggered = true;
                    CameraController.Main.BlackOut(1.0f, 1.0f);
                }
            }
            else if(!CameraController.Main.IsBlackOutTriggered)
            {
                StateManager.GameOver.State = true;
                if (StateManager.Scene.State == EScene.Scene_Abyss_Tutorial)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }
                else
                {
                    StateManager.Scene.State = EScene.Scene_Sanctuary;
                }
            }
        }

        public override void End()
        {
            mGiant.MainAnimator.SetBool("die", false);
            mGiant.MainColliderHandler.Get(EMainColliderType.HitBox_00).gameObject.SetActive(true);
        }
    }
}