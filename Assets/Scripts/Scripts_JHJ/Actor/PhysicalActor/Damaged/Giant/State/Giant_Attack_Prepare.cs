﻿using UnityEngine;

namespace RedBlackTree
{
    public class Giant_Attack_Prepare : State<EStateType_Giant>
    {
        private Giant mGiant;

        public Giant_Attack_Prepare(Giant giant, EStateType_Giant state) : base(state)
        {
            mGiant = giant;
        }

        public override void Start()
        {
            mGiant.MainAnimator.SetInteger("state", (int)Type);
        }

        public override void Update()
        {
            if (mGiant.CheckDefenseCommand())
            {
                return;
            }
        }

        public override void FixedUpdate()
        {
            mGiant.SetHorizontalSpeed(mGiant.Status.MoveSpeed, EHorizontalDirection.Right);

            // 애니메이션이 실행중인 상태로 공격 상태 조사
            if (mGiant.SetAttack(true))
            {
                return;
            }
        }
    }
}