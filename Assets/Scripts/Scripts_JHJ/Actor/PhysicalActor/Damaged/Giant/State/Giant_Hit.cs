﻿using UnityEngine;

namespace RedBlackTree
{
    public class Giant_Hit : State<EStateType_Giant>
    {
        private Giant mGiant;
        private bool mIsStarted;

        public Giant_Hit(Giant giant) : base(EStateType_Giant.Hit)
        {
            mGiant = giant;
        }

        public override void Start()
        {
            if(!mIsStarted)
            {
                mIsStarted = true;
                mGiant.MainAnimator.SetInteger("state", (int)Type);
            }
            else
            {
                mGiant.MainAnimator.SetTrigger("hit");
            }
        }

        public override void FixedUpdate()
        {
            Vector2 velocity = mGiant.Velocity;

            // 액터의 넉백 적용 후 속도가 한계값 미만이면
            if (Mathf.Abs(velocity.x) < ValueConstants.HitMovementThreshold
                && Mathf.Abs(velocity.y) < ValueConstants.HitMovementThreshold)
            {
                mGiant.State = EStateType_Giant.Wait;
            }
        }
    }
}