﻿using UnityEngine;

namespace RedBlackTree
{
    public class Giant_Move : State<EStateType_Giant>
    {
        private Giant mGiant;

        public Giant_Move(Giant giant) : base(EStateType_Giant.Move)
        {
            mGiant = giant;
        }

        public override void Start()
        {
            mGiant.MainAnimator.SetInteger("state", (int)Type);
        }

        public override void Update()
        {
            if (mGiant.CheckDefenseCommand())
            {
                return;
            }
        }

        public override void FixedUpdate()
        {
            bool isMoved = false;
            // 오른쪽으로 움직이는 입력이 감지된 경우
            if (mGiant.Agent.MoveHorizontal > 0.0f)
            {
                // 오른쪽으로 이동
                mGiant.SetHorizontalSpeed(mGiant.Status.MoveSpeed, EHorizontalDirection.Right);
                isMoved = true;
            }

            if(mGiant.SetAttack(false))
            {
                return;
            }

            if(!isMoved)
            {
                mGiant.State = EStateType_Giant.Wait;
                return;
            }
        }
    }
}