﻿using UnityEngine;
using System.Collections;

namespace RedBlackTree
{
    public class Giant_Defense : State<EStateType_Giant>
    {
        private Giant mGiant;
        private bool mIsStarted = false;

        public Giant_Defense(Giant giant) : base(EStateType_Giant.Defense)
        {
            mGiant = giant;
        }

        public override void Start()
        {
            mGiant.Defense();
            if (!mIsStarted)
            {
                mIsStarted = true;
                mGiant.MainAnimator.SetInteger("state", (int)Type);
            }
            else
            {
                mGiant.MainAnimator.SetTrigger("defense");
            }
        }

        public override void End()
        {
            mIsStarted = false;
        }

        public override void Update()
        {
            if (mGiant.CheckDefenseCommand())
            {
                return;
            }
        }

        public override void FixedUpdate()
        {
            mGiant.SetHorizontalSpeed(mGiant.Status.MoveSpeed, EHorizontalDirection.Right);

            if (mGiant.SetAttack(true))
            {
                return;
            }
        }
    }
}