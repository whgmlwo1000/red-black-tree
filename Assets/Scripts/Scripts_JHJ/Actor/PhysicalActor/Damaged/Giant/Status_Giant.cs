﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New Status_Giant", menuName = "Giant/Status", order = 1000)]
    public class Status_Giant : Status_Damaged
    {
        [SerializeField]
        private BasicStatus_Giant mBasicStatus;
        public new BasicStatus_Giant BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }

        public bool IsInvincible { get; set; }
        
        public virtual float MoveSpeed_Increase { get; }
        public float MoveSpeed
        {
            get { return BasicStatus.MoveSpeed * (1.0f + MoveSpeed_Increase); }
        }

        public virtual int Offense_Increment { get; }
        public virtual int Offense_Increase { get; }
        public int Offense
        {
            get { return (int)((BasicStatus.Offense + Offense_Increment) * (1.0f + Offense_Increase)); }
        }

        public virtual float Force_Increment { get; }
        public virtual float Force_Increase { get; }
        public float Force
        {
            get { return (BasicStatus.Force + Force_Increment) * (1.0f + Force_Increase); }
        }

        public virtual float CriticalChance_Increment { get; }
        public float CriticalChance
        {
            get { return BasicStatus.CriticalChance + CriticalChance_Increment; }
        }

        public virtual float CriticalMagnification_Increment { get; }
        public float CriticalMagnification
        {
            get { return BasicStatus.CriticalMagnification + CriticalMagnification_Increment; }
        }

        public float DefenseCoolDown { get { return BasicStatus.DefenseCoolDown; } }
    }
}