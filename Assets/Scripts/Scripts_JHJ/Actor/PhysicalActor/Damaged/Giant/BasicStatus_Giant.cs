﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_Giant", menuName = "Giant/Basic Status", order = 1000)]
    public class BasicStatus_Giant : BasicStatus_Damaged
    {
        public ERace Race;

        public int Offense;

        public float Force;
        public float MoveSpeed;

        public float CriticalChance;
        public float CriticalMagnification;

        public float DefenseCoolDown;
    }
}