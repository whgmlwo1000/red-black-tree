﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Giant))]
    public class GiantEditor : ADamagedActorEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            SerializedProperty commandProp = serializedObject.FindProperty("mCommand");
            SerializedProperty statusProp = serializedObject.FindProperty("mStatus");
            SerializedProperty defenseLineAnimator = serializedObject.FindProperty("mDefenseLineAnimator");
            SerializedProperty inventoryProp = serializedObject.FindProperty("mInventory");
            SerializedProperty playerStatusProp = serializedObject.FindProperty("mPlayerStatus");

            statusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Giant Status", "거인의 동적 수치 스테이터스")
                , statusProp.objectReferenceValue, typeof(Status_Giant), false);
            commandProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Player Command", "유저 입력에 의해 설정되는 커맨드")
                , commandProp.objectReferenceValue, typeof(PlayerCommand), false);
            defenseLineAnimator.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Defense Line Animator", "방어 이펙트 애니메이터")
                , defenseLineAnimator.objectReferenceValue, typeof(Animator), true);
            inventoryProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Inventory", "인벤토리")
                , inventoryProp.objectReferenceValue, typeof(Inventory), false);
            playerStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Player Status"), playerStatusProp.objectReferenceValue, typeof(PlayerStatus), false);

            serializedObject.ApplyModifiedProperties();

            return mainTransform;
        }

    }
}