﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_Giant))]
    public class BasicStatus_GiantEditor : BasicStatus_DamagedEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            SerializedProperty moveSpeedProp = serializedObject.FindProperty("MoveSpeed");
            SerializedProperty offenseProp = serializedObject.FindProperty("Offense");
            SerializedProperty forceProp = serializedObject.FindProperty("Force");
            SerializedProperty criticalChanceProp = serializedObject.FindProperty("CriticalChance");
            SerializedProperty criticalMagnificationProp = serializedObject.FindProperty("CriticalMagnification");
            SerializedProperty defenseCoolDownProp = serializedObject.FindProperty("DefenseCoolDown");

            moveSpeedProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Move Speed", "기본 이동속도"), moveSpeedProp.floatValue);
            if(moveSpeedProp.floatValue < 0.0f)
            {
                moveSpeedProp.floatValue = 0.0f;
            }

            offenseProp.intValue = EditorGUILayout.IntField(new GUIContent("Offense", "기본 공격력"), offenseProp.intValue);
            if(offenseProp.intValue < 0)
            {
                offenseProp.intValue = 0;
            }

            forceProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Force", "넉백시 밀쳐내는 힘으로 F=ma 중 F를 담당"), forceProp.floatValue);
            if(forceProp.floatValue < 0.0f)
            {
                forceProp.floatValue = 0.0f;
            }

            criticalChanceProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Critical Chance", "기본 치명타 확률"), criticalChanceProp.floatValue);
            if(criticalChanceProp.floatValue < 0.0f)
            {
                criticalChanceProp.floatValue = 0.0f;
            }
            else if(criticalChanceProp.floatValue > 1.0f)
            {
                criticalChanceProp.floatValue = 1.0f;
            }

            criticalMagnificationProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Critical Magnification", "기본 치명타 배율"), criticalMagnificationProp.floatValue);
            if(criticalMagnificationProp.floatValue < 1.0f)
            {
                criticalMagnificationProp.floatValue = 1.0f;
            }

            defenseCoolDownProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Defense Cool Down", "방어 재사용 대기시간"), defenseCoolDownProp.floatValue);
            if(defenseCoolDownProp.floatValue < 0.0f)
            {
                defenseCoolDownProp.floatValue = 0.0f;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}