﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Status_Giant))]
    public class Status_GiantEditor : Status_DamagedEditor
    {
        public override void SetProperties()
        {
            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");

            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Giant Basic Status", "거인 기본 수치 스테이터스")
                , basicStatusProp.objectReferenceValue, typeof(BasicStatus_Giant), false);
            serializedObject.ApplyModifiedProperties();


            Status_Giant status = target as Status_Giant;
            if(status.BasicStatus != null)
            {
                status.BasicStatus = status.BasicStatus;
                base.SetProperties();
                EditorGUILayout.LabelField(string.Format("Invincible : {0}", status.IsInvincible), EditorStyles.largeLabel);
                EditorGUILayout.LabelField(string.Format("Move Speed : {0}", status.MoveSpeed), EditorStyles.largeLabel);
                EditorGUILayout.LabelField(string.Format("Offense : {0}", status.Offense), EditorStyles.largeLabel);
                EditorGUILayout.LabelField(string.Format("Force : {0}", status.Force), EditorStyles.largeLabel);
                EditorGUILayout.LabelField(string.Format("Critical Chance : {0}", status.CriticalChance), EditorStyles.largeLabel);
                EditorGUILayout.LabelField(string.Format("Critical Magnification : {0}", status.CriticalMagnification), EditorStyles.largeLabel);
            }
        }
    }
}