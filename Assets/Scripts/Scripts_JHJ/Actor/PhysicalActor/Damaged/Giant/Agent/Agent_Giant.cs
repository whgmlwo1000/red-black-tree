﻿using UnityEngine;
using System.Collections;

namespace RedBlackTree
{
    public enum EStateType_Agent_Giant
    {
        None = -1, 

        Move, 

        Count
    }
    public class Agent_Giant : Agent_Movable<EStateType_Agent_Giant>
    {
        public Agent_Giant()
        {
            SetStates(new Agent_Giant_Move(this));
        }
    }
}