﻿using UnityEngine;

namespace RedBlackTree
{
    public class Agent_Giant_Move : StateAgent.State<EStateType_Agent_Giant>
    {
        private Agent_Giant mAgent;

        public Agent_Giant_Move(Agent_Giant agent) : base(EStateType_Agent_Giant.Move)
        {
            mAgent = agent;
        }

        public override void Update()
        {
            mAgent.MoveHorizontal = 1.0f;
            mAgent.MoveVertical = 0.0f;
        }
    }
}