﻿using UnityEngine;


namespace RedBlackTree
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class DefenseLineTrigger : MonoBehaviour
    {
        [SerializeField]
        private bool mActiveValue = false;

        public void OnTriggerEnter2D(Collider2D collision)
        {
            Giant giant = collision.GetComponentInParent<Giant>();
            if(giant != null)
            {
                giant.DefenseLineAnimator.gameObject.SetActive(mActiveValue);
                gameObject.SetActive(false);
            }
        }
    }
}