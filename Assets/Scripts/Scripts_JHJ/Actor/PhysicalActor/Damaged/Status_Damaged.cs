﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Status_Damaged : ScriptableObject
    {
        public BasicStatus_Damaged BasicStatus { get; set; }

        public Sprite DefaultSprite { get { return BasicStatus.DefaultSprite; } }
        public string Name
        {
            get
            {
                if(DataManager.HasTag(BasicStatus.NameTag))
                {
                    return DataManager.GetScript(BasicStatus.NameTag);
                }
                return StringConstants.None;
            }
        }
        public string FlavorText
        {
            get
            {
                if(DataManager.HasTag(BasicStatus.FlavorTag))
                {
                    return DataManager.GetScript(BasicStatus.FlavorTag);
                }
                return StringConstants.None;
            }
        }

        public virtual int MaxLife_Increment { get; }
        public virtual float MaxLife_Increase { get; }
        public int MaxLife
        {
            get { return (int)((BasicStatus.MaxLife + MaxLife_Increment) * (1.0f + MaxLife_Increase)); }
        }

        public virtual int MaxShield_Increment { get; }
        public virtual float MaxShield_Increase { get; }
        public int MaxShield
        {
            get { return (int)((BasicStatus.MaxShield + MaxShield_Increment) * (1.0f + MaxShield_Increase)); }
        }

        public virtual float Weight_Increment { get; }
        public virtual float Weight_Increase { get; }
        public float Weight
        {
            get { return (BasicStatus.Weight + Weight_Increment) * (1.0f + Weight_Increase); }
        }

        [SerializeField]
        private int mLife;
        public int Life
        {
            get { return mLife; }
            set { mLife = Mathf.Clamp(value, 0, MaxLife); }
        }

        [SerializeField]
        private int mShield;
        public int Shield
        {
            get { return mShield; }
            set { mShield = Mathf.Clamp(value, 0, MaxShield); }
        }
    }
}