﻿using UnityEngine;

namespace RedBlackTree
{
    public class Box_Deactivate : State<EStateType_Box>
    {
        private Box mBox;
        public Box_Deactivate(Box box) : base(EStateType_Box.Deactivate)
        {
            mBox = box;
        }

        public override void Start()
        {
            mBox.MainAnimator.SetInteger("state", (int)Type);
        }
    }
}