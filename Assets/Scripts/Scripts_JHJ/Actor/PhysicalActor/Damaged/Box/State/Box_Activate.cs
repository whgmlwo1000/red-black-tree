﻿using UnityEngine;

namespace RedBlackTree
{
    public class Box_Activate : State<EStateType_Box>
    {
        private Box mBox;

        public Box_Activate(Box box) : base(EStateType_Box.Activate)
        {
            mBox = box;
        }

        public override void Start()
        {
            mBox.MainAnimator.SetInteger("state", (int)Type);
        }
    }
}