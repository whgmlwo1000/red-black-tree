﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Status_Box : Status_Damaged
    {
        private BasicStatus_Box mBasicStatus;
        public new BasicStatus_Box BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }

        public int CountOfPassiveItems { get { return BasicStatus.PassiveItems.Length; } }

        public int Souls { get { return BasicStatus.Souls; } }

        public float Weight_Souls { get { return BasicStatus.Weight_Souls; } }

        public float Weight_DemonSouls { get { return BasicStatus.Weight_DemonSouls; } }

        public float WeightSum
        {
            get
            {
                float sum = Weight_DemonSouls + Weight_Souls;
                
                for(int indexOfItem = 0; indexOfItem < BasicStatus.PassiveItems.Length;  indexOfItem++)
                {
                    sum += GetWeightOfPassiveItem(indexOfItem);
                }
                return sum;
            }
        }


        public EPassiveItem GetPassiveItem(int index)
        {
            return BasicStatus.PassiveItems[index];
        }

        public float GetWeightOfPassiveItem(int index)
        {
            return BasicStatus.Weights_PassiveItem[index];
        }
    }
}