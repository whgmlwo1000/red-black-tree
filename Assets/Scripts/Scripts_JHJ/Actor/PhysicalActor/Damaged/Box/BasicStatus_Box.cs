﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_Box", menuName = "Box/Basic Status", order = 1000)]
    public class BasicStatus_Box : BasicStatus_Damaged
    {
        public int Souls;
        public float Weight_Souls;

        public float Weight_DemonSouls;

        public EPassiveItem[] PassiveItems;
        public float[] Weights_PassiveItem;
    }
}