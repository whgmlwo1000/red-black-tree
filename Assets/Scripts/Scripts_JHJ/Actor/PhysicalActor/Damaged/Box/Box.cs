﻿using UnityEngine;
using ObjectManagement;
using UnityEngine.SceneManagement;

namespace RedBlackTree
{
    public enum EBox
    {
        None = -1, 

        Type_00, 

        Count
    }

    public enum EStateType_Box
    {
        Activate = 0, 
        Deactivate = 1, 
    }

    public enum EAudioType_Box
    {
        None = -1, 

        Open, 
    }

    [RequireComponent(typeof(AudioHandler_Box))]
    public class Box : ADamagedActor<EStateType_Box>, IPoolable<EBox>
    {
        public override EActorType ActorType { get { return EActorType.Box; } }

        [SerializeField]
        private BasicStatus_Box mBasicStatus = null;

        private Status_Box mStatus;
        public new Status_Box Status
        {
            get { return mStatus; }
            protected set { base.Status = mStatus = value; }
        }

        public bool IsPooled { get { return !gameObject.activeSelf; } }

        [SerializeField]
        private EBox mType = EBox.None;
        public EBox Type { get { return mType; } }

        public bool IsWorldCanvas { get { return false; } }

        public AudioHandler_Box AudioHandler { get; private set; }

        public override void Awake()
        {
            base.Awake();

            Status = ScriptableObject.CreateInstance<Status_Box>();
            Status.BasicStatus = mBasicStatus;
            Status.Shield = Status.MaxShield;

            AudioHandler = GetComponent<AudioHandler_Box>();

            StateManager.AddOnExitAbyss(OnActiveSceneChanged);

            SetStates(new Box_Activate(this)
                , new Box_Deactivate(this));
        }

        public override void Start()
        {
            base.Start();

            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        public void Spawn(Vector2 position, bool verticalFlip)
        {
            transform.position = position;
            VerticalFlip = verticalFlip;
            if(VerticalFlip)
            {
                MainRigidbody.gravityScale = -1.0f;
            }
            else
            {
                MainRigidbody.gravityScale = 1.0f;
            }

            gameObject.SetActive(true);
            MainColliderHandler.Get(EMainColliderType.HitBox_00).gameObject.SetActive(true);
            Status.Shield = Status.MaxLife;
            State = EStateType_Box.Activate;
        }

        protected override void OnDamageShield()
        {
            base.OnDamageShield();
            if (Status.Shield <= 0)
            {
                PlayOpenAudio();
                MainColliderHandler.Get(EMainColliderType.HitBox_00).gameObject.SetActive(false);
                State = EStateType_Box.Deactivate;
            }
        }

        private void DropItem_Animation()
        {
            float weightSum = Status.WeightSum;

            float selection = Random.Range(0.0f, weightSum);

            if (selection < Status.Weight_Souls)
            {
                Acquisition_Souls acquisition = AcquisitionPool.Get(EAcquisition.Souls) as Acquisition_Souls;
                if (acquisition != null)
                {
                    acquisition.SetParams(Status.Souls);
                    acquisition.Activate(CenterTransform.position);
                }
                return;
            }
            selection -= Status.Weight_Souls;

            if (selection < Status.Weight_DemonSouls)
            {
                Acquisition_DemonSouls acquistiion = AcquisitionPool.Get(EAcquisition.DemonSouls) as Acquisition_DemonSouls;
                if (acquistiion != null)
                {
                    acquistiion.Activate(CenterTransform.position);
                }
                return;
            }
            selection -= Status.Weight_DemonSouls;

            for (int indexOfPassiveItem = 0; indexOfPassiveItem < Status.CountOfPassiveItems; indexOfPassiveItem++)
            {
                if (selection < Status.GetWeightOfPassiveItem(indexOfPassiveItem))
                {
                    Acquisition_PassiveItem acquisition = AcquisitionPool.Get((EAcquisition)((int)EAcquisition._PassiveItem_ + 1 + (int)Status.GetPassiveItem(indexOfPassiveItem))) as Acquisition_PassiveItem;
                    if (acquisition != null)
                    {
                        acquisition.Activate(CenterTransform.position);
                    }
                    return;
                }
                else
                {
                    selection -= Status.GetWeightOfPassiveItem(indexOfPassiveItem);
                }
            }
        }

        private void OnActiveSceneChanged()
        {
            gameObject.SetActive(false);
        }

        private void OnActiveSceneChanged(Scene src, Scene dst)
        {
            OnActiveSceneChanged();
        }

        private void PlayOpenAudio()
        {
            AudioHandler.Get(EAudioType_Box.Open).AudioSource.Play();
        }
    }
}