﻿using UnityEngine;
using UnityEditor;
using ObjectManagement;

namespace RedBlackTree
{
    [CustomEditor(typeof(BoxPool))]
    public class BoxPoolEditor : PoolEditor<EBox>
    {
    }
}