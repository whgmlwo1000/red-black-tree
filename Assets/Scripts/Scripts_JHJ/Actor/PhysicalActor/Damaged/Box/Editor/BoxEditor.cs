﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Box))]
    public class BoxEditor : ADamagedActorEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            SerializedProperty boxTypeProp = serializedObject.FindProperty("mType");
            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");

            EBox boxType = (EBox)boxTypeProp.intValue;
            boxType = (EBox)EditorGUILayout.EnumPopup(new GUIContent("Box Type", "상자 고유 타입"), boxType);
            boxTypeProp.intValue = (int)boxType;

            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Basic Status", "정적 수치 스테이터스")
                , basicStatusProp.objectReferenceValue, typeof(BasicStatus_Box), false);

            serializedObject.ApplyModifiedProperties();

            return mainTransform;
        }
    }
}