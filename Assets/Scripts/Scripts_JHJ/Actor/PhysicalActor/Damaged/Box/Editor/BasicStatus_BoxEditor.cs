﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_Box))]
    public class BasicStatus_BoxEditor : BasicStatus_DamagedEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            SerializedProperty soulsProp = serializedObject.FindProperty("Souls");
            SerializedProperty weightSoulsProp = serializedObject.FindProperty("Weight_Souls");
            SerializedProperty weightDemonSoulsProp = serializedObject.FindProperty("Weight_DemonSouls");
            SerializedProperty passiveItemProps = serializedObject.FindProperty("PassiveItems");
            SerializedProperty weightPassiveItemProps = serializedObject.FindProperty("Weights_PassiveItem");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Souls", "영혼량"));
            soulsProp.intValue = EditorGUILayout.IntField(soulsProp.intValue);
            soulsProp.intValue = Mathf.Max(soulsProp.intValue, 0);
            EditorGUILayout.LabelField(new GUIContent("Weight", "영혼이 드랍될 가중치"), GUILayout.MaxWidth(50));
            weightSoulsProp.floatValue = EditorGUILayout.FloatField(weightSoulsProp.floatValue, GUILayout.MaxWidth(50));
            weightSoulsProp.floatValue = Mathf.Max(weightSoulsProp.floatValue, 0.0f);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Demon Souls", "악마의 영혼"));
            EditorGUILayout.LabelField(new GUIContent("Weight", "악마의 영혼이 드랍될 가중치"), GUILayout.MaxWidth(50));
            weightDemonSoulsProp.floatValue = EditorGUILayout.FloatField(weightDemonSoulsProp.floatValue, GUILayout.MaxWidth(50));
            weightDemonSoulsProp.floatValue = Mathf.Max(weightDemonSoulsProp.floatValue, 0.0f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            int countOfPassiveItems = passiveItemProps.arraySize;
            countOfPassiveItems = EditorGUILayout.IntField(new GUIContent("Count Of Passive Items", "드랍될 수 있는 패시브 아이템의 수"), countOfPassiveItems);
            countOfPassiveItems = Mathf.Max(countOfPassiveItems, 0);
            if(countOfPassiveItems != passiveItemProps.arraySize || countOfPassiveItems != weightPassiveItemProps.arraySize)
            {
                passiveItemProps.arraySize = countOfPassiveItems;
                weightPassiveItemProps.arraySize = countOfPassiveItems;
                serializedObject.ApplyModifiedProperties();
            }

            for(int indexOfItem = 0; indexOfItem < passiveItemProps.arraySize; indexOfItem++)
            {
                EditorGUILayout.BeginHorizontal();
                SerializedProperty passiveItemProp = passiveItemProps.GetArrayElementAtIndex(indexOfItem);
                SerializedProperty weightPassiveItemProp = weightPassiveItemProps.GetArrayElementAtIndex(indexOfItem);

                EditorGUILayout.LabelField(new GUIContent("Passive Item Type", "드랍될 수 있는 패시브 아이템 타입"), GUILayout.MaxWidth(120));

                EPassiveItem passiveItem = (EPassiveItem)passiveItemProp.intValue;
                passiveItem = (EPassiveItem)EditorGUILayout.EnumPopup(passiveItem);
                passiveItemProp.intValue = (int)passiveItem;

                EditorGUILayout.LabelField(new GUIContent("Weight"), GUILayout.MaxWidth(50));

                weightPassiveItemProp.floatValue = EditorGUILayout.FloatField(weightPassiveItemProp.floatValue, GUILayout.MaxWidth(50));
                weightPassiveItemProp.floatValue = Mathf.Max(weightPassiveItemProp.floatValue, 0.0f);
                EditorGUILayout.EndHorizontal();
            }

            serializedObject.ApplyModifiedProperties();

        }
    }
}