﻿using UnityEngine;

namespace RedBlackTree
{
    public class Scarecrow_Hit : State<EStateType_Scarecrow>
    {
        private Scarecrow mScarecrow;
        private bool mIsStarted;

        public Scarecrow_Hit(Scarecrow scarecrow) : base(EStateType_Scarecrow.Hit)
        {
            mScarecrow = scarecrow;
            mIsStarted = false;
        }

        public override void Start()
        {
            if (!mIsStarted)
            {
                mIsStarted = true;
                mScarecrow.MainAnimator.SetInteger("state", (int)Type);
            }
            else
            {
                mScarecrow.MainAnimator.SetTrigger("hit");
            }
        }

        public override void End()
        {
            mIsStarted = false;
        }
    }
}