﻿using UnityEngine;

namespace RedBlackTree
{
    public class Scarecrow_Wait : State<EStateType_Scarecrow>
    {
        private Scarecrow mScarecrow;

        public Scarecrow_Wait(Scarecrow scarecrow) : base(EStateType_Scarecrow.Wait)
        {
            mScarecrow = scarecrow;
        }

        public override void Start()
        {
            mScarecrow.MainAnimator.SetInteger("state", (int)Type);
        }
    }
}