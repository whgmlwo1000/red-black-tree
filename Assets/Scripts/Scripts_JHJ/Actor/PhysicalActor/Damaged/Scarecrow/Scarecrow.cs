﻿using UnityEngine;

namespace RedBlackTree
{
    public enum EStateType_Scarecrow
    {
        Wait, 
        Hit
    }

    public class Scarecrow : ADamagedActor<EStateType_Scarecrow>
    {
        public static Scarecrow Main { get; private set; }

        public bool IsInvincible { get; private set; }
        private float mRemainTime_Invincible;

        public override EActorType ActorType { get { return EActorType.Monster; } }

        public override bool CanDamageShield { get { return base.CanDamageShield && !IsInvincible; } }

        public override void Awake()
        {
            base.Awake();

            Main = this;
            Status = ScriptableObject.CreateInstance<Status_Damaged>();
            Status.BasicStatus = ScriptableObject.CreateInstance<BasicStatus_Damaged>();
            Status.BasicStatus.MaxShield = 9999;
            Status.BasicStatus.MaxLife = 9999;
            Status.Shield = Status.MaxShield;
            Status.Life = Status.MaxLife;
            Status.BasicStatus.Weight = 1.0f;

            SetStates(new Scarecrow_Wait(this)
                , new Scarecrow_Hit(this));
        }

        public override void Update()
        {
            if (!StateManager.Pause.State)
            {
                base.Update();
                if(IsInvincible)
                {
                    if(mRemainTime_Invincible > 0.0f)
                    {
                        mRemainTime_Invincible -= Time.deltaTime;
                    }
                    else
                    {
                        IsInvincible = false;
                    }
                }
            }
        }

        public void OnDestroy()
        {
            Main = null;
        }

        public void SetInvincible(float time)
        {
            IsInvincible = true;
            mRemainTime_Invincible = time;
        }

        protected override void OnDamageShield()
        {
            base.OnDamageShield();
            Status.Shield = Status.MaxShield;
            State = EStateType_Scarecrow.Hit;
        }

        private void SetWait_Animation()
        {
            State = EStateType_Scarecrow.Wait;
        }
    }
}