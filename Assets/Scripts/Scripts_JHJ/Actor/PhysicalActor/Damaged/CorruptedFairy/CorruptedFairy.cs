﻿using UnityEngine;
using ObjectManagement;
using UnityEngine.SceneManagement;
using TouchManagement;
using CameraManagement;

namespace RedBlackTree
{
    public enum ECorruptedFairy
    {
        None = -1, 
        CorruptedFairy = 0
    }

    public enum EStateType_CorruptedFairy
    {
        Activate, 
        Die
    }

    public enum EAudioType_CorruptedFairy
    {
        None = -1, 

        Move = 0
    }

    [RequireComponent(typeof(Deactivator), typeof(TouchBox), typeof(AudioHandler_CorruptedFairy))]
    public class CorruptedFairy : ADamagedActor<EStateType_CorruptedFairy>, IPoolable<ECorruptedFairy>
    {
        public override EActorType ActorType { get { return EActorType.CorruptedFairy; } }

        public bool IsWorldCanvas { get { return false; } }

        public bool IsPooled { get { return !gameObject.activeSelf; } }

        [SerializeField]
        private ECorruptedFairy mType = ECorruptedFairy.None;
        public ECorruptedFairy Type { get { return mType; } }

        [SerializeField]
        private int mCountOfAnimations = 1;
        public int CountOfAnimations { get { return mCountOfAnimations; } }

        public AudioHandler_CorruptedFairy AudioHandler { get; private set; }

        public ITouchReceiver TouchReceiver { get; private set; }

        private Status_CorruptedFairy mStatus;
        public new Status_CorruptedFairy Status
        {
            get { return mStatus; }
            protected set
            {
                value.BasicStatus = mBasicStatus;
                base.Status = mStatus = value;
            }
        }

        [SerializeField]
        private BasicStatus_CorruptedFairy mBasicStatus = null;

        public override void Awake()
        {
            base.Awake();

            Status = ScriptableObject.CreateInstance<Status_CorruptedFairy>();

            AudioHandler = GetComponent<AudioHandler_CorruptedFairy>();
            TouchReceiver = GetComponent<ITouchReceiver>();

            StateManager.AddOnExitAbyss(OnActiveSceneChanged);
            TouchReceiver.OnTouchStart += OnTouchStart;

            StateManager.Pause.AddEnterEvent(true, OnPauseEnter);
            StateManager.Pause.AddExitEvent(true, OnPauseExit);

            SetStates(new CorruptedFairy_Activate(this)
                , new CorruptedFairy_Die(this));
        }

        public override void Start()
        {
            base.Start();

            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        public void Spawn()
        {
            transform.position = VerticalCast.Position;
            gameObject.SetActive(true);

            Status.Shield = Status.MaxShield;
            MainAnimator.SetInteger("damaged", 0);
            State = EStateType_CorruptedFairy.Activate;
        }

        public float FollowCamera(float prevCameraPosition_x)
        {
            float cameraPosition_x = CameraController.Main.Position.x;
            Vector2 curPosition = MainRigidbody.position;
            float cameraDeltaPosition_x = cameraPosition_x - prevCameraPosition_x;
            MainRigidbody.position = new Vector2(curPosition.x + cameraDeltaPosition_x, curPosition.y);
            return cameraPosition_x;
        }

        protected override void OnDamageShield()
        {
            base.OnDamageShield();
            if (Status.Shield <= 0)
            {
                State = EStateType_CorruptedFairy.Die;
            }
            else
            {
                float maxShield = Status.MaxShield;
                int shield = Status.Shield;
                float slice = maxShield / mCountOfAnimations;

                for (int indexOfDamaged = 0; indexOfDamaged < mCountOfAnimations - 1; indexOfDamaged++)
                {
                    if (shield < maxShield && shield > maxShield - slice)
                    {
                        MainAnimator.SetInteger("damaged", indexOfDamaged);
                        break;
                    }
                    else
                    {
                        maxShield -= slice;
                    }
                }
            }
        }

        private void OnTouchStart(Touch touch, Vector2 worldPosition)
        {
            Damage.Set(true
                , EDamageType.Shield
                , Fairy.Main.Status.Offense
                , 0.0f
                , false
                , Fairy.Main
                , worldPosition);
            InvokeDamage();
            Effect effect = EffectPool.Get(EEffectType.CorruptedFairy);
            if (effect != null)
            {
                effect.Activate(null, worldPosition);
            }
        }

        private void OnActiveSceneChanged()
        {
            gameObject.SetActive(false);
        }

        private void OnActiveSceneChanged(Scene src, Scene dst)
        {
            OnActiveSceneChanged();
        }

        private void Deactivate_Animation()
        {
            gameObject.SetActive(false);
        }

        private void OnPauseEnter()
        {
            if(gameObject.activeInHierarchy && State == EStateType_CorruptedFairy.Activate)
            {
                AudioHandler.Get(EAudioType_CorruptedFairy.Move).AudioSource.Stop();
            }
        }

        private void OnPauseExit()
        {
            if(gameObject.activeInHierarchy && State == EStateType_CorruptedFairy.Activate)
            {
                AudioHandler.Get(EAudioType_CorruptedFairy.Move).AudioSource.Play();
            }
        }
    }
}