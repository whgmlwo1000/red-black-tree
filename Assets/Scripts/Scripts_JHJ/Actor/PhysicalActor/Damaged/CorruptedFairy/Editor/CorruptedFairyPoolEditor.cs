﻿using UnityEngine;
using UnityEditor;
using ObjectManagement;

namespace RedBlackTree
{
    [CustomEditor(typeof(CorruptedFairyPool))]
    public class CorruptedFairyPoolEditor : PoolEditor<ECorruptedFairy>
    {
        
    }
}