﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_CorruptedFairy))]
    public class BasicStatus_CorruptedFairyEditor : BasicStatus_DamagedEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            EditorGUILayout.Space();

            SerializedProperty speedProp = serializedObject.FindProperty("Speed");
            SerializedProperty cycleTimeProp = serializedObject.FindProperty("CycleTime");
            SerializedProperty cycleSpeedProp = serializedObject.FindProperty("CycleSpeed");
            SerializedProperty passiveItemProps = serializedObject.FindProperty("PassiveItems");
            SerializedProperty passiveItemWeightProps = serializedObject.FindProperty("Weights_PassiveItem");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Speed", GUILayout.MaxWidth(100));
            speedProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(speedProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Cycle Time", GUILayout.MaxWidth(100));
            cycleTimeProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(cycleTimeProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Cycle Speed", GUILayout.MaxWidth(100));
            cycleSpeedProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(cycleSpeedProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Passive Item Count", GUILayout.MaxWidth(150));
            int passiveItemCount = Mathf.Max(EditorGUILayout.IntField(passiveItemProps.arraySize, GUILayout.MaxWidth(50)), 1);
            if(passiveItemProps.arraySize != passiveItemCount || passiveItemWeightProps.arraySize != passiveItemCount)
            {
                passiveItemProps.arraySize = passiveItemCount;
                passiveItemWeightProps.arraySize = passiveItemCount;
                serializedObject.ApplyModifiedProperties();
            }
            EditorGUILayout.EndHorizontal();

            for(int indexOfProp = 0; indexOfProp < passiveItemCount; indexOfProp++)
            {
                SerializedProperty passiveItemProp = passiveItemProps.GetArrayElementAtIndex(indexOfProp);
                SerializedProperty passiveItemWeightProp = passiveItemWeightProps.GetArrayElementAtIndex(indexOfProp);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(string.Format("{0}.", indexOfProp + 1), GUILayout.MaxWidth(50));
                EPassiveItem passiveItem = (EPassiveItem)passiveItemProp.intValue;
                passiveItem = (EPassiveItem)EditorGUILayout.EnumPopup(passiveItem, GUILayout.MaxWidth(150));
                passiveItemProp.intValue = (int)passiveItem;

                EditorGUILayout.LabelField("Weight", GUILayout.MaxWidth(50));
                passiveItemWeightProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(passiveItemWeightProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);
                EditorGUILayout.EndHorizontal();
            }
        }
    }
}