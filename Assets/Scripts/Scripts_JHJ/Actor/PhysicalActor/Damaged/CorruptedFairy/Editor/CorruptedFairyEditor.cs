﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(CorruptedFairy))]
    public class CorruptedFairyEditor : ADamagedActorEditor
    {
        public override Transform SetProperties()
        {
            SerializedProperty typeProp = serializedObject.FindProperty("mType");
            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Type", GUILayout.MaxWidth(100));
            ECorruptedFairy type = (ECorruptedFairy)typeProp.intValue;
            type = (ECorruptedFairy)EditorGUILayout.EnumPopup(type, GUILayout.MaxWidth(150));
            typeProp.intValue = (int)type;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Basic Status", GUILayout.MaxWidth(100));
            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(basicStatusProp.objectReferenceValue
                , typeof(BasicStatus_CorruptedFairy), false);
            EditorGUILayout.EndHorizontal();

            Transform main = base.SetProperties();

            EditorGUILayout.Space();

            SerializedProperty animationCountProp = serializedObject.FindProperty("mCountOfAnimations");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Animation Count", GUILayout.MaxWidth(120));
            animationCountProp.intValue = Mathf.Max(EditorGUILayout.IntField(animationCountProp.intValue, GUILayout.MaxWidth(50)), 1);
            EditorGUILayout.EndHorizontal();

            return main;
        }
    }
}