﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_CorruptedFairy", menuName = "Support Fairy/Corrupted Fairy Basic Status", order = 1000)]
    public class BasicStatus_CorruptedFairy : BasicStatus_Damaged
    {
        public float Speed;
        public float CycleTime;
        public float CycleSpeed;

        public EPassiveItem[] PassiveItems;
        public float[] Weights_PassiveItem;
    }
}