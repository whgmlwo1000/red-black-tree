﻿using UnityEngine;
using UnityEditor;


namespace RedBlackTree
{
    public class Status_CorruptedFairy : Status_Damaged
    {
        private BasicStatus_CorruptedFairy mBasicStatus;
        public new BasicStatus_CorruptedFairy BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }

        public float Speed { get { return mBasicStatus.Speed; } }
        public float CycleTime { get { return mBasicStatus.CycleTime; } }
        public float CycleSpeed { get { return mBasicStatus.CycleSpeed; } }

        public int CountOfPassiveItems { get { return mBasicStatus.PassiveItems.Length; } }

        public float SumOfWeights
        {
            get
            {
                float sum = 0.0f;
                for(int indexOfItem = 0; indexOfItem < CountOfPassiveItems; indexOfItem++)
                {
                    sum += GetWeight(indexOfItem);
                }
                return sum;
            }
        }

        public EPassiveItem GetItem(int index)
        {
            return mBasicStatus.PassiveItems[index];
        }

        public float GetWeight(int index)
        {
            return mBasicStatus.Weights_PassiveItem[index];
        }
    }
}