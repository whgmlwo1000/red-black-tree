﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class CorruptedFairy_Activate : State<EStateType_CorruptedFairy>
    {
        private CorruptedFairy mFairy;
        private EVerticalDirection mVerticalDirection;
        private float mRemainTime_Cycle;
        private float mPrevCameraPosition_x;

        public CorruptedFairy_Activate(CorruptedFairy fairy) : base(EStateType_CorruptedFairy.Activate)
        {
            mFairy = fairy;
        }

        public override void Start()
        {
            mFairy.AudioHandler.Get(EAudioType_CorruptedFairy.Move).AudioSource.Play();
            mFairy.MainRigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
            mFairy.MainAnimator.SetInteger("state", (int)Type);
            mFairy.TouchReceiver.enabled = true;

            mRemainTime_Cycle = mFairy.Status.CycleTime;

            int selection = (int)Random.Range(0.0f, 2.0f);
            if (selection == 0)
            {
                mVerticalDirection = EVerticalDirection.Down;
                mFairy.Velocity = new Vector2(-mFairy.Status.Speed, -mFairy.Status.CycleSpeed * mFairy.Status.CycleTime * 0.5f);
            }
            else
            {
                mVerticalDirection = EVerticalDirection.Up;
                mFairy.Velocity = new Vector2(-mFairy.Status.Speed, mFairy.Status.CycleSpeed * mFairy.Status.CycleTime * 0.5f);
            }

            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }

        public override void End()
        {
            mFairy.AudioHandler.Get(EAudioType_CorruptedFairy.Move).AudioSource.Stop();
        }

        public override void FixedUpdate()
        {
            Vector2 curVelocity = mFairy.Velocity;

            if (mRemainTime_Cycle > 0.0f)
            {
                mRemainTime_Cycle -= Time.fixedDeltaTime;

                if (mVerticalDirection == EVerticalDirection.Down)
                {
                    mFairy.Velocity = new Vector2(curVelocity.x, curVelocity.y + mFairy.Status.CycleSpeed * Time.fixedDeltaTime);
                }
                else
                {
                    mFairy.Velocity = new Vector2(curVelocity.x, curVelocity.y - mFairy.Status.CycleSpeed * Time.fixedDeltaTime);
                }
            }
            else
            {
                mRemainTime_Cycle += mFairy.Status.CycleTime - Time.fixedDeltaTime;

                if (mVerticalDirection == EVerticalDirection.Down)
                {
                    mVerticalDirection = EVerticalDirection.Up;
                    mFairy.Velocity = new Vector2(curVelocity.x, curVelocity.y - mFairy.Status.CycleSpeed * Time.fixedDeltaTime);
                }
                else
                {
                    mVerticalDirection = EVerticalDirection.Down;
                    mFairy.Velocity = new Vector2(curVelocity.x, curVelocity.y + mFairy.Status.CycleSpeed * Time.fixedDeltaTime);
                }
            }

            mPrevCameraPosition_x = mFairy.FollowCamera(mPrevCameraPosition_x);
        }
    }
}