﻿using UnityEngine;

namespace RedBlackTree
{
    public class CorruptedFairy_Die : State<EStateType_CorruptedFairy>
    {
        private CorruptedFairy mFairy;

        public CorruptedFairy_Die(CorruptedFairy fairy) : base(EStateType_CorruptedFairy.Die)
        {
            mFairy = fairy;
        }

        public override void Start()
        {
            mFairy.MainRigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
            mFairy.MainAnimator.SetInteger("state", (int)Type);
            mFairy.TouchReceiver.enabled = false;

            float weightSum = mFairy.Status.SumOfWeights;
            float selection = Random.Range(0.0f, weightSum);
            for(int indexOfItem = 0; indexOfItem < mFairy.Status.CountOfPassiveItems; indexOfItem++)
            {
                if(selection < mFairy.Status.GetWeight(indexOfItem))
                {
                    Acquisition_PassiveItem acquisition = AcquisitionPool.Get(EAcquisition._PassiveItem_ + (int)mFairy.Status.GetItem(indexOfItem) + 1) as Acquisition_PassiveItem;
                    if(acquisition != null)
                    {
                        acquisition.Activate(mFairy.CenterTransform.position);
                    }
                    break;
                }
                selection -= mFairy.Status.GetWeight(indexOfItem);
            }
        }
    }
}