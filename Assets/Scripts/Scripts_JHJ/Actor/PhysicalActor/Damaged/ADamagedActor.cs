﻿using UnityEngine;

namespace RedBlackTree
{
    public interface IDamagedActor : IPhysicalActor, IDamaged
    {
        bool CanKnockback { get; }
        bool CanDamageLife { get; }
        bool CanDamageShield { get; }
        bool CanHealLife { get; }
        bool CanHealShield { get; }

        int DamageDecrement { get; }

        Status_Damaged Status { get; set; }
    }

    public abstract class ADamagedActor<EStateType> : APhysicalActor<EStateType>, IDamagedActor
        where EStateType : System.Enum
    {
        public Damage Damage { get; private set; }

        public Heal Heal { get; private set; }

        public virtual bool CanKnockback { get { return !Mathf.Approximately(Damage.SrcForce, 0.0f); } }
        public virtual bool CanDamageLife { get { return Status.Shield == 0 && Status.Life > 0; } }
        public virtual bool CanDamageShield { get { return Status.Shield > 0; } }
        public virtual bool CanHealShield { get { return Status.Shield > 0; } }
        public virtual bool CanHealLife { get { return Status.Life > 0; } }

        public virtual int DamageDecrement { get; }

        public Status_Damaged Status { get; set; }

        private float mInvincibleRemainTime;
        private float mHealRemainTime;
        private float mSpriteColorEffectTime;
        private bool mIsSpriteColorEffect;

        public override void Awake()
        {
            base.Awake();

            Damage = new Damage();
            Heal = new Heal();
        }

        public override void Update()
        {
            if (!StateManager.Pause.State)
            {
                base.Update();
                if (mInvincibleRemainTime > 0.0f)
                {
                    mInvincibleRemainTime -= Time.deltaTime;
                }

                if (mHealRemainTime > 0.0f)
                {
                    mHealRemainTime -= Time.deltaTime;
                }

                if (mIsSpriteColorEffect)
                {
                    if (mSpriteColorEffectTime > 0.0f)
                    {
                        mSpriteColorEffectTime -= Time.deltaTime;
                    }
                    else
                    {
                        mIsSpriteColorEffect = false;
                        SpriteRenderer renderer = MainRendererHandler.GetRenderer(0);
                        float alpha = renderer.color.a;
                        renderer.color = new Color(1.0f, 1.0f, 1.0f, alpha);
                    }
                }
            }
        }

        public bool InvokeDamage()
        {
            if (!Damage.IsImportant && mInvincibleRemainTime > 0.0f)
            {
                return false;
            }
            
            bool isDamaged = false;
            mInvincibleRemainTime = ValueConstants.InvincibleTimeByHit;
            if (Damage.Type == EDamageType.Shield && CanDamageShield)
            {
                isDamaged = true;
                int damage = Mathf.Max(Damage.Value - DamageDecrement, 1);
                Status.Shield -= damage;
                OnDamageShield();

                Effect_Damage effect;

                if (Damage.IsCritical)
                {
                    effect = EffectPool.Get(EEffectType.CriticalDamage) as Effect_Damage;
                }
                else
                {
                    effect = EffectPool.Get(EEffectType.NormalDamage) as Effect_Damage;
                }

                if (effect != null)
                {
                    effect.SetParams(damage);
                    effect.Activate(null, CenterTransform.position);
                }

                mIsSpriteColorEffect = true;
                mSpriteColorEffectTime = ValueConstants.SpriteHitEffectTime;
                SpriteRenderer renderer = MainRendererHandler.GetRenderer(0);
                float alpha = renderer.color.a;
                renderer.color = new Color(1.0f, 0.0f, 0.0f, alpha);
            }
            else if (Damage.Type == EDamageType.Life && CanDamageLife)
            {
                isDamaged = true;
                int damage = Mathf.Max(Damage.Value - DamageDecrement, 1);
                Status.Life -= damage;
                OnDamageLife();
                Effect_Damage effect;

                if (Damage.IsCritical)
                {
                    effect = EffectPool.Get(EEffectType.CriticalDamage) as Effect_Damage;
                }
                else
                {
                    effect = EffectPool.Get(EEffectType.NormalDamage) as Effect_Damage;
                }

                if (effect != null)
                {
                    effect.SetParams(damage);
                    effect.Activate(null, CenterTransform.position);
                }

                mIsSpriteColorEffect = true;
                mSpriteColorEffectTime = ValueConstants.SpriteHitEffectTime;
                SpriteRenderer renderer = MainRendererHandler.GetRenderer(0);
                float alpha = renderer.color.a;
                renderer.color = new Color(1.0f, 0.0f, 0.0f, alpha);
            }

            if (CanKnockback)
            {
                isDamaged = true;
                float acceleration = Damage.SrcForce / Status.Weight;
                Vector2 centerPosition = CenterTransform.position;
                Vector2 srcPosition = Damage.SrcPosition;
                Vector2 forceDirection = centerPosition - srcPosition;
                Vector2 normalizedForceDirection = forceDirection.normalized;

                Velocity = normalizedForceDirection * acceleration;
                OnKnockback();
            }


            return isDamaged;
        }

        public bool InvokeHeal()
        {
            if (!Heal.IsImportant && mHealRemainTime > 0.0f)
            {
                return false;
            }

            bool isHealed = false;
            mHealRemainTime = ValueConstants.InvincibleTimeByHit;

            if (Heal.Type == EDamageType.Life && CanHealLife)
            {
                isHealed = true;

                Effect_Damage effect;

                int value = Heal.Value + (int)(Status.MaxLife * Heal.RateValue);
                Status.Life += value;
                OnHealLife();
                effect = EffectPool.Get(EEffectType.NormalHeal) as Effect_Damage;
                if (effect != null)
                {
                    effect.SetParams(value);
                    effect.Activate(null, CenterTransform.position);
                }

                mIsSpriteColorEffect = true;
                mSpriteColorEffectTime = ValueConstants.SpriteHitEffectTime;
                SpriteRenderer renderer = MainRendererHandler.GetRenderer(0);
                float alpha = renderer.color.a;
                renderer.color = new Color(0.0f, 1.0f, 0.0f, alpha);
            }
            else if (Heal.Type == EDamageType.Shield && CanHealShield)
            {
                isHealed = true;

                Effect_Damage effect;

                int value = Heal.Value + (int)(Status.MaxShield * Heal.RateValue);
                Status.Shield += value;
                OnHealShield();

                effect = EffectPool.Get(EEffectType.NormalHeal) as Effect_Damage;
                if (effect != null)
                {
                    effect.SetParams(value);
                    effect.Activate(null, CenterTransform.position);
                }

                mIsSpriteColorEffect = true;
                mSpriteColorEffectTime = ValueConstants.SpriteHitEffectTime;
                SpriteRenderer renderer = MainRendererHandler.GetRenderer(0);
                float alpha = renderer.color.a;
                renderer.color = new Color(0.0f, 1.0f, 0.0f, alpha);
            }

            return isHealed;
        }

        protected virtual void OnDamageShield() { }
        protected virtual void OnDamageLife() { }
        protected virtual void OnHealShield() { }
        protected virtual void OnHealLife() { }
        protected virtual void OnKnockback() { }
    }
}