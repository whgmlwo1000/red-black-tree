﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class BasicStatus_DamagedEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SetProperties();
            serializedObject.ApplyModifiedProperties();
        }
        public virtual void SetProperties()
        {
            ShowDamagableProperty();
        }

        public void ShowDamagableProperty()
        {
            SerializedProperty defaultSpriteProp = serializedObject.FindProperty("DefaultSprite");
            SerializedProperty nameTagProp = serializedObject.FindProperty("NameTag");
            SerializedProperty flavorTagProp = serializedObject.FindProperty("FlavorTag");
            SerializedProperty maxLifeProp = serializedObject.FindProperty("MaxLife");
            SerializedProperty maxShieldProp = serializedObject.FindProperty("MaxShield");
            SerializedProperty weightProp = serializedObject.FindProperty("Weight");

            defaultSpriteProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Default Sprite", "기본 스프라이트"), defaultSpriteProp.objectReferenceValue, typeof(Sprite), false) as Sprite;

            nameTagProp.stringValue = EditorGUILayout.TextField(new GUIContent("Name Tag", "이름 문자열 태그"), nameTagProp.stringValue);
            if(DataManager.HasTag(nameTagProp.stringValue))
            {
                EditorGUILayout.HelpBox(DataManager.GetScript(nameTagProp.stringValue), MessageType.None);
            }
            else
            {
                EditorGUILayout.HelpBox(StringConstants.None, MessageType.None);
            }

            flavorTagProp.stringValue = EditorGUILayout.TextField(new GUIContent("Flavor Text Tag", "플레이버 텍스트 태그"), flavorTagProp.stringValue);
            if(DataManager.HasTag(flavorTagProp.stringValue))
            {
                EditorGUILayout.HelpBox(DataManager.GetScript(flavorTagProp.stringValue), MessageType.None);
            }
            else
            {
                EditorGUILayout.HelpBox(StringConstants.None, MessageType.None);
            }

            maxLifeProp.intValue = EditorGUILayout.IntField(new GUIContent("Max Life", "기본 최대 생명력"), maxLifeProp.intValue);
            if(maxLifeProp.intValue < 1)
            {
                maxLifeProp.intValue = 1;
            }

            maxShieldProp.intValue = EditorGUILayout.IntField(new GUIContent("Max Shield", "기본 최대 실드"), maxShieldProp.intValue);
            if (maxShieldProp.intValue < 0)
            {
                maxShieldProp.intValue = 0;
            }

            weightProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Weight", "넉백 시 날라가는 정도로 F=ma 중 m을 의미"), weightProp.floatValue);
            if(weightProp.floatValue < 1.0f)
            {
                weightProp.floatValue = 1.0f;
            }
        }
    }
}