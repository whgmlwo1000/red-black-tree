﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Status_DamagedEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SetProperties();
        }

        public virtual void SetProperties()
        {
            ShowDamagableProperty();
        }

        public void ShowDamagableProperty()
        {
            Status_Damaged status = target as Status_Damaged;
            if(status.BasicStatus != null)
            {
                SerializedProperty lifeProp = serializedObject.FindProperty("mLife");
                SerializedProperty shieldProp = serializedObject.FindProperty("mShield");

                EditorGUILayout.LabelField(string.Format("Max Life : {0}", status.MaxLife), EditorStyles.largeLabel);
                lifeProp.intValue = EditorGUILayout.IntSlider("Life", lifeProp.intValue, 0, status.MaxLife);

                EditorGUILayout.LabelField(string.Format("Max Shield : {0}", status.MaxShield), EditorStyles.largeLabel);
                shieldProp.intValue = EditorGUILayout.IntSlider("Shield", shieldProp.intValue, 0, status.MaxShield);

                EditorGUILayout.LabelField(string.Format("Weight : {0}", status.Weight), EditorStyles.largeLabel);

                serializedObject.ApplyModifiedProperties();
            }
        }
    }
}