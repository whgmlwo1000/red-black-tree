﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class Monster_GlobalHeal : State<EStateType_Monster>
    {
        private Monster mMonster;
        private float mPrevCameraPosition_x;

        public Monster_GlobalHeal(Monster monster) : base(EStateType_Monster.GlobalHeal)
        {
            mMonster = monster;
        }

        public override void Start()
        {
            mMonster.MainAnimator.SetInteger("state", (int)Type);
            mMonster.MainAnimator.SetBool("cast", true);
            mMonster.IsCasting = true;

            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }

        public override void End()
        {
            mMonster.CurrentSkill = null;
        }

        public override void FixedUpdate()
        {
            mPrevCameraPosition_x = mMonster.FollowCamera_x(mPrevCameraPosition_x);
        }
    }
}