﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class Monster_Exhausted : State<EStateType_Monster>
    {
        private Monster mMonster;
        private bool mIsFall;
        private float mPrevGravityScale;
        private float mPrevCameraPosition_x;

        public Monster_Exhausted(Monster monster) : base(EStateType_Monster.Exhausted)
        {
            mMonster = monster;
        }

        public override void Start()
        {
            mMonster.MainAnimator.SetInteger("state", (int)Type);
            if(!mMonster.IsGround)
            {
                mMonster.MainAnimator.SetBool("fall", true);
                mIsFall = true;
            }
            else
            {
                mMonster.MainAnimator.SetBool("fall", false);
                mIsFall = false;
            }

            mPrevGravityScale = mMonster.MainRigidbody.gravityScale;
            mMonster.MainRigidbody.gravityScale = 1.0f;
            mMonster.MainColliderHandler.Get(EMainColliderType.HitBox_00).Collider.isTrigger = true;

            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }

        public override void End()
        {
            mMonster.MainColliderHandler.Get(EMainColliderType.HitBox_00).Collider.isTrigger = false;
            mMonster.MainRigidbody.gravityScale = mPrevGravityScale;
        }

        public override void FixedUpdate()
        {
            if(mIsFall)
            {
                if (mMonster.IsGround)
                {
                    mIsFall = false;
                    mMonster.MainAnimator.SetBool("fall", false);
                }
                else
                {
                    mPrevCameraPosition_x = mMonster.FollowCamera_x(mPrevCameraPosition_x);
                    return;
                }
            }
            else
            {
                if(!mMonster.IsGround)
                {
                    mIsFall = true;
                    mMonster.MainAnimator.SetBool("fall", true);
                    mPrevCameraPosition_x = mMonster.FollowCamera_x(mPrevCameraPosition_x);
                    return;
                }
            }

            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }
    }
}