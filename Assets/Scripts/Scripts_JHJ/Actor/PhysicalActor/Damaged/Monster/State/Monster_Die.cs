﻿using UnityEngine;

namespace RedBlackTree
{
    public class Monster_Die : State<EStateType_Monster>
    {
        private Monster mMonster;
        private bool mDieParticleTriggered;
        private float mParticleRemainTime;
        private float mWaitRemainTime;

        private SpriteRenderer mRenderer;

        public Monster_Die(Monster monster) : base(EStateType_Monster.Die)
        {
            mMonster = monster;
        }

        public override void Start()
        {
            mDieParticleTriggered = false;
            mParticleRemainTime = ValueConstants.MonsterDieParticleTime;
            mWaitRemainTime = ValueConstants.MonsterDieWaitTime;

            mRenderer = mMonster.MainRendererHandler.GetRenderer(1);
            mRenderer.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);

            mMonster.MainColliderHandler.Get(EMainColliderType.HitBox_00).gameObject.SetActive(false);
            mMonster.MainRigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
            mRenderer.gameObject.SetActive(true);

            float selection = Random.Range(0.0f, 1.0f);
            if(selection < 0.05f)
            {
                Acquisition_DemonSouls demonSouls = AcquisitionPool.Get(EAcquisition.DemonSouls) as Acquisition_DemonSouls;
                if(demonSouls != null)
                {
                    demonSouls.Activate(mMonster.CenterTransform.position);
                }
            }
            else
            {
                Acquisition_Souls souls = AcquisitionPool.Get(EAcquisition.Souls) as Acquisition_Souls;
                if (souls != null)
                {
                    souls.SetParams(mMonster.Status.PossessedSouls);
                    souls.Activate(mMonster.CenterTransform.position);
                }
            }
        }

        public override void End()
        {
            mMonster.MainColliderHandler.Get(EMainColliderType.HitBox_00).gameObject.SetActive(true);
            mMonster.MainRigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
            mMonster.MainRendererHandler.GetRenderer(0).color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            mRenderer.gameObject.SetActive(false);
        }

        public override void Update()
        {
            if(!mDieParticleTriggered)
            {
                if(mParticleRemainTime > 0.0f)
                {
                    mParticleRemainTime -= Time.deltaTime;
                    float curTime = ValueConstants.MonsterDieParticleTime - mParticleRemainTime;
                    
                    mRenderer.color = new Color(1.0f, 1.0f, 1.0f, Mathf.Lerp(0.0f, 1.0f, curTime / ValueConstants.MonsterDieParticleTime));
                }
                else
                {
                    mDieParticleTriggered = true;
                    mRenderer.gameObject.SetActive(false);
                    mMonster.MainRendererHandler.GetRenderer(0).color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
                    mMonster.DieParticle.Play();
                }
            }
            else if(mWaitRemainTime > 0.0f)
            {
                mWaitRemainTime -= Time.deltaTime;
            }
            else
            {
                mMonster.State = EStateType_Monster.Idle;
                mMonster.gameObject.SetActive(false);
            }
        }
    }
}