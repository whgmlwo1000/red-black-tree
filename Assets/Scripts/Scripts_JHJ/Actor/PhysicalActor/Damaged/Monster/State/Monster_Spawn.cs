﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class Monster_Spawn : State<EStateType_Monster>
    {
        private Monster mMonster;
        private float mRemainTime;
        private SpriteRenderer mRenderer;
        private float mPrevCameraPosition_x;

        public Monster_Spawn(Monster monster) : base(EStateType_Monster.Spawn)
        {
            mMonster = monster;
        }

        public override void Start()
        {
            mMonster.SpawnEffect.SetActive(true);
            mRemainTime = ValueConstants.MonsterSpawnTime;
            mRenderer = mMonster.MainRendererHandler.GetRenderer(0);
            mRenderer.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
            mMonster.MainColliderHandler.Get(EMainColliderType.HitBox_00).gameObject.SetActive(false);

            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }

        public override void End()
        {
            mMonster.SpawnEffect.SetActive(false);
            mMonster.MainColliderHandler.Get(EMainColliderType.HitBox_00).gameObject.SetActive(true);
            mRenderer.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        }

        public override void Update()
        {
            if (mRemainTime > 0.0f)
            {
                mRemainTime -= Time.deltaTime;
                mRenderer.color = new Color(1.0f, 1.0f, 1.0f
                    , Mathf.Lerp(0.0f, 1.0f, ValueConstants.MonsterSpawnTime - mRemainTime));
            }
            else
            {
                mMonster.State = EStateType_Monster.Idle;
            }
        }

        public override void FixedUpdate()
        {
            mPrevCameraPosition_x = mMonster.FollowCamera_x(mPrevCameraPosition_x);
        }
    }
}