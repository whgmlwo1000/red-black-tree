﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class Monster_GlobalEnhance : State<EStateType_Monster>
    {
        private Monster mMonster;
        private float mEnhanceRate;
        private float mPrevCameraPosition_x;
        private bool mIsCastOver;

        public Monster_GlobalEnhance(Monster monster) : base(EStateType_Monster.GlobalEnhance)
        {
            mMonster = monster;
        }

        public override void Start()
        {
            mMonster.MainAnimator.SetInteger("state", (int)Type);
            mMonster.MainAnimator.SetBool("cast", true);
            
            // 실행 도중 끝나지 않았는데 재실행 되는경우
            if(mIsCastOver)
            {
                mIsCastOver = false;
                Monster.EnhanceRate -= mEnhanceRate;
            }

            mMonster.IsCasting = true;

            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }

        public override void End()
        {
            mMonster.CurrentSkill = null;
            if (mIsCastOver)
            {
                mIsCastOver = false;
                Monster.EnhanceRate -= mEnhanceRate;
            }
        }

        public override void Update()
        {
            if(!mIsCastOver && !mMonster.IsCasting)
            {
                mIsCastOver = true;

                ActiveSkill_Monster_GlobalEnhance skill = mMonster.CurrentSkill as ActiveSkill_Monster_GlobalEnhance;
                Monster.EnhanceRate += mEnhanceRate = skill.EnhanceRate;
            }
        }

        public override void FixedUpdate()
        {
            mPrevCameraPosition_x = mMonster.FollowCamera_x(mPrevCameraPosition_x);
        }
    }
}