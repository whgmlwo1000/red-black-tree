﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class Monster_FireStraight : State<EStateType_Monster>
    {
        private Monster mMonster;
        private float mPrevCameraPosition_x;

        public Monster_FireStraight(Monster monster) : base(EStateType_Monster.FireStraight)
        {
            mMonster = monster;
        }

        public override void Start()
        {
            mMonster.MainAnimator.SetInteger("state", (int)Type);
            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }

        public override void End()
        {
            mMonster.CurrentSkill = null;
        }

        public override void FixedUpdate()
        {
            mPrevCameraPosition_x = mMonster.FollowCamera_x(mPrevCameraPosition_x);
        }
    }
}