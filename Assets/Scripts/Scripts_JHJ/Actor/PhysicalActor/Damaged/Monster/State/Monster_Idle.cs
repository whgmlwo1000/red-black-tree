﻿using UnityEngine;
using CameraManagement;

namespace RedBlackTree
{
    public class Monster_Idle : State<EStateType_Monster>
    {
        private Monster mMonster;
        private float mPrevCameraPosition_x;

        public Monster_Idle(Monster monster) : base(EStateType_Monster.Idle)
        {
            mMonster = monster;
        }

        public override void Start()
        {
            mMonster.MainAnimator.SetInteger("state", (int)Type);
            mPrevCameraPosition_x = CameraController.Main.Position.x;
        }

        public override void Update()
        {
            mMonster.CheckSkillCommand();
        }

        public override void FixedUpdate()
        {
            mPrevCameraPosition_x = mMonster.FollowCamera_x(mPrevCameraPosition_x);
        }
    }
}