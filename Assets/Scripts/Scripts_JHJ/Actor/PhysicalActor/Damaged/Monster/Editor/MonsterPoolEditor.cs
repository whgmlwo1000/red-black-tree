﻿using UnityEngine;
using UnityEditor;
using ObjectManagement;

namespace RedBlackTree
{
    [CustomEditor(typeof(MonsterPool))]
    public class MonsterPoolEditor : PoolEditor<EMonster>
    {
    }
}