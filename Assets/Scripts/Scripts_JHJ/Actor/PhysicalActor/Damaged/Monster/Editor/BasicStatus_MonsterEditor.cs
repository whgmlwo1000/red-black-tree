﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_Monster))]
    public class BasicStatus_MonsterEditor : BasicStatus_DamagedEditor
    {
        public override void SetProperties()
        {
            base.SetProperties();

            SerializedProperty moveSpeedProp = serializedObject.FindProperty("MoveSpeed");
            SerializedProperty offenseProp = serializedObject.FindProperty("Offense");
            SerializedProperty possessedSoulsProp = serializedObject.FindProperty("PossessedSouls");
            SerializedProperty maxLifeIncreaseProp = serializedObject.FindProperty("MaxLife_ROI");
            SerializedProperty maxShieldIncreaseProp = serializedObject.FindProperty("MaxShield_ROI");
            SerializedProperty offenseIncreaseProp = serializedObject.FindProperty("Offense_ROI");
            SerializedProperty possessedSoulsIncreaseProp = serializedObject.FindProperty("PossessedSouls_ROI");

            moveSpeedProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(new GUIContent("Move Speed"), moveSpeedProp.floatValue), 0.0f);
            offenseProp.intValue = Mathf.Max(EditorGUILayout.IntField(new GUIContent("Offense"), offenseProp.intValue), 0);
            possessedSoulsProp.intValue = Mathf.Max(EditorGUILayout.IntField(new GUIContent("Possessed Souls"), possessedSoulsProp.intValue), 0);
            maxLifeIncreaseProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(new GUIContent("Max Life ROI"), maxLifeIncreaseProp.floatValue), 0.0f);
            maxShieldIncreaseProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(new GUIContent("Max Shield ROI"), maxShieldIncreaseProp.floatValue), 0.0f);
            offenseIncreaseProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(new GUIContent("Max Offense ROI"), offenseIncreaseProp.floatValue), 0.0f);
            possessedSoulsIncreaseProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(new GUIContent("Possessed Souls ROI"), possessedSoulsIncreaseProp.floatValue), 0.0f);
        }
    }
}