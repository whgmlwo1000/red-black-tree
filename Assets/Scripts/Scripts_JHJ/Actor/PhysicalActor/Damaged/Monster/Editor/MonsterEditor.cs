﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Monster))]
    public class MonsterEditor : ADamagedActorEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            SerializedProperty spawnEffectProp = serializedObject.FindProperty("mSpawnEffect");
            SerializedProperty hitParticleProp = serializedObject.FindProperty("mHitParticle");
            SerializedProperty dieParticleProp = serializedObject.FindProperty("mDieParticle");
            SerializedProperty projectileHolderProps = serializedObject.FindProperty("mProjectileHolders");
            SerializedProperty skillTableProp = serializedObject.FindProperty("mSkillTable");
            SerializedProperty enabledActiveSkillProps = serializedObject.FindProperty("mEnabledActiveSkills");
            SerializedProperty enabledPassiveSkillProps = serializedObject.FindProperty("mEnabledPassiveSkills");


            skillTableProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Skill Table"),
                skillTableProp.objectReferenceValue, typeof(SkillTable_Monster), false);
            spawnEffectProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Spawn Effect"),
                spawnEffectProp.objectReferenceValue, typeof(GameObject), true);
            hitParticleProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Hit Particle System"),
                hitParticleProp.objectReferenceValue, typeof(ParticleSystem), true);
            dieParticleProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Die Particle System"),
                dieParticleProp.objectReferenceValue, typeof(ParticleSystem), true);


            EditorGUILayout.Space();
            EditorGUILayout.LabelField(new GUIContent("Projectile Holders"), EditorStyles.boldLabel);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Projectile Holder Count"), GUILayout.MaxWidth(150));
            int holderCount = Mathf.Max(EditorGUILayout.IntField(projectileHolderProps.arraySize, GUILayout.MaxWidth(50)), 0);
            if(holderCount != projectileHolderProps.arraySize)
            {
                projectileHolderProps.arraySize = holderCount;
                serializedObject.ApplyModifiedProperties();
            }
            EditorGUILayout.EndHorizontal();

            for(int indexOfHolder = 0; indexOfHolder < holderCount; indexOfHolder++)
            {
                SerializedProperty holderProp = projectileHolderProps.GetArrayElementAtIndex(indexOfHolder);
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. ", indexOfHolder)), GUILayout.MaxWidth(50));
                holderProp.objectReferenceValue = EditorGUILayout.ObjectField(holderProp.objectReferenceValue, typeof(Transform), true);
                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.Space();
            EditorGUILayout.LabelField(new GUIContent("Enabled Active Skills"), EditorStyles.boldLabel);

            int countOfActiveSkills = (int)EActiveSkill_Monster.Count;
            if(enabledActiveSkillProps.arraySize != countOfActiveSkills)
            {
                enabledActiveSkillProps.arraySize = countOfActiveSkills;
                serializedObject.ApplyModifiedProperties();
            }

            for (int indexOfSkill = 0; indexOfSkill < countOfActiveSkills; indexOfSkill++)
            {
                SerializedProperty enabledSkillProp = enabledActiveSkillProps.GetArrayElementAtIndex(indexOfSkill);
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. {1}", indexOfSkill, (EActiveSkill_Monster)indexOfSkill)), GUILayout.MaxWidth(200));
                enabledSkillProp.boolValue = EditorGUILayout.Toggle(enabledSkillProp.boolValue);
                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.Space();
            EditorGUILayout.LabelField(new GUIContent("Enabled Passive Skills"), EditorStyles.boldLabel);

            int countOfPassiveSkills = (int)EPassiveSkill_Monster.Count;
            if (enabledPassiveSkillProps.arraySize != countOfPassiveSkills)
            {
                enabledPassiveSkillProps.arraySize = countOfPassiveSkills;
                serializedObject.ApplyModifiedProperties();
            }

            for (int indexOfSkill = 0; indexOfSkill < countOfPassiveSkills; indexOfSkill++)
            {
                SerializedProperty enabledSkillProp = enabledPassiveSkillProps.GetArrayElementAtIndex(indexOfSkill);
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent(string.Format("{0}. {1}", indexOfSkill, (EPassiveSkill_Monster)indexOfSkill)), GUILayout.MaxWidth(100));
                enabledSkillProp.boolValue = EditorGUILayout.Toggle(enabledSkillProp.boolValue);
                EditorGUILayout.EndHorizontal();
            }

            return mainTransform;
        }
    }
}