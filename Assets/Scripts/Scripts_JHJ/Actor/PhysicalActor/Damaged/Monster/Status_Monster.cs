﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Status_Monster : Status_Damaged
    {
        private BasicStatus_Monster mBasicStatus;
        public new BasicStatus_Monster BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }

        public virtual float MoveSpeed_Increase { get; }
        public float MoveSpeed
        {
            get { return BasicStatus.MoveSpeed * (1.0f + MoveSpeed_Increase); }
        }

        public override float MaxLife_Increase
        {
            get { return base.MaxLife_Increase + BasicStatus.MaxLife_ROI * 0.0f; }
        }

        public override float MaxShield_Increase
        {
            get { return base.MaxShield_Increase + BasicStatus.MaxShield_ROI * 0.0f; }
        }

        public virtual int Offense_Increment { get; }
        public virtual float Offense_Increase
        {
            get { return BasicStatus.Offense_ROI * 0.0f + Monster.EnhanceRate; }
        }
        public int Offense
        {
            get { return (int)((BasicStatus.Offense + Offense_Increment) * (1.0f + Offense_Increase)); }
        }

        public virtual int PossessedSouls_Increment { get; }
        public virtual float PossessedSouls_Increase
        {
            get { return BasicStatus.PossessedSouls_ROI * 0.0f; }
        }
        public int PossessedSouls
        {
            get { return (int)((BasicStatus.PossessedSouls + PossessedSouls_Increment) * (1.0f + PossessedSouls_Increase)); }
        }
    }
}