﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Command_Monster : ScriptableObject
    {
        public float MoveHorizontal { get; set; }

        public bool[] UseSkill { get; } = new bool[(int)EActiveSkill_Monster.Count];
    }
}