﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class BasicStatus_Monster : BasicStatus_Damaged
    {
        public float MoveSpeed;
        public int Offense;

        public int PossessedSouls;

        public float MaxLife_ROI;
        public float MaxShield_ROI;
        public float Offense_ROI;
        public float PossessedSouls_ROI;
    }
}