﻿using UnityEngine;

namespace RedBlackTree
{
    public class GeneralMonster : Monster
    {
        [SerializeField]
        private EMonster mType = EMonster.None;
        public override EMonster Type { get { return mType; } }

        [SerializeField]
        private ESize mSize = ESize.None;
        public override ESize Size { get { return mSize; } }

        [SerializeField]
        private ECombatType mCombatType = ECombatType.None;
        public override ECombatType CombatType { get { return mCombatType; } }

        [SerializeField]
        private BasicStatus_GeneralMonster mBasicStatus = null;

        private Status_GeneralMonster mGeneralStatus;
        public new Status_GeneralMonster Status
        {
            get { return mGeneralStatus; }
            protected set
            {
                value.BasicStatus = mBasicStatus;
                base.Status = mGeneralStatus = value;
            }
        }

        private Command_GeneralMonster mCommand;
        public new Command_GeneralMonster Command
        {
            get { return mCommand; }
            protected set { base.Command = mCommand = value; }
        }

        private Agent_GeneralMonster mAgent;

        public float Acceleration { get { return Status.MoveSpeed * Time.fixedDeltaTime * 10.0F; } }
        public float AirResistance { get { return Status.MoveSpeed * Time.fixedDeltaTime * 2.0F; } }

        public override void Awake()
        {
            base.Awake();

            Command = ScriptableObject.CreateInstance<Command_GeneralMonster>();
            Status = ScriptableObject.CreateInstance<Status_GeneralMonster>();
            mAgent = new Agent_GeneralMonster(this);

            SetState(new GeneralMonster_Idle(this));
            SetState(new GeneralMonster_GlobalEnhance(this));
            SetState(new GeneralMonster_GlobalHeal(this));
        }

        public override void Update()
        {
            if (!StateManager.Pause.State)
            {
                base.Update();
                mAgent.Update();
            }
        }

        public override void FixedUpdate()
        {
            if (!StateManager.Pause.State)
            {
                base.FixedUpdate();
                AddResistance(AirResistance);
            }
        }

        public void CheckMoveCommand()
        {
            float moveSpeed = Status.MoveSpeed;
            Velocity = new Vector2(Command.MoveHorizontal * moveSpeed, Command.MoveVertical * moveSpeed);
        }

        public bool CheckCancelCommand()
        {
            return !Mathf.Approximately(Command.MoveHorizontal, 0.0f) || !Mathf.Approximately(Command.MoveVertical, 0.0f);
        }

        public override void Spawn()
        {
            base.Spawn();
            MainRigidbody.gravityScale = 0.0f;
        }
    }
}