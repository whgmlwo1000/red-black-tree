﻿using UnityEngine;

namespace RedBlackTree
{
    public class GeneralMonster_GlobalEnhance : Monster_GlobalEnhance
    {
        private GeneralMonster mMonster;

        public GeneralMonster_GlobalEnhance(GeneralMonster monster) : base(monster)
        {
            mMonster = monster;
        }

        public override void Update()
        {
            base.Update();

            if(mMonster.CheckCancelCommand())
            {
                mMonster.State = EStateType_Monster.Idle;
            }
        }
    }
}