﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(GeneralMonster))]
    public class GeneralMonsterEditor : MonsterEditor
    {
        public override Transform SetProperties()
        {
            SerializedProperty typeProp = serializedObject.FindProperty("mType");
            SerializedProperty sizeProp = serializedObject.FindProperty("mSize");
            SerializedProperty combatTypeProp = serializedObject.FindProperty("mCombatType");
            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");

            EMonster type = (EMonster)typeProp.intValue;
            type = (EMonster)EditorGUILayout.EnumPopup(new GUIContent("Monster Type"), type);
            typeProp.intValue = (int)type;
            ESize size = (ESize)sizeProp.intValue;
            size = (ESize)EditorGUILayout.EnumPopup(new GUIContent("Size"), size);
            sizeProp.intValue = (int)size;
            ECombatType combatType = (ECombatType)combatTypeProp.intValue;
            combatType = (ECombatType)EditorGUILayout.EnumPopup(new GUIContent("Combat Type"), combatType);
            combatTypeProp.intValue = (int)combatType;

            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Basic Status"),
                basicStatusProp.objectReferenceValue, typeof(BasicStatus_GeneralMonster), false);

            return base.SetProperties();
        }
    }
}