﻿using UnityEngine;

namespace RedBlackTree
{
    public class GeneralMonster_GlobalHeal : Monster_GlobalHeal
    {
        private GeneralMonster mMonster;

        public GeneralMonster_GlobalHeal(GeneralMonster monster) : base(monster)
        {
            mMonster = monster;
        }

        public override void Update()
        {
            base.Update();

            if(mMonster.CheckCancelCommand())
            {
                mMonster.State = EStateType_Monster.Idle;
            }
        }
    }
}