﻿using UnityEngine;

namespace RedBlackTree
{
    public class Agent_GeneralMonster_Wait : StateAgent.State<EStateType_Agent_GeneralMonster>
    {
        private const float MIN_WAIT_TIME = 0.0F;
        private const float MAX_WAIT_TIME = 3.0F;
        private Agent_GeneralMonster mAgent;
        private float mRemainTime;
        private int mPrevShield;

        public Agent_GeneralMonster_Wait(Agent_GeneralMonster agent) : base(EStateType_Agent_GeneralMonster.Wait)
        {
            mAgent = agent;
        }

        public override void Start()
        {
            mPrevShield = mAgent.Monster.Status.Shield;
            mRemainTime = Random.Range(MIN_WAIT_TIME, MAX_WAIT_TIME);
            mAgent.Monster.Command.MoveHorizontal = 0.0f;
            mAgent.Monster.Command.MoveVertical = 0.0f;
            for(EActiveSkill_Monster e = EActiveSkill_Monster.None + 1; e < EActiveSkill_Monster.Count; e++)
            {
                mAgent.Monster.Command.UseSkill[(int)e] = false;
            }
        }

        public override void Update()
        {
            for(int indexOfAttackSkill = 0; indexOfAttackSkill < mAgent.AttackSkills.Length; indexOfAttackSkill++)
            {
                // 사용가능한 Attack Skill이 존재하는 경우
                if(mAgent.Monster.IsEnableSkill(mAgent.AttackSkills[indexOfAttackSkill])
                    && mAgent.Monster.GetSkill(mAgent.AttackSkills[indexOfAttackSkill]).CanUse)
                {
                    // 사용 가능한 Attack Skill 을 기억한 후, 
                    // Attack 상태로 전이
                    mAgent.SelectedSkill = mAgent.AttackSkills[indexOfAttackSkill];
                    mAgent.State = EStateType_Agent_GeneralMonster.Attack;
                    return;
                }
            }
            // 사용 가능한 Attack Skill 이 존재하지 않는다면, 
            // 사용 가능한 Buff Skill 이 존재하는지 검사
            for(int indexOfBuffSkill = 0; indexOfBuffSkill < mAgent.BuffSkills.Length; indexOfBuffSkill++)
            {
                // 사용가능한 Buff Skill 이 존재하는 경우
                if(mAgent.Monster.IsEnableSkill(mAgent.BuffSkills[indexOfBuffSkill])
                    && mAgent.Monster.GetSkill(mAgent.BuffSkills[indexOfBuffSkill]).CanUse)
                {
                    // 사용가능한 Buff Skill을 기억한 후, 
                    // Buff 상태로 전이
                    mAgent.SelectedSkill = mAgent.BuffSkills[indexOfBuffSkill];
                    mAgent.State = EStateType_Agent_GeneralMonster.Buff;
                    return;
                }
            }

            // 사용 가능한 Buff Skill이 존재하지 않는다면
            // Wait Time 검사
            if(mPrevShield > mAgent.Monster.Status.Shield)
            {
                mAgent.State = EStateType_Agent_GeneralMonster.Move;
            }
            else if(mRemainTime > 0.0f)
            {
                mRemainTime -= mAgent.DeltaTime;
            }
            else
            {
                // Wait Time을 경과한 경우 Move 상태로 전이
                mAgent.State = EStateType_Agent_GeneralMonster.Move;
            }
        }
    }
}