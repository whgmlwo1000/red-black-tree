﻿using UnityEngine;

namespace RedBlackTree
{
    public class GeneralMonster_Idle : Monster_Idle
    {
        private GeneralMonster mMonster;
        public GeneralMonster_Idle(GeneralMonster monster) : base(monster)
        {
            mMonster = monster;
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();
            mMonster.CheckMoveCommand();
        }
    }
}