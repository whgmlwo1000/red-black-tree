﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Status_GeneralMonster : Status_Monster
    {
        private BasicStatus_GeneralMonster mBasicStatus;
        public new BasicStatus_GeneralMonster BasicStatus
        {
            get { return mBasicStatus; }
            set { base.BasicStatus = mBasicStatus = value; }
        }
    }
}