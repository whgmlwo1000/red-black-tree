﻿using UnityEngine;

namespace RedBlackTree
{
    public class Agent_GeneralMonster_Attack : StateAgent.State<EStateType_Agent_GeneralMonster>
    {
        private Agent_GeneralMonster mAgent;
        private bool mIsSetCommand;
        private int mPrevShield;

        public Agent_GeneralMonster_Attack(Agent_GeneralMonster agent) : base(EStateType_Agent_GeneralMonster.Attack)
        {
            mAgent = agent;
        }

        public override void Start()
        {
            mIsSetCommand = true;
            mPrevShield = mAgent.Monster.Status.Shield;
            mAgent.Monster.Command.MoveHorizontal = 0.0f;
            mAgent.Monster.Command.MoveVertical = 0.0f;

            for (EActiveSkill_Monster skill = EActiveSkill_Monster.None + 1; skill < EActiveSkill_Monster.Count; skill++)
            {
                mAgent.Monster.Command.UseSkill[(int)skill] = false;
            }

            mAgent.Monster.Command.UseSkill[(int)mAgent.SelectedSkill] = true;
        }

        public override void Update()
        {
            if(mIsSetCommand && mAgent.Monster.CurrentSkill != null
                && mAgent.Monster.CurrentSkill.Type == mAgent.SelectedSkill)
            {
                // 원하는 스킬을 사용중인 경우 
                // 커맨드 해제
                mIsSetCommand = false;
                mAgent.Monster.Command.UseSkill[(int)mAgent.SelectedSkill] = false;
            }
            // 커맨드 해제 후 스킬이 끝나는 경우
            else if(mAgent.Monster.CurrentSkill == null)
            {
                // 피격당한 경우
                if(mAgent.Monster.Status.Shield < mPrevShield)
                {
                    // Move 상태 전이
                    mAgent.State = EStateType_Agent_GeneralMonster.Move;
                    return;
                }
                else
                {
                    mAgent.State = EStateType_Agent_GeneralMonster.Wait;
                    return;
                }
            }
        }
    }
}