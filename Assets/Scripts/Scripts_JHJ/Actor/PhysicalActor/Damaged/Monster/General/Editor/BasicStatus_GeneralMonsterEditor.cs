﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_GeneralMonster))]
    public class BasicStatus_GeneralMonsterEditor : BasicStatus_MonsterEditor
    {
    }
}