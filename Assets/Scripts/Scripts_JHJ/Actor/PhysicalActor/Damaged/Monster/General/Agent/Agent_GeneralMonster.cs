﻿using UnityEngine;
using StateAgent;

namespace RedBlackTree
{
    public enum EStateType_Agent_GeneralMonster
    {
        Wait, 
        Move, 
        Attack, 
        Buff, 
    }


    public class Agent_GeneralMonster : Agent<EStateType_Agent_GeneralMonster>
    {
        public EActiveSkill_Monster[] AttackSkills { get; private set; } =
        {
            EActiveSkill_Monster.FireStraight
        };

        public EActiveSkill_Monster[] BuffSkills { get; private set; } =
        {
            EActiveSkill_Monster.GlobalEnhance,
            EActiveSkill_Monster.GlobalHeal
        };

        public EActiveSkill_Monster[] MoveSkills { get; private set; } =
        {
            EActiveSkill_Monster.Blink
        };

        public EActiveSkill_Monster SelectedSkill { get; set; }

        public GeneralMonster Monster { get; private set; }

        public Agent_GeneralMonster(GeneralMonster monster)
        {
            Monster = monster;

            SetStates(new Agent_GeneralMonster_Wait(this)
                , new Agent_GeneralMonster_Move(this)
                , new Agent_GeneralMonster_Attack(this)
                , new Agent_GeneralMonster_Buff(this));
        }
    }
}