﻿using UnityEngine;

namespace RedBlackTree
{
    public class Agent_GeneralMonster_Move : StateAgent.State<EStateType_Agent_GeneralMonster>
    {
        private Agent_GeneralMonster mAgent;
        private float mRemainTime;
        private bool mIsEnableMoveSkill;
        private bool mIsSetSkillCommand;

        public Agent_GeneralMonster_Move(Agent_GeneralMonster agent) : base(EStateType_Agent_GeneralMonster.Move)
        {
            mAgent = agent;
        }

        public override void Start()
        {
            Vector2 dstPosition = MonsterSpace.GetRandomPosition();
            mIsEnableMoveSkill = false;

            for(int indexOfSkill = 0; indexOfSkill < mAgent.MoveSkills.Length; indexOfSkill++)
            {
                if(mAgent.Monster.IsEnableSkill(mAgent.MoveSkills[indexOfSkill])
                    && mAgent.Monster.GetSkill(mAgent.MoveSkills[indexOfSkill]).CanUse)
                {
                    mIsEnableMoveSkill = true;
                    mAgent.SelectedSkill = mAgent.MoveSkills[indexOfSkill];
                    break;
                }
            }

            if (mIsEnableMoveSkill)
            {
                mIsSetSkillCommand = true;
                mAgent.Monster.Command.MoveHorizontal = 0.0f;
                mAgent.Monster.Command.MoveVertical = 0.0f;

                for (EActiveSkill_Monster skillType = EActiveSkill_Monster.None + 1; skillType < EActiveSkill_Monster.Count; skillType++)
                {
                    mAgent.Monster.Command.UseSkill[(int)skillType] = false;
                }

                mAgent.Monster.Command.UseSkill[(int)mAgent.SelectedSkill] = true;
            }
            else
            {
                Vector2 curPosition = mAgent.Monster.CenterTransform.position;
                Vector2 direction = dstPosition - curPosition;
                float moveSpeed = mAgent.Monster.Status.MoveSpeed;
                float distance = direction.magnitude;
                direction.Normalize();

                if (moveSpeed > Mathf.Epsilon)
                {
                    mRemainTime = distance / moveSpeed;
                }

                mAgent.Monster.Command.MoveHorizontal = direction.x;
                mAgent.Monster.Command.MoveVertical = direction.y;

                for (EActiveSkill_Monster skillType = EActiveSkill_Monster.None + 1; skillType < EActiveSkill_Monster.Count; skillType++)
                {
                    mAgent.Monster.Command.UseSkill[(int)skillType] = false;
                }
            }
        }

        public override void Update()
        {
            if (mIsEnableMoveSkill)
            {
                if (mIsSetSkillCommand && mAgent.Monster.CurrentSkill != null
                    && mAgent.Monster.CurrentSkill.Type == mAgent.SelectedSkill)
                {
                    // 원하는 스킬을 사용중인 경우 
                    // 커맨드 해제
                    mIsSetSkillCommand = false;
                    mAgent.Monster.Command.UseSkill[(int)mAgent.SelectedSkill] = false;
                }
                // 커맨드 해제 후 스킬이 끝나는 경우
                else if (mAgent.Monster.CurrentSkill == null)
                {
                    // 사용 가능한 Attack Skill이 존재하는지 검사
                    for (int indexOfAttackSkill = 0; indexOfAttackSkill < mAgent.AttackSkills.Length; indexOfAttackSkill++)
                    {
                        if (mAgent.Monster.IsEnableSkill(mAgent.AttackSkills[indexOfAttackSkill])
                            && mAgent.Monster.GetSkill(mAgent.AttackSkills[indexOfAttackSkill]).CanUse)
                        {
                            // 사용 가능한 Attack Skill이 존재하는 경우 
                            // 해당 스킬을 기억하고 Attack 상태로 전이
                            mAgent.SelectedSkill = mAgent.AttackSkills[indexOfAttackSkill];
                            mAgent.State = EStateType_Agent_GeneralMonster.Attack;
                            return;
                        }
                    }

                    for (int indexOfBuffSkill = 0; indexOfBuffSkill < mAgent.BuffSkills.Length; indexOfBuffSkill++)
                    {
                        if (mAgent.Monster.IsEnableSkill(mAgent.BuffSkills[indexOfBuffSkill])
                            && mAgent.Monster.GetSkill(mAgent.BuffSkills[indexOfBuffSkill]).CanUse)
                        {
                            // 사용가능한 Buff Skill이 존재하는 경우
                            // 해당 스킬을 기억하고 Buff 상태로 전이
                            mAgent.SelectedSkill = mAgent.BuffSkills[indexOfBuffSkill];
                            mAgent.State = EStateType_Agent_GeneralMonster.Buff;
                            return;
                        }
                    }

                    // 사용가능한 Attack Skill, Buff Skill이 존재하지 않는 경우, Wait 상태 전이
                    mAgent.State = EStateType_Agent_GeneralMonster.Wait;
                }
            }
            else
            {
                if (mRemainTime > 0.0f)
                {
                    mRemainTime -= mAgent.DeltaTime;
                }
                else
                {
                    // 사용 가능한 Attack Skill이 존재하는지 검사
                    for (int indexOfAttackSkill = 0; indexOfAttackSkill < mAgent.AttackSkills.Length; indexOfAttackSkill++)
                    {
                        if (mAgent.Monster.IsEnableSkill(mAgent.AttackSkills[indexOfAttackSkill])
                            && mAgent.Monster.GetSkill(mAgent.AttackSkills[indexOfAttackSkill]).CanUse)
                        {
                            // 사용 가능한 Attack Skill이 존재하는 경우 
                            // 해당 스킬을 기억하고 Attack 상태로 전이
                            mAgent.SelectedSkill = mAgent.AttackSkills[indexOfAttackSkill];
                            mAgent.State = EStateType_Agent_GeneralMonster.Attack;
                            return;
                        }
                    }

                    for (int indexOfBuffSkill = 0; indexOfBuffSkill < mAgent.BuffSkills.Length; indexOfBuffSkill++)
                    {
                        if (mAgent.Monster.IsEnableSkill(mAgent.BuffSkills[indexOfBuffSkill])
                            && mAgent.Monster.GetSkill(mAgent.BuffSkills[indexOfBuffSkill]).CanUse)
                        {
                            // 사용가능한 Buff Skill이 존재하는 경우
                            // 해당 스킬을 기억하고 Buff 상태로 전이
                            mAgent.SelectedSkill = mAgent.BuffSkills[indexOfBuffSkill];
                            mAgent.State = EStateType_Agent_GeneralMonster.Buff;
                            return;
                        }
                    }

                    // 사용가능한 Attack Skill, Buff Skill이 존재하지 않는 경우, Wait 상태 전이
                    mAgent.State = EStateType_Agent_GeneralMonster.Wait;
                }
            }
        }
    }
}