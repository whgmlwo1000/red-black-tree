﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_GeneralMonster", menuName = "Monster/General Basic Status", order = 1000)]
    public class BasicStatus_GeneralMonster : BasicStatus_Monster
    {

    }
}