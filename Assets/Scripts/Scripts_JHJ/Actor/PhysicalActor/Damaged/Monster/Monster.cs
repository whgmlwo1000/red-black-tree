﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using ObjectManagement;
using CameraManagement;

namespace RedBlackTree
{
    public enum EStateType_Monster
    {
        Idle = 0,

        Exhausted = 1,
        Die = 2,

        Spawn = 3,

        _ActiveSkill_ = 20,
        FireStraight = 21,
        GlobalHeal = 22,
        GlobalEnhance = 23,
        Blink = 24,
    }

    public enum EMonster
    {
        None = -1,

        DealerMage,
        HealerMage,
        EnhancerMage,

        Count
    }

    public enum ETriggerType_Monster
    {
        IsGround,


    }

    public enum EAudioType_Monster
    {
        None = -1, 

        Summon_0 = 0, 
        Summon_1, 
        Summon_End, 

        Hit_0 = 10, 
        Hit_1, 
        Hit_2, 
        Hit_End, 

        Launch = 15, 
        Cast = 20, 
        Exhausted = 25, 
        Died = 30, 
        Blink = 35
    }

    [RequireComponent(typeof(TriggerHandler_Monster), typeof(AudioHandler_Monster))]
    public abstract class Monster : ADamagedActor<EStateType_Monster>, IPoolable<EMonster>
    {
        public static List<Monster> MonsterList { get; } = new List<Monster>();
        public static float EnhanceRate { get; set; }

        public override EActorType ActorType { get { return EActorType.Monster; } }

        public abstract EMonster Type { get; }

        public abstract ESize Size { get; }

        public abstract ECombatType CombatType { get; }

        public virtual bool IsPooled { get { return !gameObject.activeSelf; } }

        public bool IsWorldCanvas { get { return false; } }

        private Status_Monster mStatus;
        public new Status_Monster Status
        {
            get { return mStatus; }
            protected set { base.Status = mStatus = value; }
        }

        public Command_Monster Command { get; protected set; }

        public TriggerHandler_Monster TriggerHandler { get; private set; }
        public AudioHandler_Monster AudioHandler { get; private set; }

        [SerializeField]
        private GameObject mSpawnEffect = null;
        public GameObject SpawnEffect { get { return mSpawnEffect; } }

        [SerializeField]
        private ParticleSystem mHitParticle = null;
        public ParticleSystem HitParticle { get { return mHitParticle; } }

        [SerializeField]
        private ParticleSystem mDieParticle = null;
        public ParticleSystem DieParticle { get { return mDieParticle; } }

        [SerializeField]
        private Transform[] mProjectileHolders = null;

        [SerializeField]
        private SkillTable_Monster mSkillTable = null;

        [SerializeField]
        private bool[] mEnabledActiveSkills = new bool[(int)EActiveSkill_Monster.Count];
        [SerializeField]
        private bool[] mEnabledPassiveSkills = new bool[(int)EPassiveSkill_Monster.Count];

        private ActiveSkill_Monster[] mActiveSkills;
        private PassiveSkill_Monster[] mPassiveSkills;

        private ActiveSkill_Monster mCurrentSkill;
        public ActiveSkill_Monster CurrentSkill
        {
            get { return mCurrentSkill; }
            set
            {
                if (value != null)
                {
                    State = EStateType_Monster._ActiveSkill_ + (int)value.Category + 1;
                    value.OnUse();
                }
                mCurrentSkill = value;
            }
        }

        public bool IsCasting { get; set; }

        public bool IsGround
        {
            get
            {
                return Velocity.y <= Mathf.Epsilon
                  && TriggerHandler.IsTriggered(ETriggerType_Monster.IsGround);
            }
        }

        public override void Awake()
        {
            base.Awake();
            EnhanceRate = 0.0f;

            TriggerHandler = GetComponent<TriggerHandler_Monster>();
            AudioHandler = GetComponent<AudioHandler_Monster>();

            mActiveSkills = mSkillTable.CreateActiveSkills();
            mPassiveSkills = mSkillTable.CreatePassiveSkills();

            StateManager.AddOnExitAbyss(OnActiveSceneChanged);

            SetStates(new Monster_Idle(this)
                , new Monster_Exhausted(this)
                , new Monster_Die(this)
                , new Monster_Spawn(this)
                , new Monster_FireStraight(this)
                , new Monster_GlobalEnhance(this)
                , new Monster_GlobalHeal(this)
                , new Monster_Blink(this));
        }

        public override void Start()
        {
            base.Start();

            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        public virtual void OnEnable()
        {
            MonsterList.Add(this);
        }

        public virtual void OnDisable()
        {
            MonsterList.Remove(this);
        }

        public override void Update()
        {
            if (!StateManager.Pause.State)
            {
                if (Status.Shield <= 0 && State != EStateType_Monster.Die)
                {
                    if (Status.Life <= 0)
                    {
                        State = EStateType_Monster.Die;
                    }
                    else if (State != EStateType_Monster.Exhausted)
                    {
                        State = EStateType_Monster.Exhausted;
                    }
                }

                base.Update();

                for (int indexOfSkill = 0; indexOfSkill < mActiveSkills.Length; indexOfSkill++)
                {
                    if (IsEnableSkill((EActiveSkill_Monster)indexOfSkill))
                    {
                        mActiveSkills[indexOfSkill].Update();
                    }
                }

                // update agent
            }
        }

        public virtual void Spawn()
        {
            gameObject.SetActive(true);
            transform.position = MonsterSpace.GetRandomPosition();
            State = EStateType_Monster.Spawn;
            Status.Life = Status.MaxLife;
            Status.Shield = Status.MaxShield;
            PlaySummonAudio();
        }

        public bool CheckSkillCommand()
        {
            for (int indexOfSkill = 0; indexOfSkill < (int)EActiveSkill_Monster.Count; indexOfSkill++)
            {
                ActiveSkill_Monster skill = GetSkill((EActiveSkill_Monster)indexOfSkill);

                if (IsEnableSkill((EActiveSkill_Monster)indexOfSkill) && Command.UseSkill[indexOfSkill] &&
                    skill.CanUse)
                {
                    CurrentSkill = skill;
                    return true;
                }
            }
            return false;
        }

        public void SetEnableSkill(EActiveSkill_Monster skill, bool isEnabled)
        {
            mEnabledActiveSkills[(int)skill] = isEnabled;
        }

        public void SetEnableSkill(EPassiveSkill_Monster skill, bool isEnabled)
        {
            mEnabledPassiveSkills[(int)skill] = isEnabled;
        }

        public bool IsEnableSkill(EActiveSkill_Monster skill)
        {
            return mEnabledActiveSkills[(int)skill];
        }

        public bool IsEnableSkill(EPassiveSkill_Monster skill)
        {
            return mEnabledPassiveSkills[(int)skill];
        }

        public ActiveSkill_Monster GetSkill(EActiveSkill_Monster skill)
        {
            return mActiveSkills[(int)skill];
        }

        public PassiveSkill_Monster GetSkill(EPassiveSkill_Monster skill)
        {
            return mPassiveSkills[(int)skill];
        }

        public float FollowCamera_x(float prevCameraPosition_x)
        {
            float curCameraPosition_x = CameraController.Main.Position.x;
            float deltaCameraPosition_x = curCameraPosition_x - prevCameraPosition_x;
            Vector3 curPosition = MainRigidbody.position;
            MovePosition(new Vector2(curPosition.x + deltaCameraPosition_x, curPosition.y));
            return curCameraPosition_x;
        }

        private void OnActiveSceneChanged()
        {
            gameObject.SetActive(false);
        }

        private void OnActiveSceneChanged(Scene src, Scene dst)
        {
            OnActiveSceneChanged();
        }

        protected override void OnDamageShield()
        {
            base.OnDamageShield();
            HitParticle.Play();
            PlayHitAudio();
            if (Status.Shield == 0)
            {
                PlayExhaustedAudio();
                State = EStateType_Monster.Exhausted;
            }
        }

        protected override void OnDamageLife()
        {
            base.OnDamageLife();
            if (Status.Life == 0)
            {
                PlayDieAudio();
                State = EStateType_Monster.Die;
            }
            else
            {
                HitParticle.Play();
            }
        }

        private void MoveToIdle_Animation()
        {
            State = EStateType_Monster.Idle;
        }

        private void EndGlobalCast_Animation()
        {
            MainAnimator.SetBool("cast", false);
            IsCasting = false;
        }

        private void FireStraight_Animation(int holder)
        {
            ActiveSkill_Monster_FireStraight skill = CurrentSkill as ActiveSkill_Monster_FireStraight;
            if (skill != null)
            {
                Projectile_Straight projectile = ProjectilePool.Get(skill.ProjectileType) as Projectile_Straight;
                if (projectile != null)
                {
                    projectile.SetParams(srcActor: this,
                        dstPosition: Giant.Main.CenterTransform.position,
                        offense: Status.Offense,
                        force: 0.0f,
                        criticalChance: 0.0f,
                        criticalMagnification: 1.0f);
                    projectile.Activate(null, mProjectileHolders[holder].position);
                }
            }
        }

        private void GlobalHeal_Animation()
        {
            MonsterList.ForEach(delegate (Monster monster)
            {
                if (monster != this)
                {
                    ActiveSkill_Monster_GlobalHeal skill = CurrentSkill as ActiveSkill_Monster_GlobalHeal;
                    monster.Heal.Set(true, EDamageType.Shield, skill.HealValue, 0.0f);
                    monster.InvokeHeal();
                }
            });
        }

        private void EffectOfGlobalEnhance_Animation()
        {
            MonsterList.ForEach(delegate (Monster monster)
            {
                if (monster != this && monster.CombatType == ECombatType.Dealer && monster.CanHealShield)
                {
                    Effect effect = EffectPool.Get(EEffectType.GlobalEnhance);
                    if (effect != null)
                    {
                        effect.Activate(monster.CenterTransform, monster.CenterTransform.position);
                    }
                }
            });
        }

        private void EffectOfGlobalHeal_Animation()
        {
            MonsterList.ForEach(delegate (Monster monster)
            {
                if (monster != this && monster.CanHealShield)
                {
                    Effect effect = EffectPool.Get(EEffectType.GlobalHeal);
                    if (effect != null)
                    {
                        effect.Activate(monster.CenterTransform, monster.CenterTransform.position);
                    }
                }
            });
        }

        private void Blink_Animation()
        {
            transform.position = MonsterSpace.GetRandomPosition();
        }

        private void PlaySummonAudio()
        {
            EAudioType_Monster audioType = 
                (EAudioType_Monster)Random.Range((int)EAudioType_Monster.Summon_0, (int)EAudioType_Monster.Summon_End);
            AudioHandler.Get(audioType).AudioSource.Play();
        }

        private void PlayHitAudio()
        {
            EAudioType_Monster audioType =
                (EAudioType_Monster)Random.Range((int)EAudioType_Monster.Hit_0, (int)EAudioType_Monster.Hit_End);
            AudioHandler.Get(audioType).AudioSource.Play();
        }

        private void PlayLaunchAudio_Animation()
        {
            AudioHandler.Get(EAudioType_Monster.Launch).AudioSource.Play();
        }

        private void PlayCastAudio_Animation()
        {
            AudioHandler.Get(EAudioType_Monster.Cast).AudioSource.Play();
        }

        private void PlayExhaustedAudio()
        {
            AudioHandler.Get(EAudioType_Monster.Exhausted).AudioSource.Play();
        }

        private void PlayDieAudio()
        {
            AudioHandler.Get(EAudioType_Monster.Died).AudioSource.Play();
        }

        private void PlayBlinkAudio_Animation()
        {
            AudioHandler.Get(EAudioType_Monster.Blink).AudioSource.Play();
        }
    }
}