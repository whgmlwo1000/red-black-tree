﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class APhysicalActorEditor : AActorEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();
            AddMainColliderHandler(mainTransform);

            SerializedProperty centerTransformProp = serializedObject.FindProperty("mCenterTransform");
            centerTransformProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Center Transform"), centerTransformProp.objectReferenceValue, typeof(Transform), true);
            Transform centerTransform = centerTransformProp.objectReferenceValue as Transform;
            if(centerTransform == null)
            {
                centerTransform = new GameObject("Center Transform").transform;
                centerTransform.SetParent(mainTransform);
                centerTransform.localPosition = Vector3.zero;
                centerTransform.localRotation = Quaternion.identity;
                centerTransform.localScale = Vector3.one;
                centerTransformProp.objectReferenceValue = centerTransform;
                serializedObject.ApplyModifiedProperties();
            }

            return mainTransform;
        }

        /// <summary>
        /// 액터의 mainTransform 의 하위 계층에  Main Collider Handler를 추가하는 메소드
        /// </summary>
        public MainColliderHandler AddMainColliderHandler(Transform mainTransform)
        {
            SerializedProperty mainColliderHandlerProp = serializedObject.FindProperty("mMainColliderHandler");
            mainColliderHandlerProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Main Collider Handler"), mainColliderHandlerProp.objectReferenceValue, typeof(MainColliderHandler), true);
            if(mainColliderHandlerProp.objectReferenceValue == null)
            {
                GameObject mainColliderHandlerObject = new GameObject("Main Colliders", typeof(MainColliderHandler));
                mainColliderHandlerObject.transform.eulerAngles = Vector3.zero;
                mainColliderHandlerObject.transform.localScale = Vector3.one;
                mainColliderHandlerObject.transform.SetParent(mainTransform);
                mainColliderHandlerObject.transform.localPosition = Vector3.zero;
                mainColliderHandlerProp.objectReferenceValue = mainColliderHandlerObject.GetComponent<MainColliderHandler>();
                serializedObject.ApplyModifiedProperties();
            }

            return mainColliderHandlerProp.objectReferenceValue as MainColliderHandler;
        }
    }
}