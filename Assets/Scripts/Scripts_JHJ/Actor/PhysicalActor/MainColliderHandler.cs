﻿using UnityEngine;
using ObjectManagement;

namespace RedBlackTree
{
    public enum EMainColliderType
    {
        TerrainTrigger,
        HitBox_00, HitBox_01, HitBox_02, HitBox_03, HitBox_04
    }

    public class MainColliderHandler : Handler<EMainColliderType, MainCollider>
    {
        public Collider2D GetCollider(EMainColliderType type)
        {
            return Get(type).Collider;
        }
    }
}