﻿using UnityEngine;
using ObjectManagement;

namespace RedBlackTree
{
    public class MainCollider : MonoBehaviour, IHandleable<EMainColliderType>
    {
        [SerializeField]
        private EMainColliderType mType = EMainColliderType.TerrainTrigger;
        public EMainColliderType Type { get { return mType; } }

        public Collider2D Collider { get; private set; }

        public void Awake()
        {
            Collider = GetComponent<Collider2D>();
        }
    }
}