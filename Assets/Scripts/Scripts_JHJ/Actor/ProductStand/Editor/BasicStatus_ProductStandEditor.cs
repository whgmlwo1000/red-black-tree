﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_ProductStand))]
    public class BasicStatus_ProductStandEditor : Editor
    {
        public sealed override void OnInspectorGUI()
        {
            SetProperties();
            serializedObject.ApplyModifiedProperties();
        }

        public virtual void SetProperties()
        {
            SerializedProperty productProps = serializedObject.FindProperty("Products");
            SerializedProperty weightProps = serializedObject.FindProperty("Weights");
            SerializedProperty countOfProductsProp = serializedObject.FindProperty("CountOfProducts");

            int countOfAll = EditorGUILayout.IntField(new GUIContent("Count Of All Products", "판매 가능한 모든 상품들의 개수"), productProps.arraySize);
            countOfAll = Mathf.Max(countOfAll, 1);
            if(productProps.arraySize != countOfAll || weightProps.arraySize != countOfAll)
            {
                productProps.arraySize = countOfAll;
                weightProps.arraySize = countOfAll;
                serializedObject.ApplyModifiedProperties();
            }

            countOfProductsProp.intValue = EditorGUILayout.IntField(new GUIContent("Count Of Product in Stand", "가판대에 배치될 수 있는 상품의 수"), countOfProductsProp.intValue);
            countOfProductsProp.intValue = Mathf.Max(countOfProductsProp.intValue, 1);

            EditorGUILayout.Space();

            for(int indexOfProp = 0; indexOfProp < productProps.arraySize; indexOfProp++)
            {
                SerializedProperty productProp = productProps.GetArrayElementAtIndex(indexOfProp);
                SerializedProperty weightProp = weightProps.GetArrayElementAtIndex(indexOfProp);

                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField("Product", GUILayout.MaxWidth(50));

                EProduct product = (EProduct)productProp.intValue;
                product = (EProduct)EditorGUILayout.EnumPopup(product);
                productProp.intValue = (int)product;

                EditorGUILayout.LabelField("Weight", GUILayout.MaxWidth(50));

                weightProp.floatValue = EditorGUILayout.FloatField(weightProp.floatValue, GUILayout.MaxWidth(50));
                weightProp.floatValue = Mathf.Max(weightProp.floatValue, 0.0f);

                EditorGUILayout.EndHorizontal();
            }
        }
    }
}