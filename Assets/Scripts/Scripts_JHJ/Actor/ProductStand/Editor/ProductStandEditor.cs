﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(ProductStand))]
    public class ProductStandEditor : AActorEditor
    {
        private void OnSceneGUI()
        {
            ProductStand stand = target as ProductStand;

            if(stand.StartPoint != null && stand.EndPoint != null)
            {
                Handles.DrawLine(stand.StartPoint.position, stand.EndPoint.position);

                Vector3 startPosition = stand.StartPoint.position;
                Vector3 endPosition = stand.EndPoint.position;
                float scale = 1.0f;

                Handles.TransformHandle(ref startPosition, Quaternion.identity, ref scale);
                Handles.TransformHandle(ref endPosition, Quaternion.identity, ref scale);

                stand.StartPoint.position = startPosition;
                stand.EndPoint.position = endPosition;
            }
        }

        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            EditorGUILayout.Space();

            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");
            SerializedProperty startPointProp = serializedObject.FindProperty("mStartPoint");
            SerializedProperty endPointProp = serializedObject.FindProperty("mEndPoint");

            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Basic Status"), basicStatusProp.objectReferenceValue, typeof(BasicStatus_ProductStand), false);

            EditorGUILayout.Space();

            startPointProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Product Start Point", "Product 가 진열되기 시작하는 점")
                , startPointProp.objectReferenceValue, typeof(Transform), true);

            if(startPointProp.objectReferenceValue == null)
            {
                ProductStand stand = target as ProductStand;
                Transform startPoint = new GameObject("Product Start Point").transform;
                startPoint.SetParent(stand.transform);
                startPoint.localPosition = new Vector3(-5.0f, 0.0f, 0.0f);
                startPoint.localRotation = Quaternion.identity;
                startPoint.localScale = Vector3.one;
                startPointProp.objectReferenceValue = startPoint;
            }


            endPointProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Product End Point", "Product 가 진열되는 끝점")
                , endPointProp.objectReferenceValue, typeof(Transform), true);

            if(endPointProp.objectReferenceValue == null)
            {
                ProductStand stand = target as ProductStand;
                Transform endPoint = new GameObject("Product End Point").transform;
                endPoint.SetParent(stand.transform);
                endPoint.localPosition = new Vector3(5.0f, 0.0f, 0.0f);
                endPoint.localRotation = Quaternion.identity;
                endPoint.localScale = Vector3.one;
                endPointProp.objectReferenceValue = endPoint;
            }

            return mainTransform;
        }
    }
}