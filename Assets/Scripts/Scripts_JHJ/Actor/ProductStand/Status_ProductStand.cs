﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Status_ProductStand : ScriptableObject
    {
        public BasicStatus_ProductStand BasicStatus { get; set; }

        public int CountOfAllProducts { get { return BasicStatus.Products.Length; } }
        public int CountOfProducts { get { return BasicStatus.CountOfProducts; } }
        
        public float TotalWeight
        {
            get
            {
                float total = 0.0f;
                for(int indexOfWeight = 0; indexOfWeight < CountOfAllProducts; indexOfWeight++)
                {
                    total += GetWeight(indexOfWeight);
                }

                return total;
            }
        }

        public EProduct GetProduct(int index)
        {
            return BasicStatus.Products[index];
        }

        public float GetWeight(int index)
        {
            return BasicStatus.Weights[index];
        }
    }
}