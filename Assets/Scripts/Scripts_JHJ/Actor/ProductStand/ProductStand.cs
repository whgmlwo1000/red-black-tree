﻿using UnityEngine;
using ObjectManagement;

namespace RedBlackTree
{
    public enum EStateType_ProductStand
    {
        Activate,
    }

    public class ProductStand : AActor<EStateType_ProductStand>
    {
        public override EActorType ActorType { get { return EActorType.ProductStand; } }

        [SerializeField]
        private BasicStatus_ProductStand mBasicStatus = null;
        private Status_ProductStand mStatus;
        public Status_ProductStand Status
        {
            get { return mStatus; }
            protected set
            {
                value.BasicStatus = mBasicStatus;
                mStatus = value;
            }
        }

        [SerializeField]
        private Transform mStartPoint = null;
        public Transform StartPoint { get { return mStartPoint; } }
        [SerializeField]
        private Transform mEndPoint = null;
        public Transform EndPoint { get { return mEndPoint; } }

        private Product[] mDisplayedProducts;

        public override void Awake()
        {
            base.Awake();
            Status = ScriptableObject.CreateInstance<Status_ProductStand>();
            mDisplayedProducts = new Product[Status.CountOfProducts];
        }

        /// <summary>
        /// 가판대 활성화
        /// </summary>
        public void Activate()
        {
            gameObject.SetActive(true);
            DisplayProducts();
        }

        public void Deactivate()
        {
            for (int countOfProduct = 0; countOfProduct < Status.CountOfProducts; countOfProduct++)
            {
                if (mDisplayedProducts[countOfProduct] != null)
                {
                    if (mDisplayedProducts[countOfProduct].State == EStateType_Product.Displayed)
                    {
                        mDisplayedProducts[countOfProduct].Deactivate();
                    }
                    mDisplayedProducts[countOfProduct] = null;
                }
            }
            gameObject.SetActive(false);
        }

        public bool IsSoldOut(int souls)
        {
            for (int indexOfProduct = 0; indexOfProduct < mDisplayedProducts.Length; indexOfProduct++)
            {
                if (mDisplayedProducts[indexOfProduct] != null && mDisplayedProducts[indexOfProduct].State == EStateType_Product.Displayed
                    && mDisplayedProducts[indexOfProduct].Price <= souls)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 상품 진열
        /// </summary>
        public void DisplayProducts()
        {
            for (int countOfProduct = 0; countOfProduct < Status.CountOfProducts; countOfProduct++)
            {
                if (mDisplayedProducts[countOfProduct] != null && mDisplayedProducts[countOfProduct].State == EStateType_Product.Displayed)
                {
                    mDisplayedProducts[countOfProduct].Deactivate();
                }
            }

            Vector2 endPosition = mEndPoint.position;
            Vector2 startPosition = mStartPoint.position;
            Vector2 gap = new Vector2((endPosition.x - startPosition.x) / (Status.CountOfProducts - 1), (endPosition.y - startPosition.y) / (Status.CountOfProducts - 1));

            Vector2 curPosition = startPosition;
            for (int countOfProduct = 0; countOfProduct < Status.CountOfProducts; countOfProduct++)
            {
                float total = Status.TotalWeight;
                float selection = Random.Range(0.0f, total);
                for (int indexOfProduct = 0; indexOfProduct < Status.CountOfAllProducts; indexOfProduct++)
                {
                    if (selection < Status.GetWeight(indexOfProduct))
                    {
                        mDisplayedProducts[countOfProduct] = ProductPool.Get(Status.GetProduct(indexOfProduct));
                        break;
                    }
                    selection -= Status.GetWeight(indexOfProduct);
                }

                if (mDisplayedProducts[countOfProduct] != null)
                {
                    mDisplayedProducts[countOfProduct].Activate(this, curPosition);
                }
                curPosition += gap;
            }
        }
    }
}