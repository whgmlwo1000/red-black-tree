﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_ProductStand", menuName = "Product/Stand Basic Status", order = 1000)]
    public class BasicStatus_ProductStand : ScriptableObject
    {
        public EProduct[] Products;
        public float[] Weights;
        public int CountOfProducts;
    }
}