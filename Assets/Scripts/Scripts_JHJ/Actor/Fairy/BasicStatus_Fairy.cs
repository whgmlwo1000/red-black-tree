﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_Fairy", menuName = "Fairy/Basic Status", order = 1000)]
    public class BasicStatus_Fairy : ScriptableObject
    {
        public Sprite DefaultSprite;

        public string NameTag;
        public string FlavorTag;

        public int Offense;

        public float CriticalChance;
        public float CriticalMagnification;

        public float AttackCoolDown;
    }
}