﻿using UnityEngine;

namespace RedBlackTree
{
    public class Fairy_Attack : State<EStateType_Fairy>
    {
        private Fairy mFairy;
        private bool mIsStarted;

        public Fairy_Attack(Fairy fairy) : base(EStateType_Fairy.Attack)
        {
            mFairy = fairy;
        }

        public override void Start()
        {
            mFairy.AudioHandler.Get(EAudioType_Fairy.Launch).AudioSource.Play();
            if (!mIsStarted)
            {
                mIsStarted = true;
                mFairy.MainAnimator.SetInteger("state", (int)Type);
                mFairy.Angle = 0.0f;
            }
            else
            {
                mFairy.MainAnimator.SetTrigger("attack");
            }

            mFairy.OnAttack();
        }

        public override void Update()
        {
            if (mFairy.CheckAttack())
            {
                return;
            }
        }

        public override void End()
        {
            mIsStarted = false;
        }
    }
}