﻿using UnityEngine;

namespace RedBlackTree
{
    public class Fairy_Wait : State<EStateType_Fairy>
    {
        private Fairy mFairy;
        private Vector2 mPrevPosition;

        public Fairy_Wait(Fairy fairy) : base(EStateType_Fairy.Wait)
        {
            mFairy = fairy;
        }

        public override void Start()
        {
            // 애니메이션 설정
            mFairy.MainAnimator.SetInteger("state", (int)Type);
            // 기본 각도로 설정
            mFairy.Angle = 0.0f;
            // 다음 Loop 에 사용될 현재 Loop 좌표 저장
            mPrevPosition = mFairy.transform.position;
        }

        public override void Update()
        {
            // Attack 커맨드 감지시
            if (mFairy.CheckAttack())
            {
                return;
            }
        }

        public override void FixedUpdate()
        {
            // 현재 좌표와 이전 좌표간의 차이가 한계 수치 이상인 경우 Move 상태로 전이
            Vector2 curPosition = mFairy.transform.position;
            Vector2 diff = curPosition - mPrevPosition;

            if (Mathf.Abs(diff.x) > ValueConstants.MovementThreshold + Mathf.Epsilon
                || Mathf.Abs(diff.y) > ValueConstants.MovementThreshold + Mathf.Epsilon)
            {
                mFairy.State = EStateType_Fairy.Move;
                return;
            }
            mPrevPosition = curPosition;
        }
    }
}