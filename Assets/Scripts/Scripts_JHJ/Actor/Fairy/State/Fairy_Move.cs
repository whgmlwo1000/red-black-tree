﻿using UnityEngine;

namespace RedBlackTree
{
    public class Fairy_Move : State<EStateType_Fairy>
    {
        private Fairy mFairy;
        private Vector2 mPrevPosition;

        public Fairy_Move(Fairy fairy) : base(EStateType_Fairy.Move)
        {
            mFairy = fairy;
        }

        public override void Start()
        {
            // 애니메이터 설정
            mFairy.MainAnimator.SetInteger("state", (int)Type);
            // 다음 Loop 에 사용될 현재 Loop 좌표 저장
            mPrevPosition = mFairy.transform.position;

            mFairy.AudioHandler.Get(EAudioType_Fairy.Move).AudioSource.Play();
        }

        public override void End()
        {
            mFairy.AudioHandler.Get(EAudioType_Fairy.Move).AudioSource.Stop();
        }

        public override void Update()
        {
            // Attack 커맨드 감지시
            if (mFairy.CheckAttack())
            {
                return;
            }
        }

        public override void FixedUpdate()
        {
            // 움직임 한계값 이하일 경우 Wait 상태 전이
            Vector2 curPosition = mFairy.transform.position;
            Vector2 diff = curPosition - mPrevPosition;

            if (Mathf.Abs(diff.x) <= ValueConstants.MovementThreshold + Mathf.Epsilon
                && Mathf.Abs(diff.y) <= ValueConstants.MovementThreshold + Mathf.Epsilon)
            {
                mFairy.State = EStateType_Fairy.Wait;
                return;
            }
            mPrevPosition = curPosition;
            // 방향 설정
            mFairy.SetDirection(diff, false);
        }
    }
}