﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_Fairy))]
    public class BasicStatus_FairyEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SerializedProperty defaultSpriteProp = serializedObject.FindProperty("DefaultSprite");
            SerializedProperty nameTagProp = serializedObject.FindProperty("NameTag");
            SerializedProperty flavorTagProp = serializedObject.FindProperty("FlavorTag");
            SerializedProperty offenseProp = serializedObject.FindProperty("Offense");
            SerializedProperty criticalChanceProp = serializedObject.FindProperty("CriticalChance");
            SerializedProperty criticalMagnificationProp = serializedObject.FindProperty("CriticalMagnification");
            SerializedProperty attackCoolDownProp = serializedObject.FindProperty("AttackCoolDown");


            defaultSpriteProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Default Sprite", "기본 스프라이트"), defaultSpriteProp.objectReferenceValue, typeof(Sprite), false) as Sprite;
            EditorGUILayout.Space();
            nameTagProp.stringValue = EditorGUILayout.TextField(new GUIContent("Name Tag", "이름 문자열 태그"), nameTagProp.stringValue);
            if (DataManager.HasTag(nameTagProp.stringValue))
            {
                EditorGUILayout.HelpBox(DataManager.GetScript(nameTagProp.stringValue), MessageType.None);
            }
            else
            {
                EditorGUILayout.HelpBox(StringConstants.None, MessageType.None);
            }
            flavorTagProp.stringValue = EditorGUILayout.TextField(new GUIContent("Flavor Text Tag", "플레이버 텍스트 태그"), flavorTagProp.stringValue);
            if (DataManager.HasTag(flavorTagProp.stringValue))
            {
                EditorGUILayout.HelpBox(DataManager.GetScript(flavorTagProp.stringValue), MessageType.None);
            }
            else
            {
                EditorGUILayout.HelpBox(StringConstants.None, MessageType.None);
            }
            EditorGUILayout.Space();
            offenseProp.intValue = EditorGUILayout.IntField(new GUIContent("Offense", "기본 공격력"), offenseProp.intValue);
            if (offenseProp.intValue < 0)
            {
                offenseProp.intValue = 0;
            }

            criticalChanceProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Critical Chance", "기본 치명타 확률"), criticalChanceProp.floatValue);
            if (criticalChanceProp.floatValue < 0.0f)
            {
                criticalChanceProp.floatValue = 0.0f;
            }
            else if(criticalChanceProp.floatValue > 1.0f)
            {
                criticalChanceProp.floatValue = 1.0f;
            }

            criticalMagnificationProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Critical Magnification", "기본 치명타 배율"), criticalMagnificationProp.floatValue);
            if (criticalMagnificationProp.floatValue < 1.0f)
            {
                criticalMagnificationProp.floatValue = 1.0f;
            }

            attackCoolDownProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Attack Cool Down", "요정 기본 공격 쿨타임"), attackCoolDownProp.floatValue);
            if (attackCoolDownProp.floatValue < 0.0f)
            {
                attackCoolDownProp.floatValue = 0.0f;
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
}