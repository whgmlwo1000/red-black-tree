﻿using UnityEngine;
using UnityEditor;


namespace RedBlackTree
{
    [CustomEditor(typeof(Fairy))]
    public class FairyEditor : AActorEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();
            AddAttackSrcTransform(mainTransform);

            SerializedProperty commandProp = serializedObject.FindProperty("mCommand");
            commandProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Player Command", "플레이어의 입력에 의해 설정되는 커맨드")
                , commandProp.objectReferenceValue, typeof(PlayerCommand), false);

            SerializedProperty inventoryProp = serializedObject.FindProperty("mInventory");
            inventoryProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Inventory", "플레이어 인벤토리"), inventoryProp.objectReferenceValue, typeof(Inventory), false);

            SerializedProperty statusProp = serializedObject.FindProperty("mStatus");
            statusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Fairy Status", "요정의 동적 수치 스테이터스"), statusProp.objectReferenceValue, typeof(Status_Fairy), false);

            serializedObject.ApplyModifiedProperties();
            return mainTransform;
        }

        /// <summary>
        /// 정령 빔 발사를 위한 Src Transform을 추가하는 메소드
        /// </summary>
        public Transform AddAttackSrcTransform(Transform mainTransform)
        {
            SerializedProperty fairyBeamSrcTransformProp = serializedObject.FindProperty("mFairyBeamSrcTransform");
            fairyBeamSrcTransformProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Fairy Beam Src Transform"), fairyBeamSrcTransformProp.objectReferenceValue, typeof(Transform), false);
            Transform fairyBeamSrcTransform = fairyBeamSrcTransformProp.objectReferenceValue as Transform;
            if (fairyBeamSrcTransform == null)
            {
                fairyBeamSrcTransform = new GameObject("Fairy Beam Src Transform").transform;
                fairyBeamSrcTransform.SetParent(mainTransform);
                fairyBeamSrcTransform.localPosition = Vector3.zero;
                fairyBeamSrcTransform.localRotation = Quaternion.identity;
                fairyBeamSrcTransform.localScale = Vector3.one;

                fairyBeamSrcTransformProp.objectReferenceValue = fairyBeamSrcTransform;
                serializedObject.ApplyModifiedProperties();
            }

            return fairyBeamSrcTransform;
        }
    }
}