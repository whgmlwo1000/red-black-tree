﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Status_Fairy))]
    public class Status_FairyEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");
            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Basic Status", "요정 기본 수치 스테이터스")
                , basicStatusProp.objectReferenceValue, typeof(BasicStatus_Fairy), false);

            serializedObject.ApplyModifiedProperties();

            EditorGUILayout.Space();

            Status_Fairy status = target as Status_Fairy;
            if (status.BasicStatus != null)
            {
                EditorGUILayout.LabelField(string.Format("Offense : {0}", status.Offense), EditorStyles.largeLabel);
                EditorGUILayout.LabelField(string.Format("Critical Chance : {0}", status.CriticalChance), EditorStyles.largeLabel);
                EditorGUILayout.LabelField(string.Format("Critical Magnification : {0}", status.CriticalMagnification), EditorStyles.largeLabel);
                EditorGUILayout.LabelField(string.Format("Attack Cool Down : {0}", status.AttackCoolDown), EditorStyles.largeLabel);
            }
        }
    }
}