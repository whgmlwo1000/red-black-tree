﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New Status_Fairy", menuName = "Fairy/Status", order = 1000)]
    public class Status_Fairy : ScriptableObject
    {
        [SerializeField]
        private BasicStatus_Fairy mBasicStatus = null;
        public BasicStatus_Fairy BasicStatus
        {
            get { return mBasicStatus; }
        }

        public Sprite DefaultSprite { get { return mBasicStatus.DefaultSprite; } }

        public string Name 
        { 
            get
            {
                if(DataManager.HasTag(BasicStatus.NameTag))
                {
                    return DataManager.GetScript(BasicStatus.NameTag);
                }
                return StringConstants.None;
            } 
        }

        public string FlavorText
        {
            get
            {
                if(DataManager.HasTag(BasicStatus.FlavorTag))
                {
                    return DataManager.GetScript(BasicStatus.FlavorTag);
                }
                return StringConstants.None;
            }
        }

        public virtual int Offense_Increment { get; }
        public virtual float Offense_Increase { get; }
        public int Offense
        {
            get { return (int)((BasicStatus.Offense + Offense_Increment) * (1.0f + Offense_Increase)); }
        }

        public virtual float CriticalChance_Increment { get; }
        public float CriticalChance { get { return BasicStatus.CriticalChance + CriticalChance_Increment; } }

        public virtual float CriticalMagnification_Increment { get; }
        public float CriticalMagnification { get { return BasicStatus.CriticalMagnification + CriticalMagnification_Increment; } }


        public virtual float AttackCoolDown_Decrement { get; }
        public virtual float AttackCoolDown_Decrease { get; }
        public float AttackCoolDown
        {
            get { return (BasicStatus.AttackCoolDown - AttackCoolDown_Decrease) * (1.0f - AttackCoolDown_Decrease); }
        }

        public float AttackCoolDown_Remain;
    }
}