﻿using UnityEngine;
using System.Collections;

namespace RedBlackTree
{
    public enum EStateType_Fairy
    {
        None = -1,

        Wait = 0,

        Move = 1,

        Attack = 2,

        Count
    }

    public enum ESkin_Fairy
    {
        None = -1,

        Normal = 0,
        King = 1,
        Santa = 2,

        Count
    }

    public enum EAudioType_Fairy
    {
        None = -1, 
        
        Move = 0, 
        Launch = 1
    }

    [RequireComponent(typeof(AudioHandler_Fairy))]
    public class Fairy : AActor<EStateType_Fairy>
    {
        public static Fairy Main { get; private set; } = null;

        public sealed override EActorType ActorType { get { return EActorType.Fairy; } }

        [SerializeField]
        private Status_Fairy mStatus = null;
        public Status_Fairy Status
        {
            get { return mStatus; }
        }

        [SerializeField]
        private PlayerCommand mCommand = null;
        public PlayerCommand Command { get { return mCommand; } }

        [SerializeField]
        private Inventory mInventory = null;
        public Inventory Inventory { get { return mInventory; } }

        [SerializeField]
        private Transform mFairyBeamSrcTransform = null;
        private ESkin_Fairy mSkin;
        private ESkin_Fairy Skin
        {
            set
            {
                if (mSkin != value)
                {
                    mSkin = value;
                    MainAnimator.SetInteger("skin", (int)mSkin);
                }
            }
        }

        public AudioHandler_Fairy AudioHandler { get; private set; }

        public override void Awake()
        {
            base.Awake();

            Main = this;
            AudioHandler = GetComponent<AudioHandler_Fairy>();

            StateManager.Pause.AddEnterEvent(true, OnPauseEnter);
            StateManager.Pause.AddExitEvent(true, OnPauseExit);

            SetStates(new Fairy_Wait(this)
                , new Fairy_Move(this)
                , new Fairy_Attack(this));
        }

        public override void Update()
        {
            if (!StateManager.Pause.State)
            {
                base.Update();
                Skin = PlayerData.Current.FairySkin;
                if (mStatus.AttackCoolDown_Remain > 0.0f)
                {
                    mStatus.AttackCoolDown_Remain -= Time.deltaTime;
                }
            }
        }

        public virtual void OnEnable()
        {
            Skin = PlayerData.Current.FairySkin;
        }

        public virtual void OnDestroy()
        {
            Main = null;

            StateManager.Pause.RemoveEnterEvent(true, OnPauseEnter);
            StateManager.Pause.RemoveExitEvent(true, OnPauseExit);
        }

        /// <summary>
        /// 요정 Data와 Player Command를 참조해서 요정 빔을 발사~
        /// </summary>
        public void Attack()
        {
            Projectile_FairyBeam beam = ProjectilePool.Get(EProjectile.FairyBeam) as Projectile_FairyBeam;
            if (beam != null)
            {
                int offense = Status.Offense;
                PassiveItem_TyrSword item = Inventory.Get(EPassiveItem.TyrSword) as PassiveItem_TyrSword;
                if (item != null && item.IsAcquired)
                {
                    offense += item.OffenseIncrement;
                }

                beam.SetParams(damageType: EDamageType.Shield
                    , offense: offense
                    , criticalChance: Status.CriticalChance
                    , criticalMag: Status.CriticalMagnification
                    , srcActor: this
                    , dstPosition: Command.Fairy_AttackPosition);

                beam.Activate(transform, mFairyBeamSrcTransform.position);
            }
        }

        public bool CheckAttack()
        {
            if (mStatus.AttackCoolDown_Remain <= 0.0f && mCommand.Fairy_Attack)
            {
                State = EStateType_Fairy.Attack;
                return true;
            }
            return false;
        }

        public virtual void OnAttack()
        {
            mStatus.AttackCoolDown_Remain = Status.AttackCoolDown;
        }

        private void OnPauseEnter()
        {
            if(gameObject.activeInHierarchy && State == EStateType_Fairy.Move)
            {
                AudioHandler.Get(EAudioType_Fairy.Move).AudioSource.Stop();
            }
        }

        private void OnPauseExit()
        {
            if(gameObject.activeInHierarchy && State == EStateType_Fairy.Move)
            {
                AudioHandler.Get(EAudioType_Fairy.Move).AudioSource.Play();
            }
        }
    }
}