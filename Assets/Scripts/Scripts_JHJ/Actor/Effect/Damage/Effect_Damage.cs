﻿using UnityEngine;
using UnityEngine.UI;

namespace RedBlackTree
{
    public class Effect_Damage : Effect
    {
        private Text mText;

        public override void Awake()
        {
            base.Awake();

            mText = GetComponentInChildren<Text>(true);
        }

        public void SetParams(int damage)
        {
            mText.text = damage.ToString();
        }
    }
}