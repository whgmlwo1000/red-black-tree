﻿using UnityEngine;
using UnityEditor;
using ObjectManagement;

namespace RedBlackTree
{
    [CustomEditor(typeof(EffectPool))]
    public class EffectPoolEditor : PoolEditor<EEffectType>
    {
        
    }
}