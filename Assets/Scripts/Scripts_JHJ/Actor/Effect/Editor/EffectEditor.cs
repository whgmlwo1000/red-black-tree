﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Effect))]
    public class EffectEditor : AActorEditor
    {
        public override Transform SetProperties()
        {
            ShowEffectType();

            EditorGUILayout.Space();

            return base.SetProperties();
        }

        /// <summary>
        /// 이펙트 별 고유한 타입을 출력하고 설정하는 메소드
        /// </summary>
        public void ShowEffectType()
        {
            SerializedProperty effectTypeProp = serializedObject.FindProperty("mType");
            SerializedProperty countOfEffectsProp = serializedObject.FindProperty("mCountOfEffects");
            SerializedProperty isWorldCanvasProp = serializedObject.FindProperty("mIsWorldCanvas");

            EEffectType effectType = (EEffectType)effectTypeProp.intValue;

            effectType = (EEffectType)EditorGUILayout.EnumPopup(new GUIContent("Effect Type", "이펙트 고유 타입"), effectType);
            effectTypeProp.intValue = (int)effectType;

            countOfEffectsProp.intValue = EditorGUILayout.IntField(new GUIContent("Count Of Effect", "랜덤으로 선택될 수 있는 이펙트의 수"), countOfEffectsProp.intValue);
            countOfEffectsProp.intValue = Mathf.Max(countOfEffectsProp.intValue, 1);

            isWorldCanvasProp.boolValue = EditorGUILayout.Toggle(new GUIContent("Use World Canvas"), isWorldCanvasProp.boolValue);

            serializedObject.ApplyModifiedProperties();
        }
    }
}