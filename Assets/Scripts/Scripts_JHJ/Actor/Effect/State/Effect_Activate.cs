﻿using UnityEngine;

namespace RedBlackTree
{
    public class Effect_Activate : State<EStateType_Effect>
    {
        private Effect mEffect;

        public Effect_Activate(Effect effect) : base(EStateType_Effect.Activate)
        {
            mEffect = effect;
        }

        public override void Start()
        {
            if (mEffect.CountOfEffects > 1)
            {
                int effect = (int)Random.Range(0.0f, mEffect.CountOfEffects);
                mEffect.MainAnimator.SetInteger("type", (int)effect);
            }
        }
    }
}