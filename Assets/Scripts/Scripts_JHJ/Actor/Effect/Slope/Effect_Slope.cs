﻿using UnityEngine;

namespace RedBlackTree
{
    public class Effect_Slope : Effect
    {
        [SerializeField]
        private float mMinAngleRange = -30.0f;
        [SerializeField]
        private float mMaxAngleRange = 30.0f;

        public override void Awake()
        {
            base.Awake();

            OnActivate += OnActivateSlopeEffect;
        }

        private void OnActivateSlopeEffect()
        {
            Angle = Random.Range(mMinAngleRange, mMaxAngleRange);
        }
    }
}