﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Effect_Slope))]
    public class Effect_SlopeEditor : EffectEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            SerializedProperty minAngleRangeProp = serializedObject.FindProperty("mMinAngleRange");
            SerializedProperty maxAngleRangeProp = serializedObject.FindProperty("mMaxAngleRange");

            EditorGUILayout.BeginHorizontal();

            minAngleRangeProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Min Angle"), minAngleRangeProp.floatValue);
            maxAngleRangeProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Max Angle"), maxAngleRangeProp.floatValue);

            EditorGUILayout.EndHorizontal();

            maxAngleRangeProp.floatValue = Mathf.Max(minAngleRangeProp.floatValue, maxAngleRangeProp.floatValue);

            

            return mainTransform;
        }
    }
}