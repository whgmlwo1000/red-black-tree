﻿using UnityEngine;
using UnityEngine.SceneManagement;
using ObjectManagement;
using CameraManagement;

namespace RedBlackTree
{
    public enum EEffectType
    {
        None = -1,

        FairyBeam = 0,
        RunDust = 1,
        CriticalFairyBeam = 2, 

        NormalDamage = 20, 
        CriticalDamage = 21, 
        
        NormalHeal = 30, 

        GlobalHeal = 40, 
        GlobalEnhance = 41, 

        InsaneWalnut = 3, 

        BlessedFairy = 4, 
        CorruptedFairy = 5, 
    }

    public enum EStateType_Effect
    {
        None = -1, 

        Activate = 0, 

        Count
    }

    public class Effect : AActor<EStateType_Effect>, IPoolable<EEffectType>
    {
        public sealed override EActorType ActorType { get { return EActorType.Effect; } }

        public virtual bool IsPooled { get { return !gameObject.activeSelf; } }

        [SerializeField]
        private bool mIsWorldCanvas = false;
        public bool IsWorldCanvas { get { return mIsWorldCanvas; } }

        [SerializeField]
        private EEffectType mType = EEffectType.None;
        public EEffectType Type { get { return mType; } }

        [SerializeField]
        private int mCountOfEffects = 1;
        public int CountOfEffects { get { return mCountOfEffects; } }

        public event System.Action OnActivate;
        public event System.Action OnDeactivate;

        private Vector2 mPrevParentPosition;

        public Transform Parent { get; private set; }
        private bool mIsCameraParent;
        private float mPrevCameraPosition_x;

        public override void Awake()
        {
            base.Awake();
            StateManager.AddOnExitAbyss(OnActiveSceneChanged);
            SetStates(new Effect_Activate(this));
        }

        public override void Start()
        {
            base.Start();
            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        public override void FixedUpdate()
        {
            if (!StateManager.Pause.State)
            {
                base.FixedUpdate();

                if (mIsCameraParent)
                {
                    float curCameraPosition_x = CameraController.Main.Position.x;
                    Vector2 curPosition = transform.position;
                    transform.position = new Vector3(curPosition.x + curCameraPosition_x - mPrevCameraPosition_x, curPosition.y);
                    mPrevCameraPosition_x = curCameraPosition_x;
                }
                else
                {
                    Vector2 parentPosition = Parent.position;
                    Vector2 curPosition = transform.position;
                    transform.position = curPosition + (parentPosition - mPrevParentPosition);
                    mPrevParentPosition = parentPosition;
                }
            }
        }

        public void Activate(Transform parent, Vector2 position)
        {
            Parent = parent;
            transform.position = position;

            if (Parent == null)
            {
                mIsCameraParent = true;
                mPrevCameraPosition_x = CameraController.Main.Position.x;
            }
            else
            {
                mIsCameraParent = false;
                mPrevParentPosition = Parent.position;
            }

            gameObject.SetActive(true);
            State = EStateType_Effect.Activate;
            OnActivate?.Invoke();
        }

        public void Deactivate_Animation()
        {
            OnDeactivate?.Invoke();
            gameObject.SetActive(false);
        }

        private void OnActiveSceneChanged()
        {
            Deactivate_Animation();
        }
        private void OnActiveSceneChanged(Scene src, Scene dst)
        {
            Deactivate_Animation();
        }
    }
}