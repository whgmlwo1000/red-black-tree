﻿using UnityEngine;
using ObjectManagement;

namespace RedBlackTree
{
    public class MainRendererHandler : Handler<int, MainRenderer>
    {
        public SpriteRenderer GetRenderer(int type)
        {
            return Get(type).Renderer;
        }
    }
}