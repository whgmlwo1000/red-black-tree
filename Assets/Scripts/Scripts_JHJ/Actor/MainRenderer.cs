﻿using UnityEngine;
using ObjectManagement;

namespace RedBlackTree
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class MainRenderer : MonoBehaviour, IHandleable<int>
    {
        [SerializeField]
        private int mType = 0;
        public int Type { get { return mType; } }

        private SpriteRenderer mRenderer;
        public SpriteRenderer Renderer
        {
            get
            {
                if(mRenderer == null)
                {
                    mRenderer = GetComponent<SpriteRenderer>();
                }
                return mRenderer;
            }
        }

        public void Awake()
        {
            mRenderer = GetComponent<SpriteRenderer>();
        }
    }
}