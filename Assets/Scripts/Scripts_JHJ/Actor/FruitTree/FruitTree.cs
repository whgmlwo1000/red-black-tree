﻿using UnityEngine;
using UnityEngine.SceneManagement;
using ObjectManagement;

namespace RedBlackTree
{
    public enum EFruitTree
    {
        None = -1, 

        Midgard_0, 

        Count
    }

    public enum EStateType_FruitTree
    {
        Normal = 0
    }

    public class FruitTree : AActor<EStateType_FruitTree>, IPoolable<EFruitTree>
    {
        public override EActorType ActorType { get { return EActorType.FruitTree; } }

        [SerializeField]
        private EFruitTree mTreeType = EFruitTree.None;
        public EFruitTree Type { get { return mTreeType; } }
        public bool IsPooled { get { return !gameObject.activeSelf; } }

        [SerializeField]
        private BasicStatus_FruitTree mBasicStatus = null;
        public Status_FruitTree Status { get; protected set; }

        public bool IsWorldCanvas { get { return false; } }

        [SerializeField]
        private int mCountOfAnimations = 1;

        [SerializeField]
        private FruitHolder[] mFruitHolders = new FruitHolder[0];

        [System.Serializable]
        private class FruitHolder
        {
            public Transform[] Holders = new Transform[0];
        }

        public override void Awake()
        {
            base.Awake();

            Status = ScriptableObject.CreateInstance<Status_FruitTree>();
            Status.BasicStatus = mBasicStatus;

            StateManager.AddOnExitAbyss(OnActiveSceneChanged);

            SetStates(new FruitTree_Normal(this));
        }

        public override void Start()
        {
            base.Start();

            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        public void GenerateOn(Vector2 position, EHorizontalDirection direction, bool verticalFlip)
        {
            transform.position = position;
            HorizontalDirection = direction;
            VerticalFlip = verticalFlip;

            gameObject.SetActive(true);

            int treeSelection = (int)Random.Range(0.0f, mCountOfAnimations);
            MainAnimator.SetInteger("tree", treeSelection);

            Vector2 treePosition = transform.position;
            
            int holderSelection = (int)Random.Range(0.0f, mFruitHolders[treeSelection].Holders.Length);
            Vector2 fruitPosition = mFruitHolders[treeSelection].Holders[holderSelection].position;

            EHorizontalDirection fruitDirection;
            if (treePosition.x > fruitPosition.x)
            {
                fruitDirection = EHorizontalDirection.Left;
            }
            else
            {
                fruitDirection = EHorizontalDirection.Right;
            }

            float weightSum = Status.SumOfFruitWeight;
            float weightSelection = Random.Range(0.0f, weightSum);
            for (int indexOfWeight = 0; indexOfWeight < Status.CountOfFruits; indexOfWeight++)
            {
                float weight = Status.GetFruitWeight(indexOfWeight);
                if (weightSelection < weight)
                {
                    EFruit fruitType = Status.GetFruitType(indexOfWeight);
                    Fruit fruit = FruitPool.Get(fruitType);
                    if (fruit != null)
                    {
                        fruit.GenerateOn(fruitPosition, fruitDirection);
                        return;
                    }
                }
                else
                {
                    weightSelection -= weight;
                }
            }
        }

        private void OnActiveSceneChanged()
        {
            gameObject.SetActive(false);
        }

        private void OnActiveSceneChanged(Scene src, Scene dst)
        {
            gameObject.SetActive(false);
        }
    }
}