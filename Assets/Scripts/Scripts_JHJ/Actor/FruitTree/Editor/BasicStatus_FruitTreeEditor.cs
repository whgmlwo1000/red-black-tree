﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(BasicStatus_FruitTree))]
    [CanEditMultipleObjects]
    public class BasicStatus_FruitTreeEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SerializedProperty countOfFruitsProp = serializedObject.FindProperty("CountOfFruits");
            SerializedProperty fruitsProp = serializedObject.FindProperty("Fruits");
            SerializedProperty fruitWeightsProp = serializedObject.FindProperty("FruitWeights");

            countOfFruitsProp.intValue = EditorGUILayout.IntField(new GUIContent("Count Of Fruits", "생성 가능 열매의 수")
                , countOfFruitsProp.intValue);
            countOfFruitsProp.intValue = (countOfFruitsProp.intValue < 1 ? 1 : countOfFruitsProp.intValue);

            for(int indexOfArray = 0; indexOfArray < fruitsProp.arraySize && indexOfArray < fruitWeightsProp.arraySize; indexOfArray++)
            {
                EditorGUILayout.Space();
                SerializedProperty fruitTypeProp = fruitsProp.GetArrayElementAtIndex(indexOfArray);
                SerializedProperty fruitWeightProp = fruitWeightsProp.GetArrayElementAtIndex(indexOfArray);

                EFruit fruitType = (EFruit)fruitTypeProp.intValue;
                fruitType = (EFruit)EditorGUILayout.EnumPopup(new GUIContent("Fruit Type", "생성될 열매 고유 타입")
                    , fruitType);
                fruitTypeProp.intValue = (int)fruitType;

                fruitWeightProp.floatValue = EditorGUILayout.FloatField(new GUIContent("Weight", "열매 생성 가중치")
                    , fruitWeightProp.floatValue);
                fruitWeightProp.floatValue = (fruitWeightProp.floatValue < 0.0f ? 0.0f : fruitWeightProp.floatValue);
                EditorGUILayout.Space();
            }

            if(fruitsProp.arraySize != countOfFruitsProp.intValue)
            {
                fruitsProp.arraySize = countOfFruitsProp.intValue;
            }
            if(fruitWeightsProp.arraySize != countOfFruitsProp.intValue)
            {
                fruitWeightsProp.arraySize = countOfFruitsProp.intValue;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}