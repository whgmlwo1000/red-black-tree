﻿using UnityEngine;
using UnityEditor;
using ObjectManagement;

namespace RedBlackTree
{
    [CustomEditor(typeof(FruitTreePool))]
    public class FruitTreePoolEditor : PoolEditor<EFruitTree>
    {

    }
}