﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(FruitTree))]
    public class FruitTreeEditor : AActorEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            SerializedProperty fruitTreeTypeProp = serializedObject.FindProperty("mTreeType");
            SerializedProperty basicStatusProp = serializedObject.FindProperty("mBasicStatus");
            SerializedProperty countOfAnimationsProp = serializedObject.FindProperty("mCountOfAnimations");
            SerializedProperty fruitHolderProps = serializedObject.FindProperty("mFruitHolders");

            EFruitTree fruitTreeType = (EFruitTree)fruitTreeTypeProp.intValue;
            fruitTreeType = (EFruitTree)EditorGUILayout.EnumPopup(new GUIContent("Fruit Tree Type", "열매 나무 고유 타입"), fruitTreeType);
            fruitTreeTypeProp.intValue = (int)fruitTreeType;

            basicStatusProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Basic Status", "열매 나무 정적 수치 스테이터스")
                , basicStatusProp.objectReferenceValue, typeof(BasicStatus_FruitTree), false);


            EditorGUILayout.Space();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(new GUIContent("Count Of Animations", "나무 종류 수"), GUILayout.MaxWidth(130));
            countOfAnimationsProp.intValue = EditorGUILayout.IntField(countOfAnimationsProp.intValue, GUILayout.MaxWidth(50));
            countOfAnimationsProp.intValue = Mathf.Max(countOfAnimationsProp.intValue, 1);
            EditorGUILayout.EndHorizontal();

            if(fruitHolderProps.arraySize != countOfAnimationsProp.intValue)
            {
                fruitHolderProps.arraySize = countOfAnimationsProp.intValue;
                serializedObject.ApplyModifiedProperties();
            }

            for(int indexOfProp = 0; indexOfProp < fruitHolderProps.arraySize; indexOfProp++)
            {
                EditorGUILayout.Space();
                SerializedProperty fruitHolderProp = fruitHolderProps.GetArrayElementAtIndex(indexOfProp);
                SerializedProperty holderProps = fruitHolderProp.FindPropertyRelative("Holders");

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(new GUIContent(string.Format("[{0}]. Holder Count", indexOfProp)), GUILayout.MaxWidth(100));
                int countOfHolders = EditorGUILayout.IntField(holderProps.arraySize, GUILayout.MaxWidth(50));
                if(holderProps.arraySize != countOfHolders)
                {
                    holderProps.arraySize = countOfHolders;
                    serializedObject.ApplyModifiedProperties();
                }
                EditorGUILayout.EndHorizontal();

                for(int indexOfHolder = 0; indexOfHolder < countOfHolders; indexOfHolder++)
                {
                    SerializedProperty holderProp = holderProps.GetArrayElementAtIndex(indexOfHolder);

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField(new GUIContent(string.Format("[{0}]-[{1}]. Holder", indexOfProp, indexOfHolder)), GUILayout.MaxWidth(100));
                    holderProp.objectReferenceValue = EditorGUILayout.ObjectField(holderProp.objectReferenceValue, typeof(Transform), true);
                    EditorGUILayout.EndHorizontal();
                }
            }

            serializedObject.ApplyModifiedProperties();

            return mainTransform;
        }
    }
}