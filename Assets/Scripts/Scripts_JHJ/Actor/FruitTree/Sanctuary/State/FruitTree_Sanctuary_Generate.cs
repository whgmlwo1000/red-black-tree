﻿using UnityEngine;

namespace RedBlackTree
{
    public class FruitTree_Sanctuary_Generate : State<EStateType_FruitTree_Sanctuary>
    {
        private FruitTree_Sanctuary mFruitTree;

        public FruitTree_Sanctuary_Generate(FruitTree_Sanctuary fruitTree) : base(EStateType_FruitTree_Sanctuary.Generate)
        {
            mFruitTree = fruitTree;
        }

        public override void Start()
        {
            mFruitTree.GenerateFruit();
            mFruitTree.State = EStateType_FruitTree_Sanctuary.Wait;
        }
    }
}