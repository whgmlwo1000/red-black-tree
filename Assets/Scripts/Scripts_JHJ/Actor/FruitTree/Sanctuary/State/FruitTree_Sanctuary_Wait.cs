﻿using UnityEngine;

namespace RedBlackTree
{
    public class FruitTree_Sanctuary_Wait : State<EStateType_FruitTree_Sanctuary>
    {
        private FruitTree_Sanctuary mFruitTree;
        private float mRemainTime_Wait;

        public FruitTree_Sanctuary_Wait(FruitTree_Sanctuary fruitTree) : base(EStateType_FruitTree_Sanctuary.Wait)
        {
            mFruitTree = fruitTree;
        }

        public override void Start()
        {
            mRemainTime_Wait = mFruitTree.GetWaitTime();
        }

        public override void Update()
        {
            if(mRemainTime_Wait > 0.0f)
            {
                mRemainTime_Wait -= Time.deltaTime;
            }
            else
            {
                mFruitTree.State = EStateType_FruitTree_Sanctuary.Generate;
            }
        }
    }
}