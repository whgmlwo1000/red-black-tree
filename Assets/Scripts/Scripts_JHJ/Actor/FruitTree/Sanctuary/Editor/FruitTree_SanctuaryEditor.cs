﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(FruitTree_Sanctuary))]
    public class FruitTree_SanctuaryEditor : AActorEditor
    {

        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            SerializedProperty waitTimeMinProp = serializedObject.FindProperty("mWaitTime_Min");
            SerializedProperty waitTimeMaxProp = serializedObject.FindProperty("mWaitTime_Max");
            SerializedProperty maxCountOfFruitsProp = serializedObject.FindProperty("mMaxCountOfFruits");
            SerializedProperty fruitHolderProps = serializedObject.FindProperty("mFruitHolders");

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Min Wait Time", GUILayout.MaxWidth(120));
            waitTimeMinProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(waitTimeMinProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);
            EditorGUILayout.LabelField("Max Wait Time", GUILayout.MaxWidth(120));
            waitTimeMaxProp.floatValue = Mathf.Max(EditorGUILayout.FloatField(waitTimeMaxProp.floatValue, GUILayout.MaxWidth(50)), 0.0f);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Max Fruit Count", GUILayout.MaxWidth(120));
            maxCountOfFruitsProp.intValue = Mathf.Max(EditorGUILayout.IntField(maxCountOfFruitsProp.intValue, GUILayout.MaxWidth(50)), 0);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Max Holder Count", GUILayout.MaxWidth(120));
            int fruitCount = Mathf.Max(EditorGUILayout.IntField(fruitHolderProps.arraySize, GUILayout.MaxWidth(50)), 0);
            if(fruitHolderProps.arraySize != fruitCount)
            {
                fruitHolderProps.arraySize = fruitCount;
                serializedObject.ApplyModifiedProperties();
            }
            EditorGUILayout.EndHorizontal();

            for(int indexOfHolder = 0; indexOfHolder < fruitCount; indexOfHolder++)
            {
                SerializedProperty holderProp = fruitHolderProps.GetArrayElementAtIndex(indexOfHolder);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(string.Format("{0}. Holder", indexOfHolder), GUILayout.MaxWidth(100));
                holderProp.objectReferenceValue = EditorGUILayout.ObjectField(holderProp.objectReferenceValue, typeof(Transform), true);
                EditorGUILayout.EndHorizontal();
            }

            return mainTransform;
        }
    }
}