﻿using UnityEngine;
using System.Collections.Generic;

namespace RedBlackTree
{
    public enum EStateType_FruitTree_Sanctuary
    {
        Wait, 
        Generate, 
    }

    /// <summary>
    /// 성역에서 연습용으로 사용되는 Fruit Tree
    /// 일정 시간을 간격으로 열매의 수를 검사해서
    /// 부족한 열매의 수만큼 지정된 위치에 열매를 생성
    /// </summary>
    public class FruitTree_Sanctuary : AActor<EStateType_FruitTree_Sanctuary>
    {
        public override EActorType ActorType { get { return EActorType.FruitTree; } }

        [SerializeField]
        private float mWaitTime_Min = 0.0f;
        
        [SerializeField]
        private float mWaitTime_Max = 1.0f;

        [SerializeField]
        private int mMaxCountOfFruits = 2;

        [SerializeField]
        private Transform[] mFruitHolders = null;
        private Fruit[] mFruits;
        private List<int> mHolderList;
        

        public int CountOfHolders { get { return mFruitHolders.Length; } }

        public override void Awake()
        {
            base.Awake();

            mFruits = new Fruit[CountOfHolders];
            mHolderList = new List<int>();

            SetStates(new FruitTree_Sanctuary_Wait(this)
                , new FruitTree_Sanctuary_Generate(this));
        }

        public float GetWaitTime()
        {
            return Random.Range(mWaitTime_Min, mWaitTime_Max);
        }

        public void GenerateFruit()
        {
            int countOfFruits = 0;

            mHolderList.Clear();
            for(int indexOfHolder = 0; indexOfHolder < CountOfHolders; indexOfHolder++)
            {
                if(mFruits[indexOfHolder] != null 
                    && !mFruits[indexOfHolder].IsPooled 
                    && mFruits[indexOfHolder].State == EStateType_Fruit.Bound)
                {
                    countOfFruits++;
                    if(countOfFruits == mMaxCountOfFruits)
                    {
                        return;
                    }
                }
                else
                {
                    mHolderList.Add(indexOfHolder);
                }
            }

            int selectedHolderIndex = mHolderList[Random.Range(0, mHolderList.Count)];
            Fruit fruit = FruitPool.Get((EFruit)Random.Range((int)EFruit.None + 1, (int)EFruit.Count));
            if(fruit != null)
            {
                EHorizontalDirection direction = (Random.Range(0, 2) == 0 ? EHorizontalDirection.Left : EHorizontalDirection.Right);

                mFruits[selectedHolderIndex] = fruit;
                fruit.GenerateOn(mFruitHolders[selectedHolderIndex].position, direction);
            }
        }
    }
}