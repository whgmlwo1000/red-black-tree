﻿using UnityEngine;
using System.Collections;

namespace RedBlackTree
{
    public class FruitTree_Normal : State<EStateType_FruitTree>
    {
        private FruitTree mTree;
        public FruitTree_Normal(FruitTree tree) : base(EStateType_FruitTree.Normal)
        {
            mTree = tree;
        }
    }
}