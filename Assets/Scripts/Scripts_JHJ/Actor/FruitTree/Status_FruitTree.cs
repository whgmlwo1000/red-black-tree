﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class Status_FruitTree : ScriptableObject
    {
        public BasicStatus_FruitTree BasicStatus { get; set; }

        public int CountOfFruits { get { return BasicStatus.CountOfFruits; } }

        public float SumOfFruitWeight
        {
            get
            {
                float sum = 0.0f;
                for(int indexOfWeight = 0; indexOfWeight < BasicStatus.CountOfFruits; indexOfWeight++)
                {
                    sum += GetFruitWeight(indexOfWeight);
                }
                return sum;
            }
        }

        public float GetFruitWeight(int index)
        {
            return BasicStatus.FruitWeights[index];
        }

        public EFruit GetFruitType(int index)
        {
            return BasicStatus.Fruits[index];
        }
    }
}