﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CreateAssetMenu(fileName = "New BasicStatus_FruitTree", menuName = "Fruit/Tree Basic Status", order = 1000)]
    public class BasicStatus_FruitTree : ScriptableObject
    {
        public int CountOfFruits;

        public EFruit[] Fruits;
        public float[] FruitWeights;
    }
}