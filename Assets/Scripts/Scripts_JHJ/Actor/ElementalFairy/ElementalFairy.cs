﻿using UnityEngine;

namespace RedBlackTree
{
    public enum EStateType_ElementalFairy
    {
        None = -1, 

        Wait, 
        Fire, 

        Count
    }

    public enum EElementalFairy
    {
        None = -1, 
        Flame = 0, 
        Light = 1, 
        Count
    }

    public enum ETriggerType_ElementalFairy
    {
        DetectMonster, 
    }

    public enum EAudioType_ElementalFairy
    {

    }

    [RequireComponent(typeof(TriggerHandler_ElementalFairy), typeof(AudioHandler_ElementalFairy))]
    public class ElementalFairy : AActor<EStateType_ElementalFairy>
    {
        public override EActorType ActorType { get { return EActorType.ElementalFairy; } }

        public TriggerHandler_ElementalFairy TriggerHandler { get; private set; }
        public AudioHandler_ElementalFairy AudioHandler { get; private set; }

        public bool IsActivated { get; private set; }

        public PassiveItem_ElementalFairy PassiveItem { get; private set; }

        [SerializeField]
        private EElementalFairy mElementalType = EElementalFairy.None;
        public EElementalFairy ElementalType { get { return mElementalType; } }

        [SerializeField]
        private EPassiveItem mPassiveItemType = EPassiveItem.None;
        public EPassiveItem PassiveItemType { get { return mPassiveItemType; } }

        [SerializeField]
        private EProjectile mProjectileType = EProjectile.None;

        private float mFireRemainTime;

        private Giant mGiant;

        public override void Awake()
        {
            base.Awake();

            TriggerHandler = GetComponent<TriggerHandler_ElementalFairy>();
            AudioHandler = GetComponent<AudioHandler_ElementalFairy>();

            mGiant = GetComponentInParent<Giant>();

            SetStates(new ElementalFairy_Wait(this)
                , new ElementalFairy_Fire(this));

            gameObject.SetActive(false);
        }

        public override void Update()
        {
            base.Update();

            if(!StateManager.Pause.State)
            {
                if(mFireRemainTime > 0.0f)
                {
                    mFireRemainTime -= Time.deltaTime;
                }
                else
                {
                    mFireRemainTime = PassiveItem.ProjectileTerm;
                    State = EStateType_ElementalFairy.Fire;
                }
            }
        }

        public void Activate(PassiveItem_ElementalFairy passiveItem)
        {
            PassiveItem = passiveItem;
            IsActivated = true;
            gameObject.SetActive(true);

            mFireRemainTime = PassiveItem.ProjectileTerm;
            State = EStateType_ElementalFairy.Wait;
        }

        public void Deactivate()
        {
            IsActivated = false;
            gameObject.SetActive(false);
        }

        private void EndFire_Animation()
        {
            State = EStateType_ElementalFairy.Wait;
        }

        private void Fire_Animation()
        {
            mFireRemainTime = PassiveItem.ProjectileTerm;
            Projectile_Straight projectile = ProjectilePool.Get(mProjectileType) as Projectile_Straight;

            if (projectile != null)
            {
                Collider2D[] colliders = TriggerHandler.GetColliders(ETriggerType_ElementalFairy.DetectMonster, out int count);
                
                for (int indexOfCollider = 0; indexOfCollider < count; indexOfCollider++)
                {
                    Monster monster = colliders[indexOfCollider].GetComponentInParent<Monster>();
                    if (monster != null)
                    {
                        projectile.SetParams(srcActor: mGiant
                            , dstPosition: monster.CenterTransform.position
                            , offense: (int)(PassiveItem.ProjectileOffense * mGiant.PlayerStatus.GetStatus(EPlayerStatus_Coefficient.ItemOffense))
                            , force: 0.0f
                            , criticalChance: mGiant.PlayerStatus.GetStatus(EPlayerStatus_Constant.CriticalChance)
                            , criticalMagnification: ValueConstants.CriticalMagnification);
                        projectile.Activate(null, transform.position);
                    }
                }
            }
        }
    }
}