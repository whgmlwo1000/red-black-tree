﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(ElementalFairy))]
    public class ElementalFairyEditor : AActorEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTransform = base.SetProperties();

            SerializedProperty elementalTypeProp = serializedObject.FindProperty("mElementalType");
            SerializedProperty passiveItemTypeProp = serializedObject.FindProperty("mPassiveItemType");
            SerializedProperty projectileTypeProp = serializedObject.FindProperty("mProjectileType");

            EElementalFairy elementalType = (EElementalFairy)elementalTypeProp.intValue;
            elementalType = (EElementalFairy)EditorGUILayout.EnumPopup(new GUIContent("Elemental Type", "정령 속성 타입"), elementalType);
            elementalTypeProp.intValue = (int)elementalType;

            EPassiveItem passiveItemType = (EPassiveItem)passiveItemTypeProp.intValue;
            passiveItemType = (EPassiveItem)EditorGUILayout.EnumPopup(new GUIContent("Passive Item Type", "패시브 아이템 타입"), passiveItemType);
            passiveItemTypeProp.intValue = (int)passiveItemType;

            EProjectile projectileType = (EProjectile)projectileTypeProp.intValue;
            projectileType = (EProjectile)EditorGUILayout.EnumPopup(new GUIContent("Projectile Type", "사용할 투사체 타입"), projectileType);
            projectileTypeProp.intValue = (int)projectileType;

            serializedObject.ApplyModifiedProperties();

            return mainTransform;
        }
    }
}