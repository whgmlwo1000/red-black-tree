﻿using UnityEngine;

namespace RedBlackTree
{
    public class ElementalFairy_Wait : State<EStateType_ElementalFairy>
    {
        private ElementalFairy mFairy;

        public ElementalFairy_Wait(ElementalFairy fairy) : base(EStateType_ElementalFairy.Wait)
        {
            mFairy = fairy;
        }

        public override void Start()
        {
            mFairy.MainAnimator.SetInteger("state", (int)Type);
        }
    }
}