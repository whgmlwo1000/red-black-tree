﻿using UnityEngine;

namespace RedBlackTree
{
    public class ElementalFairy_Fire : State<EStateType_ElementalFairy>
    {
        private ElementalFairy mFairy;
        private bool mIsStarted;

        public ElementalFairy_Fire(ElementalFairy fairy) : base(EStateType_ElementalFairy.Fire)
        {
            mFairy = fairy;
        }

        public override void Start()
        {
            if (!mIsStarted)
            {
                mIsStarted = true;
                mFairy.MainAnimator.SetInteger("state", (int)Type);
            }
            else
            {
                mFairy.MainAnimator.SetTrigger("fire");
            }
        }

        public override void End()
        {
            mIsStarted = false;
        }
    }
}