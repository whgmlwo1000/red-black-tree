﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using ObjectManagement;

namespace RedBlackTree
{
    public enum EStateType_Price
    {
        Activate, 
    }

    public enum EPrice
    {
        None = -1, 
        Normal = 0, 
    }

    public class Price : AActor<EStateType_Price>, IPoolable<EPrice>
    {
        public override EActorType ActorType { get { return EActorType.Price; } }

        public bool IsWorldCanvas { get { return true; } }

        public bool IsPooled { get { return !gameObject.activeSelf; } }

        [SerializeField]
        private EPrice mType = EPrice.None;
        public EPrice Type { get { return mType; } }

        private Product mProduct;
        public Text PriceText { get; private set; }
        private int mPrice;
        private int mSoulType;

        public override void Awake()
        {
            base.Awake();

            PriceText = GetComponentInChildren<Text>(true);
        }

        public override void Start()
        {
            base.Start();

            SceneManager.activeSceneChanged += OnActiveSceneChanged;
        }

        public override void Update()
        {
            if(!StateManager.Pause.State)
            {
                base.Update();

                if (mProduct.State != EStateType_Product.Displayed)
                {
                    Deactivate();
                    return;
                }

                int productPrice = mProduct.Price;
                if (mPrice != productPrice)
                {
                    mPrice = productPrice;
                    PriceText.text = mPrice.ToString();
                }

                if(mProduct.CanBeBought)
                {
                    PriceText.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                }
                else
                {
                    PriceText.color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
                }
            }
        }

        public override void FixedUpdate()
        {
            if (!StateManager.Pause.State)
            {
                base.FixedUpdate();
                transform.position = mProduct.PriceHolder.position;
            }
        }

        public void Activate(Product product)
        {
            mProduct = product;
            transform.position = mProduct.PriceHolder.position;
            gameObject.SetActive(true);
            mPrice = product.Price;
            PriceText.text = mPrice.ToString();

            if (mProduct.CanBeBought)
            {
                PriceText.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            }
            else
            {
                PriceText.color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
            }

            if (StateManager.Scene.State == EScene.Scene_Sanctuary)
            {
                mSoulType = 0;
                MainAnimator.SetInteger("type", mSoulType);
            }
            else
            {
                mSoulType = 1;
                MainAnimator.SetInteger("type", mSoulType);
            }
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);
        }

        private void OnActiveSceneChanged(Scene src, Scene dst)
        {
            Deactivate();
        }
    }
}