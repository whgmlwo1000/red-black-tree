﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    [CustomEditor(typeof(Price))]
    public class PriceEditor : AActorEditor
    {
        public override Transform SetProperties()
        {
            Transform mainTrnasform = base.SetProperties();

            SerializedProperty priceTypeProp = serializedObject.FindProperty("mType");

            EPrice priceType = (EPrice)priceTypeProp.intValue;
            priceType = (EPrice)EditorGUILayout.EnumPopup(new GUIContent("Price Type"), priceType);
            priceTypeProp.intValue = (int)priceType;

            return mainTrnasform;
        }

    }
}