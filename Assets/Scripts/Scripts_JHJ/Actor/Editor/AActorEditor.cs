﻿using UnityEngine;
using UnityEditor;

namespace RedBlackTree
{
    public class AActorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SetProperties();
            serializedObject.ApplyModifiedProperties();
        }

        public virtual Transform SetProperties()
        {
            Transform mainTransform = AddMainTransform();
            AddMainRendererHandler(mainTransform);
            ShowHorizontalDirectionAndVerticalFlip(mainTransform);
            ShowAngle();
            return mainTransform;
        }

        /// <summary>
        /// 액터의 Main Transform을 자동으로 추가 및 설정
        /// </summary>
        /// <returns></returns>
        public Transform AddMainTransform()
        {
            IActor actor = target as IActor;

            SerializedProperty mainTransformProp = serializedObject.FindProperty("mMainTransform");

            mainTransformProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Main Transform", "Rotation, Scale의 적용 대상이 되는 오브젝트"), mainTransformProp.objectReferenceValue, typeof(Transform), true);
            if (mainTransformProp.objectReferenceValue == null)
            {
                GameObject mainTransformObject = new GameObject("Main Transform");
                mainTransformObject.transform.eulerAngles = Vector3.zero;
                mainTransformObject.transform.localScale = Vector3.one;
                mainTransformObject.transform.SetParent(actor.transform);
                mainTransformObject.transform.localPosition = Vector3.zero;
                mainTransformProp.objectReferenceValue = mainTransformObject.transform;
                serializedObject.ApplyModifiedProperties();
            }
            

            return mainTransformProp.objectReferenceValue as Transform;
        }
        /// <summary>
        /// 액터의 Main Renderer Handler를 자동으로 추가 및 설정
        /// </summary>
        /// <param name="mainTransform"></param>
        /// <returns></returns>
        public MainRendererHandler AddMainRendererHandler(Transform mainTransform)
        {
            IActor actor = target as IActor;

            SerializedProperty mainRendererProp = serializedObject.FindProperty("mMainRendererHandler");
            mainRendererProp.objectReferenceValue = EditorGUILayout.ObjectField(new GUIContent("Main Renderer Handler", "Main Transform 자식으로 존재하고, 오브젝트의 주요 부분을 그리는 렌더러"), mainRendererProp.objectReferenceValue, typeof(MainRendererHandler), true);
            if (mainRendererProp.objectReferenceValue == null)
            {
                GameObject mainRendererObject = new GameObject("Main Renderers", typeof(MainRendererHandler));
                mainRendererObject.transform.eulerAngles = Vector3.zero;
                mainRendererObject.transform.localScale = Vector3.one;
                mainRendererObject.transform.SetParent(mainTransform);
                mainRendererObject.transform.localPosition = Vector3.zero;
                mainRendererProp.objectReferenceValue = mainRendererObject.GetComponent<MainRendererHandler>();
                serializedObject.ApplyModifiedProperties();
            }

            return mainRendererProp.objectReferenceValue as MainRendererHandler;
        }

        /// <summary>
        /// 액터의 수평 방향과 수직 Flip 을 출력하고 설정하는 메소드
        /// </summary>
        /// <param name="mainTransform"></param>
        public void ShowHorizontalDirectionAndVerticalFlip(Transform mainTransform)
        {
            IActor actor = target as IActor;

            SerializedProperty horizontalDirectionProp = serializedObject.FindProperty("mHorizontalDirection");
            EHorizontalDirection direction = actor.HorizontalDirection;
            
            Vector3 lossyScale = mainTransform.lossyScale;

            if (lossyScale.x > 0.0f)
            {
                if (direction != EHorizontalDirection.Right)
                {
                    direction = EHorizontalDirection.Right;
                    horizontalDirectionProp.intValue = (int)direction;
                    serializedObject.ApplyModifiedProperties();
                }
            }
            else
            {
                if(direction != EHorizontalDirection.Left)
                {
                    direction = EHorizontalDirection.Left;
                    horizontalDirectionProp.intValue = (int)direction;
                    serializedObject.ApplyModifiedProperties();
                }
            }

            SerializedProperty verticalFlipProp = serializedObject.FindProperty("mVerticalFlip");
            bool verticalFlip = actor.VerticalFlip;

            if (lossyScale.y > 0.0f)
            {
                if (verticalFlip)
                {
                    verticalFlip = false;
                    verticalFlipProp.boolValue = verticalFlip;
                    serializedObject.ApplyModifiedProperties();
                }
            }
            else
            {
                if (!verticalFlip)
                {
                    verticalFlip = true;
                    verticalFlipProp.boolValue = verticalFlip;
                    serializedObject.ApplyModifiedProperties();
                }
            }

            direction = (EHorizontalDirection)EditorGUILayout.EnumPopup(new GUIContent("Horizontal Direction", "수평 방향"), direction);
            actor.HorizontalDirection = direction;

            verticalFlip = EditorGUILayout.Toggle(new GUIContent("Vertical Flip", "수직 반전"), verticalFlip);
            actor.VerticalFlip = verticalFlip;
        }

        /// <summary>
        /// 액터의 Angle을 출력하고 설정하는 메소드
        /// </summary>
        public void ShowAngle()
        {
            IActor actor = target as IActor;

            actor.Angle = EditorGUILayout.FloatField(new GUIContent("Angle", "각도"), actor.Angle);
        }
    }
}