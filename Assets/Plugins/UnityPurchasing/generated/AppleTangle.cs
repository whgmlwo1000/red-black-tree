#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("OllbKpgbOCoXHBMwnFKc7RcbGxvaeSlt7SAdNkzxwBU7FMCgaQNVrx4cCRhPSSsJKgscGU8eEAkQW2pqBZ+ZnwGDJ10t6LOBWpQ2zquKCMJlW7KC48vQfIY+cQvKuaH+ATDZBTwqPhwZTx4RCQdbamp2fzpZf2huHfZnI5mRSTrJIt6rpYBVEHHlMeZzfHN5e25zdXQ6W29ucnVoc25jK5oOMcpzXY5sE+TucZc0WrztXVdlHCoVHBlPBwkbG+UeHyoZGxvlKgc1KpvZHBIxHBsfHx0YGCqbrACbqa8gt+4VFBqIEas7DDRuzyYXwXgMmBsaHBMwnFKc7Xl+Hxsqm+gqMBwMKg4cGU8eGQkXW2pqdn86SHV1bhJEKpgbCxwZTwc6HpgbEiqYGx4qOnt0fjp5f2huc3xzeXtuc3V0OmrTA2jvRxTPZUWB6D8ZoE+VV0cX62p2fzpZf2huc3xzeXtuc3V0OltvYCqYG2wqFBwZTwcVGxvlHh4ZGBt4dn86aW57dH57aH46bn9od2k6e60Bp4lYPggw3RUHrFeGRHnSUZoNFxwTMJxSnO0XGxsfHxoZmBsbGkaRA5PE41F27x2xOCoY8gIk4koTyW1tNHtqanZ/NHl1dzV7amp2f3l7aHt5bnN5fzppbntuf3d/dG5pNConPH06kClw7ReY1cTxuTXjSXBBfpVpm3rcAUETNYio4l5S6noihA/vSH92c3t0eX86dXQ6bnJzaTp5f2hTwmyFKQ5/u22O0zcYGRsaG7mYG7LGZDgv0D/PwxXMcc64PjkL7bu2Njp5f2huc3xzeXtufzpqdXZzeWNjOntpaW93f2k6e3l5f2pue3R5f25ydWhzbmMrDCoOHBlPHhkJF1tqsblriF1JT9u1NVup4uH5atf8uVYcGU8HFB4MHg4xynNdjmwT5O5xl8MsZdudT8O9g6MoWOHCz2uEZLtIj4RgFr5dkUHODC0p0d4VV9QOc8t0fjp5dXR+c25zdXRpOnV8Om9pfwWLwQRdSvEf90RjnjfxLLhNVk/2NFq87V1XZRJEKgUcGU8HOR4CKgxDvR8TZg1aTAsEbsmtkTkhXbnPdSyDVjdirfeWgcbpbYHobMhtKlXbdn86U3R5NCs8Kj4cGU8eEQkHW2oqmB6hKpgZuboZGBsYGBsYKhccEy8oKy4qKSxADRcpLyooKiMoKy4qanZ/Okh1dW46WVsqBA0XKiwqLig++PHLrWrFFV/7PdDrd2L3/a8NDSoLHBlPHhAJEFtqanZ/OlN0eTQrX2QFVnFKjFuT3m54EQqZW50pkJuk7mmB9Mh+FdFjVS7CuCTjYuVx0iksQCp4KxEqExwZTx4cCRhPSSsJFYcn6TFTMgDS5NSvoxTDRAbM0Sd9lRKuOu3RtjY6dWqsJRsqlq1Z1asqQvZAHiiWcqmVB8R/aeV9RH+mbnN8c3l7bn86eGM6e3RjOmp7aG4wnFKc7RcbGx8fGip4KxEqExwZTx8aGZgbFRoqmBsQGJgbGxr+i7MTEjEcGx8fHRgbDARybm5qaSA1NW06dXw6bnJ/Om5yf3Q6e2pqdnN5e34vOQ9RD0MHqY7t7IaE1Uqg20JKSrCQz8D+5soTHS2qb287");
        private static int[] order = new int[] { 13,57,2,49,17,10,51,11,32,29,37,23,34,34,33,45,19,17,28,42,59,47,42,36,46,56,55,38,38,54,44,40,37,46,44,58,38,43,45,40,52,48,49,44,45,51,53,54,56,50,51,51,54,55,54,57,57,58,58,59,60 };
        private static int key = 26;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
