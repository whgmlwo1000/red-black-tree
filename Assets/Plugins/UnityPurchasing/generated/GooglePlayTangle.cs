#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("HCBPHvZQfsRHjwiW8O781V8EefIpSH77Mv+/M2ViL8v/apDf1ZYRuOesimwmRx/hSWiKrb+D/Rx7D74FXs6ktrripeT4lNHywS5KP1578Nx1JdyQBCjZs2LdCVkf+v0rSqSYtfd0enVF93R/d/d0dHXSTg0bFRdpAH5RN9TW3WdjNdhsAk6En8Nt1V5CTrIqz/tufFNFr5Iv/ENQ+K2Pv4oEOMwNbJr3D4R9ocTK1HhxVGpVyJfysV0U6lrXZugUGeIF9aREd5QqqKEudpWO8GQadJT7pfhhj5V7pAt9nH+ZyNvwYCD3Joit1e99KbPyz0s4ymUyudPIXOvx/nm7S8IsQhhF93RXRXhzfF/zPfOCeHR0dHB1disQ/PVlg6aYTHd2dHV0");
        private static int[] order = new int[] { 12,2,7,3,6,7,13,7,10,9,10,11,13,13,14 };
        private static int key = 117;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
