# DataManager static class

## 관련 클래스 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**Dictionary\<K, T\>**](https://docs.microsoft.com/ko-kr/dotnet/api/system.collections.generic.dictionary-2?view=netframework-4.8) | CSV 파일 형태의 데이터를 관리하기 위해 저장하는 자료구조로 Dictionary를 사용한다. |
| [**ActorStatus**] | DataManagere는 Actor들의 Status를 저장하고 관리한다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| static ActorStatus **GetActorStatus**(string name) | 파라미터로 전달된 name을 이름으로 가지는 Actor의 Status를 생성하여 반환한다. |
| static ActorStatus **SetActorStatus**(string name, ActorStatus status) | 파라미터로 전달된 name을 이름으로 가지는 Actor의 Status를 파라미터로 전달된 status로 갱신하는 메소드이다. <br> 본 메소드 호출시 파일입출력이 일어난다. |
