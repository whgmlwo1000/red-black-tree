# Trigger\<ETriggerFlags\> class
in TriggerHandler2D namespace  
  
[*Object<E>*](https://gitlab.com/phantasylab/red-black-tree/blob/V2.3/Documents/Object.md)를 상속  
ETriggerFlags는 [_Enum_](https://docs.microsoft.com/ko-kr/dotnet/api/system.enum?view=netframework-4.8)을 상속

## 관련 클래스 및 열거형
| 이 름 | 설 명 |
|:---|:---|
|[**Collider2D**](https://docs.unity3d.com/kr/current/ScriptReference/Collider2D.html) | 본 클래스는 Collider2D의 Trigger 기능을 보다 효율적으로 사용하기 위해 개발되었다. |
|[**ELayerFlags**](https://gitlab.com/phantasylab/red-black-tree/blob/master/Documents/ELayerFlags.md) | Unity 자체에서는 32개의 Layer만 지원해주기 때문에 코드 내에서도 Layer의 구별을 구현해야 한다. <br> 본 클래스는 Layer를 구분해서 저장하기 위해 ELayerFlags를 사용한다. |

## 델리게이트
| 이 름 | 설 명 |
|:---|:---|
| void **OnTrigger**(Collider2D collider) | OnTriggerEnter, OnTriggerExit 시에 호출되는 이벤트로 사용된다. |

## 이벤트
| 이 름 | 설 명 |
|:---|:---|
| OnTrigger **OnTriggerEnter** | OnTriggerEnter2D(Collider2D collider) 메소드 호출 시, collider의 Layer가 등록된 Layer 내에 존재한다면 호출되는 이벤트 메소드이다. |
| OnTrigger **OnTriggerExit** | OnTriggerExit2D(Collider2D collider) 메소드 호출 시, collider의 Layer가 등록된 Layer 내에 존재한다면 호출되는 이벤트 메소드이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| Collider2D **Collider** (get) | Trigger로 설정된 Collider2D 컴포넌트를 반환하는 프로퍼티이다. |
| ETriggerFlags **ObjectType** (get) | 설정된 ETriggerFlags를 반환하는 프로퍼티이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| Collider2D[] **GetColliders**() | OnTriggerEnter2D 메소드를 호출하여 들어온 Collider2D 객체들 중 설정된 Layer를 가지는 Collider2D 객체 배열을 반환하는 메소드이다. |