# TriggerHandler\<ETriggerFlags\> class
in TriggerHandler2D namespace  
  
[*Handler<Trigger<ETriggerFlags>, ETriggerFlags>*](https://gitlab.com/phantasylab/red-black-tree/blob/V2.3/Documents/Handler.md)를 상속  
ETriggerFlags는 [_Enum_](https://docs.microsoft.com/ko-kr/dotnet/api/system.enum?view=netframework-4.8)을 상속

## 관련 클래스 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**Collider2D**](https://docs.unity3d.com/kr/current/ScriptReference/Collider2D.html) | 본 클래스는 Collider2D의 Trigger 기능을 보다 효율적으로 사용하기 위해 개발되었다. |
| [**Trigger\<ETriggerFlags\>**](https://gitlab.com/phantasylab/red-black-tree/blob/master/Documents/Trigger%3CETriggerFlags%3E.md) | 자식 오브젝트의 컴포넌트로 등록된 모든 Trigger를 관리하기 위해 각 Trigger 오브젝트의 컴포넌트로 등록되어야 하는 클래스이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| Collider2D[] **GetColliders**(ETriggerFlags triggerFlag) | triggerFlag로 설정된 Trigger 객체가 감지한 Collider2D 배열을 반환하는 메소드이다. |
| Collider2D **GetTrigger**(ETriggerFlags triggerFlag) | triggerFlag로 설정된 Trigger 객체가 사용하고 있는 Collider2D를 반환하는 메소드이다. |
| void **AddEnterEvent**(ETriggerFlags triggerFlag, Trigger\<ETriggerFlags\>.OnTrigger onTrigger) | triggerFlag로 설정된 Trigger 객체에 onTrigger 메소드를 OnEnterEvent에 등록하는 메소드이다. |
| void **RemoveEnterEvent**(ETriggerFlags triggerFlag, Trigger\<ETriggerFlags\>.OnTrigger onTrigger) | triggerFlag로 설정된 Trigger 객체에 onTrigger 메소드를 OnTriggerEnter로 부터 제거하는 메소드이다. |
| void **AddExitEvent**(ETriggerFlags triggerFlag, Trigger\<ETriggerFlags\>.OnTrigger onTrigger) | triggerFlag로 설정된 Trigger 객체에 onTrigger 메소드를 OnExitEvent에 등록하는 메소드이다. |
| void **RemoveExitEvent**(ETriggerFlags triggerFlag, Trigger\<ETriggerFlags\>.OnTrigger onTrigger) | triggerFlag로 설정된 Trigger 객체에 onTrigger 메소드를 OnTriggerExit로 부터 제거하는 메소드이다. |
