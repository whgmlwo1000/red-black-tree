# APossessable\<E\> abstract class
 [**StateBase\<E\>**]()를 상속  
 E는 [**System.Enum**]()을 상속

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:----|:---|
| [**IActor**]() | APossessable이 빙의 관련 기능을 수행하기 위해서는 빙의를 하는 객체의 참조가 필요하다. |

## 메소드
| 이 름 | 설 명 |
|:----|:---|
| void **Possess**(IActor actor) | 빙의를 시작하도록 작동하는 메소드로 actor에는 빙의를 하는 객체가 전달된다. |
