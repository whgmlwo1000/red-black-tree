# Command class
## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
|float **MoveHorizontal** (_get_, _set_)| 수평 이동 커맨드 값을 저장하는 프로퍼티이다.|
|bool **CanMoveHorizontal** (_get_, _set_)| 수평 이동 커맨드 마스크 값을 저장하는 프로퍼티이다. <br> *default = true*|
|float **MoveVertical** (_get_, _set_)| 수직 이동 커맨드 값을 저장하는 프로퍼티이다. <br>|
|float **CanMoveVertical** (_get_, _set_)| 수직 이동 커맨드 마스크 값을 저장하는 프로퍼티이다. <br> *default = true*|
|bool[] **Attack** (_get_)| 공격 커맨드 값들을 저장하는 프로퍼티이다.|
|bool[] **AttackDown** (_get_)| 공격 Down 커맨드 값들을 저장하는 프로퍼티이다.|
|bool[] **AttackUp** (_get_)| 공격 Up 커맨드 값들을 저장하는 프로퍼티이다.|
|bool[] **CanAttack** (_get_)| 공격 커맨드 마스크 값들을 저장하는 프로퍼티이다. <br> *default = true*|
|bool **AnyAttack** (_get_)| 공격 커맨드 값들 중 하나라도 true가 된 것이 있는지 반환하는 프로퍼티이다.|
|bool **AnyAttackDown** (_get_)| 공격 Down 커맨드 값들 중 하나라도 true가 된 것이 있는지 반환하는 프로퍼티이다.|
|bool **AnyAttackUp** (_get_)| 공격 Up 커맨드 값들 중 하나라도 true가 된 것이 있는지 반환하는 프로퍼티이다.|
|bool[] **Use** (_get_)| 아이템 사용 커맨드 값들을 저장하는 프로퍼티이다.|
|bool[] **UseDown** (_get_)| 아이템 사용 Down 커맨드 값들을 저장하는 프로퍼티이다.|
|bool[] **UseUp** (_get_)| 아이템 사용 Up 커맨드 값들을 저장하는 프로퍼티이다.|
|bool[] **CanUse** (_get_)| 아이템 사용 커맨드 마스크 값들을 저장하는 프로퍼티이다. <br> *default = true*|
|bool **AnyUse** (_get_)| 아이템 사용 커맨드 값들 중 하나라도 true가 된 것이 있는지 반환하는 프로퍼티이다.|
|bool **AnyUseDown** (_get_)| 아이템 사용 Down 커맨드 값들 중 하나라도 true가 된 것이 있는지 반환하는 프로퍼티이다.|
|bool **AnyUseUp** (_get_)| 아이템 사용 Up 커맨드 값들 중 하나라도 true가 된 것이 있는지 반환하는 프로퍼티이다.|
|bool[] **Skill** (_get_)| 스킬 커맨드 값들을 저장하는 프로퍼티이다.|
|bool[] **SkillDown** (_get_)| 스킬 Down 커맨드 값들을 저장하는 프로퍼티이다.|
|bool[] **SkillUp** (_get_)| 스킬 Up 커맨드 값들을 저장한는 프로퍼티이다.|
|bool[] **CanSkill** (_get_)| 스킬 커맨드 마스크 값들을 저장하는 프로퍼티이다. <br> *default = true*|
|bool **AnySkill** (_get_)| 스킬 커맨드 값들 중 하나라도 true가 된 것이 있는지 반환하는 프로퍼티이다.|
|bool **AnySkillDown** (_get_)| 스킬 Down 커맨드 값들 중 하나라도 true가 된 것이 있는지 반환하는 프로퍼티이다.|
|bool **AnySkillUp** (_get_)| 스킬 Up 커맨드 값들 중 하나라도 true가 된 것이 있는지 반환하는 프로퍼티이다.|
|bool **Interact** (_get_, _set_)| 상호작용 커맨드 값을 저장하는 프로퍼티이다.|
|bool **InteractDown** (_get_, _set_)| 상호작용 Down 커맨드 값을 저장하는 프로퍼티이다.|
|bool **InteractUp** (_get_, _set_)| 상호작용 Up 커맨드 값을 저장하는 프로퍼티이다.|
|bool **CanInteract** (_get_, _set_)| 상호작용 커맨드 마스크 값을 저장하는 프로퍼티이다. <br> *default = true*|
|bool **MoveHorizontalMenu** (_get_, _set_)| 메뉴 수평 이동 커맨드 값을 저장하는 프로퍼티이다.|
|bool **CanMoveHorizontalMenu** (_get_, _set_)| 메뉴 수평 이동 커맨드 마스크 값을 저장하는 프로퍼티이다. <br> *default = true*|
|bool **MoveVerticalMenu** (_get_, _set_)| 메뉴 수직 이동 커맨드 값을 저장하는 프로퍼티이다.|
|bool **CanMoveVerticalMenu** (_get_, _set_)| 메뉴 수직 이동 커맨드 마스크 값을 저장하는 프로퍼티이다. <br> *default = true*|
|bool **Confirm** (_get_, _set_)| 확인 커맨드 값을 저장하는 프로퍼티이다.|
|bool **ConfirmDown** (_get_, _set_)| 확인 Down 커맨드 값을 저장하는 프로퍼티이다.|
|bool **ConfirmUp** (_get_, _set_)| 확인 Up 커맨드 값을 저장하는 프로퍼티이다.|
|bool **CanConfirm** (_get_, _set_)| 확인 커맨드 마스크 값을 저장하는 프로퍼티이다. <br> *default = true*|
|bool **Cancel** (_get_, _set_)| 취소 커맨드 값을 저장하는 프로퍼티이다.|
|bool **CancelDown** (_get_, _set_)| 취소 Down 커맨드 값을 저장하는 프로퍼티이다.|
|bool **CancelUp** (_get_, _set_)| 취소 Up 커맨드 값을 저장하는 프로퍼티이다.|
|bool **CanCancel** (_get_, _set_)| 취소 커맨드 마스크 값을 저장하는 프로퍼티이다. <br> *default = true*|
|bool **Menu** (_get_, _set_)| 메뉴 커맨드 값을 저장하는 프로퍼티이다.|
|bool **MenuDown** (_get_, _set_)| 메뉴 Down 커맨드 값을 저장하는 프로퍼티이다.|
|bool **MenuUp** (_get_, _set_)| 메뉴 Up 커맨드 값을 저장하는 프로퍼티이다.|
|bool **CanMenu** (_get_, _set_)| 메뉴 커맨드 마스크 값을 저장하는 프로퍼티이다. <br> *default = true*|
|bool **Escape** (_get_, _set_)| 탈출 커맨드 값을 저장하는 프로퍼티이다.|
|bool **EscapeDown** (_get_, _set_)| 탈출 Down 커맨드 값을 저장하는 프로퍼티이다.|
|bool **EscapeUp** (_get_, _set_)| 탈출 Up 커맨드 값을 저장하는 프로퍼티이다.|
|bool **CanEscape** (_get_, _set_)| 탈출 커맨드 마스크 값을 저장하는 프로퍼티이다. <br> *default = true*|

# 생성자
| 이 름 | 설 명 |
|:---|:---|
|**Command**(int countOfAttack, int countOfUse, int countOfSkill)| **Attack** 프로퍼티를 countOfAttack, **Use** 프로퍼티를 countOfUse, **Skill** 프로퍼티를 countOfSkill 만큼 할단하는 생성자이다.|
