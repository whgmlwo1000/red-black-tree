# ActorStatus class

## 관련 클래스, 구조체 및 열거형 
| 이 름 | 설 명 |
| [**EActorSize**]() | Actor의 크기를 저장하기 위해 사용되는 열거형이다. |

## 필드
| 이 름 | 설 명 |
|:---|:---|
| int **HealthPoint** | Actor의 최대 HP를 저장한다. |
| int **ManaPoint** | Actor의 최대 MP를 저장한다. |
| float **MoveSpeed** | Actor의 기본 이동속도를 저장한다. |
| float **AttackSpeed** | Actor의 기본 공격속도를 저장한다. |
| int **OffensivePower** | Actor의 기본 공격력을 저장한다. |
| int **DefensivePower** | Actor의 기본 방어력을 저장한다. |
| float **Force** | Actor의 기본 넉백 힘을 저장한다. | 
| float **Resistance** | Actor의 넉백에 관한 저항력을 저장한다. |
| EActorSize **Size** | Actor의 크기를 저장한다. |

## 생성자 
| 이 름 | 설 명 |
|:---|:---|
| **ActorStatus**() | 모든 필드를 기본값으로 초기화하는 생성자이다. |
| **ActorStatus**(ActorStatus status) | 모든 필드를 status의 필드값으로 초기화하는 생성자이다. |

## 메소드 
| 이 름 | 설 명 |
|:---|:---|
| void **CopyFrom**(ActorStatus status) | 모든 필드를 status로 부터 복사하는 메소드이다. |