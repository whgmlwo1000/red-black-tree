# EActorSize enum

## 필드
| 이 름 | 설 명 |
|:---|:---|
| NONE = -1 | None. |
| SMALL = 0 | 소형 Actor 이다. |
| MEDIUM = 1 | 중형 Actor 이다. |
| COUNT = 2 | 열거형 필드의 수이다. |