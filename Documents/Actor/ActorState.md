# ActorState class

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**EActorStateFlags**]() | 현재 Actor의 상태를 나타내는 플래그로 사용된다. |
| [**EActorFlag**]() | DataManager로 부터 적절한 ActorStatus 객체를 가져오기 위해 Actor의 종류를 구별하는 플래그로 사용된다. |
| [**DataManager**](https://gitlab.com/phantasylab/red-black-tree/blob/V2.3/Documents/Data/DataManager.md) | ActorStatus를 객체를 가져오기 위해 사용되는 클래스이다. |
| [**ActorStatus**]() | Actor의 각종 상태를 반환하기 위해 사용하는 클래스이다. |
| [**EActorSize**]() | Actor의 크기를 구별하기 위해 사용하는 열거형이다. |

## 생성자
| 이 름 | 설 명 |
|:---|:---|
| **ActorState**(EActorFlags actorFlag) | actorFlag의 ActorStatus 객체를 DataManager로 부터 가져오는 생성자이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| EActorStateFlags **State** \{ get set \} | 등록된 Actor의 상태를 반환하고 설정할 수 있는 프로퍼티이다. <br> Setter에는 상태별 전이 조건이 명시되어 있다. |
| bool **IsReKnockback** \{ get \} | State 프로퍼티가 KNOCKBACK 상태일때, 다시 KNOCKBACK 상태로 설정할 경우 true로 설정되는 프로퍼티이다. getter 프로퍼티를 호출하는 순간 기존의 값을 반환한 후, 다시 false로 설정된다. |
| int **HealthPoint** \{ get set \} | Actor의 HP를 반환하고 설정하는 프로퍼티이다. <br> Actor가 DIE 상태일 경우, 0보다 낮은 체력을 설정할 경우 등의 예외처리가 구현되어 있다. |
| int **MaxHealthPoint** \{ get set \} | Actor의 최대 HP를 반환하고 설정할 수 있는 프로퍼티이다. <br> 각종 예외처리가 구현되어 있다. |
| int **ManaPoint** \{ get set \} | Actor의 MP를 반환하고 설정할 수 있는 프로퍼티이다. <br> 각종 예외처리가 구현되어 있다. |
| int **MaxManaPoint** \{ get set \} | Actor의 최대 MP를 반환하고 설정할 수 있는 프로퍼티이다. <br> 각종 예외처리가 구현되어 있다. | 
| float **MoveSpeed** \{ get set \} | Actor의 이동 속도를 반환하는 프로퍼티이다. |
| float **AttackSpeed** \{ get set \} | Actor의 공격 속도를 반환하는 프로퍼티이다. |
| int **OffensivePower** \{ get set \} | Actor의 공격력을 반환하는 프로퍼티이다. |
| int **DefensivePower** \{ get set \} | Actor의 방어력을 반환하는 프로퍼티이다. |
| float **Force** \{ get set \} | Actor의 넉백과 관련되 힘을 반환하는 프로퍼티이다. |
| float **Resistance** \{ get set \} | Actor의 넉백에 저항하는 힘을 반환하는 프로퍼티이다. |
| EActorSize **Size** \{ get set \} | Actor의 크기를 반환하는 프로퍼티이다. |