# AControllable abstract class
 [**MonoBehaviour**](https://docs.unity3d.com/kr/current/ScriptReference/MonoBehaviour.html)를 상속

## 관련 클래스 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**Command**](https://gitlab.com/phantasylab/red-black-tree/blob/V2.3/Documents/Actor/Command.md) | 필요한 모든 커맨드가 선언되어 있는 클래스이다.|

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
|Command **Command** _get_| 현재 클래스가 가지고 있는 **Command** 인스턴스를 반환하는 프로퍼티이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
|void **SetMoveHorizontal**(float value)| **Command**의 *CanMoveHorizontal*이 true라면 **Command**의 *MoveHorizontal*의 값을 value로 설정하는 메소드이다. <br> *CanMoveHorizontal*이 false라면 *MoveHorizontal*의 값은 0으로 설정된다.|
|void **SetMoveVertical**(float value)| **Command**의 *CanMoveVertical*이 true라면 **Command**의 *MoveVertical*의 값을 value로 설정하는 메소드이다. <br> *CanMoveVertical*이 false라면 *MoveVertical*의 값은 0으로 설정된다.|
|void **SetAttack**(bool valule, int index)| **Command**의 *CanAttack*[index]가 true라면 **Command**의 *Attack*[index]의 값을 value로 설정하고,<br> 이전의 *Attack*[index]의 값에 따라서 *AttackDown*[index], *AttackUp*[index]를 적절한 값으로 설정하는 메소드이다. <br> *CanAttack*[index]가 false라면 *Attack*[index]의 값은 false로 설정된다.|
|void **SetUse**(bool value, int index)| **Command**의 *CanUse*[index]가 true라면 **Command**의 *Use*[index]의 값을 value로 설정하고,<br> 이전의 *Use*[index]의 값에 따라서 *UseDown*[index], *UseUp*[index]를 적절한 값으로 설정하는 메소드이다. <br> *CanUse*[index]가 false라면 *Use*[index]의 값은 false로 설정된다.|
|void **SetInteract**(bool value)| **Command**의 *CanInteract*가 true라면 **Command**의 *Interact*의 값을 value로 설정하고,<br> 이전의 *Interact*의 값에 따라서 *InteractDown*, *InteractUp*를 적절한 값으로 설정하는 메소드이다. <br> *CanInteract*가 false라면 *Interact*의 값은 false로 설정된다.|
|void **SetSkill**(bool value, int index)| **Command**의 *CanSkill*[index]가 true라면 **Command**의 *Skill*[index]의 값을 value로 설정하고,<br> 이전의 *Skill*[index]의 값에 따라서 *SkillDown*[index], *SkillUp*[index]를 적절한 값으로 설정하는 메소드이다. <br> *CanSkill*[index]가 false라면 *Skill*[index]의 값은 false로 설정된다.|
|void **SetMoveHorizontalMenu**(float value)| **Command**의 *CanMoveHorizontalMenu*가 true라면 **Command**의 *MoveHorizontalMenu*의 값을 value로 설정하는 메소드이다. <br> *CanMoveHorizontalMenu*가 false라면 *MoveHorizontalMenu*의 값은 0으로 설정된다.|
|void **SetMoveVerticalMenu**(float value)| **Command**의 *CanMoveVerticalMenu*가 true라면 **Command**의 *MoveVerticalMenu*의 값을 value로 설정하는 메소드이다. <br> *CanMoveVerticalMenu*가 false라면 *MoveVerticalMenu*의 값은 0으로 설정된다.|
|void **SetMenu**(bool value)| **Command**의 *CanMenu*가 true라면 **Command**의 *Menu*의 값을 value로 설정하고,<br> 이전의 *Menu*의 값에 따라서 *MenuDown*, *MenuUp*를 적절한 값으로 설정하는 메소드이다. <br> *CanMenu*가 false라면 *Menu*의 값은 false로 설정된다.|
|void **SetConfirm**(bool value)| **Command**의 *CanConfirm*가 true라면 **Command**의 *Confirm*의 값을 value로 설정하고,<br> 이전의 *Confirm*의 값에 따라서 *ConfirmDown*, *ConfirmUp*를 적절한 값으로 설정하는 메소드이다. <br> *CanConfirm*가 false라면 *Confirm*의 값은 false로 설정된다.|
|void **SetCancel**(bool value)| **Command**의 *CanCancel*가 true라면 **Command**의 *Cancel*의 값을 value로 설정하고,<br> 이전의 *Cancel*의 값에 따라서 *CancelDown*, *CancelUp*를 적절한 값으로 설정하는 메소드이다. <br> *CanCancel*가 false라면 *Cancel*의 값은 false로 설정된다.|
|void **SetEscape**(bool value)| **Command**의 *CanEscape*가 true라면 **Command**의 *Escape*의 값을 value로 설정하고,<br> 이전의 *Escape*의 값에 따라서 *EscapeDown*, *EscapeUp*를 적절한 값으로 설정하는 메소드이다. <br> *CanEscape*가 false라면 *Escape*의 값은 false로 설정된다.|
