# AActor\<ETriggerFlags, EAudioFlags, EStateFlags\> abstract class
 [**AControllable**]()을 상속
 [**IActor**](), [**IDamagable**]()을 구현  
  
 ETriggerFlags, EAudioFlags, EStateFlags는 [**Enum**](https://docs.microsoft.com/ko-kr/dotnet/api/system.enum?view=netframework-4.8)을 상속

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**EActorFlag**]() | Actor의 종류를 구별하기 위한 열거형이다. |
| [**AInput**]() | Actor를 조작하기 위한 Input 클래스이다. |
| [**FiniteStateMachine\<EStateFlags\>**] | Actor의 행동을 수행하기 위한 유한상태기계 클래스이다. |
| [**Animator**]() | Actor의 애니메이션을 작동시키기 위한 애니메이터 클래스이다. |
| [**Rigidbody2D**]() | Actor의 물리 관련 처리를 위한 리지드바디 클래스이다. |
| [**SpriteRenderer**]() | Actor의 렌더링 관련 처리를 위한 스프라이트 렌더러 클래스이다. |
| [**Collider2D**]() | Actor의 충돌 처리를 위한 콜라이더 클래스이다. |
| [**TriggerHandler_Terrain**]() | TriggerHandler\<ETriggerFlags_Terrain\>을 상속한 TriggerHandler로 터레인과 접촉 여부를 판단하기 위해 사용된다. |
| [**TriggerHandler\<ETriggerFlags\>**]() | Actor 별로 사용하는 TriggerHandler이다. |
| [**AudioHandle\<EAudioFlags\>**]() | Actor의 Audio 재생을 위해 사용되는 클래스이다. |
| [**AttackHandler\<ETriggerFlags\>**]() | Actor의 공격 별로 필요한 정보를 저장하는 클래스이다. |
| [**ActorState**]() | Actor의 여러가지 상태를 저장하기 위한 클래스이다. |
| [**Damage**]() | Actor의 데미지 정보를 저장하기 위한 클래스이다. |
| [**Heal**]() | Actor의 치유 정보를 저장하기 위한 클래스이다. |
| [**EHorizontalDirection**]() | Actor의 현재 방향을 구별하기 위한 열거형이다. |
| [**Vector2**]() | Actor의 속도, 좌표 등의 연산을 위해 필요한 구조체이다. |
| [**PhysicsMaterial2D**]() | Actor의 물리 관련 기능을 처리하기 위해 필요한 정보를 가지는 클래스이다. |
| [**AttackTable**]() | Attack Type에 따라 추가적인 정보를 제공하는 클래스이다. |
| [**EAttackType**]() | Attack의 Type을 구별하기 위한 열거형이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| EAttackFlag **AttackFlag** \{ get \} | Actor의 종류를 반환하는 프로퍼티이다. |
| AInput **Input** \{ get set \} | Actor를 조작하기 위한 Input을 저장하고 반환하는 프로퍼티이다. |
| FIniteStateMachine\<EStateFlags\> Fsm \{ get \} | Actor의 행동을 수행하는 유한상태기계 객체를 반환하는 프로퍼티이다. |
| Animator **Animator** \{ get \} | Actor의 애니메이션 기능을 수행하는 Animator 객체를 반환하는 프로퍼티이다. |
| Rigidbody2D **Rigidbody** \{ get \} | Actor의 물리 관련 처리를 위한 리지드바디 객체를 반환하는 프로퍼티이다. |
| SpriteRenderer **MainRenderer** \{ get \} | Actor를 렌더하는 스프라이트 렌더러 객체를 반환하는 프로퍼티이다. |
| Collider2D **MainCollider** \{ get \} | Actor의 충돌 관련 처리를 위한 콜라이더 객체를 반환하는 프로퍼티이다. |
| TriggerHandler_Terrain **TerrainTrigger** \{ get \} | Actor와 Terrain과 접촉 여부를 반환하는 트리거 핸들러 프로퍼티이다. |
| TriggerHandler\<ETriggerFlags\> **Triggerhandler** \{ get \} | Actor가 사용하는 트리거 핸들러를 반환하는 프로퍼티이다. |
| AudioHandler\<EAudioFlags\> **AudioHandler** \{ get \} | Actor가 사용하는 오디오 핸들러를 반환하는 프로퍼티이다. |
| ActorState **State** \{ get \} | Actor의 ActorState 객체를 반환하는 프로퍼티이다. |
| AttackHandler\<ETriggerFlags\> **AttackHandler** \{ get \} | Actor의 공격 별로 정보가 저장된 AttackHandler를 반환하는 프로퍼티이다. |
| Damage **Damage** \{ get \} | Actor가 받는 데미지 정보를 저장하는 객체를 반환하는 프로퍼티이다. |
| Heal **Heal** \{ get \} | Actor가 받는 치유 정보를 저장하는 객체를 반환하는 프로퍼티이다. |
| EHorizontalDirection **HorizontalDirection** \{ get set \} | Actor의 방향을 설정하고 반환하는 프로퍼티이다. |
| Vector2 **Velocity** \{ get set \} | Actor의 속도를 설정하고 반환하는 프로퍼티이다. |
| PhysicsMaterial2D **PhysicsMaterial** \{ get set \} | Actor의 물리 Material을 설정하고 반환하는 프로퍼티이다. |
| bool **IsGround** \{ get \} | Actor의 지면 접촉 여부를 반환하는 프로퍼티이다. |
| float **GravityScale \{ get \} | Actor의 중력 계수를 설정하고 반환하는 프로퍼티이다. |

## 메소드 
| 이 름 | 설 명 |
|:---|:---|
| void **InvokeDamage**() | 저장된 Damage 객체의 정보를 기반으로 데미지 처리를 수행하는 메소드이다. |
| void **InvokeHeal**() | 저장된 Heal 객체의 정보를 기반으로 치유 처리를 수행하는 메소드이다. |
| void **MoveHorizontal**(float speed, EHorizontalDirection direction) | Actor가 수평으로 움직이도록 Velocity와 HorizontalDirection을 설정하는 메소드이다. |
| void **Attack**(EAttackType attackType, ETriggerFlags triggerFlag) | triggerFlag에 존재하는 IDamagable을 구현한 모든 객체에게 attackType의 정보를 이용한 데미지 정보를 전달하고 해당 객체의 InvokeDamage 메소드를 호출하는 메소드이다. |
