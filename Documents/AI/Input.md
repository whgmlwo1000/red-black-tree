# Input class
in AI namespace

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**StringBuilder**]() | 디버깅 용 문자열을 생성할때 사용되는 클래스이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| float **MoveHorizontal** \{ get set \} | 수평 움직임에 대한 입력값이다. |
| float **MoveVertical** \{ get set \} | 수직 움직임에 대한 입력값이다. |
| bool[] **Attack** \{ get \} | 공격에 대한 입력값이다. |
| bool[] **Use** \{ get \} | 아이템 사용에 대한 입력값이다. |
| bool[] **Skill** \{ get \} | 스킬에 대한 입력값이다. |
| bool **Interact** \{ get set \} | 상호작용에 대한 입력값이다. |

## 생성자
 이 름 | 설 명 |
|:---|:---|
| **Input**(int countOfAttacks, int countOfUse, int countOfSkills) | 공격, 아이템 사용, 스킬의 사용 가능한 입력의 갯수를 받아 할당하는 생성자이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| void **ToString**() | 디버깅용 문자열을 반환하는 메소드이다. |
