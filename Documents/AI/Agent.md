# Agent\<EStateFlags\> class
in AI namespace
 [**AInput**]()을 상속  
 
 EStateFlags는 [**Enum**](https://docs.microsoft.com/ko-kr/dotnet/api/system.enum?view=netframework-4.8)을 상속

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**Input**]() | Controllable 객체에 전달할 입력 값을 저장하는 클래스이다. |
| [**ASensor**]() | Agent의 센서 역할을 하는 클래스이다. |
| [**FiniteStateMachine\<E\>**] | Agent의 상태에 따라 Input을 결정하는 유한상태기계 클래스이다. |
| [**List\<T\>**]() | ASensor 객체를 저장하기 위해 사용되는 자료구조이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| FiniteStateMachine\<EStateFlags\> **Fsm** \{ get \} | Agent의 Input을 결정하는 유한상태기계를 반환하는 프로퍼티이다. |

## 생성자
 이 름 | 설 명 |
|:---|:---|
| **Agent**(int countOfAttacks, int countOfUse, int countOfSkills) | Agent의 Input을 할당하기 위해 필요한 정보를 파라미터로 전달하여 초기화하는 생성자이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| void **Check**(AControllable controllable) | Agent를 작동시켜 controllable에게 입력값을 전달하는 메소드이다. |
| void **AddSensor**(ASensor sensor) | Agent에게 sensor를 추가하는 메소드이다. |
