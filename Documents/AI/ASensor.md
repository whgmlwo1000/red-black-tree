# ASensor abstract class
in AI namespace

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| bool **Check**() | 센서의 실행 조건을 반환하는 메소드이다. |
| void **Execute**() | 센서를 실행하는 메소드이다. |