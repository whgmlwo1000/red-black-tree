# Object\<E\> class
in Object namespace  
  
[*MonoBehaviour*](https://docs.unity3d.com/kr/current/ScriptReference/MonoBehaviour.html)를 상속  
 E는 [*Enum*](https://docs.microsoft.com/ko-kr/dotnet/api/system.enum?view=netframework-4.8)을 상속

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| E **Type** (_get_) | 오브젝트 풀을 만들 때 오브젝트들을 분류하기 위한 열거형 프로퍼티이다. |