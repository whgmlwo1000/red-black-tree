# Pool<T, E> class
in Object namespace  
  
[*MonoBehaviour*](https://docs.unity3d.com/kr/current/ScriptReference/MonoBehaviour.html)를 상속  
 T는 [*Object\<E\>*](https://gitlab.com/phantasylab/red-black-tree/blob/master/Documents/Object%3CE%3E.md)를 상속  
 E는 [*Enum*](https://docs.microsoft.com/ko-kr/dotnet/api/system.enum?view=netframework-4.8)을 상속
 
## 관련 클래스 및 열거형
| 이 름 | 설 명 |
|:---|:---|
|**Objects\<E\>** | 오브젝트 풀을 구성하는 오브젝트의 클래스이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
|static T **Get**(E objectType) | 오브젝트 풀에서 비활성화된 오브젝트 하나를 반환하는 메소드이다. <br> 비활성화된 오브젝트가 존재하지 않는다면 null을 반환한다. |