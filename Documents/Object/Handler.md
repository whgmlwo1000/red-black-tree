# Handler<T, E> class
in Object namespace  
  
[*MonoBehaviour*](https://docs.unity3d.com/kr/current/ScriptReference/MonoBehaviour.html)를 상속  
 T는 [*Object\<E\>*](https://gitlab.com/phantasylab/red-black-tree/blob/master/Documents/Object%3CE%3E.md)를 상속  
 E는 [*Enum*](https://docs.microsoft.com/ko-kr/dotnet/api/system.enum?view=netframework-4.8)을 상속
 
## 관련 클래스 및 열거형
| 이 름 | 설 명 |
|:---|:---|
|**Objects\<E\>** | 핸들러가 관리하는 오브젝트의 클래스이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| T **Get**(E objectType) | 핸들러가 관리하는 오브젝트 중 objectType의 플래그 값을 가지는 오브젝트를 반환해주는 메소드이다. |
| Dictionary<E, T>.Enumerator GetEnumerator() | 핸들러 내에 존재하는 오브젝트들의 Enumerator를 반환하는 메소드이다. |