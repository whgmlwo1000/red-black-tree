# AState\<E\> abstract class
in FSM namespace  
  
 E는 [*Enum*](https://docs.microsoft.com/ko-kr/dotnet/api/system.enum?view=netframework-4.8)을 상속

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| E **StateFlag** (get) | 해당 State의 Flag 값을 반환하는 프로퍼티이다. |

## 생성자
| 이 름 | 설 명 |
|:---|:---|
| **AState**(E stateFlag) | 파라미터로 전달된 stateFlag로 설정하는 생성자이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| void **Add**(AState<E> nextState) | 현재 State에서 전이될 수 있는 다음 State를 추가하는 메소드이다. |
| bool **Check**() | 해당 State의 실행 조건을 반환하는 메소드이다. <br> 실행할 수 있으면 true, 아니라면 false를 반환한다. |
| AState<E> **Execute**() | 전이될 수 다음 State가 존재하지 않는 State를 실행하고, 해당 State를 반환하는 메소드이다. |
| void **Behave**() | State의 행동을 실행하는 메소드이다. |
| void **Start**() | State의 행동을 준비하는 메소드이다. |
| void **End**() | State의 행동을 마무리하는 메소드이다. |
| static AState<E> **operator +**(AState<E> srcState, AState<E> dstState) | Add 메소드를 연산자 오버로딩으로 구현한 메소드이다. |
| string **ToString**() | State의 Flag를 string 형태로 반환하는 메소드이다. |