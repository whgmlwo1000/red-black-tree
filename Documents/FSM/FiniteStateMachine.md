# FiniteStateMachine\<E\> class
in FSM namespace  
  
 E는 [*Enum*](https://docs.microsoft.com/ko-kr/dotnet/api/system.enum?view=netframework-4.8)을 상속

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**AState\<E\>**](https://gitlab.com/phantasylab/red-black-tree/blob/V2.3/Documents/FSM/AState.md) | FSM을 구성하는 State 클래스이다. |

## 생성자
| 이 름 | 설 명 |
|:---|:---|
| **FiniteStateMachine**() | State를 저장할 수 있는 Dictionary를 할당하는 생성자이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| E **State** ( get, set ) | FSM의 State Flag를 설정하거나 현재 State Flag를 반환할 수 있는 프로퍼티이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| void **AddState**(AState<E> state) | FSM에 State를 등록하는 메소드이다. |
| void **Execute**() | 현재 실행가능한 State를 한번 실행하는 메소드이다. |
| void **GetState**(E e) | FSM에 등록된 State 중 e를 Flag로 가지는 State를 반환하는 메소드이다. |
