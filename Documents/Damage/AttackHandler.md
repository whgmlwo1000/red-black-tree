# AttackHandler\<EAttackFlags\> class
 EAttackFlags는 [*Enum*](https://docs.microsoft.com/ko-kr/dotnet/api/system.enum?view=netframework-4.8)을 상속

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**AttackInfo**]() | 공격에 관련된 정보를 저장하는 클래스이다. |

## 생성자 
| 이 름 | 설 명 | 
|:---|:---|
| **AttackTable**() | AttackTable 기본 생성자이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| void **AddInfo**(EAttackFlags attackFlag, AttackInfo attackInfo) | attackFlag의 공격에 attackInfo 공격 객체 정보를 저장하는 메소드이다. |
| AttackInfo GetInfo(EAttackFlags attackFlag) | attackFlag의 공격 정보를 반환하는 메소드이다. |
