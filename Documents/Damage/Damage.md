# Damage class

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**MonoBehaviour**](https://docs.unity3d.com/kr/current/ScriptReference/MonoBehaviour.html) | Unity Scene에 존재하는 모든 객체는 반드시 상속받아야 하는 클래스이다. |
| [**Vector2**](https://docs.unity3d.com/kr/2019.1/ScriptReference/Vector2.html) | 좌표 연산을 처리하기 위해 사용되는 Unity의 2차원 좌표를 저장하는 구조체이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| int **Value** \{ get set \} | 데미지 양을 저장하는 프로퍼티이다. |
| MonoBehaviour **SrcInstance** \{ get set \} | 데미지를 입힌 객체를 저장하는 프로퍼티이다. |
| Vector2 **Force** \{ get set \} | 데미지를 가한 힘과 방향을 저장하는 프로퍼티이다. |
| Vector2 **SrcPosition** \{ get set \} | 데미지를 가한 객체의 중심 좌표를 저장하는 프로퍼티이다. |