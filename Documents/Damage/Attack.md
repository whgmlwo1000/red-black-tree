# Attack class

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| float **ForceConstant_x \{ get \} | 힘이 가해지는 x좌표를 반환하는 프로퍼티이다. |
| float **ForceConstant_y \{ get \} | 힘이 가해지는 y좌표를 반환하는 프로퍼티이다. |

## 생성자 
| 이 름 | 설 명 |
|:---|:---|
| **AttackInfo**(float forceConstant_x, float forceConstant_y) | 파라미터로 전달된 변수로 필드를 초기화하는 생성자이다. |
