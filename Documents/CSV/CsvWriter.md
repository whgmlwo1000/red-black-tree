# CsvWriter static class
in Csv namespace

## 관련 클래스 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**Dictionary\<K, T\>**](https://docs.microsoft.com/ko-kr/dotnet/api/system.collections.generic.dictionary-2?view=netframework-4.8) | CSV 파일을 쓰기 위해 Dictionary\<string, Dictionary\<string, string\>\> 형태의 파라미터를 전달해야 한다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| static void **Write**(string filePath, Dictionary\<string, Dictionary\<string, string\>\> data) | data의 바깥 Dictionary의 Key 값을 행 이름으로, 안쪽 Key 값을 열 이름으로, 안쪽 Value 값을 셀의 값에 삽입하여 filePath에 CSV 파일의 형태로 저장한다. |