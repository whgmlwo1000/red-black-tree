# CsvReader static class
in Csv namespace

## 관련 클래스 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**Dictionary\<K, T\>**](https://docs.microsoft.com/ko-kr/dotnet/api/system.collections.generic.dictionary-2?view=netframework-4.8) | CSV 파일을 읽은 결과로 Dictionary\<string, Dictionary\<string, string\>\> 형태로 반환한다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| static Dictionary\<string, Dictionary\<string, string\>\> **Read**(string filePath) | filePath에 존재하는 CSV 파일을 읽어서 Dictionary\<string, Dictionary\<string, string\>\> 형태로 반환한다. <br> 바깥 Dictionary의 Key 값으로 행의 이름이 들어가고, 안쪽 Key 값으로 열의 이름이 들어가며 안쪽 Value 값으로 셀의 값이 들어간다. |