# AActorEffect abstract class
[**MonoBehaviour**](https://docs.unity3d.com/kr/current/ScriptReference/MonoBehaviour.html)를 상속

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**EActorEffectFlags**]() | 액터 효과의 종류를 결정하기 위해 사용되는 열거형이다. |
| [**Sprite**]() | 아이콘 이미지를 출력하기 위해 사용되는 Unity의 클래스이다. |
| [**IActor**]() | 모든 형태의 Actor가 구현하는 인터페이스이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| Sprite **Icon** \{ get \} | 액터 효과의 아이콘 스프라이트를 반환하는 프로퍼티이다. |
| EActorEffectFlags **ActorEffectFlag** \{ get \} | 액터 효과의 종류를 반환하는 프로퍼티이다. |
| bool **HasDuration** \{ get set \} | 액터 효과의 지속시간을 가지는 여부에 관한 프로퍼티이다. |
| float **Duration** \{ get set \} | 액터 효과의 지속시간에 관한 프로퍼티이다. |
| IActor **Target** \{ get \} | 액터 효과의 대상이 되는 액터의 객체를 참조하는 프로퍼티이다. |
| bool **IsActive** \{ get \} | 액터 효과가 활성화되고 있는지의 여부에 관한 프로퍼티이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| void **Activate**(IActor actor) | 액터 효과를 활성화시키는 메소드이다. |
| void **Deactivate**() | 액터 효과를 비활성화 시키는 메소드이다. <br> 본 메소드가 호출되어도 완전히 비활성화 되지는 않고 마무리 메소드가 지속적으로 호출된 후에 비활성화 된다. |