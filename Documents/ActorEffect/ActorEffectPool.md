# ActorEffectPool class
 [Pool\<AActorEffect, EActorEffectFlags\>]()를 상속

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**AActorEffect**]() | 모든 액터 효과가 상속하는 클래스이다. |
| [**EActorEffectFlags**]() | 액터 효과의 종류를 결정하는 열거형이다. |