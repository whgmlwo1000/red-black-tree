# WayPointGenerator class
 [**MonoBehaviour**](https://docs.unity3d.com/kr/current/ScriptReference/MonoBehaviour.html)를 상속

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**ELayerFlags**]() | WayPoint로 등록하지 않을 충돌체 레이어를 구분하기 위해 사용되는 열거형이다. |
| [**Vector2**]() | WayPoint를 구분하기 위해 전달되는 실제 좌표의 구조체이다. |
| [**Vector2Int**]() | WayPoint를 구분하기 위한 Key 값의 구조체이다. |
| [**Trasnform**]() | WayPoint를 이용하는 모든 객체는 Transform 객체를 가지고 있기 때문에 감지된 객체를 저장하기 위해 사용된다. |
| [**WayPointManager**]() | WayPoint를 관리하는 클래스이다. |
| [**Physics2D**]() | WayPoint를 생성하기 위해 사용하는 물리 관련 클래스이다. |
| [**Collider2D**]() | 충돌체를 검사하기 위해 사용되는 콜라이더 클래스이다. |
| [**Queue\<T\>] | WayPoint를 BFS 알고리즘 기반으로 생성하기 위한 큐 자료구조이다. |

## 메소드 
| 이 름 | 설 명 |
|:---|:---|
| void **Generate**() | WayPoint를 지정된 범위 내에서 생성하는 메소드이다. |