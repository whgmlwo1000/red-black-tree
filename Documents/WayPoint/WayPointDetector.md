# WayPointDetector class
 [**MonoBehaviour**](https://docs.unity3d.com/kr/current/ScriptReference/MonoBehaviour.html)를 상속

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**WayPoint**]() | Way Point 정보를 저장하는 클래스이다. |
| [**EWayPointFLags**]() | WayPoint를 감지하는 Transform을 서로 구분하기 위해 사용되는 열거형이다. |
| [**Transform**]() | WayPoint에 객체를 등록하기 위해 사용되는 클래스이다. |
| [**WayPointManager**]() | WayPoint를 관리하기 위해 사용되는 클래스이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| WayPoint **CurWayPoint** \{ get \} | 현재 감지하고 있는 WayPoint 객체를 반환하는 프로퍼티이다. |

## 메소드 
| 이 름 | 설 명 |
|:---|:---|
| void **Detect**() | WayPoint를 감지하고, 등록하는 메소드이다. |