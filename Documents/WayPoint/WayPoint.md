# WayPoint class

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**Vector2Int**]() | WayPoint를 서로 구분하기 위한 Key로 사용되는 구조체이다. |
| [**EWayPointFlags**]() | WayPoint에 저장되는 Transform을 서로 구분하여 저장하기 위해 사용되는 열거형이다. |
| [**Trasnform**]() | WayPoint를 이용하는 모든 객체는 Transform 객체를 가지고 있기 때문에 감지된 객체를 저장하기 위해 사용된다. |
| [**HashSet<T>**]() | 순서가 중요하지 않고, 탐색 속도가 중요한 Transform을 저장하고 삭제하기 위해 사용된다. |
| [**Dictionary\<K, T\>]() | EWayPointFlags 별로 Transform을 저장하기 위한 자료구조이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| Vector2Int **Key** \{ get \} | WayPoint가 위치하고 있는 좌표의 키 값을 반환하는 프로퍼티이다. |

## 생성자
| 이 름 | 설 명 |
|:---|:---|
| **WayPoint**(Vector2Int key) | 패러미터로 전달된 key 값을 가지는 WayPoint를 생성하는 생성자이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| void **AddTarget**(EWayPointFlags wayPointFlag, Transform targetTransform) | targetTransform을 해당 WayPoint에 등록하는 메소드이다. |
| void **RemoveTarget**(EWayPointFlags wayPointFlag, Transform targetTransform) | targetTransform을 해당 WayPoint에서 제거하는 메소드이다. |
| Transform[] **GetTargets**(EWayPointFlags wayPointFlag) | 해당 WayPoint에 등록되어 있는 wayPointFlag의 Transform 객체 배열을 반환하는 메소드이다. |
