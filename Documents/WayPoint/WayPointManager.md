# WayPointManager static class

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**Vector2**]() | WayPoint를 구분하기 위해 전달되는 실제 좌표의 구조체이다. |
| [**Dictionary\<K, T\>**]() | WayPoint를 저장하기 위한 자료구조이다. |
| [**WayPoint**]() | Way Point에 관한 정보를 가지는 클래스이다. |
| [**Vector2Int**]() | WayPoint를 구분하기 위한 Key 값의 구조체이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| Vector2 **GapSize** \{ get \} | WayPoint 간의 거리를 반환하는 프로퍼티이다. |

## 생성자 
| 이 름 | 설 명 |
|:---|:---|
| **WayPointManager**() | WayPointManager의 기본 생성자이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| static void **Clear**() | 저장된 모든 WayPoint를 삭제하는 메소드이다. |
| static Vector2Int **ConvertPositionToKey**(Vector2 position) | position을 Key 값인 Vector2Int로 변환해서 반환하는 메소드이다. |
| static Vector2 **ConvertKeyToPosition**(Vector2Int key) \ key를 실제 좌표인 Vector2로 변환해서 반환하는 메소드이다. |
| static bool **AddWayPoint**(Vector2Int key) | key를 키 값으로 갖는 WayPoint를 생성하는 메소드이다. |
| static WayPoint **GetWayPoint**(Vector2 position) | position에 존재하는 WayPoint를 반환하는 메소드이다. |
| static WayPoint **GetWayPoint**(Vector2Int key) | key를 키 값으로 갖는 WayPoint를 반환하는 메소드이다. |
