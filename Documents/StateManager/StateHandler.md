# StateHandler\<E\> class

## 델리게이트
| 이 름 | 설 명 |
|:----|:---|
| void **Action**() | 상태 진입 이벤트, 탈출 이벤트로 설정되는 이벤트 메소드의 원형이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:----|:---|
| E **State** \{ get set \} | 현재 상태를 반환하거나 현재의 상태를 다른 상태로 설정할 수 있는 프로퍼티이다. |
| E **PrevState** \{ get \} | 현재 상태가 설정되기 직전의 상태를 반환하는 프로퍼티이다. |

## 생성자
| 이 름 | 설 명 |
|:----|:---|
| **StateHandler**() | StateHandler의 기본 생성자이다. |

## 메소드
| 이 름 | 설 명 |
|:----|:---|
| void **AddEnterEvent**(E e, Action action) | e 상태 진입시 호출되는 이벤트 메소드를 등록하는 메소드이다. |
| void **AddExitEvent**(E e, Action action) | e 상태 탈출시 호출되는 이벤트 메소드를 등록하는 메소드이다. |
| void **RemoveEnterEvent**(E e, Action action) | e 상태 진입시 호출되는 이벤트 메소드 중 action 메소드를 제거하는 메소드이다. |
| void **RemoveExitEvent**(E e, Action action) | e 상태 탈출시 호출되는 이벤트 메소드 중 action 메소드를 제거하는 메소드이다. |
