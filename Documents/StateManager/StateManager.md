# StateManager static class

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:----|:---|
| [**StateHandler**]() | StateManager에서 선언된 각종 State를 관리하기 위해 사용되는 클래스이다. |
| [**EInputModeFlags**]() | 입력 모드들을 구분하기 위한 열거형이다. |
| [**ELanguageFlags**]() | 언어를 구분하기 위한 열거형이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:----|:---|
| static StateHandler\<EInputModeFlags\> **InputMode** \{ get \} | Input Mode를 설정하기 위한 StateHandler 객체이다. |
| static StateHandler\<ELanguageFlags\> **Language** \{ get \} | 언어를 설정하기 위한 StateHandler이다. | 
| static StateHandler\<bool\> **Pause** \{ get \} | 일시정지 상태를 설정하기 위한 StateHandler이다. |
| static StateHandler\<bool\> **GameOver** \{ get \} | 게임 오버 상태를 설정하기 위한 StateHandler이다. |
| static StateHandler\<EGameFlags\> **Game** \{ get \} | 현재 게임의 상태를 설정하기 위한 StateHandler이다. |

## 생성자
| 이 름 | 설 명 |
|:----|:---|
| static **StateManager**() | 모든 StateHandler를 초기화하는 기본 생성자이다. <br> 파일로 저장된 StateHandler들은 저장된 상태를 불러온다. |
