# StateBase\<E\> class
 [**MonoBehaviour**]()를 상속  
 E는 [**System.Enum**]()을 상속

## 델리게이트
| 이 름 | 설 명 |
|:----|:---|
| void **Action**() | 이벤트로 등록되는 메소드들의 원형이다. | 

## 프로퍼티
| 이 름 | 설 명 |
|:----|:---|
| E **State** \{ get set \} | 현재 상태를 반환하고 설정할 수 있는 프로퍼티이다. |
| E **PrevState** \{ get \} | 이전 상태를 반환하는 프로퍼티이다. |

## 메소드
| 이 름 | 설 명 |
|:----|:---|
| void **AddEnterEvent**(E state, Action action) | state 진입 이벤트 메소드를 등록하는 메소드이다. |
| void **AddStayEvent**(E state, Action action) | state 유지 이벤트 메소드를 등록하는 메소드이다. |
| void **AddExitEvent**(E state, Action action) | state 탈출 이벤트 메소드를 등록하는 메소드이다. |
| void **RemoveEnterEvent**(E state, Action action) | state 진입 이벤트에서 action 메소드를 제거하는 메소드이다. |
| void **RemoveStayEvent**(E state, Action action) | state 유지 이벤트에서 action 메소드를 제거하는 메소드이다. |
| void **RemoveExitEvent**(E state, Action action) | state 탈출 이벤트에서 action 메소드를 제거하는 메소드이다. |
