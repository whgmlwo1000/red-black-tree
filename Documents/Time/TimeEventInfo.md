# TimeEventInfo struct

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**TimeEvent**](https://gitlab.com/phantasylab/red-black-tree/blob/V2.3/Documents/Time/TimeEvent.md) | 본 구조체가 사용되는 곳이며, Action 델리게이트 형을 참조하기 위한 클래스이다. |

## 필드
| 이 름 | 설 명 |
|:---|:---|
| float **RemainTime** | 이벤트가 지속되는 시간이다. |
| bool **IsScaledTime** | 지속시간을 유니티 내의 scaledDeltaTime의 영향을 받을 것인지, unscaledDeltaTime의 영향을 받을 것인지를 설정하는 메소드이다. |
| TimeEvent.Action **OnStay** | 지속시간이 남아있는 경우 호출되는 이벤트 메소드이다. <br> 유니티의 Time.timeScale = 0 인 경우에는 호출되지 않는다. |
| TimeEvent.Action **OnExit** | 지속시간이 끝난 경우 호출되는 콜백 메소드이다. |
