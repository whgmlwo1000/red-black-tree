# TimeEvent class
[_MonoBehaviour_](https://docs.unity3d.com/kr/current/ScriptReference/MonoBehaviour.html)를 상속

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:---|:---|
|[**TimeEventInfo**](https://gitlab.com/phantasylab/red-black-tree/blob/V2.3/Documents/Time/TimeEventInfo.md) | 시간 이벤트의 정보를 저장하기 위한 구조체이다. |

## 델리게이트
| 이 름 | 설 명 |
|:---|:---|
| void **Action**() | Time Event를 설정할 때 사용되는 이벤트 메소드의 원형이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
|static TimeEventInfo **SetTimeEvent**(float maintainTime, bool isScaledTime, Action onStay, Action onExit) | Time Event를 설정하는 메소드이다. maintainTime은 이벤트 지속 시간이고, isScaledTime은 유니티 내의 deltaTime을 사용하여 지속 시간을 측정할 것인지 설정하는 파라미터이다. <br> onStay는 이벤트 지속시 호출되는 이벤트 메소드이고, onExit는 이벤트 탈출시 호출되는 콜백 메소드이다. |