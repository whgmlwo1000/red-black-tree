# AudioPlayer\<ETriggerFlags\> class
in TriggerHandler2D namespace  
  
[*Handler<Audio<EAudioFlags>, EAudioFlags>*](https://gitlab.com/phantasylab/red-black-tree/blob/V2.3/Documents/Handler.md)를 상속  
EAudioFlags는 [_Enum_](https://docs.microsoft.com/ko-kr/dotnet/api/system.enum?view=netframework-4.8)을 상속

## 관련 클래스 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**Audio\<EAudioFlags\>**](https://gitlab.com/phantasylab/red-black-tree/blob/V2.3/Documents/Audio.md) | 자식 오브젝트의 컴포넌트로 등록된 모든 오디오를 관리하기 위해 각 오디오 오브젝트의 컴포넌트로 등록되어야 하는 클래스이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| void **Play**(EAudioFlags audioFlag, float time = 0.0f) | audioFlag로 설정된 오디오를 time 위치에서 재생시키는 메소드이다. |
| void **Stop**(EAudioFlags audioFlag) | audioFlag로 설정된 오디오를 중지시키는 메소드이다. |
| void **StopAll**() | 현재 오디오 플레이어를 구성하는 모든 오디오를 중지시키는 메소드이다. |
| void **Pause**(EAudioFlags audioFlag) | audioFlag로 설정된 오디오를 일시중지시키는 메소드이다. |
| void **UnPause**(EAudioFlags audioFlag) | audioFlag로 설정된 오디오를 일시중지를 해제시키는 메소드이다. |
| void **AddOnPlayEvent**(EAudioFlags audioFlag, Audio\<EAudioFlags\>.Action action) | audioFlag로 설정된 오디오의 OnPlay 이벤트에 action 메소드를 등록시키는 메소드이다. |
| void **RemoveOnPlayEvent**(EAudioFlags audioFlag, Audio\<EAudioFlags\>.Action action) | audioFlag로 설정된 오디오의 OnPlay 이벤트에 action 메소드를 해제시키는 메소드이다. |
| void **AddOnPauseEvent**(EAudioFlags audioFlag, Audio\<EAudioFlags\>.Action action) | audioFlag로 설정된 오디오의 OnPause 이벤트에 action 메소드를 등록시키는 메소드이다. |
| void **RemoveOnPauseEvent**(EAudioFlags audioFlag, Audio\<EAudioFlags\>.Action action) | audioFlag로 설정된 오디오의 OnPause 이벤트에 action 메소드를 해제시키는 메소드이다. |
| void **AddOnStopEvent**(EAudioFlags audioFlag, Audio\<EAudioFlags\>.Action action) | audioFlag로 설정된 오디오의 OnStop 이벤트에 action 메소드를 등록시키는 메소드이다. |
| void **RemoveOnStopEvent**(EAudioFlags audioFlag, Audio\<EAudioFlags\>.Action action) | audioFlag로 설정된 오디오의 OnStop 이벤트에 action 메소드를 해제시키는 메소드이다. |
