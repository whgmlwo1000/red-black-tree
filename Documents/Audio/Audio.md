# Audio\<EAudioFlags\> class
in TriggerHandler2D namespace  
  
[*Object<E>*](https://gitlab.com/phantasylab/red-black-tree/blob/V2.3/Documents/Object/Object.md)를 상속  
EAudioFlags는 [_Enum_](https://docs.microsoft.com/ko-kr/dotnet/api/system.enum?view=netframework-4.8)을 상속

## 관련 클래스 및 열거형
| 이 름 | 설 명 |
|:---|:---|
|[**AudioSource**](https://docs.unity3d.com/kr/current/ScriptReference/AudioSource.html) | 본 클래스는 Collider2D의 Trigger 기능을 보다 효율적으로 사용하기 위해 개발되었다. |

## 델리게이트
| 이 름 | 설 명 |
|:---|:---|
| void **Action**() | OnPlay, OnStop, OnPause 이벤트 및 콜백 메소드의 원형이다. |

## 이벤트
| 이 름 | 설 명 |
|:---|:---|
| Action **OnPlay** | 오디오 재생 시에 지속적으로 호출되는 이벤트 메소드이다. |
| Action **OnPause** | 오디오 일시중지 시에 지속적으로 호출되는 이벤트 메소드이다. |
| Action **OnExit** | 오디오 중지 시에 호출되는 콜백 메소드이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
| bool **IsPlaying** (get) | 현재 오디오가 재생중인지를 반환하는 프로퍼티이다. |
| EAudioFlags **ObjectType** (get) | 설정된 EAudioFlags를 반환하는 프로퍼티이다. |
| bool **IsPaused** (get) | 현재 오디오가 일시중지 상태인지 반환하는 프로퍼티이다. |
| bool **IsLoop** (get set) | 현재 오디오가 반복 설정이 되어있는지에 관한 프로퍼티이다. |

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| void **Play**(float time = 0.0f) | 오디오를 time 위치에서 재생하는 메소드이다. |
| void **Pause**() | 오디오를 일시중지하는 메소드이다. |
| void **UnPause**() | 오디오를 일시중지 해제는 메소드이다. |
| void **Stop**() | 오디오를 중지하는 메소드이다. |