# EInputFlags enum
## 필드
| 이 름 | 설 명 |
|:---|:---|
| **NONE** = -1| None|
| **LEFT**| 왼쪽 이동|
| **RIGHT**| 오른쪽 이동|
| **UP**| 위로 이동|
| **DOWN**| 아래로 이동|
| **ATTACK**| 공격|
| **INTERACT**| 상호작용|
| **SKILL**| 스킬|
| **USE_WEAPON**| 무기 사용|
| **USE_CONSUMABLE**| 소모품 사용|
| **USE_SCROLL_1**| 주문서 1 사용|
| **USE_SCROLL_2**| 주문서 2 사용|
| **MENU_LEFT**| 메뉴 왼쪽 이동|
| **MENU_RIGHT**| 메뉴 오른쪽 이동|
| **MENU_UP**| 메뉴 위로 이동|
| **MENU_DOWN**| 메뉴 아래로 이동|
| **CONFIRM**| 메뉴 확인|
| **CANCEL**| 메뉴 취소|
| **ESCAPE**| 탈출|
| **MENU**| 메뉴 호출|
| **COUNT**| 필드의 수|