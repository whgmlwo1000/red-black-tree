# AInputUnit abstract class
## 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
|bool **BoolValue** _get_ | 입력 상태를 Bool형으로 반환하는 프로퍼티이다.|
|float **FloatValue** _get_ | 입력 상태를 Float형으로 반환하는 프로퍼티이다.|