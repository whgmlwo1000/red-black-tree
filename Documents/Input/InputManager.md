# InputManager class

## 관련 클래스 및 열거형

| 이 름 | 설 명 |
|:---|:---|
|[**InputManager**](https://gitlab.com/phantasylab/red-black-tree/blob/V2.1/Documents/Input/InputManager.md) | **EInputFlags**에 선언된 Input 값들 중 원하는 Input의 상태를 Bool, Float의 형태로 가져올 수 있다. <br> 또한 **EInputFlags**와 연결된 **KeyCode** 혹은 **Axis**를 변경할 수 있다.|
|[**AInputUnit**](https://gitlab.com/phantasylab/red-black-tree/blob/V2.1/Documents/Input/AInputUnit.md) | 등록된 **KeyCode** 혹은 **Axis**의 상태를 Bool, Float의 형태로 가져오는 메소드가 선언된 추상클래스이다. |
|[**ButtonInput**](https://gitlab.com/phantasylab/red-black-tree/blob/V2.1/Documents/Input/ButtonUnit.md) | **KeyCode**의 상태를 Bool, Float의 형태로 가져올 수 있는 클래스이다. |
|[**AxisInput**](https://gitlab.com/phantasylab/red-black-tree/blob/V2.1/Documents/Input/AxisUnit.md) | **Axis**의 상태를 Bool, Float의 형태로 가져올 수 있는 클래스이다. |
|[**EInputModeFlags**](https://gitlab.com/phantasylab/red-black-tree/blob/V2.1/Documents/Input/EInputModeFlags.md) | 입력 장치 모드 플래그가 선언된 열거형이다. |
|[**EInputFlags**](https://gitlab.com/phantasylab/red-black-tree/blob/V2.1/Documents/Input/EInputFlags.md) | 게임 내 조작을 위해 필요한 플래그들이 선언된 열거형이다. |


## 메소드

| 이 름 | 설 명 |
|:---|:---|
|bool **GetBool**(EInputFlags inputFlag, EInputModeFlags inputModeFlag)| inputFlag에 대응되는 inputModeFlag의 입력 상태를 Bool 형태로 반환하는 메소드이다.|
|bool **GetBool**(EInputModeFlags inputModeFlag)| inputModeFlag의 모든 EInputFlags에 대응되는 입력 상태를 Bool 형태로 반환하는 메소드이다.|
|bool **GetBool**(EInputFlags inputFlag)| inputFlag에 대응되는 모든 입력 장치의 입력 상태를 Bool 형태로 반환하는 메소드이다.|
|float **GetFloat**(EInputFlags inputFlag, EInputModeFlags inputModeFlag)| inputFlag에 대응되는 inputModeFlag의 입력 상태를 Float 형태로 반환하는 메소드이다. <br>*음수의 범위는 반환하지 않는다.*|
|float **GetFloat**(EInputModeFlags inputModeFlag)| inputModeFlag의 모든 EInputFlags에 대응되는 입력 상태를 Float 형태로 반환하는 메소드이다. <br>*음수의 범위는 반환하지 않는다.*|
|float **GetFloat**(EInputFlags inputFlag)| inputFlag에 대응되는 모든 입력 장치의 입력 상태를 Float 형태로 반환하는 메소드이다. <br>*음수의 범위는 반환하지 않는다.*|
|void **SetButton**(EInputFlags inputFlag, EInputModeFlags inputModeFlag, KeyCode keyCode)| inputModeFlag 입력 장치의 inputFlag에 대응되는 입력을 keyCode로 변경하는 메소드이다. <br> Assets/StreamingAssets/input.csv에 변경된 사항을 저장한다.|
|void **SetAxis**(EInputFlags inputFlag, EInputModeFLags inputModeFlag, string axisString)| inputModeFlag 입력 장치의 inputFlag에 대응되는 입력을 axisString으로 변경하는 메소드이다. <br> Assets/StreamingAssets/input.csv에 변경된 사항을 저장한다.|
|KeyCode **GetAnyButton()**| 호출된 순간의 프레임에 입력된 KeyCode를 반환하는 메소드이다.|
|KeyCode **GetAnyButtonDown()**| 호출된 순간의 프레임에 입력이 시작된 KeyCode를 반환하는 메소드이다.|
|string **GetAnyAxis()**| 호출된 순간의 프레임에 입력된 AxisString을 반환하는 메소드이다.|

## 추가 설명
* input.csv에 적절한 입력값이 저장되어 있지 않은 경우
    - 해당 입력값을 기본 입력값으로 대체하고 저장한다.