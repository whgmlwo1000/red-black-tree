# ButtonUnit class
 [*AInputUnit*](https://gitlab.com/phantasylab/red-black-tree/blob/V2.1/Documents/Input/AInputUnit.md)을 상속
## 관련 클래스 및 열거형
| 이 름 | 설 명 |
|:---|:---|
|[**AInputUnit**](https://gitlab.com/phantasylab/red-black-tree/blob/V2.1/Documents/Input/AInputUnit.md)| *BoolValue*, *FloatValue* 프로퍼티가 선언되어 있는 추상 클래스이다.|
|[**KeyCode**](https://docs.unity3d.com/kr/current/ScriptReference/KeyCode.html)| UnityEngine에 선언되어 있는 Key 열거형이다.|

## 생성자
| 이 름 | 설 명 |
|:---|:---|
|**ButtonUnit**(KeyCode keyCode)| 입력 상태를 검사할 Button을 keyCode로 설정하는 생성자이다.|

## 오버라이드 프로퍼티
| 이 름 | 설 명 |
|:---|:---|
|bool **BoolValue** _get_ | 설정된 Button의 입력 상태를 Bool형으로 반환하는 프로퍼티이다.|
|float **FloatValue** _get_ | 설정된 Button의 입력 상태를 Float형으로 반환하는 프로퍼티이다.|

## 오버라이드 메소드
| 이 름 | 설 명 |
|:---|:---|
|string **ToString**()| 설정된 Button의 KeyCode를 string형으로 반환하는 메소드이다.|
