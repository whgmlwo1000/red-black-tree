# AInput abstract class
## 관련 클래스 및 열거형
| 이 름 | 설 명 |
|:---|:---|
| [**AControllable**](https://gitlab.com/phantasylab/red-black-tree/blob/V2.3/Documents/Actor/AControllable.md)| **Command**의 프로퍼티를 설정할 수 있는 메소드가 선언되어 있는 추상 클래스이다.|

## 메소드
| 이 름 | 설 명 |
|:---|:---|
| void **Check**(AControllable controllable)| controllable의 **Command** 프로퍼티를 설정하는 메소드를 호출하는 메소드이다.|