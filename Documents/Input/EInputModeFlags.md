# EInputModeFlags enum
## 필드
| 이 름 | 설 명 |
|:---|:---|
| **NONE** = -1| None|
| **KEYBOARD** = 0| 키보드, 마우스 입력 모드|
| **GAMEPAD** = 1| 게임 패드 입력 모드|
| **COUNT** = 2| 필드의 수|