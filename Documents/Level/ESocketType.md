# ESocketType enum
 in Level namespace

## 필드
| 이 름 | 설 명 |
|:----|:---|
| **NONE** = -1 | None. |
| **NORMAL_0** = 0 | 기본 소켓 타입(0) 이다. |
| **COUNT** = 1 | 선언된 소켓 타입의 개수이다. |
