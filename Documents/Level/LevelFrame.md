# LevelFrame class
 in Level namespace
 [**LevelLayer**]()를 상속

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:----|:---|
| [**LevelSocket**]() | 프레임이 가져야 하는 각 소켓의 정보를 저장하기 위한 클래스이다. |
| [**Transform**]() | 프레임의 위치를 표시하기 위한 클래스이다. |

## 프로퍼티
| 이 름 | 설 명 |
|:----|:---|
| Transform **Anchor_LeftBottom** \{ get \} | 프레임의 좌측 하단 위치 정보를 반환하는 프로퍼티이다. |
| Transform **Anchor_RightTop** \{ get \} | 프레임의 우측 상단 위치 정보를 반환하는 프로퍼티이다. |

## 메소드
| 이 름 | 설 명 |
|:----|:---|
| LevelSocket **MoveSocket**(EDirection direction, ESocketType type) | 프레임이 가지고 있는 direction 방향의 type 타입을 가지는 소켓 중 현재 가리키고 있는 소켓을 반환하고 다음 소켓을 가리키는 메소드이다. |
| LevelSocket **GetSocket**(EDirection direction, ESocketType type) | 프레임이 가지고 있는 direction 방향의 type 타입을 가지는 소켓 중 현재 가리키고 있는 소켓을 반환하는 메소드이다. |
| bool **HasSocket**(EDirection direction, ESocketType type, out int count) | 프레임이 direction 방향의 type 타입 소켓을 가지고 있는지의 여부를 반환하는 메소드이다. <br> count는 현재 해당 소켓을 몇개 가지고 있는지에 관한 정보로 초기화한다. |
| LevelSocket **DequeueSocket**(EDirection direction, ESocketType type) | 프레임이 가지고 있는 direction 방향의 type 타입을 가지는 소켓 중 현재 가리키고 있는 소켓을 반환하고 프레임이 가지는 소켓 큐에서 제거하는 메소드이다. |
| LevelSocket **DequeueSocket**() | 프레임이 가지고 있는 모든 소켓 큐 중 하나를 선택해서 해당 소켓을 반환하고 소켓 큐에서 제거하는 메소드이다. |
| LevelSocket **DequeueSocket**(int tag) | 프레임이 가지고 있는 모든 소켓 중 tag 번호를 가지는 소켓을 선택해서 반환하고 해당 소켓이 등록된 소켓 큐에서 제거하는 메소드이다. |
| void **Initialize**() | 해당 프레임을 생성하기 위한 정보를 초기화하는 메소드이다. |