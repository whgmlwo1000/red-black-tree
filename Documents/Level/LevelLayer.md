# LevelLayer class
 in Level namespace
 [**MonoBehaviour**]()를 상속

## 관련 클래스, 구조체 및 열거형
| 이 름 | 설 명 |
|:----|:---|
| [**ELayerType**]() | 레이어의 타입을 구분하기 위한 용도로 사용된다. |

## 프로퍼티 
| 이 름 | 설 명 |
|:----|:---|
| ELayerType **Type** \{ get \} | 레이어의 타입을 반환하는 프로퍼티이다. |
| int **CountOfLevel** \{ get set \} | 레이어가 배치될 수 있는 남은 횟수를 반환하고, 설정할 수 있는 프로퍼티이다. |
| float **WeightOfLevel** \{ get \} | 레이어가 배치될 때 고려되는 가중치를 반환하는 프로퍼티이다. |
| int **DepthOfLevel** \{ get \} | 레이어가 배치될 수 있는 최소 깊이를 반환하는 프로퍼티이다. |

## 메소드
| 이 름 | 설 명 |
|:----|:---|
| void **Initialize**() | 레벨을 생성하기 위해 필요한 정보를 초기화하는 메소드이다. |
